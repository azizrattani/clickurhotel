<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->pageTitle = '$label - View';\n";
echo "\$this->breadcrumbs=array(
	'$label' => array('index'),
	'View',
);\n";
?>
?>

<?php echo "<?php"; ?> $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?php echo "<?= \$this->createUrl('index'); ?>" ?>" class="btn btn-default">Back</a>
</div>