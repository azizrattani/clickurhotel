<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<?php echo "<?php \$form = \$this->beginWidget('CActiveForm', array(
	'id' => '".$this->class2id($this->modelClass)."-form',
)); ?>\n"; ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo "<?= \$form->errorSummary(\$model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>\n"; ?>

	<br />

<?php
foreach($this->tableSchema->columns as $column)
{
	if($column->autoIncrement)
		continue;

	if(in_array($column->name, array('created', 'modified', 'deleted')))
		continue;		
?>
	<div class="form-group">
		<?php echo "<?= ".$this->generateActiveLabel($this->modelClass, $column)."; ?>\n"; ?>
		<?php echo "<?= ".$this->generateActiveField($this->modelClass, $column)."; ?>\n"; ?>
	</div>

<?php
}
?>
	<div class="box-footer">
		<a href="<?php echo "<?= \$this->createUrl('index'); ?>"; ?>" class="btn btn-default">Back</a>
		<?php echo "<?= CHtml::submitButton(\$model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>\n"; ?>
	</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>