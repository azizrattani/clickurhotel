<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $dataProvider CActiveDataProvider */

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->pageTitle = 'Manage $label';\n";
echo "\$this->breadcrumbs = array(
	'$label',
    'Manage'
);\n";
?>
?>

<?php echo "<?php if(\$createAllowed) { ?>\n"; ?>
<p class="text-right">
    <a href="<?php echo "<?php echo \$this->createUrl('create'); ?>" ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Add New
    </a>
</p>
<?php echo "<?php } ?>\n"; ?>

<div class="box-body table-responsive no-padding">
    <?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
        'id' => '<?php echo $this->class2id($this->modelClass); ?>-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
        'columns' => array(
    <?php
    $count=0;
    foreach($this->tableSchema->columns as $column)
    {
        if(++$count==7)
            echo "\t\t/*\n";
        echo "\t\t'".$column->name."',\n";
    }
    if($count>=7)
        echo "\t\t*/\n";
    ?>
            array(
                'htmlOptions' => array('style' => 'width: 75px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'View', 'class' => 'fa fa-eye', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$viewAllowed"
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Update', 'class' => 'fa fa-pencil-square-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$updateAllowed"
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Delete', 'class' => 'fa fa-trash-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$deleteAllowed"
                    )
                )
            )
        ),
    )); ?>
</div>