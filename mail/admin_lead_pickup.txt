<p>Dear {NAME},</p>
<br />
<p>Customer has requested a pickup, Requested date/time is <strong>{DATE}</strong>.</p>
<p>Click <a href="{URL}">HERE</a> to view the lead or use below URL to visit:</p>
<p><a href="{URL}">{URL}</a></p>
<br />
<p>Thank you,<br />Team TravelPackageCompare.com</p>