<?php $themeUrl = Yii::app()->theme->baseUrl; ?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $this->pageTitle; ?> - <?php echo Yii::app()->name; ?></title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link href="<?php echo $themeUrl; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link href="<?php echo $themeUrl; ?>/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $themeUrl; ?>/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/sweet-alert/sweet-alert.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/css/custom.css" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <img src="<?=Yii::app()->createAbsoluteUrl(isset(Yii::app()->user->logo)?Yii::app()->user->logo:"")?>" alt="" style="max-height: 100px;"/>
            <?php if($this->showCrmTitle){ ?>
          
            
            <?php } ?>
        </div>

        <?php echo $content ?>
    </div>

<script src="<?php echo $themeUrl; ?>/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<script src="<?php echo $themeUrl; ?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo $themeUrl; ?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/sweet-alert/sweet-alert.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
});

function showReset(){
    $('#login-div').slideUp();
    $('#forgot-pwd-div').slideDown();
}

function showLogin(){
    $('#forgot-pwd-div').slideUp();
    $('#login-div').slideDown();
}
</script>
</body>
</html>