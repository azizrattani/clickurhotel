<?php $themeUrl = Yii::app()->theme->baseUrl; ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script src="<?= $themeUrl ?>/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<title><?= $this->pageTitle; ?> - <?= Yii::app()->name; ?></title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<link href="<?= $themeUrl; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">


<link rel="stylesheet" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link href="<?= $themeUrl ?>/css/AdminLTE.css" rel="stylesheet" type="text/css"/>

<link href="<?= $themeUrl ?>/css/skins/skin-blue-light.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/select2/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/sweet-alert/sweet-alert.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<?= $themeUrl ?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/jQueryUI/jquery-ui.min.js" type="text/javascript"></script>
<!--[if lt IE 11]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.4/bluebird.min.js"></script>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script>var baseUrl = '<?= Yii::app()->getBaseUrl(true) ?>';</script>
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
    <div class="wrapper">
        
        <?php $this->renderPartial('//includes/header'); ?>

        <?php $this->renderPartial('//includes/sidebar'); ?>

        <div class="content-wrapper">
            <section class="content-header">
                <?php           
                if (isset($this->breadcrumbs)){                            
                    $this->widget('Breadcrumbs', array(
                        'links' => $this->breadcrumbs,
                        'htmlOptions' => array('class' => 'breadcrumb'),
                        'tagName' => 'ol',
                        'separator' => '',
                    ));
                }
                ?>
            </section>

            <section class="content">
                <?php $this->widget('Alerts'); ?>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $this->pageTitle; ?></h3>
                    </div>
                    <div class="box-body">
                        <?= $content ?>        
                    </div>
                </div>
            </section>
        </div>

        <footer class="main-footer">
            <img src="<?php echo $themeUrl; ?>/img/logo.png" alt="" style="max-height: 50px;"/> <strong>Hotel Booking Management System.</strong> Powered By <a href="http://www.ideabox.com.pk" target="_blank"><strong>IdeaBox</strong></a>
        </footer>        
    </div>

<script src="<?= $themeUrl ?>/js/app.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/select2/select2.min.js" type="text/javascript"></script>

<script src="//unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="<?= $themeUrl ?>/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/js/common.js" type="text/javascript"></script>
</body>
</html>
<?php
Yii::app()->clientScript->scriptMap = array(
    'jquery.js' => false,
);
?>