<?php $themeUrl = Yii::app()->theme->baseUrl; 
		$siteUrl = Yii::app()->baseUrl; 


?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $this->pageTitle; ?> - <?php echo Yii::app()->name; ?></title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link href="<?php echo $themeUrl; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link href="<?php echo $themeUrl; ?>/css/AdminLTE.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $themeUrl; ?>/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/sweet-alert/sweet-alert.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>

<link href="<?= $themeUrl ?>/plugins/bootstrap-datepicker/datepicker.css" rel="stylesheet" type="text/css"/>

<link href="<?= $themeUrl ?>/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?= $themeUrl ?>/css/styles.css" rel="stylesheet" type="text/css"/>

<link href="<?= $themeUrl ?>/css/custom.css" rel="stylesheet" type="text/css"/>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="hold-transition login-page">


	<div class="jumborton">
    	
        <div class="container">
        	
            <div class="row">
            
             <div class="login-logo">
                <img src="<?=Yii::app()->createAbsoluteUrl(Yii::app()->user->logo)?>" alt="" style="max-height: 100px;"/>
                <?php if($this->showCrmTitle){ ?>
                <br /><br />
                Customer Relationship Management (CRM)
                <?php } ?>
            </div>
            
                <?php echo $content ?>
            
            
            
            </div>
        
        </div>
    
    
    </div>



    <div class="wrapper">
       
    </div>

<script src="<?php echo $themeUrl; ?>/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<script src="<?php echo $themeUrl; ?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo $themeUrl; ?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script src="<?= $themeUrl ?>/plugins/sweet-alert/sweet-alert.min.js" type="text/javascript"></script>

<script src="<?= $themeUrl ?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo $themeUrl; ?>/js/select2.full.js"></script>



<script type="text/javascript">
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
});

function showReset(){
    $('#login-div').slideUp();
    $('#forgot-pwd-div').slideDown();
}

function showLogin(){
    $('#forgot-pwd-div').slideUp();
    $('#login-div').slideDown();
}
</script>


<script src="<?= $themeUrl ?>/js/jquery.mask.min.js"></script>


<script>
    $(function () {
		$('.date-picker').datepicker();
		
        $(".addmore.first").show();

        showExtra('second');
		
		
		
		
		/*$("#destination").select2({
			placeholder: "Select Destination 1",
			allowClear: true
		});*/
		
		
		$("#destination,#destination-2,#destination-3").select2({
		  ajax: {
			url: "<?php echo "/site/getCitiesSearch"?>",
			dataType: 'json',
			delay: 250,
			data: function (params) {
			  return {
				q: params.term, // search term
				page: params.page
			  };
			},
			processResults: function (data, params) {
			  // parse the results into the format expected by Select2
			  // since we are using custom formatting functions we do not need to
			  // alter the remote JSON data, except to indicate that infinite
			  // scrolling can be used
			  
			  console.log(data);
			  params.page = params.page || 1;
		
			  return {
				results: data.items,
				pagination: {
				  more: (params.page * 30) < data.total_count
				}
			  };
			},
			cache: true
		  },
		  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		  minimumInputLength: 2
		});
		
		
		$("span.select2.select2-container").width("100%");
		/*$("#destination-2").select2({
			placeholder: "Select Destination 2",
			allowClear: true
		});
		
		$("#destination-3").select2({
			placeholder: "Select Destination 3",
			allowClear: true
		});*/

    });
	
	

    function onCountryChange()
    {
        var countryId = $("#res_country").val();

        $.ajax({
            url:'/index/get_cities',
            type: 'post',
            data: {countryId:countryId},
            success: function (html)
            {
                //$("#res_city").html(html);
            }
        });
    }

    function showExtra(selector)
    {
        jQuery("." + selector).show();
        if(selector == 'second') {
            $(".remove.third").hide();
            $(".addmore.first").hide();
            $(".remove.second").show();
        }
        else if(selector == 'third') {
            $(".remove.second").hide();
            $(".addmore.second").hide();
            $(".addmore.first").hide()
        }
    }
    function hideExtra(selector)
    {
        jQuery("."+selector).hide();
        if(selector == 'second') {
            $(".addmore.first").show();
            $(".remove.second").hide();
            $(".addmore.second").hide();
        }
        else if(selector == 'third') {
            $(".addmore.first").hide();
            $(".addmore.second").hide();
            $(".addmore.second").show();
            $(".remove.second").show();
        }
    }
</script>




<script>
 // $(document).ready(function(e) {
    
		
		
		$("#from-cell-number").mask('0000-000-0000');
		$("#local-number").mask('0000-000-0000');
		
		$("#international-number").mask('+###');
		
		
		$("#customize-form").submit(function(e) {
		
        e.preventDefault();
        $('.alert').remove();

        {
            $.ajax({
                url: '/site/changedestinationajax',
                cache: false,
				data: $(this).serialize(),
                type: 'POST',
                beforeSend: function() {
                    $('#loader').show();
                },
               
                success: function(data) {
					
					arr = data.split("|<>|");
					data = arr[0];
					if(arr.length > 1){
						lead_id = arr[1];
					}else{
						lead_id = 'NA';
					}
					
					
                    if (data === 'email-sent') {
                        $('#customize-form').hide();
                        $('#customize-form').after('<br><div class="clearfix"></div><div class="alert alert-success">Success Message. Your Enquiry Has Been Sent<span class="close"></span></div>');
                        $("#customize-form").get(0).reset();
                    }
					
					//alert(obj.link_title);
						
					ga('require', 'ecommerce');
					ga('ecommerce:addItem', {
						  'id': lead_id,                     // Transaction ID. Required.
						  'name': "Customize trip",    // Product name. Required.
					  //'category': obj.section_name + " > "  + obj.category_name,         // Category or variation.
					
					});
					
					ga('ecommerce:send');
					
					
                },
                complete: function(data) {
                    $('#loader').hide();
                },
                error: function(data, status, errorThrown) {}
            });
        }
    });
		
//	});
</script>




</body>
</html>