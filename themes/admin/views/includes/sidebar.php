<?php
$themeUrl     = Yii::app()->theme->baseUrl;
$moduleId     = isset(Yii::app()->controller->module->id) ? Yii::app()->controller->module->id : 'dashboard';
$controllerId = isset(Yii::app()->controller->id) ? Yii::app()->controller->id : '';
$actionId     = Yii::app()->controller->action->id;
$menu         = Yii::app()->functions->getSidebarMenu();
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <?php
                $avatar = $themeUrl . '/img/avatar.png';
                
                ?>
                <img src="<?= $avatar ?>" class="img-circle" alt="">
            </div>
            <div class="pull-left info">
                <p><?= Yii::app()->user->profile->first_name . ' ' . Yii::app()->user->profile->last_name ?></p>
                <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li><hr /></li>
            <li>
                <a href="<?= Yii::app()->getBaseUrl(true) ?>">
                    <i class="fa fa-home"></i> <span>Dashboard</span>
                </a>
            </li>

			<?php
            $selection = false;
			?>
            <?php foreach($menu as $k => $v) { ?>
            <?php if($k == 'none'){ ?>

            <?php foreach ($v as $c) { ?>
            <li>
                <?php $icon = isset($c['icon']) ? $c['icon'] : 'angle-right'; ?>
                <?php $url  = '/' . $c['module'] . '/' . $c['controller'] . '/' . $c['action']; ?>
                <?php $params = isset($c['params']) ? $c['params'] : array(); ?>

                <a href="<?php echo $this->createUrl($url, $params); ?>"><i class="fa fa-<?= $icon ?>"></i> <span><?= $c['title'] ?></span></a>
            </li>
            <?php } ?>

            <?php } else { ?>

            <?php
            $cls = '';
            if(isset($_GET['type']) && $_GET['type'])
            {
                $cls = '';
            }
            elseif(!in_array($k, array('lead1', 'lead2', 'lead3')))
            {
               
				$cls = $controllerId == $k ? "active" : "";
				if (!$cls){
					// $cls = $moduleId == $v['module'] ? 'active' : '';
				}
				
				
				//echo "<pre>";
				/*print_r($controllerId);
				echo "<br>";
				print_r($k);
				echo "<br> azz ";
				print_r($v);*/
				
				
				if(isset($v["childs"])){
					
					
				
					$action_arr = array_column($v["childs"], 'action');
					$controllers_arr = $v["module"]; //array_column($v["module"], 'action');
					
					if($actionId == "index"){
						$cls = $controllerId == $k ? "active" : "";
						if (!$cls){
							$cls = $moduleId == $v['module'] ? 'active' : '';
						}
						
					}else{
						//echo $actionId . " === "  . "<br>";
						$keys = array_search($actionId, $action_arr);
						//echo $actionId . " -- " . $keys;
						
						//echo $v["module"] . " == " . $controllerId . "<br>";
						
						if ($keys !== false && $v["module"] == $controllerId ){
							//echo $actionId . " -- " . $keys;
							$cls =  "active";
							$selection = true;
						}else{
							
							//echo $moduleId . " ---- " . $controllerId . " ---- " . $actionId;
							
							//print_r($v["childs"]);
							
							$controller_arr = array_column($v["childs"], 'controller');
							
							$keys = array_search($controllerId, $controller_arr);
							
							if ($v["module"] == $moduleId && $keys !== false && in_array($actionId,array("view","edit","update"))){
								$cls =  "active";
									$selection = true;
							}else{							
								$cls = "";	
							}
							
							
						}
					}
					
					
				}
				//echo "</pre>";
				//die ();
				
            }
            ?>
            <li class="<?= $cls; ?> <?= (isset($v['childs']) && !empty($v['childs'])) ? 'treeview' : '' ?>" data= "<?= $controllerId . " == " . $k  . " ==== " . $actionId  ?>">
            
            	 <?php
				 
				 if (isset($v['direct'])){ ?>
                 	
                    <a href="<?=$v['direct']?>">
                        <i class="fa <?= $v['icon'] ?>"></i> <span><?= $v['title'] ?></span> 
                    </a>
                    
                 
                 <?php
				 }else{
				 ?>
                        <a href="#">
                            <i class="fa <?= $v['icon'] ?>"></i> <span><?= $v['title'] ?></span> <i class="fa fa-angle-left pull-right"></i>
                        </a>
        
                        <?php if(isset($v['childs']) && !empty($v['childs'])){ ?>
                        <ul class="treeview-menu">
                            <?php foreach($v['childs'] as $k2 => $c) { ?>
                            <?php
                            
                            
                            $active = 0;
                            if(isset($_GET['status']) && isset($c['params']['status']))
                            {
                                if($_GET['status'] == $c['params']['status'])
                                    $active = 1;
                            }
                            elseif( $actionId == $c['action'] && $controllerId == $c['controller'])
                            {
								//echo "<pre>" . $actionId .  " ==== " . $controllerId . "</pre>";
                                $active = $actionId == $c["action"] ? 1 : 0;
                            }
                            ?>
        
                            <li <?php echo $active ? ' class="active"' : ''; ?>>
                                <?php $icon = isset($c['icon']) ? $c['icon'] : 'angle-right'; ?>
                                <?php $url  = '/' . $v['module'] . '/' . $c['controller'] . '/' . $c['action']; ?>
                                <?php $params = isset($c['params']) ? $c['params'] : array(); ?>
                                <a href="<?php echo $this->createUrl($url, $params); ?>"><i class="fa fa-<?= $icon ?>"></i> <?= $c['title'] ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                <?php
						}
				
				?>
                <?php } ?>
            </li>
            <?php }?>
            <?php } ?>
        </ul>
    </section>
</aside>
