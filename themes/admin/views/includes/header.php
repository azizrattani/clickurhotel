<?php
$themeUrl = Yii::app()->theme->baseUrl;
$notificationListAllowed = RolePermissions::isAllowed('Index', '', 'Notifications', 'misc');
$notificationViewAllowed = RolePermissions::isAllowed('View', '', 'Notifications', 'misc');
$profileViewAllowed = true;
$notificationListAllowed = false;
if($notificationListAllowed)
{
    $notifications = Notifications::getHeaderNotifications();
}

?>
<header class="main-header">
    <a href="<?= Yii::app()->getBaseUrl(true) ?>" class="logo">
        <span class="logo-mini"><b><?=Yii::app()->user->site_name?></b></span>
        <span class="logo-lg"><b><?=Yii::app()->user->site_name?></b></span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php if($notificationListAllowed) { ?>
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-danger <?= !$notifications['unread'] ? 'hidden' : '' ?>"><?= $notifications['unread'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header text-bold">You have <?= $notifications['unread'] ?> new notifications</li>

                        <?php if($notifications['list']) { ?>
                        <li>
                            <ul class="menu">
                                <?php foreach ($notifications['list'] as $n) { ?>
                                <?php $url = $notificationViewAllowed ? $this->createAbsoluteUrl('/misc/notifications/view', array('id' => $n['id'])) : '#'; ?>
                                <li><a <?= !$n['read'] ? 'class="unread"' : '' ?> href="<?= $url ?>"><?= $n['title'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <li class="footer">
                            <a href="<?= $this->createUrl('/misc/notifications') ?>">View all</a>
                        </li>
                    </ul>
                </li>
                <?php } ?>

                <?php
                $avatar = $themeUrl . '/img/avatar.png';
                if(Yii::app()->user->profile->role_id == Yii::app()->user->client_role)
                {
                    $c = Partners::model()->findByPk(Yii::app()->user->profile->child_id);

                    if(isset($c->image) && $c->image) {
                        $avatar = '/images/partner/' . Yii::app()->user->profile->child_id . "/" . $c->image;
                    }
                }
                ?>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img class="user-image" alt="" src="<?= $avatar ?>" style="max-width:60px;">
                        <span class="hidden-xs"><?= Yii::app()->user->profile->first_name . ' ' . Yii::app()->user->profile->last_name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img class="img-circle" alt="" src="<?= $avatar ?>">
                            <p>
                                <?= Yii::app()->user->profile->first_name . ' ' . Yii::app()->user->profile->last_name ?>
                                <br />
                                <small>Member since <?= date('d M, Y', strtotime(Yii::app()->user->profile->created)) ?></small>
                            </p>
                        </li>
                        
                        <li class="user-footer">
                            <div class="pull-left">
                                <?php if($profileViewAllowed) { ?>
                                <a href="<?= $this->createUrl('/profile') ?>" class="btn btn-default btn-flat">Profile</a>
                                <?php } ?>
                            </div>
                            <div class="pull-right">
                                <a href="<?= $this->createUrl('/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>