CKEDITOR.config.title = false;
$(document).ready(function() {
    $('.disabled').prop('disabled', true);
    
    $('.numberonly').keyup(function () { 
        this.value = this.value.replace(/[^0-9]/g,'');
    });

    if($('.mselect').length){
        $('.mselect').select2({
			
			placeholder: $(this).data("placeholder"),
           	
			
		});
        $(".select2-selection span").attr('title', '');
    }
    
    if($('.datepicker').length)
    {
        $('.datepicker').daterangepicker({
            format: 'DD MMM YYYY',
            singleDatePicker: true,
            showDropdowns: true
        });
    }

    if($('.daterange').length)
    {
        $('.daterange').daterangepicker({
            format: 'DD MMM YYYY',
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb, $(this).attr('id'));
    }

    if($('.datetimerange').length)
    {
        $('.datetimerange').daterangepicker({
            format: 'DD MMM YYYY h:mm A',
            timePicker: true,
            timePickerIncrement: 5,
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb, $(this).attr('id'));
    }

    if($('.datetimepicker').length)
    {
        $('.datetimepicker').daterangepicker({
            format: 'DD MMM YYYY h:mm A',
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            timePickerIncrement: 5,
            drops: "up",
        });
    }


    if($('.iCheck').length)
    {
        $('.iCheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    }
	
	$(".hasDatepicker").attr("autocomplete","off");
	
	
	
});

function cb(start, end, id) {
    $('#' + id).val(start.format('DD MMM YYYY') + ' - ' + end.format('DD MMM YYYY'));
}