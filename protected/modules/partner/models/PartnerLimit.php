<?php

/**
 * This is the model class for table "partner_limit".
 *
 * The followings are the available columns in table 'partner_limit':
 * @property integer $id
 * @property integer $staff_id
 * @property double $booking_limit
 * @property double $checkin_limit
 * @property string $date_start
 * @property string $date_end
 * @property integer $deleted
 * @property string $created
 * @property string $modified
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Partners $staff
 */
class PartnerLimit extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner_limit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('booking_limit, checkin_limit', 'required'),
			array('staff_id, deleted, created_by', 'numerical', 'integerOnly'=>true),
			array('booking_limit, checkin_limit', 'numerical'),
			array('date_start, date_end, created, modified', 'safe'),
			
			array('booking_limit', 'validateBookingLimit', 'on' => array('create', 'update')),
			array('checkin_limit', 'validateCheckinLimit', 'on' => array('create', 'update')),
			

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, staff_id, booking_limit, checkin_limit, date_start, date_end, deleted, created, modified, created_by', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'staff' => array(self::BELONGS_TO, 'Partners', 'staff_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'staff_id' => 'Partner',
			'booking_limit' => 'Booking Limit',
			'checkin_limit' => 'Checkin Limit',
			'date_start' => 'Limit From',
			'date_end' => 'Limit Till',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('staff_id', $this->staff_id);
		$criteria->compare('booking_limit', $this->booking_limit);
		$criteria->compare('checkin_limit', $this->checkin_limit);
		$criteria->compare('date_start', $this->date_start, true);
		$criteria->compare('date_end', $this->date_end, true);
		$criteria->compare('deleted', 0);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('created_by', $this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			
		));
	}
	
	public function searchAll()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('staff_id', $this->staff_id);
		$criteria->compare('booking_limit', $this->booking_limit);
		$criteria->compare('checkin_limit', $this->checkin_limit);
		$criteria->compare('date_start', $this->date_start, true);
		$criteria->compare('date_end', $this->date_end, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('created_by', $this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'Pagination' => array (

                  'PageSize' => 5000

              ),
		));
	}
	
	/**
     * Validate unique username.
     */
    public function validateBookingLimit($attribute, $params)
    {
    	
		$partner_id = Yii::app()->user->partner["id"];
		$parent_id = Yii::app()->user->partner["parent_id"];
		
		
		
		if($parent_id != 0){
			$bookingLimit = Yii::app()->functions->getAgentLimit($partner_id);
			$bookingLimitMin = Yii::app()->functions->getAgentLimitMin($this->staff_id);
		
			
			if ($this->{$attribute} < $bookingLimitMin){
				 $this->addError($attribute, 'Minimum Booking Limit is '.$bookingLimitMin);	
			}elseif ($this->{$attribute} > $bookingLimit){
				 $this->addError($attribute, 'Maximum Booking Limit is '.$bookingLimit);	
			}
		}
    }
	
	
	/**
     * Validate unique username.
     */
    public function validateCheckinLimit($attribute, $params)
    {
    	$partner_id = Yii::app()->user->partner["id"];
		$parent_id = Yii::app()->user->partner["parent_id"];
		
		if($parent_id != 0){
			$bookingLimit = Yii::app()->functions->getCheckinLimit($partner_id);
			$bookingLimitMin = Yii::app()->functions->getCheckinLimitMin($this->staff_id);
			
			if ($this->{$attribute} < $bookingLimitMin){
				 $this->addError($attribute, 'Minimum Checkin  Limit is '.$bookingLimitMin);	
			}elseif ($this->{$attribute} > $bookingLimit){
				 $this->addError($attribute, 'Maximum Checkin  Limit is '.$bookingLimit);	
			}
		}
		
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PartnerLimit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
			
			
			$sql = "update partner_limit set deleted='1', date_end='".date("Y-m-d")."', modified='".date("Y-m-d")."' where staff_id='".$_POST['PartnerLimit']["staff_id"]."' and deleted =0";
			$data=Yii::app()->db->createCommand($sql)->execute();
			
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
