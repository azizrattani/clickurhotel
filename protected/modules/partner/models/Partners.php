<?php

/**
 * This is the model class for table "partners".
 *
 * The followings are the available columns in table 'partners':
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $cell
 * @property string $cell2
 * @property string $email
 * @property string $ccEmail
 * @property string $address
 * @property string $image
 * @property string $joining_date
 * @property string $username
 * @property string $password
 * @property integer $is_whitelabale
 * @property string $mail_host
 * @property string $mail_username
 * @property string $mail_password
 * @property string $important_note
 * @property string $note
 * @property string $status
 * @property string $created
 * @property string $modified
 * @property integer $deleted
 *
 * The followings are the available model relations:
 * @property Booking[] $bookings
 * @property PartnerLimit[] $partnerLimits
 * @property PartnerLoginLogs[] $partnerLoginLogs
 * @property PartnerPayment[] $partnerPayments
 * @property PartnerPriceType[] $partnerPriceTypes
 */
class Partners extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partners';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, cell, joining_date', 'required'),
			array('parent_id, is_whitelabale, deleted', 'numerical', 'integerOnly'=>true),
			array('name, cell, cell2, email, address, image, username, password, mail_host, mail_username, mail_password,base_url', 'length', 'max'=>255),
			array('status', 'length', 'max'=>1),
			array('ccEmail, important_note, note, created, modified', 'safe'),
			array('username', 'validateUsername', 'on' => array('create', 'update')),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, name, cell, cell2, email, ccEmail, address, image, joining_date, username, password, base_url, is_whitelabale, mail_host, mail_username, mail_password, important_note, note, status, created, modified, deleted', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bookings' => array(self::HAS_MANY, 'Booking', 'staff_id'),
			'partnerLimits' => array(self::HAS_MANY, 'PartnerLimit', 'staff_id'),
			'partnerLoginLogs' => array(self::HAS_MANY, 'PartnerLoginLogs', 'staff_id'),
			'partnerPayments' => array(self::HAS_MANY, 'PartnerPayment', 'partner_id'),
			'partnerPriceTypes' => array(self::HAS_MANY, 'PartnerPriceType', 'staff_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => '0 mean Sales office',
			'name' => 'Name',
			'cell' => 'Cell',
			'cell2' => 'Cell2',
			'email' => 'Email',
			'ccEmail' => 'Cc Email',
			'address' => 'Address',
			'image' => 'Image',
			'joining_date' => 'Joining Date',
			'username' => 'Username',
			'password' => 'Password',
			'is_whitelabale' => 'Is Whitelabale',
			'mail_host' => 'Mail Host',
			'mail_username' => 'Mail Username',
			'mail_password' => 'Mail Password',
			'important_note' => 'Important Note',
			'note' => 'Note',
			'status' => 'Status',
			'created' => 'Created',
			'modified' => 'Modified',
			'deleted' => 'Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('cell', $this->cell, true);
		$criteria->compare('cell2', $this->cell2, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('ccEmail', $this->ccEmail, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('image', $this->image, true);
		$criteria->compare('joining_date', $this->joining_date, true);
		$criteria->compare('username', $this->username, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('is_whitelabale', $this->is_whitelabale);
		$criteria->compare('mail_host', $this->mail_host, true);
		$criteria->compare('mail_username', $this->mail_username, true);
		$criteria->compare('mail_password', $this->mail_password, true);
		$criteria->compare('important_note', $this->important_note, true);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('deleted', 0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Validate unique username.
	 */
	public function validateUsername($attribute, $params)
	{
		$user = Partners::model()->find('LOWER(username) = "' . $this->{$attribute} .'" AND deleted = 0');

		if ($this->isNewRecord && $user !== null)
		{
			$this->addError($attribute, 'Username already registered.');
		}
		elseif ($user !== null && !$this->isNewRecord && $this->id != $user->id)
		{
			$this->addError($attribute, 'Username already registered.');
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Partners the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getParentName(){
		$parent_id = $this->parent_id;
		if ($parent_id == 0){
			echo "N/A";
		}else{
			$partner = Partners::model()->findByPk($parent_id);
			echo $partner->name;
		}
	}

	public function beforeSave()
	{
		if ($this->isNewRecord)
		{
			$this->created = new CDbExpression('NOW()');
		}

		$this->modified = new CDbExpression('NOW()');

		return parent::beforeSave();
	}
}
