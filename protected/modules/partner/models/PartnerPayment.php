<?php

/**
 * This is the model class for table "partner_payment".
 *
 * The followings are the available columns in table 'partner_payment':
 * @property integer $id
 * @property integer $partner_id
 * @property string $pay_date
 * @property double $amount
 * @property string $payment_date
 * @property string $reference_number
 * @property string $narration
 * @property integer $bid
 * @property string $created
 * @property string $modified
 * @property string $deleted
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Partners $partner
 */
class PartnerPayment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner_payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('partner_id,amount,pay_date', 'required'),
			array('partner_id, bid, created_by', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('reference_number', 'length', 'max'=>255),
			array('deleted', 'length', 'max'=>1),
			array('pay_date, payment_date, narration, created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, partner_id, pay_date, amount, payment_date, reference_number, narration, bid, created, modified, deleted, created_by', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'partner' => array(self::BELONGS_TO, 'Partners', 'partner_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'partner_id' => 'Partner',
			'pay_date' => 'Pay Date',
			'amount' => 'Amount',
			'payment_date' => 'Payment Date',
			'reference_number' => 'Reference Number',
			'narration' => 'Narration',
			'bid' => 'Bid',
			'created' => 'Created',
			'modified' => 'Modified',
			'deleted' => 'Deleted',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('partner_id', $this->partner_id);
		$criteria->compare('pay_date', $this->pay_date, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('payment_date', $this->payment_date, true);
		$criteria->compare('reference_number', $this->reference_number, true);
		$criteria->compare('narration', $this->narration, true);
		$criteria->compare('bid', $this->bid);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('deleted', 0);
		$criteria->compare('created_by', $this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PartnerPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }
		$this->pay_date = date("Y-m-d",strtotime($this->pay_date));

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
