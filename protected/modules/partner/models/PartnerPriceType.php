<?php

/**
 * This is the model class for table "partner_price_type".
 *
 * The followings are the available columns in table 'partner_price_type':
 * @property integer $id
 * @property integer $staff_id
 * @property string $price_type
 * @property double $fix_price
 * @property double $floating_price
 * @property string $hybrid_condition
 * @property string $date_from
 * @property string $date_to
 * @property integer $created_by
 * @property string $deleted
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property PartnerLimit[] $partnerLimits
 * @property Partners $staff
 * @property Users $createdBy
 */
class PartnerPriceType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner_price_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('staff_id, date_from', 'required'),
			array('staff_id, created_by', 'numerical', 'integerOnly'=>true),
			array('fix_price, floating_price', 'numerical'),
			array('price_type, hybrid_condition, deleted', 'length', 'max'=>1),
			array('created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, staff_id, price_type, fix_price, floating_price, hybrid_condition, date_from, date_to, created_by, deleted, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'partnerLimits' => array(self::HAS_MANY, 'PartnerLimit', 'staff_price_type'),
			'staff' => array(self::BELONGS_TO, 'Partners', 'staff_id'),
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'staff_id' => 'Partner',
			'price_type' => 'Price Type',
			'fix_price' => 'Fix Price',
			'floating_price' => 'Percentage',
			'hybrid_condition' => 'Hybrid Condition',
			'date_from' => 'Date From',
			'date_to' => 'Date To',
			'created_by' => 'Created By',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('staff_id', $this->staff_id);
		$criteria->compare('price_type', $this->price_type, true);
		$criteria->compare('fix_price', $this->fix_price);
		$criteria->compare('floating_price', $this->floating_price);
		$criteria->compare('hybrid_condition', $this->hybrid_condition, true);
		$criteria->compare('date_from', $this->date_from, true);
		$criteria->compare('date_to', $this->date_to, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('deleted', 0);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PartnerPriceType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
