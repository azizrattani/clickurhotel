<?php

class PartnersController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new Partners('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Partners']))
		{
			$model->attributes = $_GET['Partners'];
		}
		if (Yii::app()->user->profile['role_id'] == 2){
			//$model->parent_id = 0 ;
		}else{
			$model->parent_id = Yii::app()->user->profile['child_id'] ;
		}
		//$model->parent_id = Yii::app()->user->profile['id'] ;

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);
		

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new Partners;
		$model->scenario = 'create';

		if(isset($_POST['Partners']))
		{
			$model->attributes = $_POST['Partners'];
			$model->joining_date = date("Y-m-d");
			
			if (Yii::app()->user->profile['role_id'] == 2){
				$model->parent_id = 0 ;
			}else{
				$model->parent_id = Yii::app()->user->profile['child_id'] ;
			}
			
			if (isset($_FILES["Partners"]["name"]["image"]) && $_FILES["Partners"]["name"]["image"] != ""){
				$model->image=CUploadedFile::getInstance($model,'image');
			}

			if ($model->save())
            {
                
				$path = Yii::app()->basePath . '/../images/partner/'.$model->id;
					
				if(!is_dir($path)){
					mkdir($path,0777);
				}
				$path .='/';
				
				if (isset($_FILES["Partners"]["name"]["image"]) && $_FILES["Partners"]["name"]["image"] != ""){					
					$model->image->saveAs($path.$model->image);
				}
				
				
				$to = $subject = $body =  $from_email =  $from_name = '';
				
				$body = "Greetings ".$model->name." <br><br>
Welcome! We are very excited to have you onboard. Followign are access credentials: <br><br>
Username: ".$model->username."<br>
Password :  ".$model->password."<br>
URL:              http://www.clickhotel.online/<br><br>

Please ensure your credentials are safe and secure. Any activity done from your ID will be considered authroized by you.<br><br>

Regards<br>
". (isset(Yii::app()->user->partner)? Yii::app()->user->partner["name"] :"Admin");
				
				$subject = "Welcome";
				
				$from_email = "no-reply@clickurhotel.com";
				$from_name = "Booking Department";
				
				
				$customer_email = $model->email ;
				
				
				Yii::app()->functions->email($customer_email, $subject , $body, $from_email , $from_name);
				
				
				
				Yii::app()->user->setFlash('success', 'Partners saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$model->scenario = 'update';

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['Partners']))
			{
				
				$image = $model->image;
				
				
				if ($_POST['Partners']["password"] == ""){
					$_POST['Partners']["password"] = $model->password;	
				}
				
				$model->attributes = $_POST['Partners'];
				
				if (isset($_FILES["Partners"]["name"]["image"]) && $_FILES["Partners"]["name"]["image"] != ""){
					$model->image=CUploadedFile::getInstance($model,'image');
				}
				
				

				if ($model->save())
	            {
	                
					$path = Yii::app()->basePath . '/../images/partner/'.$model->id;
					
					if(!is_dir($path)){
						mkdir($path,0777);
					}
					$path .='/';
					
					if (isset($_FILES["Partners"]["name"]["image"]) && $_FILES["Partners"]["name"]["image"] != ""){						
						$model->image->saveAs($path.$model->image);
					}else{
						$model->image = $image;
						$model->save(false);
					}
					
					Yii::app()->user->setFlash('success', 'Partners updated successfully');
	                $this->redirect(array('index'));
	            }
			}
			$model->password = '';

			$this->render('update', compact('model'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);


		$sql = "update partners set deleted='1' where parent_id='".$id."' ";
		$data=Yii::app()->db->createCommand($sql)->execute();

		$sql = "update users set deleted='1' where child_id='".$id."' or child_id in (SELECT id FROM partners WHERE parent_id='".$id."') ";
		$data=Yii::app()->db->createCommand($sql)->execute();
		

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		
		if (Yii::app()->user->profile['role_id'] == 2){
			$model = Partners::model()->findByPk($id);
		}else{
			$model = Partners::model()->find(array("condition"=>" parent_id='".Yii::app()->user->profile['child_id']."' and id='".$id."'"));
		
		}
		
		if($model === null)
		{
			
			
			//throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
