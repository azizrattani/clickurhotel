<?php

class PartnerLimitController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new PartnerLimit('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PartnerLimit']))
		{
			$model->attributes = $_GET['PartnerLimit'];
		}
		
		if (Yii::app()->user->profile['role_id'] == 2){
			//$model->parent_id = 0 ;
		}else{
			$model->created_by = Yii::app()->user->profile['id'] ;
		}

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new PartnerLimit;
		$model->scenario = 'create';

		if(isset($_POST['PartnerLimit']))
		{
			
			$model->attributes = $_POST['PartnerLimit'];
			$model->date_start = date("Y-m-d");
			$model->created_by = Yii::app()->user->profile['id'] ;

			if ($model->save())
            {
                Yii::app()->user->setFlash('success', 'PartnerLimit saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['PartnerLimit']))
			{
				$model->attributes = $_POST['PartnerLimit'];

				if ($model->save())
	            {
	                Yii::app()->user->setFlash('success', 'PartnerLimit updated successfully');
	                $this->redirect(array('index'));
	            }
			}

			$this->render('update', compact('model'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}
	
	
	public function actionLimitDetail($staff_id){
		
		
		$model = new PartnerLimit('searchAll');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PartnerLimit']))
		{
			$model->attributes = $_GET['PartnerLimit'];
		}
		
		if (Yii::app()->user->profile['role_id'] == 2){
			//$model->parent_id = 0 ;
		}else{
			$model->created_by = Yii::app()->user->profile['id'] ;
		}
		
		$model->staff_id=$staff_id;

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->renderPartial('limitDetail', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
		
		
	}
	
	
	public function actionMinimumLimit($staff_id){
		
		$output["booking"] = "(Minimum  Limit " . Yii::app()->functions->getAgentLimitMin($staff_id) .")";
		$output["checking"] = "(Minimum Limit " . Yii::app()->functions->getCheckinLimitMin($staff_id) . "%)";
		
		echo json_encode($output);
		
	}

	public function loadModel($id)
	{
		$model = PartnerLimit::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
