<?php

class PartnerPriceTypeController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new PartnerPriceType('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PartnerPriceType']))
		{
			$model->attributes = $_GET['PartnerPriceType'];
		}
		
		if (Yii::app()->user->profile['role_id'] == 2){
			//$model->parent_id = 0 ;
		}else{
			$model->created_by = Yii::app()->user->profile['id'] ;
		}

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new PartnerPriceType;

		if(isset($_POST['PartnerPriceType']))
		{
			$sql = "update partner_price_type set deleted='1', date_to='".date("Y-m-d")."', modified='".date("Y-m-d")."' where staff_id='".$_POST['PartnerPriceType']["staff_id"]."' and deleted =0";
			$data=Yii::app()->db->createCommand($sql)->execute();
			
			$model->attributes = $_POST['PartnerPriceType'];
			$model->date_from = date("Y-m-d");
			$model->created_by = Yii::app()->user->profile['id'] ;
			

			if ($model->save())
            {
                Yii::app()->user->setFlash('success', 'PartnerPriceType saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['PartnerPriceType']))
			{
				$model->attributes = $_POST['PartnerPriceType'];

				if ($model->save())
	            {
	                Yii::app()->user->setFlash('success', 'PartnerPriceType updated successfully');
	                $this->redirect(array('index'));
	            }
			}

			$this->render('update', compact('model'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = PartnerPriceType::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
