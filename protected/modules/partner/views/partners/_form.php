<?php
/* @var $this PartnersController */
/* @var $model Partners */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'partners-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	
	<div class="form-group">
		<?= $form->labelEx($model,'name'); ?>
		<?= $form->textField($model, 'name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'cell'); ?>
		<?= $form->textField($model, 'cell', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'cell2'); ?>
		<?= $form->textField($model, 'cell2', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'email'); ?>
		<?= $form->textField($model, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>
    
    <div class="form-group">
		<?= $form->labelEx($model,'ccEmail'); ?> (user emails with , separated)
		<?= $form->textArea($model,'ccEmail',array('class' => 'form-control','rows'=>5, 'cols'=>50,"tabindex"=>"3")); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'address'); ?>
		<?= $form->textField($model, 'address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'image'); ?>
        <?= $form->fileField($model, 'image', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
         <?php
        if(!$model->isNewRecord)
    	{
			$path = Yii::app()->baseUrl . '/images/partner/'.$model->id.'/';
			
			echo '<img src="'.$path.$model->image.'" style= "max-height:200px;" />';
		}
		?>
		
	</div>
	

	<div class="form-group">
		<?= $form->labelEx($model,'username'); ?>
		<?= $form->textField($model, 'username', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'password'); ?><?php if (!$model->isNewRecord){?><small>( Leave blank if not want to change password)</small><?php }?>
		<?= $form->passwordField($model, 'password', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'base_url'); ?>
		<?= $form->textField($model, 'base_url', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_host'); ?>
		<?= $form->textField($model, 'mail_host', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_username'); ?>
		<?= $form->textField($model, 'mail_username', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_password'); ?>
		<?= $form->textField($model, 'mail_password', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'important_note'); ?>
		<?= $form->textArea($model,'important_note',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'note'); ?>
		<?= $form->textArea($model,'note',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'is_whitelabale'); ?>
		<?= $form->dropDownList($model, 'is_whitelabale', array('1' => 'Active', '0' => 'Inactive'), array('class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'status'); ?>
		 <?= $form->dropDownList($model, 'status', array('1' => 'Active', '0' => 'Inactive'), array('class' => 'form-control')); ?>
	</div>

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
