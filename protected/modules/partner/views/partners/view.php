<?php
/* @var $this PartnersController */
/* @var $model Partners */

$this->pageTitle = 'Partners - View';
$this->breadcrumbs=array(
	'Partners' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'parent_id',
		'name',
		'cell',
		'cell2',
		'email',
		'address',
		'image',
		'joining_date',
		'username',
		'status',
		'created',
		'modified',
		'deleted',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>