<?php
/* @var $this PartnersController */
/* @var $model Partners */

$this->pageTitle = 'Partners - Update';
$this->breadcrumbs = array(
	'Partners' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>