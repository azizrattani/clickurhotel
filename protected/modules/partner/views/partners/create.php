<?php
/* @var $this PartnersController */
/* @var $model Partners */

$this->pageTitle = 'Partners - Create';
$this->breadcrumbs = array(
	'Partners' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>