<?php
/* @var $this PartnerPriceTypeController */
/* @var $model PartnerPriceType */
/* @var $form CActiveForm */


$parent_id = (Yii::app()->user->partner["parent_id"]);
$otherCondition = "";
if ($parent_id != 0){
	$otherCondition = " and parent_id='".Yii::app()->user->partner["id"]."'";
}

?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'partner-price-type-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'staff_id'); ?>
         <?= $form->dropDownList($model, 'staff_id', CHtml::listData(Partners::model()->findAll(array("condition"=>' id != '.Yii::app()->user->partner["id"].' and deleted=0' . $otherCondition)), 'id', 'name'), array('class' => 'form-control mselect'));?>
		
	</div>

	<div class="form-group ">
		<?= $form->labelEx($model,'price_type'); ?>
        <?= $form->dropDownList($model, 'price_type', array(1=>"Flat",2=>"Percentage",3=>"Hybrid"), array('class' => 'form-control mselect'));?>
		
	</div>

	<div class="form-group fix_price">
		<?= $form->labelEx($model,'fix_price'); ?>
		<?= $form->textField($model,'fix_price',array('class' => 'form-control')); ?>
	</div>

	<div class="form-group floating_price">
		<?= $form->labelEx($model,'floating_price'); ?>
		<?= $form->textField($model,'floating_price',array('class' => 'form-control')); ?>
	</div>

	<div class="form-group hybrid_condition">
		<?= $form->labelEx($model,'hybrid_condition'); ?>
        <?= $form->dropDownList($model, 'hybrid_condition', array(1=>"Which ever is low",2=>"Which ever is high"), array('class' => 'form-control mselect'));?>
	</div>

	
	

	

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

<script>

$("#PartnerPriceType_price_type").change(function(e) {
	
	
    
	val = $(this).val();

	if (val == "1"){
		$(".fix_price").removeClass("hidden");
		$(".floating_price").addClass("hidden");
		$(".hybrid_condition").addClass("hidden");
		$("#PartnerPriceType_fix_price").attr("requried","requried");
		$("#PartnerPriceType_floating_price").removeAttr("requried");
	}else if (val == "2"){
		$(".fix_price").addClass("hidden");
		$(".floating_price").removeClass("hidden");
		$(".hybrid_condition").addClass("hidden");
		$("#PartnerPriceType_fix_price").removeAttr("requried");
		$("#PartnerPriceType_floating_price").attr("requried","requried");
	}else if (val == "3"){
		
		
		$(".fix_price").removeClass("hidden");
		$(".floating_price").removeClass("hidden");
		$(".hybrid_condition").removeClass("hidden");
		
		$("#PartnerPriceType_fix_price").attr("requried","requried");
		$("#PartnerPriceType_floating_price").attr("requried","requried");
		
		
		
		$("#PartnerPriceType_hybrid_condition").select2('destroy');
		
		$("#PartnerPriceType_hybrid_condition").select2({
            placeholder: "Select a Hybrid Condition",
        });
		
	}else{
		$(".fix_price").addClass("hidden");
		$(".floating_price").addClass("hidden");
		$(".hybrid_condition").addClass("hidden");
	}
	
});

$("#PartnerPriceType_price_type").trigger("change");

</script>
