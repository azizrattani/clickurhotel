<?php
/* @var $this PartnerPriceTypeController */
/* @var $model PartnerPriceType */

$this->pageTitle = 'Partner Price Types - Create';
$this->breadcrumbs = array(
	'Partner Price Types' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>