<?php
/* @var $this PartnerPriceTypeController */
/* @var $model PartnerPriceType */

$this->pageTitle = 'Partner Price Types - View';
$this->breadcrumbs=array(
	'Partner Price Types' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'staff_id',
		'price_type',
		'fix_price',
		'floating_price',
		'hybrid_condition',
		'date_from',
		'date_to',
		'created_by',
		'deleted',
		'created',
		'modified',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>