<?php
/* @var $this PartnerPriceTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Manage Partner Price Types';
$this->breadcrumbs = array(
	'Partner Price Types',
    'Manage'
);
?>

<?php if($createAllowed) { ?>
<p class="text-right">
    <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Add New
    </a>
</p>
<?php } ?>

<div class="box-body table-responsive no-padding">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'partner-price-type-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
        'columns' => array(
    	
		array('name'=>'staff_id','value'=>'$data->staff->name'),	
		array('name'=>'price_type','value'=>'$data->price_type == 1 ? "Fix" : ($data->price_type == 2 ? "Percentage" : ($data->price_type == 3 ? "Hybrid" : "--"))'),
		'fix_price',
		'floating_price',
		array('name'=>'hybrid_condition','value'=>'($data->hybrid_condition == 1 && $data->price_type == 3) ? "Which ever is low" : (($data->hybrid_condition == 2 && $data->price_type == 3) ? "Which ever is high" : "N/A")'),
		
		/*
		'date_from',
		'date_to',
		'created_by',
		'deleted',
		'created',
		'modified',
		*/
            array(
                'htmlOptions' => array('style' => 'width: 75px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'View', 'class' => 'fa fa-eye', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$viewAllowed"
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Update', 'class' => 'fa fa-pencil-square-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$updateAllowed"
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Delete', 'class' => 'fa fa-trash', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$deleteAllowed"
                    )
                )
            )
        ),
    )); ?>
</div>