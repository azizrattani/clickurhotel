<?php
/* @var $this PartnerPriceTypeController */
/* @var $model PartnerPriceType */

$this->pageTitle = 'Partner Price Types - Update';
$this->breadcrumbs = array(
	'Partner Price Types' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>