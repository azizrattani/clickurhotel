<?php
/* @var $this PartnerPaymentController */
/* @var $model PartnerPayment */

$this->pageTitle = 'Partner Payments - Create';
$this->breadcrumbs = array(
	'Partner Payments' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>