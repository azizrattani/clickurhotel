<?php
/* @var $this PartnerPaymentController */
/* @var $model PartnerPayment */

$this->pageTitle = 'Partner Payments - Update';
$this->breadcrumbs = array(
	'Partner Payments' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>