<?php
/* @var $this PartnerPaymentController */
/* @var $model PartnerPayment */
/* @var $form CActiveForm */

$parent_id = (Yii::app()->user->partner["parent_id"]);
$otherCondition = "";
if ($parent_id != 0){
	$otherCondition = " and parent_id='".Yii::app()->user->partner["id"]."'";
}
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'partner-payment-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'partner_id'); ?>
		 <?= $form->dropDownList($model, 'partner_id', CHtml::listData(Partners::model()->findAll(array("condition"=>' id != '.Yii::app()->user->partner["id"].' and deleted=0' . $otherCondition)), 'id', 'name'), array('class' => 'form-control mselect'));?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'pay_date'); ?>
        <?php
		 if ($model->pay_date){
			 $dt = strtotime($model->pay_date);
		 }else{
			 $dt = strtotime("+1 day");
		 }
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'PartnerPayment[pay_date]',
								
                                'id'=>'Modelname_start_date',
                            	'value'=>Yii::app()->dateFormatter->format("M/d/y",strtotime($dt)),
                                'options'=>array(								
                                'showAnim'=>'fold',
                                ),
								
                                'htmlOptions'=>array(
                                'display'=>'block;',
								
								'class' => 'form-control',
                                ),
                        ));
		?>
		
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'amount'); ?>
		<?= $form->textField($model,'amount', array('class' => 'form-control')); ?>
	</div>


	<div class="form-group">
		<?= $form->labelEx($model,'reference_number'); ?>
		<?= $form->textField($model, 'reference_number', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'narration'); ?>
		<?= $form->textArea($model,'narration',array('class' => 'form-control','rows'=>6, 'cols'=>50)); ?>
	</div>

	
	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
