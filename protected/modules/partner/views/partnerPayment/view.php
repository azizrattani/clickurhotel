<?php
/* @var $this PartnerPaymentController */
/* @var $model PartnerPayment */

$this->pageTitle = 'Partner Payments - View';
$this->breadcrumbs=array(
	'Partner Payments' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'partner_id',
		'pay_date',
		'amount',
		'payment_date',
		'reference_number',
		'narration',
		'bid',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>