<?php
/* @var $this PartnerLimitController */
/* @var $model PartnerLimit */

$this->pageTitle = 'Partner Limits - Update';
$this->breadcrumbs = array(
	'Partner Limits' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>