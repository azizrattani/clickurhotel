<?php
/* @var $this PartnerLimitController */
/* @var $model PartnerLimit */
/* @var $form CActiveForm */

$parent_id = (Yii::app()->user->partner["parent_id"]);
$partner_id = Yii::app()->user->partner["id"];
$otherCondition = "";
if ($parent_id != 0){
	$otherCondition = " and parent_id='".$partner_id."'";
}
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'partner-limit-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'staff_id'); ?>
		 <?= $form->dropDownList($model, 'staff_id', CHtml::listData(Partners::model()->findAll(array("condition"=>' id != '.Yii::app()->user->partner["id"].' and deleted=0' . $otherCondition)), 'id', 'name'), array('class' => 'form-control mselect'));?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'booking_limit'); ?> <span id="bookingMinimum"></span> (<?php if ($parent_id == 0){ echo "No Limit";}else{ echo "Maximum Limit " . Yii::app()->functions->getAgentLimit($partner_id);}?>) <span id="minum_limit"></span>
		<?= $form->textField($model,'booking_limit', array('class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'checkin_limit'); ?> <span id="checkingMinimum"></span> (<?php if ($parent_id == 0){ echo "No Limit";}else{ echo "Maximum Limit ". Yii::app()->functions->getCheckinLimit($partner_id) . "%";}?>)
		<?= $form->textField($model,'checkin_limit', array('class' => 'form-control')); ?>
	</div>

	

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>


<div id="limitDetail"></div>

<script>

	function staffLimit(){
		
		$staff_id = $("#PartnerLimit_staff_id").val();
	
		url  = "<?= $this->createUrl('limitDetail') ?>/staff_id/"+$staff_id;
		$("#limitDetail").html('');
		
		$.ajax({
			url: url,
			method: "GET",
			success: function(result){					
				
				$("#limitDetail").html(result); 
			}
		});
		
		url  = "<?= $this->createUrl('minimumLimit') ?>/staff_id/"+$staff_id;
		
		
		$.ajax({
			url: url,
			method: "GET",
			success: function(result){	
			
				data = $.parseJSON(result);
				if(data){
					$("#checkingMinimum").html(data.checking); 
					$("#bookingMinimum").html(data.booking); 
				}else{
					$("#checkingMinimum").html(); 
					$("#bookingMinimum").html(); 
				}
			
				//checkingMinimum
				//bookingMinimum			
				
				
			}
		});
		
		
	}
	
	
	$("#PartnerLimit_staff_id").change(function(e) {
        
		staffLimit();
		
    });
	
	staffLimit();

	


</script>
