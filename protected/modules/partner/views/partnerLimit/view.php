<?php
/* @var $this PartnerLimitController */
/* @var $model PartnerLimit */

$this->pageTitle = 'Partner Limits - View';
$this->breadcrumbs=array(
	'Partner Limits' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'staff_id',
		'booking_limit',
		'checkin_limit',
		'staff_price_type',
		'date_start',
		'date_end',
		'deleted',
		'created_date',
		'modified_date',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>