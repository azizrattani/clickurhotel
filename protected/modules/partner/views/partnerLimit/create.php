<?php
/* @var $this PartnerLimitController */
/* @var $model PartnerLimit */

$this->pageTitle = 'Partner Limits - Create';
$this->breadcrumbs = array(
	'Partner Limits' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>