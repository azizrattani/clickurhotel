<?php

/**
 * This is the model class for table "site_details".
 *
 * The followings are the available columns in table 'site_details':
 * @property integer $id
 * @property string $base_url
 * @property string $sitename
 * @property string $logo
 * @property string $address
 * @property string $email
 * @property string $send_email
 * @property string $mail_host
 * @property string $mail_username
 * @property string $mail_password
 * @property string $mail_from
 * @property string $mail_cc
 * @property string $mail_bcc
 * @property string $important_note
 * @property string $note
 * @property string $created
 * @property string $modified
 * @property integer $deleted
 * @property integer $status
 */
class SiteDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'site_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('base_url, sitename, email', 'required'),
			array('deleted, status', 'numerical', 'integerOnly'=>true),
			array('base_url, sitename, logo, address, email, send_email, mail_host, mail_username, mail_password, mail_from', 'length', 'max'=>255),
			array('mail_cc, mail_bcc, important_note, note, created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, base_url, sitename, logo, address, email, send_email, mail_host, mail_username, mail_password, mail_from, mail_cc, mail_bcc, important_note, note, created, modified, deleted, status', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'base_url' => 'Base Url',
			'sitename' => 'Sitename',
			'logo' => 'Logo',
			'address' => 'Address',
			'email' => 'Email',
			'send_email' => 'Send Email',
			'mail_host' => 'Mail Host',
			'mail_username' => 'Mail Username',
			'mail_password' => 'Mail Password',
			'mail_from' => 'Mail From',
			'mail_cc' => 'Mail Cc',
			'mail_bcc' => 'Mail Bcc',
			'important_note' => 'Important Note For Invoice',
			'note' => 'Note For Invoice',
			'created' => 'Created',
			'modified' => 'Modified',
			'deleted' => 'Deleted',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('base_url', $this->base_url, true);
		$criteria->compare('sitename', $this->sitename, true);
		$criteria->compare('logo', $this->logo, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('send_email', $this->send_email, true);
		$criteria->compare('mail_host', $this->mail_host, true);
		$criteria->compare('mail_username', $this->mail_username, true);
		$criteria->compare('mail_password', $this->mail_password, true);
		$criteria->compare('mail_from', $this->mail_from, true);
		$criteria->compare('mail_cc', $this->mail_cc, true);
		$criteria->compare('mail_bcc', $this->mail_bcc, true);
		$criteria->compare('important_note', $this->important_note, true);
		$criteria->compare('note', $this->note, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('deleted', 0);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SiteDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
