<?php

class SitedetailsController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new SiteDetails('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['SiteDetails']))
		{
			$model->attributes = $_GET['SiteDetails'];
		}

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new SiteDetails;

		if(isset($_POST['SiteDetails']))
		{
			$model->attributes = $_POST['SiteDetails'];

			if (isset($_FILES["SiteDetails"]["name"]["logo"]) && $_FILES["SiteDetails"]["name"]["logo"] != ""){
				$model->image=CUploadedFile::getInstance($model,'logo');
			}

			if ($model->save())
            {

				$path = Yii::app()->basePath . '/../images/sitedetails/'.$model->id;

				if(!is_dir($path)){
					mkdir($path,0777);
				}
				$path .='/';

				if (isset($_FILES["Partners"]["name"]["image"]) && $_FILES["Partners"]["name"]["image"] != ""){
					$model->image->saveAs($path.$model->image);
				}

                Yii::app()->user->setFlash('success', 'SiteDetails saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['SiteDetails']))
			{
				$model->attributes = $_POST['SiteDetails'];

				if ($model->save())
	            {
	                Yii::app()->user->setFlash('success', 'SiteDetails updated successfully');
	                $this->redirect(array('index'));
	            }
			}

			$this->render('update', compact('model'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = SiteDetails::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
