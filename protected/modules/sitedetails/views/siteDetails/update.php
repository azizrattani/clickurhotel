<?php
/* @var $this SiteDetailsController */
/* @var $model SiteDetails */

$this->pageTitle = 'Site Details - Update';
$this->breadcrumbs = array(
	'Site Details' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>