<?php
/* @var $this SiteDetailsController */
/* @var $model SiteDetails */

$this->pageTitle = 'Site Details - View';
$this->breadcrumbs=array(
	'Site Details' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'base_url',
		'sitename',
		'logo',
		'address',
		'email',
		'send_email',
		'mail_host',
		'mail_username',
		'mail_password',
		'mail_from',
		'mail_cc',
		'mail_bcc',
		'important_note',
		'note',
		'created',
		'modified',
		'deleted',
		'status',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>