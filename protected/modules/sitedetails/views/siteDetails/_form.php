<?php
/* @var $this SiteDetailsController */
/* @var $model SiteDetails */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'site-details-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'base_url'); ?>
		<?= $form->textField($model, 'base_url', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'sitename'); ?>
		<?= $form->textField($model, 'sitename', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'logo'); ?>
		<?= $form->fileField($model, 'logo', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
		<?php
		if(!$model->isNewRecord)
		{
			$path = Yii::app()->baseUrl . '/images/sitedetails/'.$model->id.'/';

			echo '<img src="'.$path.$model->logo.'" style= "max-height:200px;" />';
		}
		?>

	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'address'); ?>
		<?= $form->textField($model, 'address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'email'); ?>
		<?= $form->textField($model, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'send_email'); ?>
		<?= $form->textField($model, 'send_email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_host'); ?>
		<?= $form->textField($model, 'mail_host', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_username'); ?>
		<?= $form->textField($model, 'mail_username', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_password'); ?>
		<?= $form->textField($model, 'mail_password', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_from'); ?>
		<?= $form->textField($model, 'mail_from', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_cc'); ?>
		<?= $form->textArea($model,'mail_cc',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'mail_bcc'); ?>
		<?= $form->textArea($model,'mail_bcc',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'important_note'); ?>
		<?= $form->textArea($model,'important_note',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'note'); ?>
		<?= $form->textArea($model,'note',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'status'); ?>
		<?= $form->dropDownList($model, 'status', array('1' => 'Active', '0' => 'Inactive'), array('class' => 'form-control')); ?>
	</div>

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
