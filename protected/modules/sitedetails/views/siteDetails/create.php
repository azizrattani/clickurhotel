<?php
/* @var $this SiteDetailsController */
/* @var $model SiteDetails */

$this->pageTitle = 'Site Details - Create';
$this->breadcrumbs = array(
	'Site Details' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>