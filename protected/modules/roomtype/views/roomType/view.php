<?php
/* @var $this RoomTypeController */
/* @var $model RoomType */

$this->pageTitle = 'Room Types - View';
$this->breadcrumbs=array(
	'Room Types' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'title',
		'deleted',
		'created',
		'modified',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>