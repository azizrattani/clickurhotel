<?php
/* @var $this RoomTypeController */
/* @var $model RoomType */

$this->pageTitle = 'Room Types - Update';
$this->breadcrumbs = array(
	'Room Types' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>