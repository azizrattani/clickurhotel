<?php
/* @var $this RoomTypeController */
/* @var $model RoomType */

$this->pageTitle = 'Room Types - Create';
$this->breadcrumbs = array(
	'Room Types' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>