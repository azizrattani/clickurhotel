<?php

/**
 * This is the model class for table "role_permissions".
 *
 * The followings are the available columns in table 'role_permissions':
 * @property integer $id
 * @property integer $role_id
 * @property integer $permission_action_id
 * @property integer $deleted
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property PermissionActions $permissionAction
 * @property Roles $role
 */
class RolePermissions extends CActiveRecord
{
	public $module;
	public $controller;
	public $action;
	public $meta_code;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'role_permissions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('role_id, permission_action_id', 'required'),
			array('role_id, permission_action_id, deleted', 'numerical', 'integerOnly'=>true),
			array('created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, role_id, permission_action_id, deleted, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'permissionAction' => array(self::BELONGS_TO, 'PermissionActions', 'permission_action_id'),
			'role' => array(self::BELONGS_TO, 'Roles', 'role_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'role_id' => 'Role',
			'permission_action_id' => 'Permission Action',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('role_id', $this->role_id);
		$criteria->compare('permission_action_id', $this->permission_action_id);
		$criteria->compare('deleted', 0);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array('criteria'=>$criteria,
		  'pagination' => array(
			  'pageSize' => 100,
			),      
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RolePermissions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}

	public static function getPermissionsByRole($roleId = 0)
	{
		$criteria = new CDbCriteria;

		$criteria->select = 'pa.*';
		$criteria->join   = 'INNER JOIN `permission_actions` pa ON pa.`id` = t.`permission_action_id`';
		
		$criteria->compare('pa.deleted', 0);
		$criteria->compare('t.deleted', 0);
		$criteria->compare('t.role_id', $roleId);

		$data = self::model()->findAll($criteria);

        $result = array();
        foreach($data as $d)
        {
            $result[] = [
            	'module'     => strtolower($d->module),
                'controller' => strtolower($d->controller),
                'action'     => strtolower($d->action),
                'meta_code'  => strtolower($d->meta_code)
            ];
        }

        return $result;  
	}

	public static function isAllowed($action = '', $metacode = '', $controller = '', $module = '')
	{
		$module      = strtolower($module);
        $controller  = strtolower($controller);
        $action      = strtolower($action);
        $metacode    = strtolower($metacode);
		$valid  	 = false;
        $permissions = Yii::app()->user->permissions;

        foreach($permissions as $per)
        {
            $valid = ($per['module'] == $module && $per['controller'] == $controller && $per['action'] == $action && $per['meta_code'] == $metacode) ? true : $valid;
        }
        
        return $valid;
	}
}
