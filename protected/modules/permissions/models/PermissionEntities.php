<?php

/**
 * This is the model class for table "permission_entities".
 *
 * The followings are the available columns in table 'permission_entities':
 * @property integer $id
 * @property string $title
 * @property integer $permission_group_id
 * @property integer $deleted
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property PermissionActions[] $permissionActions
 * @property PermissionGroups $permissionGroup
 */
class PermissionEntities extends CActiveRecord
{
	public $permission_group;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'permission_entities';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, permission_group_id', 'required'),
			array('permission_group_id, deleted', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, permission_group_id, deleted, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'permissionActions' => array(self::HAS_MANY, 'PermissionActions', 'permission_entity_id'),
			'permissionGroup' => array(self::BELONGS_TO, 'PermissionGroups', 'permission_group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'permission_group_id' => 'Permission Group',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('t.id', $this->id);
		$criteria->compare('t.title', $this->title, true);
		$criteria->compare('t.permission_group_id', $this->permission_group_id);
		$criteria->compare('t.deleted', 0);
		$criteria->compare('t.created', $this->created, true);
		$criteria->compare('t.modified', $this->modified, true);

		$criteria->select = 't.*, g.`title` AS `permission_group`';
		$criteria->join = 'INNER JOIN `permission_groups` g ON g.`id` = t.`permission_group_id`';

		return new CActiveDataProvider($this, array('criteria'=>$criteria,
		  'pagination' => array(
			  'pageSize' => 100,
			),      
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PermissionEntities the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
