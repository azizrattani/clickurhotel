<?php

class ActionsController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex($id = 0)
	{
		$model = new PermissionActions('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PermissionActions']))
		{
			$model->attributes = $_GET['PermissionActions'];
		}

        if($id)
        {
            $model->permission_entity_id = $id;
        }

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'id', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate($id = 0)
	{
		$model = new PermissionActions;

        if($id)
        {
            $model->permission_entity_id = $id;
        }

		if(isset($_POST['PermissionActions']))
		{
			$model->attributes = $_POST['PermissionActions'];

			if ($model->save())
            {
                Yii::app()->user->setFlash('success', 'Permission Actions saved successfully');
                
                if($id > 0) {
                    $url = $this->createUrl('index', array('id' => $id));
                } else {
                    $url = $this->createUrl('index', array('id' => $model->permission_entity_id));
                }

                $this->redirect($url);
            }
		}

        $mca         = $this->getModulesController();
        $modules     = $mca['modules'];
        $controllers = $mca['controllers'];
        $actions     = $mca['actions'];
        $entities    = CHtml::listData(PermissionEntities::model()->findAllByAttributes(array('deleted' => 0)), 'id', 'title');

		$this->render('create', compact('model', 'id', 'modules', 'controllers', 'actions', 'entities'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['PermissionActions']))
			{
				$model->attributes = $_POST['PermissionActions'];

				if ($model->save())
	            {
	                Yii::app()->user->setFlash('success', 'Permission Actions updated successfully');

                    $url = $this->createUrl('index', array('id' => $model->permission_entity_id));
	                $this->redirect($url);
	            }
			}

            $mca         = $this->getModulesController();
            $modules     = $mca['modules'];
            $controllers = $mca['controllers'];
            $actions     = $mca['actions'];
            $entities    = CHtml::listData(PermissionEntities::model()->findAllByAttributes(array('deleted' => 0)), 'id', 'title');

            $this->render('update', compact('model', 'id', 'modules', 'controllers', 'actions', 'entities'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

    public function actionGetParents()
    {
        $mid = $_POST['mid'];
        $cid = $_POST['cid'];
        $aid = $_POST['aid'];

        $rec = PermissionActions::model()->findAllByAttributes(array('module' => $mid, 'controller' => $cid));

        $data = array();

        foreach ($rec as $r)
        {
            if($r->action != $aid)
            {
                $data[$r->id] = $r->title . ' -> ' . $r->action;
            }
        }

        echo json_encode($data);
        exit;
    }

	public function loadModel($id)
	{
		$model = PermissionActions::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}

    private function getModulesController()
    {
        $modules = $controllers = $actions = array();

        $path = 'protected/modules';
        foreach (scandir($path) as $dir)
        {
            if ($dir === '.' or $dir === '..')
            {
                continue;
            }
            
            if (is_dir($path . '/' . $dir))
            {
                $modules[$dir] = $dir;
            }
        }

        $paths = array(0 => 'protected/controllers');
        foreach ($modules as $module)
        {
            $paths[$module] = 'protected/modules/' . $module . '/controllers';
        }

        foreach($paths as $key => $path)
        {
            foreach (scandir($path) as $dir)
            {
                if ($dir === '.' or $dir === '..')
                {
                    continue;
                }
                
                if (!is_dir($path . '/' . $dir) && strpos($dir, 'Controller.php'))
                {
                    $c = str_replace('Controller.php', '', $dir);
                    $controllers[$key][$c] = $c;


                    $filepath = $path . '/' . $dir;
                    $handle   = fopen($filepath, "r");
                    $contents = fread($handle, filesize($filepath));
                    fclose($handle);
                
                    $regx = '/function[\s\n]+(\S+)[\s\n]*\(/';
                    preg_match_all($regx, $contents, $functionsArray);
                    $functionsArray = isset($functionsArray[1]) ? $functionsArray[1] : array();
                    
                    foreach ($functionsArray as $fnc)
                    {
                        if (strpos($fnc, 'action') !== false)
                        {
                            $k = $key . '_' . $c;
                            $a = str_replace('action', '', $fnc);

                            $actions[$k][$a] = $a;
                        }
                    }
                }
            }
        }

        return compact('modules', 'controllers', 'actions');
    }
}
