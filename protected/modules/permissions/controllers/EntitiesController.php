<?php

class EntitiesController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex($id = 0)
	{
		$model = new PermissionEntities('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PermissionEntities']))
		{
			$model->attributes = $_GET['PermissionEntities'];
		}

        if($id)
        {
            $model->permission_group_id = $id;
        }

        $createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $actionAllowed = $this->isAllowed('index', '', 'Actions');

        $this->render('index', compact('model', 'id', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'actionAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate($id = 0)
	{
		$model = new PermissionEntities;

        if($id)
        {
            $model->permission_group_id = $id;
        }

		if(isset($_POST['PermissionEntities']))
		{
			$model->attributes = $_POST['PermissionEntities'];

			if ($model->save())
            {
                Yii::app()->user->setFlash('success', 'Permission Entities saved successfully');

                if($id > 0) {
                    $url = $this->createUrl('index', array('id' => $id));
                } else {
                    $url = $this->createUrl('index', array('id' => $model->permission_group_id));
                }

                $this->redirect($url);
            }
		}

        $groups = CHtml::listData(PermissionGroups::model()->findAllByAttributes(array('deleted' => 0)), 'id', 'title');

		$this->render('create', compact('model', 'groups', 'id'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['PermissionEntities']))
			{
				$model->attributes = $_POST['PermissionEntities'];

				if ($model->save())
	            {
	                Yii::app()->user->setFlash('success', 'Permission Entities updated successfully');

                    $url = $this->createUrl('index', array('id' => $model->permission_group_id));
	                $this->redirect($url);
	            }
			}

            $groups = CHtml::listData(PermissionGroups::model()->findAllByAttributes(array('deleted' => 0)), 'id', 'title');

			$this->render('update', compact('model', 'groups'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = PermissionEntities::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
