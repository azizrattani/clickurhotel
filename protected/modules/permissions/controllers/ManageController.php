<?php

class ManageController extends AdminController
{
	public function actionIndex()
	{
        $roles    = Roles::model()->findAllByAttributes(array('deleted' => 0));
        $groups   = PermissionGroups::model()->findAllByAttributes(array('deleted' => 0));
        $entities = PermissionEntities::model()->findAllByAttributes(array('deleted' => 0));
        $actions  = PermissionActions::model()->findAllByAttributes(array('deleted' => 0));

        $currentPermissions = RolePermissions::model()->findAllByAttributes(array('deleted' => 0));

        $updateAllowed = $this->isAllowed('index', 'update');

        if(isset($_POST['update']) && $updateAllowed)
        {
            $permissions = isset($_POST['permission']) ? $_POST['permission'] : array();
            $currentIds  = array(0);

            foreach($permissions as $role => $actions)
            {
                foreach($actions as $action => $val)
                {
                    $record = RolePermissions::model()->findByAttributes(['permission_action_id' => $action, 'role_id' => $role]);

                    if(!$record)
                    {
                        $record = new RolePermissions;
                    }

                    $record->permission_action_id = $action;
                    $record->role_id = $role;
                    $record->deleted = 0;
                    $record->save();
  
                    $currentIds[] = $record->id;
                }
            }

            RolePermissions::model()->updateAll(
                array('deleted' => 1),
                array('condition' => "id NOT IN(" . implode(',', $currentIds) .")")
            );

            Yii::app()->user->setFlash('success', 'Roles Permissions saved successfully');
            $this->redirect($this->createUrl('index'));
        }

		$this->render('index', compact('roles', 'groups', 'entities', 'actions', 'currentPermissions', 'updateAllowed'));
	}
}
