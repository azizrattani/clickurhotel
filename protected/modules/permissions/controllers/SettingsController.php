<?php

class SettingsController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new Settings('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Settings']))
		{
			$model->attributes = $_GET['Settings'];
		}

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

        if(!$model)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
		    $this->render('view', compact('model'));
        }
	}

	public function actionCreate()
	{
		$model = new Settings;

		if(isset($_POST['Settings']))
		{
			$model->attributes = $_POST['Settings'];

			if ($model->save())
            {
                Yii::app()->user->setFlash('success', 'Settings saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['Settings']))
			{
				$model->attributes = $_POST['Settings'];

				if ($model->save())
	            {
	                Yii::app()->user->setFlash('success', 'Settings updated successfully');
	                $this->redirect(array('index'));
	            }
			}

			$this->render('update', compact('model'));
		}
	}

	public function loadModel($id)
	{
		$model = Settings::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
