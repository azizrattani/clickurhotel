<?php
/* @var $this EntitiesController */
/* @var $model PermissionEntities */

$this->pageTitle = 'Permission Entities - Update';
$this->breadcrumbs = array(
	'Permission Entities' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model', 'groups', 'id')); ?>