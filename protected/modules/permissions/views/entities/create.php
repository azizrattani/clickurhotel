<?php
/* @var $this EntitiesController */
/* @var $model PermissionEntities */

$this->pageTitle = 'Permission Entities - Create';
$this->breadcrumbs = array(
	'Permission Entities' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model', 'groups', 'id')); ?>