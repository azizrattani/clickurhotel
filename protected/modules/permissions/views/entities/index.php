<?php
/* @var $this EntitiesController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Manage Permission Entities';
$this->breadcrumbs = array(
	'Permission Entities',
    'Manage'
);

if($id > 0) {
    $addUrl = $this->createUrl('create', array('id' => $id));
} else {
    $addUrl = $this->createUrl('create');
}
?>

<?php if($createAllowed) { ?>
<p class="text-right">
    <a href="<?php echo $addUrl; ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Add New
    </a>
</p>
<?php } ?>
<div class="box-body table-responsive no-padding">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'permission-entities-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
        'columns' => array(
    		array(
                'name' => 'id',
                'htmlOptions' => array('style' => 'width: 50px; text-align: center;'),
            ),
		    'title',
            array(
                'name' => 'permission_group_id',
                'value' => '$data->permission_group',
                'filter' => CHtml::listData(PermissionGroups::model()->findAllByAttributes(array('deleted' => 0)), 'id', 'title'),
                'visible' => $id > 0 ? false : true
            ),
            array(
                'htmlOptions' => array('style' => 'width: 75px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '{actions}{update}{delete}',
                'buttons' => array(
                    'actions' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'url' => 'Yii::app()->createUrl("/permissions/actions", array("id" => $data->id))',
                        'options' => array('title' => 'Actions', 'class' => 'fa fa-list', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$actionAllowed"
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Update', 'class' => 'fa fa-pencil-square-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$updateAllowed"
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Delete', 'class' => 'fa fa-trash', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$deleteAllowed"
                    )
                )
            )
        ),
    )); ?>
</div>