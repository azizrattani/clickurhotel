<?php
/* @var $this EntitiesController */
/* @var $model PermissionEntities */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'permission-entities-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model, 'title'); ?>
		<?= $form->textField($model, 'title', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'permission_group_id'); ?>
		<?php if($model->isNewRecord && $id > 0) { ?>
		<?= $form->textField($model, 'group', array('class' => 'form-control', 'value' => $groups[$model->permission_group_id], 'readonly' => 'readonly')); ?>
		<?php } else { ?>
		<?= $form->dropDownList($model, 'permission_group_id', $groups, array('class' => 'form-control')); ?>
		<?php } ?>
	</div>

	<div class="box-footer">
		<?php
		if($model->isNewRecord && $id > 0) {
            $backUrl = $this->createUrl('index', array('id' => $id));
        } else {
            $backUrl = $this->createUrl('index', array('id' => $model->permission_group_id));
        }
    	?>
		<a href="<?= $backUrl; ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
