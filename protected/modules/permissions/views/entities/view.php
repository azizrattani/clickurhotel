<?php
/* @var $this EntitiesController */
/* @var $model PermissionEntities */

$this->pageTitle = 'Permission Entities - View';
$this->breadcrumbs=array(
	'Permission Entities' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'title',
		'permission_group_id',
		'created',
		'modified',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
    <a href="<?= $this->createUrl('update'); ?>/id/<?=$model->id?>" class="btn btn-default pull-right">update</a>
</div>