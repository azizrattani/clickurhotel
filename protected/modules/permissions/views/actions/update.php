<?php
/* @var $this ActionsController */
/* @var $model PermissionActions */

$this->pageTitle = 'Permission Actions - Update';
$this->breadcrumbs = array(
	'Permission Actions' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model', 'modules', 'controllers', 'actions', 'entities')); ?>