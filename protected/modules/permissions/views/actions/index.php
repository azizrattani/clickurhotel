<?php
/* @var $this ActionsController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Manage Permission Actions';
$this->breadcrumbs = array(
	'Permission Actions',
    'Manage'
);

if($id > 0) {
    $addUrl = $this->createUrl('create', array('id' => $id));
    $parents = CHtml::listData(PermissionActions::model()->findAllByAttributes(array('deleted' => 0, 'permission_entity_id' => $id)), 'id', 'title');
} else {
    $addUrl = $this->createUrl('create');
    $parents = CHtml::listData(PermissionActions::model()->findAllByAttributes(array('deleted' => 0)), 'id', 'title');
}
?>

<?php if($createAllowed) { ?>
<p class="text-right">
    <a href="<?php echo $addUrl; ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Add New
    </a>
</p>
<?php } ?>

<div class="box-body table-responsive no-padding">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'permission-actions-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
        'columns' => array(
    		array(
                'name' => 'id',
                'htmlOptions' => array('style' => 'width: 50px; text-align: center;'),
            ),
            'title',
            array(
                'name' => 'parent',
                'value' => '$data->parent_title',
                'filter' => $parents,
            ),
            'module',
            'controller',
            'action',
            'meta_code',
            array(
                'name' => 'permission_entity_id',
                'value' => '$data->permission_entity',
                'filter' => CHtml::listData(PermissionEntities::model()->findAllByAttributes(array('deleted' => 0)), 'id', 'title'),
                'visible' => $id > 0 ? false : true
            ),

            array(
                'htmlOptions' => array('style' => 'width: 75px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Update', 'class' => 'fa fa-pencil-square-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$updateAllowed"
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Delete', 'class' => 'fa fa-trash', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$deleteAllowed"
                    )
                )
            )
        ),
    )); ?>
</div>