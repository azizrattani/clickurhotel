<?php
/* @var $this ActionsController */
/* @var $model PermissionActions */

$this->pageTitle = 'Permission Actions - Create';
$this->breadcrumbs = array(
	'Permission Actions' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model', 'id', 'modules', 'controllers', 'actions', 'entities')); ?>