<?php
/* @var $this ActionsController */
/* @var $model PermissionActions */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'permission-actions-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<p class="note">Atleast one fields with <span class="required">**</span> must be filled.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'title'); ?>
		<?= $form->textField($model, 'title', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'permission_entity_id'); ?>
		<?php if($model->isNewRecord && $id > 0) { ?>
		<?= $form->textField($model, 'entity', array('class' => 'form-control', 'value' => $entities[$model->permission_entity_id], 'readonly' => 'readonly')); ?>
		<?php } else { ?>
		<?= $form->dropDownList($model, 'permission_entity_id', $entities, array('class' => 'form-control')); ?>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<?= $form->labelEx($model,'module'); ?>
				<?= $form->dropDownList($model, 'module', $modules, array('class' => 'form-control', 'empty' => '')); ?>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<?= $form->labelEx($model,'controller'); ?>
				<?= $form->dropDownList($model, 'controller', array(), array('class' => 'form-control')); ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label class="required" for="PermissionActions_action">
					Action <span class="required">**</span>
				</label>
				<?= $form->dropDownList($model, 'action', array(), array('class' => 'form-control')); ?>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label class="required" for="PermissionActions_meta_code">
					Meta Code <span class="required">**</span>
				</label>
				<?= $form->textField($model, 'meta_code', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'parent'); ?>
		<?= $form->dropDownList($model, 'parent', array(), array('class' => 'form-control')); ?>
	</div>

	<div class="box-footer">
		<?php
		if($model->isNewRecord && $id > 0) {
            $backUrl = $this->createUrl('index', array('id' => $id));
        } else {
            $backUrl = $this->createUrl('index', array('id' => $model->permission_entity_id));
        }
    	?>
		<a href="<?= $backUrl; ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
var controllers = <?= json_encode($controllers); ?>;
var actions = <?= json_encode($actions); ?>;

$(document).ready(function(){
	populateContollers();

	$("#PermissionActions_controller").val('<?= $model->controller ?>');
	$("#PermissionActions_action").val('<?= $model->action ?>');

	$('#PermissionActions_module').change(function(){
		populateContollers();
	});

	$('#PermissionActions_controller').change(function(){
		populateActions();
	});
	
	$('#PermissionActions_action').change(function(){
		populateParent();
	});
});

function populateContollers(){
	var id    = $('#PermissionActions_module').val();
	var clist = $('#PermissionActions_controller');

	id = id ? id : 0;
	clist.empty();

	$.each(controllers, function(k, val) {
		if(id == k){
			$.each(val, function(value, label) {
 				clist.append($('<option />').attr('value', value).text(label));
			});
 		}
	});

	populateActions();
}

function populateActions(){
	var mid   = $('#PermissionActions_module').val();
	var cid   = $('#PermissionActions_controller').val();
	var alist = $('#PermissionActions_action');

	mid = mid ? mid : 0;

	var id = mid + '_' + cid;

	alist.empty();
	alist.append($('<option />').attr('value', '').text(''));

	$.each(actions, function(k, val) {
		if(id == k){
			$.each(val, function(value, label) {
 				alist.append($('<option />').attr('value', value).text(label));
			});
 		}
	});

	setTimeout(populateParent, 500);
}

function populateParent(){
	var mid = $('#PermissionActions_module').val();
	var cid = $('#PermissionActions_controller').val();
	var aid = $('#PermissionActions_action').val();

	var plist = $('#PermissionActions_parent');
 	plist.empty();
 	plist.append($('<option />').attr('value', '').text(''));

	$.ajax({
		url: "<?= $this->createUrl('getParents') ?>",
		method: "POST",
		dataType: "JSON",
		data: {mid: mid, cid: cid, aid: aid},
		success: function(result){
			$.each(result, function(value, label) {
 				plist.append($('<option />').attr('value', value).text(label));
			});

			plist.val('<?= $model->parent ?>');
    	}
    });
}
</script>