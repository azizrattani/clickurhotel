<?php
/* @var $this ActionsController */
/* @var $model PermissionActions */

$this->pageTitle = 'Permission Actions - View';
$this->breadcrumbs=array(
	'Permission Actions' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'title',
		'parent_id',
		'permission_entity_id',
		'module',
		'controller',
		'action',
		'meta_code',
		'created',
		'modified',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
    <a href="<?= $this->createUrl('update'); ?>/id/<?=$model->id?>" class="btn btn-default pull-right">update</a>
</div>