<?php
/* @var $this SettingsController */
/* @var $model Settings */

$this->pageTitle = 'Settings - Create';
$this->breadcrumbs = array(
	'Settings' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>