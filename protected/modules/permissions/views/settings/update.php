<?php
/* @var $this SettingsController */
/* @var $model Settings */

$this->pageTitle = 'Settings - Update';
$this->breadcrumbs = array(
	'Settings' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>