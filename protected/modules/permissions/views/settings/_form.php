<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'settings-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'meta_key'); ?>
		<?= $form->textField($model, 'meta_key', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'meta_value'); ?>
		<?= $form->textArea($model, 'meta_value', array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
