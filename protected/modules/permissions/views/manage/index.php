<?php
$this->pageTitle = 'Manage Roles Permission';
$this->breadcrumbs = array(
	'Roles Permission',
    'Manage'
);
?>
<div class="box-body table-responsive no-padding">

    <?php
    if($updateAllowed)
    {
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'role-permission-form',
        ));
    }
    ?>

    <input type="hidden" name="update" value="1" />

    <table class="table table-bordered table-striped table-permissions">
        <tr class="bg1">
            <th rowspan="2" class="text-center" style="vertical-align: middle">Entities / Permissions</th>
            <th colspan="<?= count($roles) ?>" class="text-center">Roles</th>
        </tr>
        <tr class="bg1">
            <?php foreach($roles as $role) { ?>
            <td class="text-center text-bold" style="width: 100px"><?= $role->title ?></td>
            <?php } ?>
        </tr>

        <?php
        $cnt1 = 0;
        foreach($groups as $group)
        {
            $cnt1++;
        ?>
        <tr class="bg2">
            <td class="text-bold" colspan="<?= count($roles) + 1 ?>">
                <a href="javascript:void(0)" onclick="showGroup(<?= $group['id'] ?>)"><i id="fa-grp<?= $group['id'] ?>" class="fa fa-<?= $cnt1 > 1 ? 'plus' : 'minus' ?>-circle"></i> &nbsp; <?= $group->title ?></a>
            </td>
        </tr>

        <?php
            $cnt2 = 0;
            foreach($entities as $entity)
            {
                if($entity->permission_group_id == $group->id)
                {
                    $cnt2++;
        ?>
        <tr class="bg3 group<?= $group->id ?> <?= $cnt1 > 1 ? 'hidden' : '' ?>">
            <td colspan="<?= count($roles) + 1 ?>" style="padding-left:35px">
                <a href="javascript:void(0)" onclick="showEntity(<?= $entity->id ?>)"><i id="fa-ent<?= $entity->id ?>" class="fa fa-<?= $cnt1 > 1 || $cnt2 > 1 ? 'plus' : 'minus' ?>-circle"></i> &nbsp; <?= $entity->title ?></a>
            </td>
        </tr>
        <?php
                    foreach($actions as $action)
                    {
                        if($action->permission_entity_id == $entity->id)
                        {
        ?>
        <tr class="groupsub<?= $group->id ?> entity<?= $entity->id ?> <?= $cnt1 > 1 || $cnt2 > 1 ? 'hidden' : '' ?>">
            <td style="padding-left:70px"><?= $action->title ?></td>
            <?php
                            foreach($roles as $role)
                            {
                                $checked = '';

                                foreach($currentPermissions as $cp)
                                {
                                    $checked = ($cp->permission_action_id == $action->id && $cp->role_id == $role->id) ? 'checked' : $checked;
                                }

                                $cls = 'cb' . $role->id . ($action->parent ? $action->parent : 0);
                      
                                $disabled = $action->parent ? 'disabled' : '';
            ?>
            <td class="text-center">
                <input class="permission-cb <?= $cls ?>" type="checkbox" data-id="<?= $action->id ?>" data-role="<?= $role->id ?>" name="permission[<?= $role->id ?>][<?= $action->id ?>]" <?= $checked ?> <?= $disabled ?> />
            </td>
            <?php } ?>
        </tr>
        <?php
                        }
                    }
                }
            }
        }
        ?>
    </table>

    <?php if($updateAllowed){ ?>
    <div class="box-footer">
        <?= CHtml::submitButton('Update', array('class' => 'btn btn-primary pull-right')); ?>
    </div>
    
    <?php $this->endWidget(); ?>
    <?php } ?>
</div>

<script type="text/javascript">
$(document).ready(function(){
    <?php if($updateAllowed) { ?>
    $('.permission-cb').each(function(){
        if($(this).is(":checked")){
            var id     = $(this).attr('data-id');
            var role   = $(this).attr('data-role');
            $('.cb' + role + id).prop('disabled', false);
        }
    });

    $('.permission-cb').click(function(){
        var id     = $(this).attr('data-id');
        var role   = $(this).attr('data-role');

        if($(this).is(":checked")){
            $('.cb' + role + id).prop('disabled', false);
        } else {
            $('.cb' + role + id).prop('disabled', true);
        }
    });
    <?php } else { ?>
    $('.permission-cb').prop('disabled', true);
    <?php } ?>
});

function showGroup(id){
    var elm = $('#fa-grp' + id);
    if(elm.hasClass('fa-plus-circle')){
        elm.addClass('fa-minus-circle').removeClass('fa-plus-circle');
        $('.group' + id).removeClass('hidden');
    } else {
        elm.addClass('fa-plus-circle').removeClass('fa-minus-circle');
        $('.group' + id).addClass('hidden');
        $('.groupsub' + id).addClass('hidden');
    }
}

function showEntity(id){
    var elm = $('#fa-ent' + id);
    if(elm.hasClass('fa-plus-circle')){
        elm.addClass('fa-minus-circle').removeClass('fa-plus-circle');
        $('.entity' + id).removeClass('hidden');
    } else {
        elm.addClass('fa-plus-circle').removeClass('fa-minus-circle');
        $('.entity' + id).addClass('hidden');
    }
}
</script>