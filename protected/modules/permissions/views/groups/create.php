<?php
/* @var $this GroupsController */
/* @var $model PermissionGroups */

$this->pageTitle = 'Permission Groups - Create';
$this->breadcrumbs = array(
	'Permission Groups' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>