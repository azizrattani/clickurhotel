<?php
/* @var $this GroupsController */
/* @var $model PermissionGroups */

$this->pageTitle = 'Permission Groups - Update';
$this->breadcrumbs = array(
	'Permission Groups' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>