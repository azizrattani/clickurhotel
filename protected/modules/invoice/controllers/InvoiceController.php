<?php

class InvoiceController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionMy()
	{
		$model = new Booking('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Booking']))
		{
			$model->attributes = $_GET['Booking'];
		}
		
		
        $viewAllowed   = $this->isAllowed('myDetail');

        $this->render('my', compact('model', 'viewAllowed'));
	}
	
	public function actionMyDetail($id)
	{
		$model = Booking::model()->findByPk($id);

        $this->render('my_detail', compact('model'));
	}
	
	public function actionMyDetailPDF($id)
	{
		
		$path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;
		
	
		
		require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';
		
		$model = Booking::model()->findByPk($id);
		
		
		
		
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
        'default_font' => '',

        'orientation' => 'P']);
		$mpdf->SetDisplayMode('fullpage');

		
		
		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

		/*LOAD A Fonts*/
		#$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
		#$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		
		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		
		
		
		$html =  $this->renderPartial('booking_invoice', compact('model'),true);
		
		
		
		$mpdf->WriteHTML($html);
		$mpdf->Output(preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf",'I');
	}
	
	
	
	public function actionDues()
	{
		
		
		$model = new Booking('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Booking']))
		{
			$model->attributes = $_GET['Booking'];
		}
		
		
		 $viewAllowed   = $this->isAllowed('myDetail');

        $this->render('dues', compact('model',  'viewAllowed'));
	}
	
	public function actionDuesDetail($id)
	{
		
		$model = Booking::model()->findByPk($id);

        $this->render('dues_detail', compact('model'));
	}
		
	public function actionDuesDetailPDF($id)
	{
		$path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;
			
		require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';
		
		$model = Booking::model()->findByPk($id);
		
		
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
        'default_font' => '',

        'orientation' => 'P']);
		$mpdf->SetDisplayMode('fullpage');

		
		
		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

		/*LOAD A Fonts*/
		#$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
		#$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		
		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		
		
		
		$html =  $this->renderPartial('booking_dueinvoice', compact('model'),true);
		
		$mpdf->WriteHTML($html);
		$mpdf->Output(preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf",'I');
	}
	
	
	public function actionVoucher($id)
	{
		
		$model = Booking::model()->findByPk($id);

        $this->render('voucher', compact('model'));
	}
		
	public function actionVoucherPDF($id)
	{
		$path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;
			
		require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';
		
		$model = Booking::model()->findByPk($id);
		
		
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
        'default_font' => '',

        'orientation' => 'P']);
		$mpdf->SetDisplayMode('fullpage');

		
		
		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

		/*LOAD A Fonts*/
		#$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
		#$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		
		// LOAD a stylesheet
		$stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		
		
		
		$html =  $this->renderPartial('booking_voucher', compact('model'),true);
		
		$mpdf->WriteHTML($html);
		$mpdf->Output(preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf",'I');
	}	
}
