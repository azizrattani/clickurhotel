<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->pageTitle = 'Voucher Detail';
$this->breadcrumbs = array(
	'Booking' => array('index'),
	'Voucher',
);
?>

<p class="text-right">
    <a href="<?php echo $this->createUrl('invoice/voucherPDF/id/'.$model->id); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-pdf"></i> PDF Export
    </a>
</p>


<?= $this->renderPartial('booking_voucher', compact('model')); ?>