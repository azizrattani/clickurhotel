<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->pageTitle = 'Invoice Detail';
$this->breadcrumbs = array(
	'Invoice' => array('index'),
	'My Dues',
);
?>

<p class="text-right">
    <a href="<?php echo $this->createUrl('invoice/duesDetailPDF/id/'.$model->id); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-pdf"></i> PDF Export
    </a>
</p>


<?= $this->renderPartial('booking_dueinvoice', compact('model')); ?>