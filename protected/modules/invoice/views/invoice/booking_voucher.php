<?php

	if($model->staff->image != ""){
		
		$logo = Yii::app()->createAbsoluteUrl("images/partner/".$model->staff->id."/".$model->staff->image);
		
	}else{
		$logo = Yii::app()->createAbsoluteUrl(Yii::app()->user->logo);
	}

?><html>
<head>
</head>
<body>
	<center>
    
    
    <br>
    <?php
    if($model->staff->id == Yii::app()->user->profile['child_id'] || $model->staff->parent_id == Yii::app()->user->profile['child_id']){
	?>
   		<div style="width:100%; text-align:center"><img src="<?=$logo?>" style="max-height:100px" class="logo"  /></div>
		<?php
	}else{ ?>

		<div style="width:100%; text-align:center"><img src="<?= Yii::app()->createAbsoluteUrl(Yii::app()->user->logo);?>" style="max-height:100px" class="logo"  /></div>

		<?php

	}
	?>
		<h2><?php if($model->btype == "book"){?>Voucher<?php }else{?>Request<?php }?> </h2> <br> <br>
        
    
    </center>
	
<p style="font-family:Arial; font-size:15px;"><strong>Date : <?=date("d F, Y")?></strong></p>
	<p style="font-family:Arial; font-size:15px;"><strong>Subject : <?php if($model->btype == "book"){?>Voucher<?php }else{?>Request<?php }?> </strong></p>
	<br />
    
    
	<p style="font-family:Arial; font-size:15px;">Assalamo Alaykum</p>
    <?php if($model->btype == "book"){?><?php }else{?><p style="font-family:Arial; font-size:15px;">Your request is in process we try our best to inform about result ASAP.</p><?php }?>
    
    <div>
    
    <p style="font-family:Arial; font-size:15px; float:left;">Guest: <?=$model->full_guest_name?></p> 	<p style="font-family:Arial; font-size:15px; float:left;">Nationality: <?=$model->nationality?></p>
    <p style="font-family:Arial; font-size:15px; float:right;">Reference no: <?=$model->id?></p>
    <p style="font-family:Arial; font-size:15px; float:right;">Hotel: <?=$model->hotel->name?></p>
	<?php if ($model->ref_number != ""){?>
    	<p style="font-family:Arial; font-size:15px; float:right;">Booking Confirmation#: <?=$model->ref_number?></p>
	<?php }?>
    </div>
    
<div style="clear:both"></div>
    
    <table width="100%" border="1" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="yw0">
    <thead>
    	<tr>

            <th width="19%" height="50">Check-In</th>
            <th width="18%">Check-Out</th>
            <th width="7%">Room(s)</th>
            <th width="13%">Room Type</th>
            <th width="8%">Meals</th>
            <th width="17%">City</th>
			<th width="10%">Room#</th>
        
        </tr>
    
    </thead>
    
    
	<tbody>
    
    <?php
                    $sql = "SELECT SUM(br.room_rate) AS rate, COUNT(br.room_rate) AS totalRoom, br.no_of_room,hr.`title`,hr.id as room_id,br.id as booking_room_id,br.breakfast,br.lunch,br.dinner,br.room_no FROM `booking_rooms` br  INNER JOIN `hotel_rooms` hr ON br.`room_id` = hr.`id` WHERE br.`booking_id` = ".$model->id." GROUP BY br.`room_id`;";
					$data = Yii::app()->db->createCommand($sql)->queryAll();
					
					$totalPayment = 0;
					
					foreach($data as $room){


						$meal = "";
						$mealPrice = 0;
						$MealRate = BookingMealRate::model()->findAll(["condition"=>'booking_room_id = "'.$room["booking_room_id"].'"']);


						$mealMsg = "";
						foreach($MealRate as $meals){


							$mealKey = $meals["meal_type"];
							$priceMeal = $meals["price"] * $room["totalRoom"];


							if($priceMeal <= 0){
								continue;
							}
							$mealArr = explode("_",$mealKey);

							$msg = "";

							foreach($mealArr as $name){

								$msg .= strtoupper($name);

							}

							$mealMsg .= $msg."/";
							$mealPrice += $priceMeal;


						}



						/*if (isset($room["breakfast"]) && $room["breakfast"] >=0){
                            $meal .= "BB/";
                            $mealPrice += $room["breakfast"] * $model->noofnight ;
                        }

                        if (isset($room["lunch"]) && $room["lunch"] >=0){
                            $meal .= "L/";
                            $mealPrice += $room["lunch"] * $model->noofnight;
                        }

                        if (isset($room["dinner"]) && $room["dinner"] >=0){
                            $meal .= "D/";
                            $mealPrice += $room["dinner"] * $model->noofnight;
                        }*/

						$meal = substr($mealMsg,0,-1);
						
						if ($meal == ""){
							$meal = "Room Only";	
						}
						
						
						$roomDetail = HotelRooms::model()->findByPk($room["room_id"]);
						
						
						$rate = $model->getRoomRateTotal($room["room_id"]);
						
					
						
						$price = ($rate * $room["no_of_room"]) + $mealPrice;
						
						$totalPayment += $price ;
					
					?>
                    
        
        
                        <tr>

                            <td height="50" align="center"><?= date("d F, Y",strtotime($model->checkin))?></td>
                            <td align="center"><?= date("d F, Y",strtotime($model->checkout))?></td>
                            <td align="center"><?=$room["no_of_room"]?></td>
                            <td align="center"><?=$roomDetail->roomType->title?>/<?=$roomDetail->views0->title?></td>
                            <td align="center"><?=$meal?></td>
                            <td align="center"><?=$model->hotel->city?></td>
							<td align="center"><?=$room["room_no"]?></td>
                        
                        </tr>
        
        
         <?php
					}
					?>
		

	</tbody>
</table>

<br />



<p style="font-family:Arial; font-size:15px;"><strong>Note:</strong></p>
	<ol>
		<?php
		$notes = explode("\n",YII::app()->user->note);

		foreach($notes as $note){ ?>

			<li><?=$note?></li>

			<?php
		}

		?>
	  <?php if($model->btype == "book"){?> <li>This is a definite confirmation. Cancellation policy will be applied in case of any changes. </li><?php }else{?> <li>This is a booking request. </li><?php }?>
</ol>
<?php
    if($model->staff->id == Yii::app()->user->profile['child_id'] || $model->staff->parent_id == Yii::app()->user->profile['child_id']){
	?>
	<p><strong>Best Regards,</strong></p>
	<p><strong> <?=$model->staff->name?> </strong><br style="font-family:Arial; font-size:15px;">
      <br>
	</p>
		<?php
	}else{ ?>

		<p><strong>Best Regards,</strong></p>
		<p><strong> <?=Yii::app()->user->site_name;?> </strong><br style="font-family:Arial; font-size:15px;">
			<br>
		</p>

		<?php

	}
?>
	<p>Note: This is an automatic system generated document. There is no need for any signature</p>
	<?php
    if($model->staff->id == Yii::app()->user->profile['child_id'] || $model->staff->parent_id == Yii::app()->user->profile['child_id']){
	?><p align="center"><?=$model->staff->name?> | Address: <?=$model->staff->address?> | Phone: <?=$model->staff->cell?> | Email: <?=$model->staff->email?>.</p>

		<?php
	}else{ ?>

		<p align="center"><?=Yii::app()->user->site_name?> | Address: <?=Yii::app()->user->address?> | Phone: <?=Yii::app()->user->cell?> | Email: <?=Yii::app()->user->email?>.</p>

		<?php

	}
	?>

    <p align="center">&nbsp;</p>
    <pagebreak>
    <div style="font-weight:bold;">Important Notes: </div><br />
    <div>
      <ul>
		  <?php
		  $notes = explode("\n",YII::app()->user->important_note);

		  foreach($notes as $note){ ?>

			  <li><?=$note?></li>

			  <?php
		  }

		  ?>
      </ul>
    </div>
    <p align="center"></p>
    <p>&nbsp;</p>
</body>
</html>