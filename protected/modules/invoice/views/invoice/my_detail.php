<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->pageTitle = 'Invoice Detail';
$this->breadcrumbs = array(
	'Invoice' => array('index'),
	'My Invoices',
);
?>

<p class="text-right">
    <a href="<?php echo $this->createUrl('invoice/myDetailPDF/id/'.$model->id); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-pdf"></i> PDF Export
    </a>
</p>


<?= $this->renderPartial('booking_invoice', compact('model')); ?>