<?php
/* @var $this BookingController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'My Invoices';
$this->breadcrumbs = array(
	'Invoice',
    'My Invoices'
);
?>


<div class="box-body table-responsive no-padding">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'booking-grid',
        'dataProvider' => $model->myInvoiceSearch(),
        'filter' => $model,
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
        'columns' => array(
    		'id',
		
		array('name'=>'hotel_id','value'=>'$data->hotel->name', 'filter' => CHtml::listData(Hotel::model()->findAll(), 'id', 'name')),
		array('name'=>'staff_id','value'=>'$data->staff->name','visible'=>(Yii::app()->user->profile['role_id'] == 2  )? true : false),
		'full_guest_name',
		//array('name'=>'checkin','value'=>'date("d F, Y",strtotime($data->checkin))'),
		//array('name'=>'checkout','value'=>'date("d F, Y",strtotime($data->checkout))'),
		
		array(
            'name' => 'checkin',
            'value'=>'date("d F, Y",strtotime($data->checkin))',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'checkin',
            'options'=>array(
				'dateFormat'=>'yy-mm-dd',
				'changeYear'=>'true',
				'changeMonth'=>'true',
				'showAnim' =>'slide',
				'yearRange'=>'2000:'.(date('Y')+1),
            ),
            'htmlOptions'=>array(
				'style'=>'color:#000000',
				'autocomplete'=>'off',
				'id'=>'checkin',
            ),

            ),
            true),

            ),
			
			
		array(
            'name' => 'checkout',
            'value'=>'date("d F, Y",strtotime($data->checkout))',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'checkout',
            'options'=>array(
				'dateFormat'=>'yy-mm-dd',
				'changeYear'=>'true',
				'changeMonth'=>'true',
				'showAnim' =>'slide',
				'yearRange'=>'2000:'.(date('Y')+1),
            ),
            'htmlOptions'=>array(
				'style'=>'color:#000000',
				'autocomplete'=>'off',
				'id'=>'checkout',
            ),

            ),
            true),

            ),
		
		
		array('name'=>'amount','value'=>'$data->getMyRateTotal()'),
		'noofnight',
		/*
		'btype',
		'full_guest_name',
		'smooking',
		'special_request',
		'nationality',
		'meal_plan',
		'email',
		'phone',
		'adult',
		'child',
		'exp_arrive',
		'extra_charge',
		'discount',
		'book_request',
		'request_email',
		'created',
		*/
            array(
                'htmlOptions' => array('style' => 'width: 75px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '{view}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
						'url'=>'Yii::app()->createUrl("invoice/invoice/myDetail/id/".$data->id)',
                        'options' => array('title' => 'View', 'class' => 'fa fa-eye', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$viewAllowed"
                    ),
                    
                )
            )
        ),
    )); ?>
</div>