<?php
/* @var $this PackageController */
/* @var $model Package */

$this->pageTitle = 'Packages - Update';
$this->breadcrumbs = array(
	'Packages' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>