<?php
/* @var $this PackageController */
/* @var $model Package */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'package-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'title'); ?>
		<?= $form->textField($model, 'title', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'day_condition'); ?>
		<?= $form->textField($model,'day_condition', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'idate_start'); ?>
		<?= $form->textField($model, 'idate_start', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'idate_end'); ?>
		<?= $form->textField($model, 'idate_end', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'date_start'); ?>
        <?php
		 if ($model->date_start){
			 $dt = ($model->date_start);
		 }else{
			 $dt = strtotime("+1 day");
		 }
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'Package[date_start]',
								
                                'id'=>'Modelname_date_start',
                            	'value'=>Yii::app()->dateFormatter->format("M/d/y",strtotime($dt)),
                                'options'=>array(
								
                                'showAnim'=>'fold',
                                ),
								
                                'htmlOptions'=>array(
                                'display'=>'block;',
								'class' => 'form-control',
                                ),
                        ));
		?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'date_end'); ?>
        <?php
		 if ($model->date_end){
			 $dt = ($model->date_end);
		 }else{
			 $dt = strtotime("+1 day");
		 }

        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'Package[date_end]',
								
                                'id'=>'Modelname_date_end',
                            	'value'=>Yii::app()->dateFormatter->format("M/d/y",strtotime($dt)),
                                'options'=>array(
								
                                'showAnim'=>'fold',
                                ),
								
                                'htmlOptions'=>array(
                                'display'=>'block;',
								'class' => 'form-control',
                                ),
                        ));
		?>
		
	</div>

	
	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
