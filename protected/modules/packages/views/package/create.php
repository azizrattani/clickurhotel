<?php
/* @var $this PackageController */
/* @var $model Package */

$this->pageTitle = 'Packages - Create';
$this->breadcrumbs = array(
	'Packages' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>