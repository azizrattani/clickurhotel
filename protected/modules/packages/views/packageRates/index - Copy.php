<?php
/* @var $this PackageRatesController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Update Package Rates';
$this->breadcrumbs = array(
	'Package Rates',
    'Manage'
);


?>

<?php if($createAllowed) { ?>
<p class="text-right" style="float:left;">
    <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Add New
    </a>
</p>
<?php } ?>

<?php if($updateAllowed) { ?>
<p class="text-left" style="float:right;">
    <a href="<?php echo $this->createUrl('update'); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Update
    </a>
</p>
<?php } ?>

<div class="clearfix"></div>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-room-rate-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />
    
    <div class="form-group">
		<?= $form->labelEx($model,'package_id'); ?>
        <?= $form->dropDownList($model, 'package_id', CHtml::listData(Package::model()->findAll(array("condition"=>' deleted=0')), 'id', 'title'), array('empty'=>'--Select a Package--', 'class' => 'form-control mselect', 'required' => 'required'));?>

	</div>
    
    <div class="form-group">
		<?= $form->labelEx($model,'hotel_id'); ?>
        <?= $form->dropDownList($model, 'hotel_id', CHtml::listData(Hotel::model()->findAll(array("condition"=>' deleted=0')), 'id', 'name'), array('empty'=>'--Select a Hotel--', 'class' => 'form-control mselect', 'required' => 'required'));?>

	</div>

	<div class="form-group roomId">
		<?= $form->labelEx($model,'room_id'); ?>
		<select id="PackageRates_room_id" name="PackageRates[room_id]" class="form-control mselect">
        	
        </select>
	</div>
    
    <div class="roomRate hidden">
 
        
    
        
    
    
        
    
        <div class="box-footer">
           
            <?= CHtml::submitButton('Search', array('class' => 'btn btn-primary pull-right')); ?>
        </div>
    
    
    
    </div>
    
    
    

<?php $this->endWidget(); ?>


<?php
if($PackageRates){
?>

<div class="box-body table-responsive no-padding">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'hotel-grid',
        'dataProvider' => $PackageRates,
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
		
        'columns' => array(
		
		array('name'=>'hotel_id','header'=>'Hotel Name','value'=>'$data->room->hotel->name'),
		array('name'=>'room_id','header'=>'Room Name','value'=>'$data->room->title'),
		array('name'=>'price','header'=>'Weekday or Fix Price'),
		
            array(
                'htmlOptions' => array('style' => 'width: 75px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'View', 'class' => 'fa fa-eye', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$viewAllowed"
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Update', 'class' => 'fa fa-pencil-square-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$updateAllowed"
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Delete', 'class' => 'fa fa-trash', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$deleteAllowed"
                    )
                )
            )
        ),
    )); ?>
</div>



<?php
}
?>



<script>

	var hotelDetail = "";

	$("#PackageRates_hotel_id").change(function(e) {
		$val = $(this).val();
		if ($val != ""){
			
			url  = "<?= $this->createUrl('/hotel/hotel/hotelDetail/id/') ?>/"+$val;
			
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						
						hotelDetail = data;
						$("#PackageRates_price").parent("div").addClass("hidden");
						$("#PackageRates_price_weekend").parent("div").addClass("hidden");
						$("#PackageRates_four_days").parent("div").addClass("hidden");
						
						if(hotelDetail.rate_type == 1){
							$("#PackageRates_price").parent("div").removeClass("hidden");
							$("#PackageRates_price").attr("requried","requried");
						}else if(hotelDetail.rate_type == 2){
							$("#PackageRates_price").parent("div").removeClass("hidden");
							$("#PackageRates_price").attr("requried","requried");
							$("#PackageRates_price_weekend").parent("div").removeClass("hidden");
							$("#PackageRates_price_weekend").attr("requried","requried");
						}else if(hotelDetail.rate_type == 3){
							$("#PackageRates_price").parent("div").removeClass("hidden");
							$("#PackageRates_price").attr("requried","requried");
							$("#PackageRates_price_weekend").parent("div").removeClass("hidden");
							$("#PackageRates_price_weekend").attr("requried","requried");
							$("#PackageRates_four_days").parent("div").removeClass("hidden");
							$("#PackageRates_four_days").attr("requried","requried");
						}else{
							$("#PackageRates_price").parent("div").removeClass("hidden");
							$("#PackageRates_price").attr("requried","requried");
						}
					}
					
				}
			});
			
			
			
			
			
			url  = "<?= $this->createUrl('/hotel/hotel/hotelRooms/id/') ?>/"+$val;
			$("#PackageRates_room_id").select2('destroy');
			$("#PackageRates_room_id option").each(function() {
				$(this).remove();
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						$('#PackageRates_room_id').append('<option value="">Select Room</option>');
						$.each(data,function(index,value){
							
							$('#PackageRates_room_id').append('<option value="'+value.id+'">'+value.title+'</option>');
							
						});
						$("#PackageRates_room_id").select2({
							placeholder: "Select Room"
						});
						$(".roomId").removeClass("hidden");	
					}
					
				}
			});
			
		}else{
			$("#PackageRates_room_id").select2('destroy');
			$("#PackageRates_room_id option").each(function() {
				$(this).remove();
			});
			$("#PackageRates_room_id").select2({
							placeholder: "Select Room"
						});
			//$(".roomId").addClass("hidden");
			$(".roomRate").addClass("hidden");	
		}
        
		
		
    });
	
	$("#PackageRates_room_id").change(function(e) {
		$val = $(this).val();
		if ($val != ""){
			
			$(".roomRate").removeClass("hidden");	
        
			url  = "<?= $this->createUrl('/hotel/hotel/hotelRooms/id/') ?>/"+$val;
			
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						
					}
					
				}
			});
			
		}else{
			$(".roomRate").addClass("hidden");	
		}
		
    });

</script>