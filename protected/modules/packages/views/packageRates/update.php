<?php
/* @var $this PackageRatesController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Update Package Rates';
$this->breadcrumbs = array(
	'Package Rates',
    'Manage'
);


?>


<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-room-rate-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />
    
    <div class="form-group">
		<?= $form->labelEx($model,'package_id'); ?>
        <?= $form->dropDownList($model, 'package_id', CHtml::listData(Package::model()->findAll(array("condition"=>' deleted=0')), 'id', 'title'), array('empty'=>'--Select a Package--', 'class' => 'form-control mselect', 'required' => 'required'));?>

	</div>
    
    <div class="form-group">
		<?= $form->labelEx($model,'hotel_id'); ?>
        <?= $form->dropDownList($model, 'hotel_id', CHtml::listData(Hotel::model()->findAll(array("condition"=>' deleted=0')), 'id', 'name'), array('empty'=>'--Select a Hotel--', 'class' => 'form-control mselect', 'required' => 'required'));?>

	</div>

	<div class="form-group roomId">
		<?= $form->labelEx($model,'room_id'); ?>
		 <?= $form->dropDownList($model, 'room_id', CHtml::listData(HotelRooms::model()->findAll(array("condition"=>' hotel_id="'.$model->hotel_id.'" and deleted=0')), 'id', 'title'), array('empty'=>'--Select a Room--', 'class' => 'form-control mselect', 'required' => 'required'));?>
	</div>
    
    <div class="roomRate hidden">
    
    
    	

        
    <div class="form-group">
            <?= $form->labelEx($model,'base_price'); ?>
            <?= $form->textField($model,'base_price',array('class' => 'form-control')); ?>
        </div>
        
        <div class="form-group">
            <?= $form->labelEx($model,'price'); ?>
            <?= $form->textField($model,'price',array('class' => 'form-control')); ?>
        </div>
    
     
    
    
        
    
        <div class="box-footer">
            <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
            <?= CHtml::submitButton($model->isNewRecord ? 'Update' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
        </div>
    
    
    
    </div>
    
    
    

<?php $this->endWidget(); ?>

<script>


    


	var hotelDetail = "";

	$("#PackageRates_hotel_id").change(function(e) {
		$val = $(this).val();
		if ($val != ""){
			
			url  = "<?= $this->createUrl('/hotel/hotel/hotelDetail/id/') ?>/"+$val;
			
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						
						hotelDetail = data;
						$("#PackageRates_price").parent("div").addClass("hidden");
						$("#PackageRates_price_weekend").parent("div").addClass("hidden");
						$("#PackageRates_four_days").parent("div").addClass("hidden");
						
						if(hotelDetail.rate_type == 1){
							$("#PackageRates_price").parent("div").removeClass("hidden");
							$("#PackageRates_price").attr("requried","requried");
						}else if(hotelDetail.rate_type == 2){
							$("#PackageRates_price").parent("div").removeClass("hidden");
							$("#PackageRates_price").attr("requried","requried");
							$("#PackageRates_price_weekend").parent("div").removeClass("hidden");
							$("#PackageRates_price_weekend").attr("requried","requried");
						}else if(hotelDetail.rate_type == 3){
							$("#PackageRates_price").parent("div").removeClass("hidden");
							$("#PackageRates_price").attr("requried","requried");
							$("#PackageRates_price_weekend").parent("div").removeClass("hidden");
							$("#PackageRates_price_weekend").attr("requried","requried");
							$("#PackageRates_four_days").parent("div").removeClass("hidden");
							$("#PackageRates_four_days").attr("requried","requried");
						}else{
							$("#PackageRates_price").parent("div").removeClass("hidden");
							$("#PackageRates_price").attr("requried","requried");
						}
					}
					
				}
			});
			
			
			
			
			
			url  = "<?= $this->createUrl('/hotel/hotel/hotelRooms/id/') ?>/"+$val;
			$("#PackageRates_room_id").select2('destroy');
			$("#PackageRates_room_id option").each(function() {
				$(this).remove();
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						$('#PackageRates_room_id').append('<option value="">Select Room</option>');
						$.each(data,function(index,value){
							
							$('#PackageRates_room_id').append('<option value="'+value.id+'">'+value.title+'</option>');
							
						});
						$("#PackageRates_room_id").select2({
							placeholder: "Select Room"
						});
						$(".roomId").removeClass("hidden");	
					}
					
				}
			});
			
		}else{
			$("#PackageRates_room_id").select2('destroy');
			$("#PackageRates_room_id option").each(function() {
				$(this).remove();
			});
			$("#PackageRates_room_id").select2({
							placeholder: "Select Room"
						});
			//$(".roomId").addClass("hidden");
			$(".roomRate").addClass("hidden");	
		}
        
		
		
    });
	
	$("#PackageRates_room_id").change(function(e) {
		$val = $(this).val();
		if ($val != ""){
			
			$(".roomRate").removeClass("hidden");	
        
			url  = "<?= $this->createUrl('/hotel/hotel/hotelRooms/id/') ?>/"+$val;
			
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						
					}
					
				}
			});
			
		}else{
			$(".roomRate").addClass("hidden");	
		}
		
    });
	
	$("#PackageRates_room_id").trigger("change");

//$(document).ready(function(e) {	
	
//});
</script>