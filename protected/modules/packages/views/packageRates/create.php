<?php
/* @var $this PackageRatesController */
/* @var $model PackageRates */

$this->pageTitle = 'Hotel Room Rates - Create';
$this->breadcrumbs = array(
	'Hotel Room Rates' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>