<?php
/* @var $this PackageRatesController */
/* @var $model PackageRates */

$this->pageTitle = 'Package Rates - View';
$this->breadcrumbs=array(
	'Package Rates' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		array('name'=>'Package Name','value'=>$model->package->title),
		'price',
		'base_price',
		'hotel_id',
		'room_id',
		'no_of_room',
		'room_book',
		array('name'=>'Created By','value'=>$model->users->first_name . " " . $model->users->last_name),
		
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>