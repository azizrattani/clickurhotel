<?php

/**
 * This is the model class for table "package".
 *
 * The followings are the available columns in table 'package':
 * @property integer $id
 * @property string $title
 * @property integer $day_condition
 * @property string $idate_start
 * @property string $idate_end
 * @property string $date_start
 * @property string $date_end
 * @property string $created
 * @property string $modified
 * @property integer $created_by
 * @property string $deleted
 *
 * The followings are the available model relations:
 * @property PackageRates[] $packageRates
 */
class Package extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'package';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, day_condition, idate_start, idate_end, date_start, date_end', 'required'),
			array('day_condition, created_by', 'numerical', 'integerOnly'=>true),
			array('title, idate_start, idate_end', 'length', 'max'=>255),
			array('deleted', 'length', 'max'=>1),
			array('created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, day_condition, idate_start, idate_end, date_start, date_end, created, modified, created_by, deleted', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'packageRates' => array(self::HAS_MANY, 'PackageRates', 'package_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'day_condition' => 'Minimum Days',
			'idate_start' => 'Islamic Date Start',
			'idate_end' => 'Islamic Date End',
			'date_start' => 'Date Start',
			'date_end' => 'Date End',
			'created' => 'Created',
			'modified' => 'Modified',
			'created_by' => 'Created By',
			'deleted' => 'Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('day_condition', $this->day_condition);
		$criteria->compare('idate_start', $this->idate_start, true);
		$criteria->compare('idate_end', $this->idate_end, true);
		$criteria->compare('date_start', $this->date_start, true);
		$criteria->compare('date_end', $this->date_end, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('created_by', Yii::app()->user->profile['id']);
		$criteria->compare('deleted', 0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Package the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
