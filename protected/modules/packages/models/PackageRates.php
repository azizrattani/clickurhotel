<?php

/**
 * This is the model class for table "package_rates".
 *
 * The followings are the available columns in table 'package_rates':
 * @property integer $id
 * @property integer $package_id
 * @property double $price
 * @property double $base_price
 * @property integer $hotel_id
 * @property integer $room_id
 * @property integer $no_of_room
 * @property integer $room_book
 * @property integer $created_by
 * @property string $created
 * @property string $modified
 * @property string $deleted
 *
 * The followings are the available model relations:
 * @property Package $package
 * @property HotelRooms $room
 * @property Hotel $hotel
 */
class PackageRates extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'package_rates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('package_id, price, hotel_id,no_of_room', 'required'),
			array('package_id, hotel_id, room_id, no_of_room, room_book, created_by', 'numerical', 'integerOnly'=>true),
			array('price, base_price', 'numerical'),
			array('deleted', 'length', 'max'=>1),
			array('created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, package_id, price, base_price, hotel_id, room_id, no_of_room, room_book, created_by, created, modified, deleted', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'package' => array(self::BELONGS_TO, 'Package', 'package_id'),
			'room' => array(self::BELONGS_TO, 'HotelRooms', 'room_id'),
			'hotel' => array(self::BELONGS_TO, 'Hotel', 'hotel_id'),
			'users' => array(self::BELONGS_TO, 'Users', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'package_id' => 'Package',
			'price' => 'Base Price',
			'base_price' => 'Cost Price',
			'hotel_id' => 'Hotel',
			'room_id' => 'Room',
			'no_of_room' => 'No Of Room',
			'room_book' => 'Room Book',
			'created_by' => 'Created By',
			'created' => 'Created',
			'modified' => 'Modified',
			'deleted' => 'Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('package_id', $this->package_id);
		$criteria->compare('price', $this->price);
		$criteria->compare('base_price', $this->base_price);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('room_id', $this->room_id);
		$criteria->compare('no_of_room', $this->no_of_room);
		$criteria->compare('room_book', $this->room_book);
		$criteria->compare('created_by',  Yii::app()->user->profile['id']);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('deleted', 0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PackageRates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
			$this->created_by = Yii::app()->user->profile['id'];
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
