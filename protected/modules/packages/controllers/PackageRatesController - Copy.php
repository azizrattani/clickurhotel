<?php

class PackageRatesController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new PackageRates;
		$PackageRates = "";
		
		if(isset($_POST['PackageRates']) || (isset($_SESSION["PackageRates"])))
		{
			if (!isset($_POST['PackageRates'])){
				$_POST['PackageRates'] = $_SESSION["PackageRates"]	;
			}
			
			$model->attributes = $_POST['PackageRates'];
			
			$_SESSION["PackageRates"] = $_POST['PackageRates'];
			$start_date = date("Y-m-d",strtotime($_POST['PackageRates']["start_date"]));
			$end_date =  date("Y-m-d",strtotime($_POST['PackageRates']["end_date"]));
			$roomID = $_POST['PackageRates']['room_id'];
			$package_id = $_POST['PackageRates']['package_id'];
			
			$now = strtotime($start_date);
			$your_date = strtotime($end_date);
			$datediff =  $your_date - $now;
			
			$totalDay = round($datediff / (60 * 60 * 24));
			
			
			$PackageRates = PackageRates::model()->findAll(array("condition"=>'package_id="'.$package_id.'" and room_id="'.$roomID.'" and deleted=0'));
			
			
			$PackageRates = new CArrayDataProvider($PackageRates,array("pagination" => array('pageSize' => (isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:100))));
			
			
			

			
		}
		
		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

		$this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed','PackageRates'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new PackageRates;
		if(isset($_POST['PackageRates']))
		{
			
			$start_date = $_POST['PackageRates']["start_date"];
			$end_date = $_POST['PackageRates']["end_date"];
			
			$now = strtotime($start_date);
			$your_date = strtotime($end_date);
			$datediff =  $your_date - $now;
			
			$totalDay = round($datediff / (60 * 60 * 24));
			
			
			for($i = 0; $i <=$totalDay; $i++){
				
				$model = new PackageRates;
				$model->attributes = $_POST['PackageRates'];
				
				$model->bookdate = date("Y-m-d",strtotime($start_date . "+ ".($i)." day"));
				$model->created_by = Yii::app()->user->profile['id'];
				
				
				$save = $model->save();
			}
			
			
			
		
			
			
			
			

			if ($save)
            {	
                Yii::app()->user->setFlash('success', 'PackageRates saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = new PackageRates;
		if(isset($_POST['PackageRates']))
		{
			
			$start_date = $_POST['PackageRates']["start_date"];
			$end_date = $_POST['PackageRates']["end_date"];
			$roomID = $_POST['PackageRates']['room_id'];
			
			$now = strtotime($start_date);
			$your_date = strtotime($end_date);
			$datediff =  $your_date - $now;
			
			$totalDay = round($datediff / (60 * 60 * 24));
			
			
			for($i = 0; $i <=$totalDay; $i++){
				
				$bookDate = date("Y-m-d",strtotime($start_date . "+ ".($i)." day"));
				
				
				
				Yii::app()->db->createCommand("update hotel_room_rate set deleted=1, modified='".date("Y-m-d H:i:s")."' where  bookdate='".$bookDate."' and deleted=0 and room_id='".$roomID."'")->execute();;
				
				$findRate = PackageRates::model()->find(array("condition"=>"bookdate='".$bookDate."' and deleted=0"));
				
				
				
				
				$model = new PackageRates;
				$model->attributes = $_POST['PackageRates'];
				
				$model->bookdate = $bookDate;
				$model->created_by = Yii::app()->user->profile['id'];
				
				
				$save = $model->save();
			}
			
			
			
		
			
			
			
			

			if ($save)
            {	
                Yii::app()->user->setFlash('success', 'PackageRates saved successfully');
                $this->redirect(array('index'));
            }
		}
		
		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

		$this->render('update', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = PackageRates::model()->findByPk($id);
		
		if (Yii::app()->user->profile['role_id'] == 4){
			$model->id = Yii::app()->user->profile['child_id'] ;
		}
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
