<?php

class PackageRatesController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new PackageRates('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PackageRates']))
		{
			$model->attributes = $_GET['PackageRates'];
		}

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new PackageRates;

		if(isset($_POST['PackageRates']))
		{
			$model->attributes = $_POST['PackageRates'];

			$model->created_by = Yii::app()->user->profile['id'] ;

			if ($model->save())
            {
                Yii::app()->user->setFlash('success', 'PackageRates saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['PackageRates']))
			{
				$model->attributes = $_POST['PackageRates'];
				$model->created_by = Yii::app()->user->profile['id'] ;

				if ($model->save())
	            {
	                Yii::app()->user->setFlash('success', 'PackageRates updated successfully');
	                $this->redirect(array('index'));
	            }
			}

			$this->render('update', compact('model'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = PackageRates::model()->findByPk($id);
		
		if (Yii::app()->user->profile['role_id'] == 4){
			$model->id = Yii::app()->user->profile['child_id'] ;
		}

		$model->created_by = Yii::app()->user->profile['id'] ;
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
