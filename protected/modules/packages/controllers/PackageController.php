<?php

class PackageController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new Package('search');
		$model->unsetAttributes();  // clear any default values

		$model->created_by = Yii::app()->user->profile['child_id'] ;
		
		if(isset($_GET['Package']))
		{
			$model->attributes = $_GET['Package'];
		}

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new Package;

		if(isset($_POST['Package']))
		{
			$model->attributes = $_POST['Package'];
			$model->created_by = Yii::app()->user->profile['id'] ;
			
			$model->date_start =  date("Y-m-d",strtotime($_POST['Package']["date_start"]));
			$model->date_end =  date("Y-m-d",strtotime($_POST['Package']["date_end"]));

			if ($model->save())
            {
                Yii::app()->user->setFlash('success', 'Package saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['Package']))
			{
				$model->attributes = $_POST['Package'];
				$model->date_start =  date("Y-m-d",strtotime($_POST['Package']["date_start"]));
				$model->date_end =  date("Y-m-d",strtotime($_POST['Package']["date_end"]));

				$model->created_by = Yii::app()->user->profile['id'] ;

				if ($model->save())
	            {
	                Yii::app()->user->setFlash('success', 'Package updated successfully');
	                $this->redirect(array('index'));
	            }
			}

			$this->render('update', compact('model'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = Package::model()->findByPk($id);


		$model->created_by = Yii::app()->user->profile['id'] ;
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
