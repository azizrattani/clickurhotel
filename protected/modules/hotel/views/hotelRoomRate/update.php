<?php
/* @var $this HotelRoomRateController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Update Hotel Room Rates';
$this->breadcrumbs = array(
	'Hotel Room Rates',
    'Manage'
);
$hotelSearch = "";
if (Yii::app()->user->profile['role_id'] == 4){
	$hotelSearch = " and id = " . Yii::app()->user->profile['child_id'] ;
}

?>

<?php if($createAllowed) { ?>
<p class="text-right">
    <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Add New
    </a>
</p>
<?php } ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-room-rate-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />
    
    <div class="form-group">
		<?= $form->labelEx($model,'hotel_id'); ?>
        <?= $form->dropDownList($model, 'hotel_id', CHtml::listData(Hotel::model()->findAll(array("condition"=>' deleted=0'.$hotelSearch)), 'id', 'name'), array('empty'=>'--Select a Hotel--', 'class' => 'form-control mselect', 'required' => 'required'));?>

	</div>

	<div class="form-group roomId">
		<?= $form->labelEx($model,'room_id'); ?>
		<select id="HotelRoomRate_room_id" name="HotelRoomRate[room_id]" class="form-control mselect">
        	
        </select>
	</div>
    
    <div class="roomRate hidden">
    
    
    	<div class="form-group">
		<?= $form->labelEx($model,'start_date'); ?>
		<?php
		 if ($model->start_date){
			 $dt = strtotime($model->start_date);
		 }else{
			 $dt = strtotime("+1 day");
		 }
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'HotelRoomRate[start_date]',
								
                                'id'=>'Modelname_start_date',
                            	'value'=>Yii::app()->dateFormatter->format("M/d/y",strtotime($model->start_date)),
                                'options'=>array(
								'minDate'=>date("m/d/Y",strtotime("+1 day")),
                                'showAnim'=>'fold',
                                ),
								
                                'htmlOptions'=>array(
                                'display'=>'block;',
								'class' => 'form-control',
                                ),
                        ));
		?>
	</div>

        <div class="form-group">
            <?= $form->labelEx($model,'end_date'); ?>
             <?php
             if ($model->end_date){
                 $dt = strtotime($model->end_date);
             }else{
                 $dt = strtotime("+2 day");
             }
             
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                    'name'=>'HotelRoomRate[end_date]',								
                                    'id'=>'Modelname_end_date',
                                    'value'=>Yii::app()->dateFormatter->format("M/d/y",$dt),
                                    'options'=>array(
                                    'minDate'=>date("m/d/Y",strtotime("+2 day")),
                                    'showAnim'=>'fold',
                                    'class' => 'form-control',
                                    ),
                                    
                                    'htmlOptions'=>array(
                                    'display'=>'block;',
                                    'class' => 'form-control',
                                    ),
                            ));
            ?>
        </div>
    
        <div class="form-group">
            <?= $form->labelEx($model,'no_of_room'); ?>
            <?= $form->textField($model,'no_of_room',array('class' => 'form-control')); ?>
        </div>
        
        <div class="form-group">
            <?= $form->labelEx($model,'base_price'); ?>
            <?= $form->textField($model,'base_price',array('class' => 'form-control')); ?>
        </div>
    
        <div class="form-group">
            <?= $form->labelEx($model,'price'); ?>
            <?= $form->textField($model,'price',array('class' => 'form-control')); ?>
        </div>
    
        <div class="form-group">
            <?= $form->labelEx($model,'price_weekend'); ?>
            <?= $form->textField($model,'price_weekend',array('class' => 'form-control')); ?>
        </div>
    
        <div class="form-group">
            <?= $form->labelEx($model,'four_days'); ?>
            <?= $form->textField($model,'four_days',array('class' => 'form-control')); ?>
        </div>
    
    
    
        
    
        <div class="box-footer">
           
            <?= CHtml::submitButton($model->isNewRecord ? 'Update' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
        </div>
    
    
    
    </div>
    
    
    

<?php $this->endWidget(); ?>

<script>

	var hotelDetail = "";

	$("#HotelRoomRate_hotel_id").change(function(e) {
		$val = $(this).val();
		if ($val != ""){
			
			url  = "<?= $this->createUrl('/hotel/hotel/hotelDetail/id/') ?>/"+$val;
			
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						
						hotelDetail = data;
						$("#HotelRoomRate_price").parent("div").addClass("hidden");
						$("#HotelRoomRate_price_weekend").parent("div").addClass("hidden");
						$("#HotelRoomRate_four_days").parent("div").addClass("hidden");
						
						if(hotelDetail.rate_type == 1){
							$("#HotelRoomRate_price").parent("div").removeClass("hidden");
							$("#HotelRoomRate_price").attr("requried","requried");
						}else if(hotelDetail.rate_type == 2){
							$("#HotelRoomRate_price").parent("div").removeClass("hidden");
							$("#HotelRoomRate_price").attr("requried","requried");
							$("#HotelRoomRate_price_weekend").parent("div").removeClass("hidden");
							$("#HotelRoomRate_price_weekend").attr("requried","requried");
						}else if(hotelDetail.rate_type == 3){
							$("#HotelRoomRate_price").parent("div").removeClass("hidden");
							$("#HotelRoomRate_price").attr("requried","requried");
							$("#HotelRoomRate_price_weekend").parent("div").removeClass("hidden");
							$("#HotelRoomRate_price_weekend").attr("requried","requried");
							$("#HotelRoomRate_four_days").parent("div").removeClass("hidden");
							$("#HotelRoomRate_four_days").attr("requried","requried");
						}else{
							$("#HotelRoomRate_price").parent("div").removeClass("hidden");
							$("#HotelRoomRate_price").attr("requried","requried");
						}
					}
					
				}
			});
			
			
			
			
			
			url  = "<?= $this->createUrl('/hotel/hotel/hotelRooms/id/') ?>/"+$val;
			$("#HotelRoomRate_room_id").select2('destroy');
			$("#HotelRoomRate_room_id option").each(function() {
				$(this).remove();
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						$('#HotelRoomRate_room_id').append('<option value="">Select Room</option>');
						$.each(data,function(index,value){
							
							$('#HotelRoomRate_room_id').append('<option value="'+value.id+'">'+value.title+'</option>');
							
						});
						$("#HotelRoomRate_room_id").select2({
							placeholder: "Select Room"
						});
						$(".roomId").removeClass("hidden");	
					}
					
				}
			});
			
		}else{
			$("#HotelRoomRate_room_id").select2('destroy');
			$("#HotelRoomRate_room_id option").each(function() {
				$(this).remove();
			});
			$("#HotelRoomRate_room_id").select2({
							placeholder: "Select Room"
						});
			//$(".roomId").addClass("hidden");
			$(".roomRate").addClass("hidden");	
		}
        
		
		
    });
	
	$("#HotelRoomRate_room_id").change(function(e) {
		$val = $(this).val();
		if ($val != ""){
			
			$(".roomRate").removeClass("hidden");	
        
			url  = "<?= $this->createUrl('/hotel/hotel/hotelRooms/id/') ?>/"+$val;
			
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						
					}
					
				}
			});
			
		}else{
			$(".roomRate").addClass("hidden");	
		}
		
    });

</script>