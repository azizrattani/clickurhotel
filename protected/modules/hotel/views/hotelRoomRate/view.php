<?php
/* @var $this HotelRoomRateController */
/* @var $model HotelRoomRate */

$this->pageTitle = 'Hotel Room Rates - View';
$this->breadcrumbs=array(
	'Hotel Room Rates' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'room_id',
		'bookdate',
		'base_price',
		'price',
		'price_weekend',
		'four_days',
		'all_day',
		'no_of_room',
		'deleted',
		'created',
		'modified',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>