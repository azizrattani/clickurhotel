<?php
/* @var $this HotelRoomRateController */
/* @var $model HotelRoomRate */

$this->pageTitle = 'Hotel Room Rates - Create';
$this->breadcrumbs = array(
	'Hotel Room Rates' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>