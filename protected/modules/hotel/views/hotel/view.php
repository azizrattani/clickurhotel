<?php
/* @var $this HotelController */
/* @var $model Hotel */

$this->pageTitle = 'Hotels - View';
$this->breadcrumbs=array(
	'Hotels' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'name',
		'username',
		
		'short_desc',
		'description',
		'hotel_star',
		'address',
		'city',
		'state',
		'country',
		'zipcode',
		'phone1',
		'phone2',
		'phone3',
		'email',
		'fax',
		'website',
		'hotel_policies',
		'pet',
		'liquor',
		'smoking',
		'child_age_from',
		'child_age_to',
		'checkin_time',
		'checkout_time',
		'total_room',
		'total_floor',
		'build_year',
		'status',
		'active',
		'deleted',
		'created',
		'modified',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>