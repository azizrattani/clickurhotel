<?php
/* @var $this HotelController */
/* @var $model Hotel */

$this->pageTitle = 'Hotels - Update';
$this->breadcrumbs = array(
	'Hotels' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model','cancellation_policy')); ?>