<?php
/* @var $this HotelController */
/* @var $model Hotel */
/* @var $form CActiveForm */
?>
<style>

ul.tabs {
	margin: 0;
	padding: 0;
	float: left;
	list-style: none;
	height: 32px;
	border-bottom: 1px solid #333;
	width: 100%;
}

ul.tabs li {
	float: left;
	margin: 0;
	cursor: pointer;
	padding: 0px 21px;
	height: 31px;
	line-height: 31px;
	border-top: 1px solid #333;
	border-left: 1px solid #333;
	border-bottom: 1px solid #333;
	background-color: #666;
	color: #ccc;
	overflow: hidden;
	position: relative;
}

.tab_last { border-right: 1px solid #333; }

ul.tabs li:hover {
	background-color: #ccc;
	color: #333;
}

ul.tabs li.active {
	background-color: #fff;
	color: #333;
	border-bottom: 1px solid #fff;
	display: block;
}

.tab_container {
	border: 1px solid #333;
	border-top: none;
	clear: both;
	float: left;
	width: 100%;
	background: #fff;
	overflow: auto;
}

.tab_content {
	padding: 20px;
	display: none;
}

.tab_drawer_heading { display: none; }

.rate .select2{
	width:100% !important;	
}

@media screen and (max-width: 480px) {
	.tabs {
		display: none;
	}
	.tab_drawer_heading {
		background-color: #ccc;
		color: #fff;
		border-top: 1px solid #333;
		margin: 0;
		padding: 5px 20px;
		display: block;
		cursor: pointer;
		-webkit-touch-callout: none;
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}
	.d_active {
		background-color: #666;
		color: #fff;
	}
}

</style>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>


    
    
    <ul class="tabs">
        <li class="active" rel="details">Details</li>
        <li rel="facilites">Facilites</li>
        <li rel="policy">Policy</li>
        <li rel="images">Images</li>
        <li rel="contact">Contact</li>
    </ul>
    <div class="tab_container">
        <h3 class="d_active tab_drawer_heading" rel="details">Details</h3>
        <div id="details" class="tab_content">
            
            <div class="row" >
            
            	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 nopadding">
                	
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        
                        <div class="form-group">
                            <?= $form->labelEx($model,'name'); ?>
                            <?= $form->textField($model, 'name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150,"tabindex"=>"1")); ?>
                        </div>	
                        
                    </div>
                    
                    
                    
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        
                         <div class="form-group">
                            <?= $form->labelEx($model,'hotel_star'); ?>
                            <?= $form->dropDownList($model, 'hotel_star', array('1' => '1 Star', '2' => '2 Star', '3' => '3 Star', '4' => '4 Star', '5' => '5 Star'), array('class' => 'form-control',"tabindex"=>"2")); ?>
                        </div>
                        
                    </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    
                        <div class="form-group">
                            <?= $form->labelEx($model,'city'); ?>
                            <?= $form->dropDownList($model, 'city', array('Makkah' => 'Makkah', 'Madina' => 'Madina'), array('class' => 'form-control',"tabindex"=>"4")); ?>
                        </div>
                    
                    </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        
                        <div class="form-group">
                            <?= $form->labelEx($model,'address'); ?>
                            <?= $form->textField($model, 'address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>"5")); ?>
                        </div>
                    
                    </div>
                    
                
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding">
                
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                        <div class="form-group">
                            <?= $form->labelEx($model,'short_desc'); ?>
                            <?= $form->textArea($model,'short_desc',array('class' => 'form-control','rows'=>5, 'cols'=>50,"tabindex"=>"3")); ?>
                        </div>
                        
                    </div>
                
                
                </div>
            	
                
                
            
            
            
            </div>
            
            <div class="row" >
            	
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	<div class="form-group">
						<?= $form->labelEx($model,'zipcode'); ?>
                        <?= $form->textField($model, 'zipcode', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100,"tabindex"=>6)); ?>
                    </div>                
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	<div class="form-group">
						<?= $form->labelEx($model,'langitude'); ?>
                        <?= $form->textField($model, 'langitude', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100,"tabindex"=>7)); ?>
                    </div>                
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	<div class="form-group">
						<?= $form->labelEx($model,'latitude'); ?>
                        <?= $form->textField($model, 'latitude', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100,"tabindex"=>8)); ?>
                    </div>                
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	<div class="form-group">
						<?= $form->labelEx($model,'phone1'); ?>
                        <?= $form->textField($model, 'phone1', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100,"tabindex"=>9)); ?>
                    </div>          
                </div>
                
                
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	<div class="form-group">
						<?= $form->labelEx($model,'phone2'); ?>
                        <?= $form->textField($model, 'phone2', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100,"tabindex"=>10)); ?>
                    </div>          
                </div>
                
                
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	<div class="form-group">
						<?= $form->labelEx($model,'phone3'); ?>
                        <?= $form->textField($model, 'phone3', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100,"tabindex"=>11)); ?>
                    </div>          
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	<div class="form-group">
						<?= $form->labelEx($model,'email'); ?>
                        <?= $form->textField($model, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	
                    <div class="form-group">
						<?= $form->labelEx($model,'website'); ?>
                        <?= $form->textField($model, 'website', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>12)); ?>
                    </div>
                    
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                	
                    <div class="form-group">
						<?= $form->labelEx($model,'username'); ?>
                        <?= $form->textField($model, 'username', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>13)); ?>
                    </div>
                    
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                
                	<div class="form-group">
                        <?= $form->labelEx($model,'password'); ?> <?php if (!$model->isNewRecord){?><small>( Leave blank if not want to change password)</small><?php }?>
                        <?= $form->passwordField($model, 'password', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>14)); ?>
                    </div>

                
                </div>
                
            </div>
            
            
            
        </div>
        <!-- #details -->
        <h3 class="tab_drawer_heading" rel="facilites">Facilites</h3>
        <div id="facilites" class="tab_content">
  
            <?php
            $facilites = Facilities::model()->findAll(array("condition"=>" deleted=0"));
			
			foreach($facilites as $k => $v){
				$select = " ";
				
				if (isset($_POST["Hotel"]["Facilities"])){
					
					foreach($_POST["Hotel"]["Facilities"] as $fac){
						
						if ($fac == $v["id"]){
							$select = " checked";
							break;	
						}
						
							
					}
					
				}elseif(!$model->isNewRecord){
					
					$facilities_hotel = HotelFacilities::model()->FindByAttributes(array(
						'hotel_id'=>$model->id,'facilities_id'=>$v["id"],
					));

					
					
					if (is_array($facilities_hotel) && count($facilities_hotel)>0){
						$select = " checked";	
					}
						
				}
					
			
			?>
            	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                	<input type="checkbox" value="<?=$v["id"]?>" name="Hotel[Facilities][]"  <?= $select?> /> <?=$v["title"]?>
                </div>
            
            
            <?php
			}
			?>
            
            <div class="clearfix"></div>
            
        </div>
        <!-- #Facilites -->
        <h3 class="tab_drawer_heading" rel="policy">Policy</h3>
        <div id="policy" class="tab_content">
            <h3>Genral Policies</h3>
            <div class="row" >
            	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                	<div class="form-group">
						<?= $form->labelEx($model,'pet'); ?>
                        <?= $form->dropDownList($model, 'pet', array('1' => 'Allowed', '0' => 'Not Allowed'), array('class' => 'form-control',"tabindex"=>1)); ?>
                    </div>
                
                   
                
                
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                
                	 <div class="form-group">
                        <?= $form->labelEx($model,'liquor'); ?>
                        <?= $form->dropDownList($model, 'liquor', array('1' => 'Allowed', '0' => 'Not Allowed'), array('class' => 'form-control',"tabindex"=>2)); ?>
                    </div>
                
                
                </div>
                
                
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                
                    <div class="form-group">
                        <?= $form->labelEx($model,'smoking'); ?>
                        <?= $form->dropDownList($model, 'smoking', array('1' => 'Allowed', '0' => 'Not Allowed'), array('class' => 'form-control',"tabindex"=>3)); ?>
                    </div>
                    
                
                </div>
                
            </div>
            
            
            <div class="row" >
            	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                	
                    <div class="form-group">
						<?= $form->labelEx($model,'child_age_from'); ?>
                        <?= $form->textField($model,'child_age_from', array('class' => 'form-control',"tabindex"=>3)); ?>
                    </div>
                
                    <div class="form-group">
                        <?= $form->labelEx($model,'child_age_to'); ?>
                        <?= $form->textField($model,'child_age_to', array('class' => 'form-control',"tabindex"=>4)); ?>
                    </div>
                    
                
                    
                
                
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                	
                    <div class="form-group">
						<?= $form->labelEx($model,'total_room'); ?>
                        <?= $form->textField($model,'total_room', array('class' => 'form-control', 'size' => 4, 'maxlength' => 3,"tabindex"=>5)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $form->labelEx($model,'total_floor'); ?>
                        <?= $form->textField($model,'total_floor', array('class' => 'form-control', 'size' => 3, 'maxlength' => 2,"tabindex"=>6)); ?>
                    </div>
                
                    
                    
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                	
                    <div class="form-group">
                        <?= $form->labelEx($model,'checkin_time'); ?>
                        <?= $form->textField($model,'checkin_time', array('class' => 'form-control', 'type'=>'time',"tabindex"=>7)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $form->labelEx($model,'checkout_time'); ?>
                        <?= $form->textField($model,'checkout_time', array('class' => 'form-control', 'type'=>'time',"tabindex"=>8)); ?>
                    </div>
                    
                   
                    
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                	
                     <div class="form-group">
                        <?= $form->labelEx($model,'build_year'); ?>
                        <?= $form->textField($model,'build_year', array('class' => 'form-control', 'size' => 4,"tabindex"=>9)); ?>
                    </div>
                    
                    <div class="form-group">
						<?= $form->labelEx($model,'status'); ?>
                        <?= $form->dropDownList($model, 'status', array('1' => 'Active', '0' => 'Inactive'), array('class' => 'form-control',"tabindex"=>10)); ?>
                    </div>
                    
                </div>
                
            </div>
            
            <h3>Rate Policy <span class="small"></span></h3>
            
            
            
            <div class="row rate" >
            
            	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                
                	<div class="form-group">
                        <?= $form->labelEx($model,'market_type'); ?><br />
                        <?= $form->dropDownList($model, 'market_type', CHtml::listData(MarketType::model()->findAll(array("condition"=>' deleted=0')), 'id', 'title'), array('class' => 'form-control mselect', 'multiple'=>"multiple","tabindex"=>12));?>
                    </div>
                    
                	
                </div>    
                
            	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                		
                	
                        <?= $form->labelEx($model,'meal_type'); ?><br />
                        
                        <?= $form->dropDownList($model, 'meal_option', array(""=>"Select Meal Type","1"=>"Room Only", "2"=>"Other"), array('class' => 'form-control mselect','id'=>"mealOption","tabindex"=>13));?>
                        
                     
                        
                        <div class="mealOption hidden">
                        
                        	<?= $form->dropDownList($model, 'meal_type', CHtml::listData(Mealstype::model()->findAll(array("condition"=>' deleted=0')), 'id', 'title'), array('class' => 'form-control mselect',  "multiple"=>"multiple","tabindex"=>14));?>
                        </div>
                        
                    
                   
                	
                </div>      
                
                
                
                
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                
                	<div class="form-group">
                        <?= $form->labelEx($model,'rate_type'); ?><br />
                        <?= $form->dropDownList($model, 'rate_type', array("1"=>"All Day","2"=>"Weekdays & Weekend only","3"=>"Weekday, Weekends & 4 or 4+ days"), array('class' => 'form-control  mselect', 'required' => 'required',"tabindex"=>15));?>
                    </div>
                    
                	
                </div>   
                
                
                 
                
                
                  
            </div>
            
            
            
            <div class="row mealRate hidden" >
         
            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                	<h4>Meal Rates  <span class="small">Will works as base rate if no rate applied</span></h4><br />
                </div>
            
            	
                
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 breakfast">
                 
                  
                 	
                    <div class="form-group">
                        <?= $form->labelEx($model,'breakfast_rate'); ?>
                        <?= $form->textField($model, 'breakfast_rate', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>16)); ?>
                    </div>                    
                    
                    
                    
                 </div>
                 
                 
                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 lunch">
                 
                  
                 	
                    <div class="form-group">
                        <?= $form->labelEx($model,'lunch_rate'); ?>
                        <?= $form->textField($model, 'lunch_rate', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>17)); ?>
                    </div>                    
                    
                    
                    
                 </div>
                 
                 
                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 dinner">
                 
                  
                 	
                    <div class="form-group">
                       	<?= $form->labelEx($model,'dinner_rate'); ?>
                        <?= $form->textField($model, 'dinner_rate', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>18)); ?>
                    </div>                    
                    
                    
                    
                 </div>
            
            
            </div>
            
            
             <h3>Cancellation Policy <span class="small">(Number of days)</span></h3>
            
            
            
            <div class="row" >
            	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                
                	<div class="form-group">
                        <?= $form->labelEx($model,'one_night_start'); ?>
                        <?= $form->textField($model, 'one_night_start', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>20)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $form->labelEx($model,'one_night_end'); ?>
                        <?= $form->textField($model, 'one_night_end', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>21)); ?>
                    </div>
                	
                </div>      
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                
                	<div class="form-group">
                        <?= $form->labelEx($model,'cancel_50_start'); ?>
                        <?= $form->textField($model, 'cancel_50_start', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>22)); ?>
                    </div>
                    
                    
                    <div class="form-group">
                        <?= $form->labelEx($model,'cancel_50_end'); ?>
                        <?= $form->textField($model, 'cancel_50_end', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>23)); ?>
                    </div>
                	
                </div>      
                
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                
                	<div class="form-group">
                        <?= $form->labelEx($model,'cancel_100_start'); ?>
                        <?= $form->textField($model, 'cancel_100_start', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>24)); ?>
                    </div>
                    
                    
                    <div class="form-group">
                        <?= $form->labelEx($model,'cancel_100_end'); ?>
                        <?= $form->textField($model, 'cancel_100_end', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>25)); ?>
                    </div>
                	
                </div>      
                
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                
                	<div class="form-group">
                        <?= $form->labelEx($model,'free_cancel_start'); ?>
                        <?= $form->textField($model, 'free_cancel_start', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>26)); ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $form->labelEx($model,'free_cancel_end'); ?>
                        <?= $form->textField($model, 'free_cancel_end', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255,"tabindex"=>27)); ?>
                    </div>
                	
                </div>                
            </div>
            
            
        </div>
        <!-- #Policy -->
        <h3 class="tab_drawer_heading" rel="images">Images</h3>
        <div id="images" class="tab_content">
            <h2>Images content</h2>
            
        </div>
        <!-- #Images --> 
        <h3 class="tab_drawer_heading" rel="contact">Contact</h3>
        <div id="contact" class="tab_content">
        
            <div class="row contact">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <br />
                    <h2 class="page-header">Hotel Contact:</h2>
                    <a href="javascript:;" id="addCButton"> + </a>
                </div>
            </div>
            
            <div class="row contact">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 widget-box hotel_contact">
                	
                    
                    <?php
					
					
					if (isset($_POST["HotelContact"])){
					
						$HotelReservation = $_POST['HotelContact']["name"];
						$hotel_contact  = array();
						foreach($HotelReservation as $key => $value){
							
							$hotel_contact[$key]["person_name"] = $value ;
							$hotel_contact[$key]["email"] = $_POST['HotelContact']["email"][$key];
							$hotel_contact[$key]["phone"] = $_POST['HotelContact']["phone"][$key];;
							$hotel_contact[$key]["mobile"] = $_POST['HotelContact']["mobile"][$key];
							$hotel_contact[$key]["fax"] = $_POST['HotelContact']["fax"][$key];
								
							
						}
						
						
					}else{
						
						
						
						if(!$model->isNewRecord){
							$hotel_contact = HotelContact::model()->findAll(array("condition"=>"hotel_id='".$model->id."' and contact_type='contact' and deleted=0"));
						}
						
					}
					
					
				
					
					if (isset($hotel_contact) && $hotel_contact){
                    	
						$c = 0;
						
						foreach($hotel_contact as $key=>$data){
							
							$c += 1;
					
							if ($c == 1){
								$cc = "cmultibox";	
							}else{
								$cc = "";
							}	
					?>
                    
                    		<div class="row widget-body  <?=$cc?>">
                                <div class="widget-main">
                                
                                
                                  <?php
					if ($c > 1){	 
					?>
						<a class="removeCButton"> X </a>
					<?php
					}
					?>
                            
                                            
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <label  for="form-field-1" class="requried">Full Name  <span class="required">*</span></label>
                                        <input class="form-control" type="text" id="hotel_contact_name"  name="HotelContact[name][]" placeholder="Name" autocomplete="off" value="<?=$data["person_name"]?>" required="required"  />
                                    </div>
                                    
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Email Adddress  <span class="required">*</span></label>
                                        <input class="form-control" type="text" id="hotel_contact_email"  name="HotelContact[email][]" placeholder="Email" autocomplete="off" value="<?=$data["email"]?>"   required="required" />
                                    </div>
                                    
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Phone Number <span class="required">*</span></label>
                                        <input class="form-control" type="text" id="hotel_contact_phone"  name="HotelContact[phone][]" placeholder="Phone" autocomplete="off" value="<?=$data["phone"]?>"   required="required" />
                                    </div>
                                    
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Mobile Number  <span class="required">*</span></label>
                                        <input class="form-control" type="text" id="hotel_contact_mobile"  name="HotelContact[mobile][]" placeholder="Mobile" autocomplete="off" value="<?=$data["mobile"]?>"   required="required" />
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Fax Number<span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_fax"  name="HotelContact[fax][]" placeholder="Fax" autocomplete="off" value="<?=$data["fax"]?>"  />
                                    </div>
                                    
                                    
                                    
                                    
                                    
                               </div>
                               
                            </div>
                    
                    
                    
                    <?php
					
						}
                    
					}else{
					?>
                    
                        <div class="row widget-body cmultibox">
                            <div class="widget-main">
                            
                        
                                        
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label  for="form-field-1" class="requried">Full Name  <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="hotel_contact_name"  name="HotelContact[name][]" placeholder="Name" autocomplete="off" required="required"  />
                                </div>
                                
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Email Adddress <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="hotel_contact_email"  name="HotelContact[email][]" placeholder="Email" autocomplete="off"  required="required"  />
                                </div>
                                
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Phone Number <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="hotel_contact_phone"  name="HotelContact[phone][]" placeholder="Phone" autocomplete="off"  />
                                </div>
                                
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Mobile Number <span class="required">*</span></label>
                                    <input class="form-control" type="text" id="hotel_contact_mobile"  name="HotelContact[mobile][]" placeholder="Mobile" autocomplete="off"  required="required" />
                                </div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Fax Number<span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_fax"  name="HotelContact[fax][]" placeholder="Fax" autocomplete="off"  />
                                </div>
                                
                                
                                
                                
                                
                           </div>
                           
                        </div>
                
                	<?php
					}
					?>
                </div>
     		</div>
            
            
            <div class="row contact">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <br />
                    <h2 class="page-header">Reservation Contact:</h2>
                    <a href="javascript:;" id="addRButton"> + </a>
                </div>
            </div>
            
            <div class="row contact">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 widget-box hotel_reservation">
                	
                    
                    
                    <?php
					
					
					if (isset($_POST["HotelReservation"])){
					
						$HotelReservation = $_POST['HotelReservation']["name"];
						$hotel_contact  = array();
						foreach($HotelReservation as $key => $value){
							
							$hotel_contact[$key]["person_name"] = $value ;
							$hotel_contact[$key]["email"] = $_POST['HotelReservation']["email"][$key];
							$hotel_contact[$key]["phone"] = $_POST['HotelReservation']["phone"][$key];;
							$hotel_contact[$key]["mobile"] = $_POST['HotelReservation']["mobile"][$key];
							$hotel_contact[$key]["fax"] = $_POST['HotelReservation']["fax"][$key];
								
							
						}
						
						
					}else{
						
						
						
						if(!$model->isNewRecord){
							$hotel_contact = HotelContact::model()->findAll(array("condition"=>"hotel_id='".$model->id."' and contact_type='reservation' and deleted=0"));
						}
						
					}
					
					
					if (isset($hotel_contact) && $hotel_contact){
                    	
						$c = 0;
						
						foreach($hotel_contact as $key=>$data){
							
							$c += 1;
					
							if ($c == 1){
								$cc = "rmultibox";	
							}else{
								$cc = "";
							}	
					?>
                    
                    		<div class="row widget-body  <?=$cc?>">
                                <div class="widget-main">
                                
                                
                                  <?php
					if ($c > 1){	 
					?>
						<a class="removeRButton"> X </a>
					<?php
					}
					?>
                            
                                            
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <label  for="form-field-1" class="requried">Full Name  <span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_name"  name="HotelReservation[name][]" placeholder="Name" autocomplete="off" value="<?=$data["person_name"]?>"  />
                                    </div>
                                    
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Email Adddress</label>
                                        <input class="form-control" type="text" id="hotel_contact_email"  name="HotelReservation[email][]" placeholder="Email" autocomplete="off" value="<?=$data["email"]?>"  />
                                    </div>
                                    
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Phone Number<span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_phone"  name="HotelReservation[phone][]" placeholder="Phone" autocomplete="off" value="<?=$data["phone"]?>"  />
                                    </div>
                                    
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Mobile Number<span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_mobile"  name="HotelReservation[mobile][]" placeholder="Mobile" autocomplete="off" value="<?=$data["mobile"]?>"  />
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Fax Number<span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_fax"  name="HotelReservation[fax][]" placeholder="Fax" autocomplete="off" value="<?=$data["fax"]?>"  />
                                    </div>
                                    
                                    
                                    
                                    
                                    
                               </div>
                               
                            </div>
                    
                    
                    
                    <?php
					
						}
                    
					}else{
					?>
                    
                        <div class="row widget-body rmultibox">
                            <div class="widget-main">
                            
                        
                                        
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label  for="form-field-1" class="requried">Full Name  <span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_name"  name="HotelReservation[name][]" placeholder="Name" autocomplete="off"  />
                                </div>
                                
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Email Adddress</label>
                                    <input class="form-control" type="text" id="hotel_contact_email"  name="HotelReservation[email][]" placeholder="Email" autocomplete="off"  />
                                </div>
                                
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Phone Number<span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_phone"  name="HotelReservation[phone][]" placeholder="Phone" autocomplete="off"  />
                                </div>
                                
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Mobile Number<span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_mobile"  name="HotelReservation[mobile][]" placeholder="Mobile" autocomplete="off"  />
                                </div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Fax Number<span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_fax"  name="HotelReservation[fax][]" placeholder="Fax" autocomplete="off"  />
                                </div>
                                
                                
                                
                                
                                
                           </div>
                           
                        </div>
                
                	<?php
					}
					?>
                    
                    
                    
                    
                
                
                </div>
     		</div>
            
            
            <div class="row contact">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <br />
                    <h2 class="page-header">Account Contact:</h2>
                    <a href="javascript:;" id="addAButton"> + </a>
                </div>
            </div>
            
            <div class="row contact">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 widget-box hotel_account">
                
                	<?php
					
					
					if (isset($_POST["HotelAccount"])){
					
						$HotelAccount = $_POST['HotelAccount']["name"];
						$hotel_contact  = array();
						foreach($HotelAccount as $key => $value){
							
							$hotel_contact[$key]["person_name"] = $value ;
							$hotel_contact[$key]["email"] = $_POST['HotelAccount']["email"][$key];
							$hotel_contact[$key]["phone"] = $_POST['HotelAccount']["phone"][$key];;
							$hotel_contact[$key]["mobile"] = $_POST['HotelAccount']["mobile"][$key];
							$hotel_contact[$key]["fax"] = $_POST['HotelAccount']["fax"][$key];
								
							
						}
						
						
					}else{
						
						
						
						if(!$model->isNewRecord){
							$hotel_contact = HotelContact::model()->findAll(array("condition"=>"hotel_id='".$model->id."' and contact_type='account' and deleted=0"));
						}
						
					}
					
					
					if (isset($hotel_contact) && $hotel_contact){
                    	
						$c = 0;
						
						foreach($hotel_contact as $key=>$data){
							
							$c += 1;
					
							if ($c == 1){
								$cc = "amultibox";	
							}else{
								$cc = "";
							}	
					?>
                    
                    		<div class="row widget-body  <?=$cc?>">
                                <div class="widget-main">
                                
                                
                                  <?php
					if ($c > 1){	 
					?>
						<a class="removeAButton"> X </a>
					<?php
					}
					?>
                            
                                            
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <label  for="form-field-1" class="requried">Full Name  <span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_name"  name="HotelAccount[name][]" placeholder="Name" autocomplete="off" value="<?=$data["person_name"]?>"   />
                                    </div>
                                    
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Email Adddress</label>
                                        <input class="form-control" type="text" id="hotel_contact_email"  name="HotelAccount[email][]" placeholder="Email" autocomplete="off" value="<?=$data["email"]?>"  />
                                    </div>
                                    
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Phone Number<span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_phone"  name="HotelAccount[phone][]" placeholder="Phone" autocomplete="off" value="<?=$data["phone"]?>"   />
                                    </div>
                                    
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Mobile Number<span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_mobile"  name="HotelAccount[mobile][]" placeholder="Mobile" autocomplete="off" value="<?=$data["mobile"]?>"  />
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                        <label  for="form-field-1" class="requried">Fax Number<span class="required"></span></label>
                                        <input class="form-control" type="text" id="hotel_contact_fax"  name="HotelAccount[fax][]" placeholder="Fax" autocomplete="off" value="<?=$data["fax"]?>"  />
                                    </div>
                                    
                                    
                                    
                                    
                                    
                               </div>
                               
                            </div>
                    
                    
                    
                    <?php
					
						}
                    
					}else{
					?>
                    
                        <div class="row widget-body amultibox">
                            <div class="widget-main">
                            
                        
                                        
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label  for="form-field-1" class="requried">Full Name  <span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_name"  name="HotelAccount[name][]" placeholder="Name" autocomplete="off"  />
                                </div>
                                
                                
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Email Adddress</label>
                                    <input class="form-control" type="text" id="hotel_contact_email"  name="HotelAccount[email][]" placeholder="Email" autocomplete="off"  />
                                </div>
                                
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Phone Number<span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_phone"  name="HotelAccount[phone][]" placeholder="Phone" autocomplete="off"  />
                                </div>
                                
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Mobile Number<span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_mobile"  name="HotelAccount[mobile][]" placeholder="Mobile" autocomplete="off"  />
                                </div>
                                
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
                                    <label  for="form-field-1" class="requried">Fax Number<span class="required"></span></label>
                                    <input class="form-control" type="text" id="hotel_contact_fax"  name="HotelAccount[fax][]" placeholder="Fax" autocomplete="off"  />
                                </div>
                                
                                
                                
                                
                                
                           </div>
                           
                        </div>
                
                	<?php
					}
					?>
                    
                    
                    
                    
                
                
                </div>
     		</div>
           
        </div>
        <!-- #Contact --> 
    </div>

    
    
    <div class="clearfix"></div>
    

	
	

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

<script>

// tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
		
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();		
		
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
	$(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
	  
	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tabs li").removeClass("active");
	  $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.tabs li').last().addClass("tab_last");
	
	
	
	$("#addCButton").on("click",function(e) {
			
		
		$tmc = $( ".widget-body.cmultibox" ).clone();
		
		console.log($tmc);
		
		
		
		$tmc.removeClass("cmultibox");
		
		
		//$tmc = $( ".widget-body.multibox" ).clone();
		
		$html = $(".widget-main", $tmc).html();
		
		$html = '<a class="removeCButton"> X </a>' + $html;
		
		$(".widget-main", $tmc).html($html);
		
		$tmc.appendTo( ".widget-box.hotel_contact" );
		
		//$( ".widget-body.multibox" ).clone().appendTo( ".widget-box" );
		
		$(".removeCButton").click(function(e) {				
			$(this).parent(".widget-main").parent(".widget-body").remove("div");
		});
		
		
		
		
	});
	
	
	$("#addRButton").click(function(e) {
			
		
		$tmc = $( ".widget-body.rmultibox" ).clone();
		
		
		
		$tmc.removeClass("rmultibox");
		
		
		//$tmc = $( ".widget-body.multibox" ).clone();
		
		$html = $(".widget-main", $tmc).html();
		
		$html = '<a class="removeRButton"> X </a>' + $html;
		
		$(".widget-main", $tmc).html($html);
		
		$tmc.appendTo( ".widget-box.hotel_reservation" );
		
		//$( ".widget-body.multibox" ).clone().appendTo( ".widget-box" );
		
		$(".removeRButton").click(function(e) {				
			$(this).parent(".widget-main").parent(".widget-body").remove("div");
		});
		
		
		
		
	});
	
	
	$("#addAButton").click(function(e) {
			
		
		$tmc = $( ".widget-body.amultibox" ).clone();
		
		
		
		$tmc.removeClass("cmultibox");
		
		
		//$tmc = $( ".widget-body.multibox" ).clone();
		
		$html = $(".widget-main", $tmc).html();
		
		$html = '<a class="removeAButton"> X </a>' + $html;
		
		$(".widget-main", $tmc).html($html);
		
		$tmc.appendTo( ".widget-box.hotel_account" );
		
		//$( ".widget-body.multibox" ).clone().appendTo( ".widget-box" );
		
		$(".removeAButton").click(function(e) {				
			$(this).parent(".widget-main").parent(".widget-body").remove("div");
		});
		
		
		
		
	});
	
	
	$("#mealOption").change(function(e) {
        
		
		if ($(this).val() == "2"){
			$(".mealOption").removeClass("hidden");	
			$(".mealRate").removeClass("hidden");	
		}else{
			$(".mealOption").addClass("hidden");
			$(".mealRate").addClass("hidden");		
		}
		
    });
	
	$("#Hotel_meal_type").change(function(e) {
		
		$(".breakfast").addClass("hidden");	
		$(".lunch").addClass("hidden");	
		$(".dinner").addClass("hidden");
		$(".mealRate").removeClass("hidden");	
		
		$found = false;	
		
		
			
		$.each($("#Hotel_meal_type option:selected"), function(){            
	   
		   if ($(this).val() == "2"){
			   $(".breakfast").removeClass("hidden");	
			   $found =true;
		   }else if ($(this).val() == "3"){
			   $(".lunch").removeClass("hidden");	
			   $found =true;
		   }else if ($(this).val() == "4"){
			   $(".dinner").removeClass("hidden");	
			   $found =true;
		   }
		   
		   
		});
			
		
		
		if (!$found)
				$(".mealRate").addClass("hidden");	
        
		
			
		
    });
	
	$("#Hotel_meal_type").trigger("change");
	$("#mealOption").trigger("change");
	
	$("#Hotel_checkin_time").attr("type","time");
	$("#Hotel_checkout_time").attr("type","time");
	
	
	if ($("#mealOption").val() != "2"){
		$(".mealRate").addClass("hidden");	
	}

</script>
