<?php
/* @var $this BookingController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Hotel Priority';
$this->breadcrumbs = array(
	'Hotel',
    'Priority'
);
?>

<style>

/* DivTable.com */
.divTable{
	display: table;
	width: 100%;
}
.divTableRow {
	display: table-row;
}
.divTableHeading {
	background-color: #EEE;
	display: table-header-group;
}
.divTableCell, .divTableHead {
	border: 1px solid #999999;
	display: table-cell;
	padding: 3px 10px;
}
.divTableHeading {
	background-color: #EEE;
	display: table-header-group;
	font-weight: bold;
}
.divTableFoot {
	background-color: #EEE;
	display: table-footer-group;
	font-weight: bold;
}
.divTableBody {
	display: table-row-group;
}

.border{
	border: 1px solid #999999;
	padding: 3px 10px;
}

</style>


<div class="box-body table-responsive no-padding">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-room-rate-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />
    
    <div class="form-group col-lg-4">
        <label for="Hotel_city" class="required">City <span class="required">*</span></label>
        <select class="form-control" name="Hotel[city]" id="city">
    		<option value="Makkah" <?php if (isset($_POST["Hotel"]["city"]) && $_POST["Hotel"]["city"] == "Makkah"){ echo " selected";}?>>Makkah</option>
    		<option value="Madina" <?php if (isset($_POST["Hotel"]["city"]) && $_POST["Hotel"]["city"] == "Madina"){ echo " selected";}?>>Madina</option>
    	</select>
    </div>
    
    
    
    <div class="box-footer  col-lg-12">
            <input class="btn btn-primary pull-right" type="submit" name="yt0" value="Show Hotel">
    </div>
    
    <div class="clearfix"></div>
    
    
    
   
    
        <?php
        if($model){
        ?>
         <div class="box-footer  col-lg-12">
            <ul id="sortable">
                
                <?php
                    foreach($model as $hotel){
                ?>
                    <li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?=ucwords(strtolower($hotel["name"]))?>
                    <input type="hidden" name="Hotel[hotel_id][]" value="<?=$hotel["id"]?>" />
                    </li>
              
                <?php
                    }
                ?>
              
            </ul>
        </div>
        
        
        <div class="box-footer  col-lg-12">
                <input class="btn btn-primary pull-right" type="submit" name="Hotel[save]" value="Save">
        </div>
       
        
        <?php
        
        }
        ?>
     
    
    
    
    
    
    
    <?php $this->endWidget(); ?>
    
    
    
  </div> 

    
     <style>
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; height: 30px; cursor:move; }
  #sortable li span { position: absolute; margin-left: -1.3em; }
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
  </script>



