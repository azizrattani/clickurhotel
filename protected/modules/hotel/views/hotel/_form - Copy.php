<?php
/* @var $this HotelController */
/* @var $model Hotel */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	

	
	

	<div class="form-group">
		<?= $form->labelEx($model,'description'); ?>
		<?= $form->textArea($model,'description',array('class' => 'form-control','rows'=>6, 'cols'=>50)); ?>
	</div>

	

	

	

	

	

	<div class="form-group">
		<?= $form->labelEx($model,'phone2'); ?>
		<?= $form->textField($model, 'phone2', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'phone3'); ?>
		<?= $form->textField($model, 'phone3', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100)); ?>
	</div>

	

	<div class="form-group">
		<?= $form->labelEx($model,'fax'); ?>
		<?= $form->textField($model, 'fax', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100)); ?>
	</div>

	

	<div class="form-group">
		<?= $form->labelEx($model,'hotel_policies'); ?>
		<?= $form->textArea($model,'hotel_policies',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>

	

	

	<div class="form-group">
		<?= $form->labelEx($model,'child_age_from'); ?>
		<?= $form->textField($model,'child_age_from', array('class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'child_age_to'); ?>
		<?= $form->textField($model,'child_age_to', array('class' => 'form-control')); ?>
	</div>
    
  


	<div class="form-group">
		<?= $form->labelEx($model,'checkin_time'); ?>
        
        	&nbsp;&nbsp;
            
            <select name="Hotel[checkin_time1]" id="check_in" >
            	<?php 
					for ($h=0; $h<=23; $h++){
						if (strlen($h) == 1 ){
							$print = "0".$h;
						}else{
							$print = $h;
						}
				?>
                		
                        <option value="<?=$print?>"> <?=$print?> </option>
                
                <?php
					}
				?>
           	</select> : 
            
            <select name="Hotel[checkin_time2]" id="check_in" ">
            	<?php 
					for ($h=0; $h<=59; $h++){
						if (strlen($h) == 1 ){
							$print = "0".$h;
						}else{
							$print = $h;
						}
				?>
                		
                        <option value="<?=$print?>"> <?=$print?> </option>
                
                <?php
					}
				?>
           	</select>
            
           
        
        
	</div>
    
    

	<div class="form-group">
		<?= $form->labelEx($model,'checkout_time'); ?>
        
        	&nbsp;&nbsp;
            
            <select name="Hotel[checkout_time1]" id="check_in" >
            	<?php 
					for ($h=0; $h<=23; $h++){
						if (strlen($h) == 1 ){
							$print = "0".$h;
						}else{
							$print = $h;
						}
				?>
                		
                        <option value="<?=$print?>"> <?=$print?> </option>
                
                <?php
					}
				?>
           	</select> : 
            
            <select name="Hotel[checkout_time2]" id="check_in" ">
            	<?php 
					for ($h=0; $h<=59; $h++){
						if (strlen($h) == 1 ){
							$print = "0".$h;
						}else{
							$print = $h;
						}
				?>
                		
                        <option value="<?=$print?>"> <?=$print?> </option>
                
                <?php
					}
				?>
           	</select>
            
           
        
        
	</div>

	

	

	

	

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
