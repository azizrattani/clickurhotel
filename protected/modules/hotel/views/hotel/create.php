<?php
/* @var $this HotelController */
/* @var $model Hotel */

$this->pageTitle = 'Hotels - Create';
$this->breadcrumbs = array(
	'Hotels' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model','cancellation_policy')); ?>