<?php
/* @var $this HotelRoomsController */
/* @var $model HotelRooms */

$this->pageTitle = 'Hotel Rooms - View';
$this->breadcrumbs=array(
	'Hotel Rooms' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'hotel_id',
		'title',
		'description',
		'max_user',
		'no_of_room',
		'room_type',
		'views',
		'meal',
		'market',
		'deleted',
		'created',
		'modified',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>