<?php
/* @var $this HotelRoomsController */
/* @var $model HotelRooms */

$this->pageTitle = 'Hotel Rooms - Update';
$this->breadcrumbs = array(
	'Hotel Rooms' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>