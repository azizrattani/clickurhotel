<?php
/* @var $this HotelRoomsController */
/* @var $model HotelRooms */

$this->pageTitle = 'Hotel Rooms - Create';
$this->breadcrumbs = array(
	'Hotel Rooms' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>