<?php
/* @var $this HotelRoomsController */
/* @var $model HotelRooms */
/* @var $form CActiveForm */

$partner_id = (Yii::app()->user->profile["child_id"]);
$otherCondition = "";
if (Yii::app()->user->profile['role_id'] == 4){
	$otherCondition = " and id='".$partner_id."'";
}
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-rooms-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'hotel_id'); ?>
        <?= $form->dropDownList($model, 'hotel_id', CHtml::listData(Hotel::model()->findAll(array("condition"=>' deleted=0 '.$otherCondition)), 'id', 'name'), array('class' => 'form-control mselect', 'required' => 'required'));?>

	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'title'); ?>
		<?= $form->textField($model,'title',array('class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'description'); ?>
		<?= $form->textArea($model,'description',array('class' => 'form-control','rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'max_user'); ?>
		<?= $form->textField($model,'max_user',array('class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'no_of_room'); ?>
		<?= $form->textField($model,'no_of_room',array('class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'room_type'); ?>
		<?= $form->dropDownList($model, 'room_type', CHtml::listData(RoomType::model()->findAll(array("condition"=>' deleted=0')), 'id', 'title'), array('class' => 'form-control mselect', 'required' => 'required'));?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'views'); ?>
		<?= $form->dropDownList($model, 'views', CHtml::listData(ViewType::model()->findAll(array("condition"=>' deleted=0')), 'id', 'title'), array('class' => 'form-control mselect', 'required' => 'required'));?>
	</div>

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>


