<?php
/* @var $this HotelMealRateController */
/* @var $model HotelMealRate */

$this->pageTitle = 'Hotel Meal Rates - Update';
$this->breadcrumbs = array(
	'Hotel Meal Rates' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>