<?php
/* @var $this HotelMealRateController */
/* @var $model HotelMealRate */

$this->pageTitle = 'Hotel Meal Rates - View';
$this->breadcrumbs=array(
	'Hotel Meal Rates' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'hotel_id',
		'market_id',
		'date_from',
		'date_to',
		'breakfast',
		'lunch',
		'dinner',
		'created_by',
		'created',
		'modified',
		'deleted',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>