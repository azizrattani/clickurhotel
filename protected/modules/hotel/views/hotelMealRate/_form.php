<?php
/* @var $this HotelMealRateController */
/* @var $model HotelMealRate */
/* @var $form CActiveForm */

$partner_id = (Yii::app()->user->profile["child_id"]);
$otherCondition = "";
if (Yii::app()->user->profile['role_id'] == 4){
	$otherCondition = " and id='".$partner_id."'";
}
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-room-rate-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />
    
     <?php
        $options = array('empty'=>'--Select a Hotel--', 'class' => 'form-control mselect', 'required' => 'required');
		/*if($model->isNewRecord){
			
		}else{
			$options['readonly'] ="readonly";
		}*/
		
		
		?>
    
    <div class="form-group">
		<?= $form->labelEx($model,'hotel_id'); ?>
        <?= $form->dropDownList($model, 'hotel_id', CHtml::listData(Hotel::model()->findAll(array("condition"=>' deleted=0 '.$otherCondition)), 'id', 'name'), $options);?>

	</div>

	
    
    <div class="roomRate">
    
    	
    
    
    	<div class="form-group">
		<?= $form->labelEx($model,'date_from'); ?>
		<?php
		 if ($model->date_from){
			 $dt = ($model->date_from);
		 }else{
			 $dt = strtotime("+1 day");
		 }
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'HotelMealRate[date_from]',
								
                                'id'=>'Modelname_start_date',
                            	'value'=>Yii::app()->dateFormatter->format("M/d/y",strtotime($dt)),
                                'options'=>array(
								'minDate'=>date("m/d/Y",strtotime("+1 day")),
                                'showAnim'=>'fold',
                                ),
								
                                'htmlOptions'=>array(
                                'display'=>'block;',
								'class' => 'form-control',
                                ),
                        ));
		?>
	</div>

        <div class="form-group">
            <?= $form->labelEx($model,'date_to'); ?>
             <?php
             if ($model->date_to){
                 $dt = ($model->date_to);
             }else{
                 $dt = strtotime("+2 day");
             }
             
            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                    'name'=>'HotelMealRate[date_to]',								
                                    'id'=>'Modelname_end_date',
                                    'value'=>Yii::app()->dateFormatter->format("M/d/y",$dt),
                                    'options'=>array(
                                    'minDate'=>date("m/d/Y",strtotime("+2 day")),
                                    'showAnim'=>'fold',
                                    'class' => 'form-control',
                                    ),
                                    
                                    'htmlOptions'=>array(
                                    'display'=>'block;',
                                    'class' => 'form-control',
                                    ),
                            ));
            ?>
        </div>
        
        
        <?php
        $options = array('class' => 'form-control mselect',"tabindex"=>12);
		if($model->isNewRecord){
			$options['multiple'] ="multiple";
		}else{
		}
		
		
		?>
        
        <div class="form-group market_id">
            <?= $form->labelEx($model,'market_id'); ?><br />
            <?= $form->dropDownList($model, 'market_id', CHtml::listData(MarketType::model()->findAll(array("condition"=>' deleted=0')), 'id', 'title'), $options);?>
        </div>
    

		<?php

		$mealType = Mealstype::model()->findAll(["condition" =>"id <> 1 and deleted = 0"]);

		foreach($mealType as $meal){

			$mealTitle = $meal["title"];
			$mealTitleId = strtolower(str_replace(" ","",$mealTitle));

			$price = "";

			if(!$model->isNewRecord){

				$hotelMealRateMeal = HotelMealRateMeal::model()->find(["condition" =>"meal_type_id = ".$meal->id." and hotel_meal_rate = ".$model->id.""]);

				if($hotelMealRateMeal){
					$price = $hotelMealRateMeal->rate;
				}
			}

			?>

			<div class="form-group <?=$meal->id?> <?=$mealTitleId?> meals hidden">
				<label for="HotelMealRate_<?=$mealTitleId?>"><?=$mealTitle?></label>
				<input class="form-control" name="HotelMealRateMeal[<?=$meal->id?>]" id="HotelMealRateMeal_<?=$mealTitleId?>" type="text" value="<?=$price?>" >
			</div>



		<?php

		}

		?>
    

    

    
        
    
        <div class="box-footer">
            <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
            <?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
        </div>
    
    
    
    </div>
    
   
    

<?php $this->endWidget(); ?>

<script>

	var hotelDetail = "";

	$("#HotelMealRate_hotel_id").change(function(e) {
		$val = $(this).val();
		if ($val != ""){
			
			url  = "<?= $this->createUrl('/hotel/hotel/hotelDetail/id/') ?>/"+$val;
			
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						
						hotelDetail = data;

						$(".meals").addClass("hidden");

						
						if(hotelDetail.meal_option == 2){
							
							$meal_type = hotelDetail.meal_type;
							$meal_type = $meal_type.split(",");
							
							
							$.each($meal_type,function(k,v){

								$(".form-group."+v).removeClass("hidden");
								$(".form-group."+v).children("input").attr("requried","requried");
								
							});
							
							
							$market_type = hotelDetail.market_type;
							$market_type = $market_type.split(",");
							
							
							console.log($market_type);
							
							//$("#HotelMealRate_price").parent("div").removeClass("hidden");
							//$("#HotelMealRate_price").attr("requried","requried");
						}
					}
					
				}
			});
			
			
			
			
			
			url  = "<?= $this->createUrl('/hotel/hotel/hotelMarket/id/') ?>/"+$val;
			 <?php
    if ($model->isNewRecord){
		
	?>
			$("#HotelMealRate_market_id").select2('destroy');
	
			$("#HotelMealRate_market_id option").each(function() {
				$(this).remove();
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){					
					data = $.parseJSON(result);
					if(data){
						$('#HotelMealRate_market_id').append('<option value="">Select Market</option>');
						$.each(data,function(index,value){
							
							$('#HotelMealRate_market_id').append('<option value="'+value.id+'">'+value.title+'</option>');
							
						});
						$("#HotelMealRate_market_id").select2({
							placeholder: "Select Market"
						});
						
					}
					
				}
			});
			
		<?php
	}
	?>
			
			
			
		}else{
			$("#HotelMealRate_market_id").select2('destroy');
			$("#HotelMealRate_market_id option").each(function() {
				$(this).remove();
			});
			$("#HotelMealRate_market_id").select2({
							placeholder: "Select Market"
						});
			//$(".roomId").addClass("hidden");
			//$(".roomRate").addClass("hidden");	
		}
        
		
		
    });
	
	
	
	
	 <?php
    if (!$model->isNewRecord){
		
	?>
    	$("#HotelMealRate_hotel_id").trigger("change");
		$("#HotelMealRate_hotel_id").removeClass('mselect');
		$("#HotelMealRate_market_id").attr("disabled","disabled");
		$("#HotelMealRate_hotel_id").attr("disabled","disabled");
		//$("#HotelMealRate_market_id").select2('destroy');
    
    <?php
	}
	?>

</script>
