<?php
/* @var $this HotelMealRateController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Manage Hotel Meal Rates';
$this->breadcrumbs = array(
	'Hotel Meal Rates',
    'Manage'
);
?>

<?php if($createAllowed) { ?>
<p class="text-right">
    <a href="<?php echo $this->createUrl('create'); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Add New
    </a>
</p>
<?php } ?>

<div class="box-body table-responsive no-padding">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'hotel-meal-rate-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
        'columns' => array(
    		'id',
		array('name'=>'hotel_id','value'=>'$data->hotel->name','filter' => CHtml::listData(Hotel::model()->findAll(), 'id', 'name')),
		array('name'=>'market_id','value'=>'$data->market->title','filter' => CHtml::listData(MarketType::model()->findAll(), 'id', 'title')),
		array('name'=>'date_from','value'=>'date("d F, Y",strtotime($data->date_from))'),
		array('name'=>'date_to','value'=>'date("d F, Y",strtotime($data->date_to))'),
		array('name'=>'breakfast','value'=>'$data->breakfast?$data->breakfast:"N/A"'),
		array('name'=>'lunch','value'=>'$data->lunch?$data->lunch:"N/A"'),
		array('name'=>'dinner','value'=>'$data->dinner?$data->dinner:"N/A"'),
		/*
		'lunch',
		'dinner',
		'created_by',
		'created',
		'modified',
		'deleted',
		*/
            array(
                'htmlOptions' => array('style' => 'width: 75px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'View', 'class' => 'fa fa-eye', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$viewAllowed"
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Update', 'class' => 'fa fa-pencil-square-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$updateAllowed"
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Delete', 'class' => 'fa fa-trash', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$deleteAllowed"
                    )
                )
            )
        ),
    )); ?>
</div>