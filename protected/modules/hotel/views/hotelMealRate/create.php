<?php
/* @var $this HotelMealRateController */
/* @var $model HotelMealRate */

$this->pageTitle = 'Hotel Meal Rates - Create';
$this->breadcrumbs = array(
	'Hotel Meal Rates' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>