<?php

/**
 * This is the model class for table "hotel_contact".
 *
 * The followings are the available columns in table 'hotel_contact':
 * @property integer $id
 * @property integer $hotel_id
 * @property string $contact_type
 * @property string $person_name
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $status
 * @property string $deleted
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Hotel $hotel
 */
class HotelContact extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hotel_contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hotel_id, person_name, email, phone, mobile', 'required'),
			array('hotel_id', 'numerical', 'integerOnly'=>true),
			array('contact_type', 'length', 'max'=>11),
			array('person_name, email', 'length', 'max'=>255),
			array('phone, mobile, fax', 'length', 'max'=>20),
			array('status, deleted', 'length', 'max'=>1),
			array('created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, hotel_id, contact_type, person_name, email, phone, mobile, fax, status, deleted, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hotel' => array(self::BELONGS_TO, 'Hotel', 'hotel_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'hotel_id' => 'Hotel',
			'contact_type' => 'Contact Type',
			'person_name' => 'Person Name',
			'email' => 'Email',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'fax' => 'Fax',
			'status' => 'Status',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('contact_type', $this->contact_type, true);
		$criteria->compare('person_name', $this->person_name, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('mobile', $this->mobile, true);
		$criteria->compare('fax', $this->fax, true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('deleted', 0);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HotelContact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
