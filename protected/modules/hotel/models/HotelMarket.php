<?php

/**
 * This is the model class for table "hotel_market".
 *
 * The followings are the available columns in table 'hotel_market':
 * @property integer $id
 * @property integer $hotel_id
 * @property integer $market_id
 * @property double $price
 * @property string $start_date
 * @property string $end_date
 * @property string $deleted
 * @property integer $created_by
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Hotel $hotel
 * @property MarketType $market
 */
class HotelMarket extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hotel_market';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hotel_id, market_id', 'required'),
			array('hotel_id, market_id, created_by', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('deleted', 'length', 'max'=>1),
			array('start_date, end_date, created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, hotel_id, market_id, price, start_date, end_date, deleted, created_by, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hotel' => array(self::BELONGS_TO, 'Hotel', 'hotel_id'),
			'market' => array(self::BELONGS_TO, 'MarketType', 'market_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'hotel_id' => 'Hotel',
			'market_id' => 'Market',
			'price' => 'price is per person',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'deleted' => 'Deleted',
			'created_by' => 'Created By',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('market_id', $this->market_id);
		$criteria->compare('price', $this->price);
		$criteria->compare('start_date', $this->start_date, true);
		$criteria->compare('end_date', $this->end_date, true);
		$criteria->compare('deleted', 0);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HotelMarket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
