<?php

/**
 * This is the model class for table "hotel_room_rate".
 *
 * The followings are the available columns in table 'hotel_room_rate':
 * @property integer $id
 * @property integer $room_id
 * @property string $bookdate
 * @property double $price
 * @property double $price_weekend
 * @property double $four_days
 * @property integer $no_of_room
 * @property integer $room_book
 * @property integer $created_by
 * @property string $deleted
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Users $createdBy
 * @property HotelRooms $room
 */
class HotelRoomRate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hotel_room_rate';
	}
	
	public $hotel_id, $start_date, $end_date, $all_day;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('room_id, price,no_of_room', 'required'),
			array('room_id, no_of_room, room_book, created_by', 'numerical', 'integerOnly'=>true),
			array('price, price_weekend, four_days,base_price', 'numerical'),
			array('deleted', 'length', 'max'=>1),
			array('bookdate, created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, room_id, bookdate, price, price_weekend, four_days, no_of_room, room_book, created_by, deleted, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdBy' => array(self::BELONGS_TO, 'Users', 'created_by'),
			'room' => array(self::BELONGS_TO, 'HotelRooms', 'room_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'hotel_id' => 'Hotel Name',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'room_id' => 'Room',
			'bookdate' => 'Bookdate',
			'base_price' => 'Cost Price',
			'price' => 'Base Price',
			'price_weekend' => 'Price Weekend',
			'four_days' => 'Four Days',
			'no_of_room' => 'No Of Room',
			'room_book' => 'Room Book',
			'created_by' => 'Created By',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('room_id', $this->room_id);
		$criteria->compare('bookdate', $this->bookdate, true);
		$criteria->compare('price', $this->price);
		$criteria->compare('price_weekend', $this->price_weekend);
		$criteria->compare('four_days', $this->four_days);
		$criteria->compare('no_of_room', $this->no_of_room);
		$criteria->compare('room_book', $this->room_book);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('deleted', 0);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getCreaterName(){
		
		$role_id = $this->createdBy->role_id;
		$child_id = $this->createdBy->child_id;
		
		if ($role_id == 2){
			
			$adminUser = AdminUsers::model()->findByPk($child_id);
			
			echo $adminUser->first_name . " " . $adminUser->last_name;
			
		}elseif ($role_id == 4){
			
			$hotelName = Hotel::model()->findByPk($child_id);
			echo $adminUser->name;
			
		}elseif ($role_id == 5){
			echo "Alloatment Company";
		}
		
		
		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HotelRoomRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
