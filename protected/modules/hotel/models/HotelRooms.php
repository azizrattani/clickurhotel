<?php

/**
 * This is the model class for table "hotel_rooms".
 *
 * The followings are the available columns in table 'hotel_rooms':
 * @property integer $id
 * @property integer $hotel_id
 * @property string $title
 * @property string $description
 * @property integer $max_user
 * @property integer $no_of_room
 * @property integer $room_type
 * @property integer $views
 * @property integer $deleted
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property BookingRooms[] $bookingRooms
 * @property HotelRoomRate[] $hotelRoomRates
 * @property Hotel $hotel
 * @property RoomType $roomType
 * @property ViewType $views0
 * @property RoomFacilities[] $roomFacilities
 * @property RoomPictures[] $roomPictures
 */
class HotelRooms extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hotel_rooms';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hotel_id, title, max_user, no_of_room, room_type, views', 'required'),
			array('hotel_id, max_user, no_of_room, room_type, views, deleted', 'numerical', 'integerOnly'=>true),
			array('description, created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, hotel_id, title, description, max_user, no_of_room, room_type, views, deleted, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bookingRooms' => array(self::HAS_MANY, 'BookingRooms', 'room_id'),
			'hotelRoomRates' => array(self::HAS_MANY, 'HotelRoomRate', 'room_id'),
			'hotel' => array(self::BELONGS_TO, 'Hotel', 'hotel_id'),
			'roomType' => array(self::BELONGS_TO, 'RoomType', 'room_type'),
			'views0' => array(self::BELONGS_TO, 'ViewType', 'views'),
			'roomFacilities' => array(self::HAS_MANY, 'RoomFacilities', 'room_id'),
			'roomPictures' => array(self::HAS_MANY, 'RoomPictures', 'room_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'hotel_id' => 'Hotel',
			'title' => 'Title',
			'description' => 'Description',
			'max_user' => 'Max User',
			'no_of_room' => 'No Of Room',
			'room_type' => 'Room Type',
			'views' => 'Views',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('max_user', $this->max_user);
		$criteria->compare('no_of_room', $this->no_of_room);
		$criteria->compare('room_type', $this->room_type);
		$criteria->compare('views', $this->views);
		$criteria->compare('deleted', 0);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HotelRooms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
