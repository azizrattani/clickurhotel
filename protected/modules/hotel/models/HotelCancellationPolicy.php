<?php

/**
 * This is the model class for table "hotel_cancellation_policy".
 *
 * The followings are the available columns in table 'hotel_cancellation_policy':
 * @property integer $id
 * @property integer $hotel_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $one_night_start
 * @property integer $one_night_end
 * @property integer $cancel_50_start
 * @property integer $cancel_50_end
 * @property integer $cancel_100_start
 * @property integer $cancel_100_end
 * @property integer $free_cancel_start
 * @property integer $free_cancel_end
 * @property string $created
 * @property string $modified
 * @property integer $created_by
 * @property string $deleted
 */
class HotelCancellationPolicy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hotel_cancellation_policy';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hotel_id, start_date, end_date, one_night_start, one_night_end, cancel_50_start, cancel_50_end, cancel_100_start, cancel_100_end, free_cancel_start, free_cancel_end, created, modified', 'required'),
			array('hotel_id, one_night_start, one_night_end, cancel_50_start, cancel_50_end, cancel_100_start, cancel_100_end, free_cancel_start, free_cancel_end, created_by', 'numerical', 'integerOnly'=>true),
			array('deleted', 'length', 'max'=>1),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, hotel_id, start_date, end_date, one_night_start, one_night_end, cancel_50_start, cancel_50_end, cancel_100_start, cancel_100_end, free_cancel_start, free_cancel_end, created, modified, created_by, deleted', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'hotel_id' => 'Hotel',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'one_night_start' => 'One Night From',
			'one_night_end' => 'One Night To',
			'cancel_50_start' => '50% Cancel Charges From',
			'cancel_50_end' => '50% Cancel Charges To',
			'cancel_100_start' => '100% Cancel Charges From',
			'cancel_100_end' => '100% Cancel Charges To',
			'free_cancel_start' => 'Free Cancel Charges From',
			'free_cancel_end' => 'Free Cancel Charges To',
			'created' => 'Created',
			'modified' => 'Modified',
			'created_by' => 'Created By',
			'deleted' => 'Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('start_date', $this->start_date, true);
		$criteria->compare('end_date', $this->end_date, true);
		$criteria->compare('one_night_start', $this->one_night_start);
		$criteria->compare('one_night_end', $this->one_night_end);
		$criteria->compare('cancel_50_start', $this->cancel_50_start);
		$criteria->compare('cancel_50_end', $this->cancel_50_end);
		$criteria->compare('cancel_100_start', $this->cancel_100_start);
		$criteria->compare('cancel_100_end', $this->cancel_100_end);
		$criteria->compare('free_cancel_start', $this->free_cancel_start);
		$criteria->compare('free_cancel_end', $this->free_cancel_end);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('deleted', 0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HotelCancellationPolicy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
