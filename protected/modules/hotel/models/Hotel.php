<?php

/**
 * This is the model class for table "hotel".
 *
 * The followings are the available columns in table 'hotel':
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $short_desc
 * @property string $description
 * @property integer $hotel_star
 * @property string $langitude
 * @property string $latitude
 * @property string $address
 * @property string $city
 * @property integer $state
 * @property integer $country
 * @property string $zipcode
 * @property string $phone1
 * @property string $phone2
 * @property string $phone3
 * @property string $email
 * @property string $fax
 * @property string $website
 * @property string $hotel_policies
 * @property string $pet
 * @property string $liquor
 * @property string $smoking
 * @property integer $child_age_from
 * @property integer $child_age_to
 * @property string $checkin_time
 * @property string $checkout_time
 * @property integer $total_room
 * @property integer $total_floor
 * @property integer $build_year
 * @property integer $mealOption
 * @property integer $rate_type
 * @property double $breakfast_rate
 * @property double $lunch_rate
 * @property double $dinner_rate
 * @property integer $one_night_start
 * @property integer $one_night_end
 * @property integer $cancel_50_start
 * @property integer $cancel_50_end
 * @property integer $cancel_100_start
 * @property integer $cancel_100_end
 * @property integer $free_cancel_start
 * @property integer $free_cancel_end
 * @property string $market_type
 * @property string $meal_option
 * @property string $meal_type
 * @property string $status
 * @property integer $active
 * @property integer $deleted
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Booking[] $bookings
 * @property HotelContact[] $hotelContacts
 * @property HotelFacilities[] $hotelFacilities
 * @property HotelMarket[] $hotelMarkets
 * @property HotelMealRate[] $hotelMealRates
 * @property HotelMealType[] $hotelMealTypes
 * @property HotelPicture[] $hotelPictures
 * @property HotelRooms[] $hotelRooms
 */
class Hotel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hotel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, username, password, hotel_star, city, phone1, email, checkin_time, checkout_time, one_night_start, one_night_end, cancel_50_start, cancel_50_end, cancel_100_start, cancel_100_end, free_cancel_start, free_cancel_end, market_type, meal_type', 'required'),
			array('email','email'),
			array('hotel_star, state, country, child_age_from, child_age_to, total_room, total_floor, build_year, mealOption, rate_type, one_night_start, one_night_end, cancel_50_start, cancel_50_end, cancel_100_start, cancel_100_end, free_cancel_start, free_cancel_end, active, deleted,sort_order', 'numerical', 'integerOnly'=>true),
			array('breakfast_rate, lunch_rate, dinner_rate', 'numerical'),
			array('name', 'length', 'max'=>150),
			array('username, password, langitude, latitude, address, city, email, website, market_type, meal_option, meal_type', 'length', 'max'=>255),
			array('zipcode, phone1, phone2, phone3, fax', 'length', 'max'=>100),
			array('pet, liquor, smoking, status', 'length', 'max'=>1),
			array('description, hotel_policies, created,sort_order, modified,short_desc', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, username, password, short_desc, description, hotel_star, langitude, latitude, address, city, state, country, zipcode, phone1, phone2, phone3, email, fax, website, hotel_policies, pet, liquor, smoking, child_age_from, child_age_to, checkin_time, checkout_time, total_room, total_floor, build_year, mealOption, rate_type, breakfast_rate, lunch_rate, dinner_rate, one_night_start, one_night_end, cancel_50_start, cancel_50_end, cancel_100_start, cancel_100_end, free_cancel_start, free_cancel_end, market_type, meal_option, meal_type, status, active, deleted, created, modified,sort_order', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bookings' => array(self::HAS_MANY, 'Booking', 'hotel_id'),
			'hotelContacts' => array(self::HAS_MANY, 'HotelContact', 'hotel_id'),
			'hotelFacilities' => array(self::HAS_MANY, 'HotelFacilities', 'hotel_id'),
			'hotelMarkets' => array(self::HAS_MANY, 'HotelMarket', 'hotel_id'),
			'hotelMealRates' => array(self::HAS_MANY, 'HotelMealRate', 'hotel_id'),
			'hotelMealTypes' => array(self::HAS_MANY, 'HotelMealType', 'hotel_id'),
			'hotelPictures' => array(self::HAS_MANY, 'HotelPicture', 'hotel_id'),
			'hotelRooms' => array(self::HAS_MANY, 'HotelRooms', 'hotel_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'username' => 'Username',
			'password' => 'Password',
			'short_desc' => 'Short Desc',
			'description' => 'Description',
			'hotel_star' => 'Hotel Star',
			'langitude' => 'Langitude',
			'latitude' => 'Latitude',
			'address' => 'Address',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'zipcode' => 'Zipcode',
			'phone1' => 'Phone1',
			'phone2' => 'Phone2',
			'phone3' => 'Phone3',
			'email' => 'Email',
			'fax' => 'Fax',
			'website' => 'Website',
			'hotel_policies' => 'Hotel Policies',
			'pet' => 'Pet',
			'liquor' => 'Liquor',
			'smoking' => 'Smoking',
			'child_age_from' => 'Child Age From',
			'child_age_to' => 'Child Age To',
			'checkin_time' => 'Checkin Time',
			'checkout_time' => 'Checkout Time',
			'total_room' => 'Total Room',
			'total_floor' => 'Total Floor',
			'build_year' => 'Build Year',
			'mealOption' => 'Meal Option',
			'rate_type' => 'Rate Type',
			'breakfast_rate' => 'Breakfast Rate',
			'lunch_rate' => 'Lunch Rate',
			'dinner_rate' => 'Dinner Rate',
			'one_night_start' => 'One Night Start',
			'one_night_end' => 'One Night End',
			'cancel_50_start' => 'Cancel 50 Start',
			'cancel_50_end' => 'Cancel 50 End',
			'cancel_100_start' => 'Cancel 100 Start',
			'cancel_100_end' => 'Cancel 100 End',
			'free_cancel_start' => 'Free Cancel Start',
			'free_cancel_end' => 'Free Cancel End',
			'market_type' => 'Market Type',
			'meal_option' => 'Meal Option',
			'meal_type' => 'Meal Type',
			'status' => 'Status',
			'active' => 'Active',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('username', $this->username, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('short_desc', $this->short_desc, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('hotel_star', $this->hotel_star);
		$criteria->compare('langitude', $this->langitude, true);
		$criteria->compare('latitude', $this->latitude, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('city', $this->city, true);
		$criteria->compare('state', $this->state);
		$criteria->compare('country', $this->country);
		$criteria->compare('zipcode', $this->zipcode, true);
		$criteria->compare('phone1', $this->phone1, true);
		$criteria->compare('phone2', $this->phone2, true);
		$criteria->compare('phone3', $this->phone3, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('fax', $this->fax, true);
		$criteria->compare('website', $this->website, true);
		$criteria->compare('hotel_policies', $this->hotel_policies, true);
		$criteria->compare('pet', $this->pet, true);
		$criteria->compare('liquor', $this->liquor, true);
		$criteria->compare('smoking', $this->smoking, true);
		$criteria->compare('child_age_from', $this->child_age_from);
		$criteria->compare('child_age_to', $this->child_age_to);
		$criteria->compare('checkin_time', $this->checkin_time, true);
		$criteria->compare('checkout_time', $this->checkout_time, true);
		$criteria->compare('total_room', $this->total_room);
		$criteria->compare('total_floor', $this->total_floor);
		$criteria->compare('build_year', $this->build_year);
		$criteria->compare('mealOption', $this->mealOption);
		$criteria->compare('rate_type', $this->rate_type);
		$criteria->compare('breakfast_rate', $this->breakfast_rate);
		$criteria->compare('lunch_rate', $this->lunch_rate);
		$criteria->compare('dinner_rate', $this->dinner_rate);
		$criteria->compare('one_night_start', $this->one_night_start);
		$criteria->compare('one_night_end', $this->one_night_end);
		$criteria->compare('cancel_50_start', $this->cancel_50_start);
		$criteria->compare('cancel_50_end', $this->cancel_50_end);
		$criteria->compare('cancel_100_start', $this->cancel_100_start);
		$criteria->compare('cancel_100_end', $this->cancel_100_end);
		$criteria->compare('free_cancel_start', $this->free_cancel_start);
		$criteria->compare('free_cancel_end', $this->free_cancel_end);
		$criteria->compare('market_type', $this->market_type, true);
		$criteria->compare('meal_option', $this->meal_option, true);
		$criteria->compare('meal_type', $this->meal_type, true);
		$criteria->compare('status', $this->status, true);
		$criteria->compare('active', $this->active);
		$criteria->compare('deleted', 0);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hotel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
