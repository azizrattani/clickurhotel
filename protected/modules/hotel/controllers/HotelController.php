<?php

class HotelController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new Hotel('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Hotel']))
		{
			$model->attributes = $_GET['Hotel'];
		}
		
		if (Yii::app()->user->profile['role_id'] == 4){		
			$model->id = Yii::app()->user->profile['child_id'] ;
		}

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);
		

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new Hotel;
		$model->scenario = 'create';
		$cancellation_policy = new HotelCancellationPolicy;

		if(isset($_POST['Hotel']))
		{
			$model->attributes = $_POST['Hotel'];
			
			
			if(isset($_POST['Hotel']["market_type"]))
				$model->market_type =implode(",",$_POST['Hotel']["market_type"]);
			
			if(isset($_POST['Hotel']["meal_type"]))
				$model->meal_type =implode(",",$_POST['Hotel']["meal_type"]);	
			
			if(isset($_POST['Hotel']['meal_option']) && $_POST['Hotel']['meal_option'] == "1")
				$model->meal_type = "0";
			
			//$model->market_type = implode(",",$_POST['Hotel']['market_type']);
			//$model->checkin_time = $_POST["Hotel"]["checkin_time1"].":".$_POST["Hotel"]["checkin_time2"];
			//$model->checkout_time = $_POST["Hotel"]["checkout_time1"].":".$_POST["Hotel"]["checkout_time2"];

			if ($model->validate())
            {
				$model->save();
				
				
				
				HotelFacilities::model()->deleteAllByAttributes(array(
					'hotel_id'=>$model->id,
				));
				
				
				if(isset($_POST['Hotel']["Facilities"])){
					
					$HotelFacilities = $_POST['Hotel']["Facilities"];
					foreach($HotelFacilities as $key => $value){
						
						$hotel_facilities = new HotelFacilities;
						$hotel_facilities->hotel_id = $model->id;
						$hotel_facilities->facilities_id = $value;
						
						$hotel_facilities->save();
						
					}
					
				}
				
				
				
				HotelContact::model()->deleteAllByAttributes(array(
					'hotel_id'=>$model->id,
				));
					
				$HotelReservation = $_POST['HotelContact']["name"];
				foreach($HotelReservation as $key => $value){
					
					$hotel_contact = new HotelContact;
					$hotel_contact->hotel_id = $model->id;
					$hotel_contact->contact_type = "contact";
					
					$hotel_contact->person_name = $value;
					$hotel_contact->email = $_POST['HotelContact']["email"][$key];
					$hotel_contact->phone = $_POST['HotelContact']["phone"][$key];
					$hotel_contact->mobile = $_POST['HotelContact']["mobile"][$key];
					$hotel_contact->fax = $_POST['HotelContact']["fax"][$key];
					
					
					$hotel_contact->created = date("Y-m-d H:i:s");
					$hotel_contact->modified = date("Y-m-d H:i:s");
					
					$hotel_contact->save();
					
					
					
				}
				
				
				$HotelReservation = $_POST['HotelReservation']["name"];
				foreach($HotelReservation as $key => $value){
					
					$hotel_contact = new HotelContact;
					$hotel_contact->hotel_id = $model->id;
					$hotel_contact->contact_type = "reservation";
					
					$hotel_contact->person_name = $value;
					$hotel_contact->email = $_POST['HotelReservation']["email"][$key];
					$hotel_contact->phone = $_POST['HotelReservation']["phone"][$key];
					$hotel_contact->mobile = $_POST['HotelReservation']["mobile"][$key];
					$hotel_contact->fax = $_POST['HotelReservation']["fax"][$key];
					
					
					$hotel_contact->created = date("Y-m-d H:i:s");
					$hotel_contact->modified = date("Y-m-d H:i:s");
					
					$hotel_contact->save();
					
					
					
				}
				
				
				
				$HotelReservation = $_POST['HotelAccount']["name"];
				foreach($HotelReservation as $key => $value){
					
					$hotel_contact = new HotelContact;
					$hotel_contact->hotel_id = $model->id;
					$hotel_contact->contact_type = "account";
					
					$hotel_contact->person_name = $value;
					$hotel_contact->email = $_POST['HotelAccount']["email"][$key];
					$hotel_contact->phone = $_POST['HotelAccount']["phone"][$key];
					$hotel_contact->mobile = $_POST['HotelAccount']["mobile"][$key];
					$hotel_contact->fax = $_POST['HotelAccount']["fax"][$key];
					
					
					$hotel_contact->created = date("Y-m-d H:i:s");
					$hotel_contact->modified = date("Y-m-d H:i:s");
					
					$hotel_contact->save();
					
					
					
				}
				
				
				$to = $subject = $body =  $from_email =  $from_name = '';
				
				$body = "Greetings ".$model->name." <br><br>
Welcome! We are very excited to have you onboard. Following are access credentials:<br><br>
Username: ".$model->username."<br>
Password :  ".$model->password."<br>
URL: http://www.clickhotel.online/ <br><br>

Please ensure your credentials are safe and secure. Any activity done from your ID will be considered authroized by you.<br><br>

Regards<br>
Support Team";
				
				$subject = "Welcome";
				
				$from_email = "no-reply@clickurhotel.com";
				$from_name = "Booking Department - ClickUrHotel";
				
				
				$customer_email = $model->email ;
				
				
				Yii::app()->functions->email($customer_email, $subject , $body, $from_email , $from_name);
				
				
				
                Yii::app()->user->setFlash('success', 'Hotel saved successfully');
                $this->redirect(array('index'));
				
				
            }else{
				$model->market_type =explode(",",$model->market_type);	
				$model->meal_type =explode(",",$model->meal_type);	
			}
		}

		$this->render('create', compact('model','cancellation_policy'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$model->scenario = 'update';
		$cancellation_policy = new HotelCancellationPolicy;

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['Hotel']))
			{
				if ($_POST['Hotel']["password"] == ""){
					$_POST['Hotel']["password"] = $model->password;	
				}
				
				
				$model->attributes = $_POST['Hotel'];
				
				if(isset($_POST['Hotel']["market_type"]))
					$model->market_type =implode(",",$_POST['Hotel']["market_type"]);
				
				if(isset($_POST['Hotel']["meal_type"]))
					$model->meal_type =implode(",",$_POST['Hotel']["meal_type"]);
				
				if(isset($_POST['Hotel']['meal_option']) && $_POST['Hotel']['meal_option'] == "1")
					$model->meal_type = "0";

				if ($model->save())
	            {
	                
					
					HotelFacilities::model()->deleteAllByAttributes(array(
						'hotel_id'=>$model->id,
					));
					
					if(isset($_POST['Hotel']["Facilities"])){
						
						$HotelFacilities = $_POST['Hotel']["Facilities"];
						foreach($HotelFacilities as $key => $value){
							
							$hotel_facilities = new HotelFacilities;
							$hotel_facilities->hotel_id = $model->id;
							$hotel_facilities->facilities_id = $value;
							
							$hotel_facilities->save();
							
						}
						
					}
					
					
					
					HotelContact::model()->deleteAllByAttributes(array(
						'hotel_id'=>$model->id,
					));
					
					$HotelReservation = $_POST['HotelContact']["name"];
					foreach($HotelReservation as $key => $value){
						
						$hotel_contact = new HotelContact;
						$hotel_contact->hotel_id = $model->id;
						$hotel_contact->contact_type = "contact";
						
						$hotel_contact->person_name = $value;
						$hotel_contact->email = $_POST['HotelContact']["email"][$key];
						$hotel_contact->phone = $_POST['HotelContact']["phone"][$key];
						$hotel_contact->mobile = $_POST['HotelContact']["mobile"][$key];
						$hotel_contact->fax = $_POST['HotelContact']["fax"][$key];
						
						
						$hotel_contact->created = date("Y-m-d H:i:s");
						$hotel_contact->modified = date("Y-m-d H:i:s");
						
						$hotel_contact->save();
						
						
						
					}
					
					
					$HotelReservation = $_POST['HotelReservation']["name"];
					foreach($HotelReservation as $key => $value){
						
						$hotel_contact = new HotelContact;
						$hotel_contact->hotel_id = $model->id;
						$hotel_contact->contact_type = "reservation";
						
						$hotel_contact->person_name = $value;
						$hotel_contact->email = $_POST['HotelReservation']["email"][$key];
						$hotel_contact->phone = $_POST['HotelReservation']["phone"][$key];
						$hotel_contact->mobile = $_POST['HotelReservation']["mobile"][$key];
						$hotel_contact->fax = $_POST['HotelReservation']["fax"][$key];
						
						
						$hotel_contact->created = date("Y-m-d H:i:s");
						$hotel_contact->modified = date("Y-m-d H:i:s");
						
						$hotel_contact->save();
						
						
						
					}
					
					
					
					$HotelReservation = $_POST['HotelAccount']["name"];
					foreach($HotelReservation as $key => $value){
						
						$hotel_contact = new HotelContact;
						$hotel_contact->hotel_id = $model->id;
						$hotel_contact->contact_type = "account";
						
						$hotel_contact->person_name = $value;
						$hotel_contact->email = $_POST['HotelAccount']["email"][$key];
						$hotel_contact->phone = $_POST['HotelAccount']["phone"][$key];
						$hotel_contact->mobile = $_POST['HotelAccount']["mobile"][$key];
						$hotel_contact->fax = $_POST['HotelAccount']["fax"][$key];
						
						
						$hotel_contact->created = date("Y-m-d H:i:s");
						$hotel_contact->modified = date("Y-m-d H:i:s");
						
						$hotel_contact->save();
						
						
						
					}
					
					
					Yii::app()->user->setFlash('success', 'Hotel updated successfully');
	                $this->redirect(array('index'));
	            }else{
					$model->market_type =explode(",",$model->market_type);	
					$model->meal_type =explode(",",$model->meal_type);	
				}
			}else{
				$model->market_type =explode(",",$model->market_type);	
				$model->meal_type =explode(",",$model->meal_type);	
	
			}
			
			
			 $model->password = '';

			$this->render('update', compact('model','cancellation_policy'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}
	
	public function actionHotelDetail($id){
		
		$model = Hotel::model()->findByPk($id);
		
		$output = array();
		
		foreach($model as $key=>$value){
			$output[$key] = $value;	
		}
		
		
		
		echo json_encode($output);
		
	}

	public function actionHotelByCity($city){

		$model = Hotel::model()->findAll(array("condition"=>' deleted=0 and city="'.$city.'"'));

		$output = array();

		foreach($model as $key=>$value){
			foreach($value as $k=>$v){
				$output[$key][$k] = $v;
			}
		}



		echo json_encode($output);

	}

	public function actionHotelRooms($id){
		
		$model = HotelRooms::model()->findAll(array("condition"=>' deleted=0 and hotel_id='.$id));
		
		$output = array();
		
		foreach($model as $key=>$value){
			foreach($value as $k=>$v){
				$output[$key][$k] = $v;	
			}
		}
		
		
		
		echo json_encode($output);
		
	}
	
	public function actionHotelMarket($id){
		
		$hotel = Hotel::model()->findByPk($id);
		
		$market_type = $hotel->market_type;
		
		
		
		
		$model = MarketType::model()->findAll(array("condition"=>' deleted=0 and id in ('.$market_type.')'));
		
		$output = array();
		
		foreach($model as $key=>$value){
			foreach($value as $k=>$v){
				$output[$key][$k] = $v;	
			}
		}
		
		
		
		echo json_encode($output);
		
	}
	
	public function actionPriority()
	{
		
		
		
		
		$model = array();
		
		if(isset($_POST['Hotel']["save"]))
		{
			
			
			foreach($_POST['Hotel']["hotel_id"] as $key=>$hotel_id){
				
				
				$model = Hotel::model()->findByPk($hotel_id);
				$model->sort_order = $key;
				$model->save();
				
				
			}
			$model = Hotel::model()->findAll(array("condition"=>' deleted=0 and city="'.$_POST['Hotel']["city"].'"',"order"=>" sort_order asc"));
			Yii::app()->user->setFlash('success', 'Hotel priority updated');
			
		

		}elseif(isset($_POST['Hotel']["city"]))
		{
			$model = Hotel::model()->findAll(array("condition"=>' deleted=0 and city="'.$_POST['Hotel']["city"].'"',"order"=>" sort_order asc"));
			

		}
	

	
        $this->render('priority', compact('model'));
	}
	
	

	public function loadModel($id)
	{
		$model = Hotel::model()->findByPk($id);
		
		if (Yii::app()->user->profile['role_id'] == 4){
			
			$model->id = Yii::app()->user->profile['child_id'] ;
		}
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
