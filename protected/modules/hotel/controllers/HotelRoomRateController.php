<?php

class HotelRoomRateController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new HotelRoomRate;
		$hotelRoomRate = "";
		
		if(isset($_POST['HotelRoomRate']) || (isset($_SESSION["HotelRoomRate"])))
		{
			if (!isset($_POST['HotelRoomRate'])){
				$_POST['HotelRoomRate'] = $_SESSION["HotelRoomRate"]	;
			}
			
			$model->attributes = $_POST['HotelRoomRate'];
			
			$_SESSION["HotelRoomRate"] = $_POST['HotelRoomRate'];
			$start_date = date("Y-m-d",strtotime($_POST['HotelRoomRate']["start_date"]));
			$end_date =  date("Y-m-d",strtotime($_POST['HotelRoomRate']["end_date"]));
			$roomID = $_POST['HotelRoomRate']['room_id'];
			
			$now = strtotime($start_date);
			$your_date = strtotime($end_date);
			$datediff =  $your_date - $now;
			
			$totalDay = round($datediff / (60 * 60 * 24));
			
			
			$hotelRoomRate = HotelRoomRate::model()->findAll(array("order" => "bookdate","condition"=>' room_id="'.$roomID.'" and bookdate between "'.$start_date.'" and "'.$end_date.'" and deleted=0 and created_by="'.Yii::app()->user->profile['id'].'"'));
			
			
			$hotelRoomRate = new CArrayDataProvider($hotelRoomRate,array("pagination" => array('pageSize' => (isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:100))));
			
			
			

			
		}
		
		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

		$this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed','hotelRoomRate'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		
		$model = new HotelRoomRate;
		if(isset($_POST['HotelRoomRate']))
		{
			
			$start_date = $_POST['HotelRoomRate']["start_date"];
			$end_date = $_POST['HotelRoomRate']["end_date"];
			
			$now = strtotime($start_date);
			$your_date = strtotime($end_date);
			$datediff =  $your_date - $now;
			
			$totalDay = round($datediff / (60 * 60 * 24));
			
			
			for($i = 0; $i <=$totalDay; $i++){
				
				$model = new HotelRoomRate;
				$model->attributes = $_POST['HotelRoomRate'];
				
				$model->bookdate = date("Y-m-d",strtotime($start_date . "+ ".($i)." day"));
				$model->created_by = Yii::app()->user->profile['id'];
				
				
				$save = $model->save();
			}
			
			
			
		
			
			
			
			

			if ($save)
            {	
                Yii::app()->user->setFlash('success', 'HotelRoomRate saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate()
	{
		$model = new HotelRoomRate;
		if(isset($_POST['HotelRoomRate']))
		{
			
			$start_date = $_POST['HotelRoomRate']["start_date"];
			$end_date = $_POST['HotelRoomRate']["end_date"];
			$roomID = $_POST['HotelRoomRate']['room_id'];
			
			$now = strtotime($start_date);
			$your_date = strtotime($end_date);
			$datediff =  $your_date - $now;
			
			$totalDay = round($datediff / (60 * 60 * 24));
			
			
			for($i = 0; $i <=$totalDay; $i++){
				
				$bookDate = date("Y-m-d",strtotime($start_date . "+ ".($i)." day"));
				
				
				
				Yii::app()->db->createCommand("update hotel_room_rate set deleted=1, modified='".date("Y-m-d H:i:s")."' where  bookdate='".$bookDate."' and deleted=0 and room_id='".$roomID."'")->execute();;
				
				$findRate = HotelRoomRate::model()->find(array("condition"=>"bookdate='".$bookDate."' and deleted=0"));
				
				
				
				
				$model = new HotelRoomRate;
				$model->attributes = $_POST['HotelRoomRate'];
				
				$model->bookdate = $bookDate;
				$model->created_by = Yii::app()->user->profile['id'];
				
				
				$save = $model->save();
			}
			
			
			
		
			
			
			
			

			if ($save)
            {	
                Yii::app()->user->setFlash('success', 'HotelRoomRate saved successfully');
                $this->redirect(array('index'));
            }
		}
		
		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

		$this->render('update', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = HotelRoomRate::model()->findByPk($id);
		
		if (Yii::app()->user->profile['role_id'] == 4){
			$model->id = Yii::app()->user->profile['child_id'] ;
		}
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
