<?php

class HotelMealRateController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new HotelMealRate('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['HotelMealRate']))
		{
			$model->attributes = $_GET['HotelMealRate'];
		}

		$createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		
		$model = new HotelMealRate;
		
		$save = false;
		
		if(isset($_POST['HotelMealRate']))
		{
			
			$start_date = date("Y-m-d",strtotime($_POST['HotelMealRate']["date_from"]));
			$end_date = date("Y-m-d",strtotime($_POST['HotelMealRate']["date_to"]));
			
		
			if (isset($_POST['HotelMealRate']["market_id"])){
				
				foreach($_POST['HotelMealRate']["market_id"] as $market){
				
				
					$model = new HotelMealRate;
					$model->attributes = $_POST['HotelMealRate'];
					
					$model->date_from 	= $start_date;
					$model->date_to 	= $end_date;
					$model->market_id 	= $market;
					
					$model->created_by = Yii::app()->user->profile['id'];
					
					$save = $model->save();

					foreach($_POST['HotelMealRateMeal'] as $mealId => $mealsPrice){

						$hotelMeal = new HotelMealRateMeal;
						$hotelMeal->hotel_meal_rate = $model->id;
						$hotelMeal->meal_type_id = $mealId;
						$hotelMeal->rate = $mealsPrice;

						$hotelMeal->save(false);

					}


					
				}
				
			}else{
				
				$model = new HotelMealRate;
				$model->attributes = $_POST['HotelMealRate'];
				
				$model->created_by = Yii::app()->user->profile['id'];
				
				if ($model->validate()){
					
				}
				
			}
			
			
			

			if ($save)
            {	
                Yii::app()->user->setFlash('success', 'HotelMealRate saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['HotelMealRate']))
			{
				$model->attributes = $_POST['HotelMealRate'];
				$start_date = date("Y-m-d",strtotime($_POST['HotelMealRate']["date_from"]));
				$end_date = date("Y-m-d",strtotime($_POST['HotelMealRate']["date_to"]));
				
				$model->date_from 	= $start_date;
				$model->date_to 	= $end_date;

				if ($model->save())
	            {

					foreach($_POST['HotelMealRateMeal'] as $mealId => $mealsPrice){

						$hotelMeal = HotelMealRateMeal::model()->find(["condition" =>"meal_type_id = ".$mealId." and hotel_meal_rate = ".$model->id.""]);

						if(!$hotelMeal){


							$hotelMeal = new HotelMealRateMeal;

							$hotelMeal->meal_type_id = $mealId;
							$hotelMeal->hotel_meal_rate = $model->id;

						}

						$hotelMeal->rate = $mealsPrice;

						$hotelMeal->save(false);

					}


					Yii::app()->user->setFlash('success', 'HotelMealRate updated successfully');
	                $this->redirect(array('index'));
	            }
			}

			$this->render('update', compact('model'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = HotelMealRate::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
