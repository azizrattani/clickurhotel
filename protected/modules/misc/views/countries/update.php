<?php
/* @var $this CountriesController */
/* @var $model Countries */

$this->pageTitle = 'Countries - Update';
$this->breadcrumbs = array(
	'Countries' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>