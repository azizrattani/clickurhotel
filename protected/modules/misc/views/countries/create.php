<?php
/* @var $this CountriesController */
/* @var $model Countries */

$this->pageTitle = 'Countries - Create';
$this->breadcrumbs = array(
	'Countries' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>