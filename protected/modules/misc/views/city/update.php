<?php
/* @var $this CityController */
/* @var $model City */

$this->pageTitle = 'Cities - Update';
$this->breadcrumbs = array(
	'Cities' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>