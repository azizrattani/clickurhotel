<?php
/* @var $this CityController */
/* @var $model City */

$this->pageTitle = 'Cities - View';
$this->breadcrumbs=array(
	'Cities' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		array('name'=>'country_id','value'=>$model->country->country),
		'title',
		
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>