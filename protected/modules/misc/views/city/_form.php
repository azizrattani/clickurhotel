<?php
/* @var $this CityController */
/* @var $model City */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'city-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'country_id'); ?>
        <?= $form->dropDownList($model, 'country_id', CHtml::listData(Countries::model()->findAll(array("condition"=>'deleted = 0')), 'id', 'country'), array('empty'=>'--Select a Country--', 'class' => 'form-control mselect'));?>
		
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'title'); ?>
		<?= $form->textField($model, 'title', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
