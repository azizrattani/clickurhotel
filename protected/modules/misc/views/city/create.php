<?php
/* @var $this CityController */
/* @var $model City */

$this->pageTitle = 'Cities - Create';
$this->breadcrumbs = array(
	'Cities' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>