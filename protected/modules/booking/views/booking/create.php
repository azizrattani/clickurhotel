<?php
/* @var $this BookingController */
/* @var $model Booking */

if ($output["btype"] == "request"){
	$bprint = "Request";	
}else{
	$bprint = "Booking";	
}

$this->pageTitle = 'Your '.$bprint;
$this->breadcrumbs = array(
	'Bookings' => array('index'),
	'Confirm '.$bprint,
);
?>

<style>
.border{
	border: 1px solid #999999;
	padding: 3px 10px;
	margin:10px 0;
}

</style>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'booking-form',
)); 

$hotel = $output["hotel"];
?>

<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>


<div class="col-lg-12 border">
	
    <div class="col-lg-2 hidden">
        <img src="/hotel_images/13/Dallah-Taibah-2.jpg" alt="<?=$hotel["name"]?>" width="90" height="90" border="0" />
    </div>
    
    <div class="col-lg-12">
        <h3><?=$hotel["name"]?></h3>
                    
    </div>
	
    <div class="col-lg-3">
        <label for="Partners_name" class="required">City </label>
        <p><?=$output["city"]?></p>
    
    
    </div>
     <div class="col-lg-3">
        <label for="Partners_name" class="required">Check In Date </label>
        <p><?= date("d F, Y",strtotime($output["checkin"]))?></p>
    
    
    </div>
    
     <div class="col-lg-3">
        <label for="Partners_name" class="required">Check Out Date </label>
        <p><?= date("d F, Y",strtotime($output["checkout"]))?></p>
    
    
    </div>
    
    <div class="col-lg-3">
        <label for="Partners_name" class="required">Total Nights</label>
        <p><?=$output["totalDays"]?></p>
    
    
    </div>
    
</div>

<div class="col-lg-12 border">

	<h4><?=$bprint?> Details</h4>
    
    <table width="100%" cellpadding="0" cellspacing="0" id="hoteldetails">
        <tbody>
          
        
          <tr>
            <td  height="29" class="txt_copyright"><div align="left"><strong>Room Name </strong></div></td>
            <td  class="txt_copyright"><div align="left"><strong># of Room </strong></div></td>
            <td  class="txt_copyright"><div align="left"><strong>Meal Type </strong></div></td>
            <td  class="txt_copyright"><div align="left"><strong>Meal Price </strong></div></td>
            <td class="txt_copyright"><div align="left"><strong> Price </strong></div></td>
          </tr>
          
            <?php  $amt = 0;
            foreach ($output["rooms"] as $room){
            
                  	$amt += ($room["roomPricer"] * $room["noofroomr"]) + $room["mealPrice"];
              ?>
              <tr>
                <td class="txt_desitnation_text"><?php  echo $room["data"]["title"]?></td>
                <td class="txt_desitnation_text"><?php  echo $room["noofroomr"]?></td>
                <td class="txt_desitnation_text"><?php  echo $room["mealMessage"]?></td>
                <td class="txt_desitnation_text"><?php  echo $room["mealPrice"]?></td>
                <td width="30%" class="txt_desitnation_text"><strong>SR</strong>																 <?php  echo (($room["roomPricer"] * $room["noofroomr"]) + $room["mealPrice"])?></td>
                
              </tr>
              <tr>
                <td colspan="6"><img src="images/spacer.gif" width="5" height="5" /></td>
              </tr>
           <?php  }
          ?>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" class="txt_desitnation_text"><table width="100%" cellpadding="0" cellspacing="0" id="hoteldetails">
              <tbody>
                <tr>
                  <td width="25%"><div class="txt_copyright"><strong>Total price: </strong></div></td>
                  <td width="75%" class="txt_desitnation_text"><strong>SR</strong> <span id="s_cost_with_addons" tc="24425.31">
                    <?php  echo $amt?>
                  </span> </td>
                </tr>
              </tbody>
            </table></td>
            </tr>
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
        </tbody>
      </table>

</div>


<?php

if ($bookingLimit >=$amt || Yii::app()->user->partner['parent_id'] == 0){
?>

	<input type="hidden" name="Booking[btype]" value="<?=$output["btype"]?>" />
    <input type="hidden" name="Booking[book_request]" value="yes" />
	
    <div class="form-group">
		<?= $form->labelEx($model,'full_guest_name'); ?>
		<?= $form->textField($model, 'full_guest_name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>
    
    <div class="form-group hidden">
		<?= $form->labelEx($model,'phone'); ?>
		<?= $form->textField($model, 'phone', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>
    
    <div class="form-group hidden">
		<?= $form->labelEx($model,'email'); ?>
		<?= $form->textField($model, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

    <input type="hidden" name="Booking[nationality]" value="<?=$_POST["nationality"]?>">
    

    
    <div class="box-footer">
		<a href="<?= $this->createUrl('reservation'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php
}else{
?>
<div class="clearfix"></div>
<div class="alert alert-warning">
  <strong>Warning!</strong> Insufficient booking limit, please contact sale office.
</div>



<?php
}
?>

<?php $this->endWidget(); ?>

