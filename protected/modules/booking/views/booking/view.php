<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->pageTitle = 'Bookings - View';
$this->breadcrumbs=array(
	'Bookings' => array('index'),
	'View',
);
?>

<table class="table table-striped table-bordered" id="yw0">
	<tbody>
    	
		<tr class="even"><th>Booking Ref#</th><td><?=$model->id?></td></tr>
        <tr class="odd"><th>Booking Type</th><td><?= ($model->btype == "book") ? "Definite" : "Request";?></td></tr>
        <tr class="even"><th>Booking Name</th><td><?=$model->full_guest_name?></td></tr>
        <tr class="odd"><th>Guest Nationality</th><td><?=$model->nationality?></td></tr>
        <tr class="even"><th>Guest Phone</th><td><?=$model->phone?></td></tr>
        <tr class="odd"><th>Guest Email</th><td><?=$model->email?></td></tr>

		<tr class="even"><th colspan="2" style="margin:15px 0;height: 50px;vertical-align: middle;"><h3>Hotel Detail</h3></th></tr>


        <tr class="even"><th>Hotel Name</th><td><?=$model->hotel->name?></td></tr>
        <tr class="odd"><th>City Name</th><td><?=$model->hotel->city?></td></tr>
        <tr class="even"><th>Booking Date</th><td><span class="null"><?=date("d F, Y",strtotime($model->created))?></span></td></tr>
        <tr class="odd"><th>Booking Checkin Date</th><td><?=date("d F, Y",strtotime($model->checkin))?></td></tr>
        <tr class="even"><th>Booking Checkout Date</th><td><span class="null"><?=date("d F, Y",strtotime($model->checkout))?></span></td></tr>
        <tr class="odd"><th># of Night</th><td><?=$model->noofnight?></td></tr>

		<tr class="even"><th colspan="2" style="margin:15px 0;height: 50px;vertical-align: middle;"><h3>Booking Details</h3></th></tr>


        <tr class="even">
        
            <table class="table table-striped table-bordered" id="yw0">
                <thead>
                    <tr >
                        <th>Room Name</th>
                        <th># of Room Book</th>
                        <th>Room Book Price</th>
                        <th>Meals</th>
                        <th>Meal Price</th>
                    </tr>
                </thead>
                
                
                <tbody>
                
                	<?php
                    $sql = "SELECT br.`room_id`,COUNT(br.room_rate) AS totalRoom,SUM(br.room_rate) AS rate, br.no_of_room,hr.`title`,br.id as booking_room_id,br.breakfast,br.lunch,br.breakfast,br.lunch,br.dinner FROM `booking_rooms` br  INNER JOIN `hotel_rooms` hr ON br.`room_id` = hr.`id` WHERE br.`booking_id` = ".$model->id." GROUP BY br.`room_id`;";
					$data = Yii::app()->db->createCommand($sql)->queryAll();
					
					$totalPayment = 0;

					foreach($data as $room){

                        $meal = "";
                        $mealPrice = 0;
                        $MealRate = BookingMealRate::model()->findAll(["condition"=>'booking_room_id = "'.$room["booking_room_id"].'"']);


                        $mealMsg = "";
                        foreach($MealRate as $meals){

                            $mealKey = $meals["meal_type"];
                            $priceMeal = $meals["price"] * $room["totalRoom"];


                            if($priceMeal <= 0){
                                continue;
                            }
                            $mealArr = explode("_",$mealKey);

                            $msg = "";

                            foreach($mealArr as $name){

                                $msg .= strtoupper($name);

                            }

                            $mealMsg .= $msg."/";
                            $mealPrice += $priceMeal;


                        }



                        /*if (isset($room["breakfast"]) && $room["breakfast"] >=0){
                            $meal .= "BB/";
                            $mealPrice += $room["breakfast"] * $model->noofnight ;
                        }

                        if (isset($room["lunch"]) && $room["lunch"] >=0){
                            $meal .= "L/";
                            $mealPrice += $room["lunch"] * $model->noofnight;
                        }

                        if (isset($room["dinner"]) && $room["dinner"] >=0){
                            $meal .= "D/";
                            $mealPrice += $room["dinner"] * $model->noofnight;
                        }*/

                        $meal = substr($mealMsg,0,-1);

                        if ($meal == ""){
                            $meal = "Room Only";
                        }

                        $rRate = $model->getRoomRateTotal($room["room_id"]);

                        $price = $rRate * $room["no_of_room"];

                        $totalPayment += $price + $mealPrice;
						
						
						
					
					?>
                    <tr >
                        <td><?=$room["title"]?></td>
                        <td><?=$room["no_of_room"]?></td>
                        <td><?=$price?></td>
                        <td><?=$meal?></td>
                        <td><?=($mealPrice>0)? number_format($mealPrice,2) : "N/A"?></td>
                    </tr>
                    
                    <?php
					}
					?>
                    
                    
                    
                    <tr >
                    	<th colspan="2"></th>
                        <th colspan="2">Total Booking Price</th>
                        
                        <td><?=$totalPayment?></td>
                    </tr>
                </tbody>
                
            </table>
            
        
        </tr>

	</tbody>
</table>






<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>