<?php
if($model->staff->image != ""){
		
		$logo = Yii::app()->createAbsoluteUrl("images/partner/".$model->staff->id."/".$model->staff->image);
		
	}else{
		$logo = Yii::app()->createAbsoluteUrl(Yii::app()->user->logo);
	}
?><html>
<head>
</head>
<body>
	
        
        <h2>Request Cancelled</h2> <br> <br>
        
    
    </center>
	
<p style="font-family:Arial; font-size:15px;"><strong>Date : <?=date("d F, Y")?></strong></p>
	<p style="font-family:Arial; font-size:15px;"><strong>Subject : Request  Cancelled </strong></p>
	<br />
    
    
	<p style="font-family:Arial; font-size:15px;">Assalamo Alaykum</p>
    
    <p style="font-family:Arial; font-size:15px;">Your request is in process we try our best to inform about result ASAP.</p>
    
    
    <div>
    
    <p style="font-family:Arial; font-size:15px; float:left;">Guest: <?=$model->full_guest_name?></p> 	<p style="font-family:Arial; font-size:15px; float:left;">Nationality: <?=$model->nationality?></p>
    <p style="font-family:Arial; font-size:15px; float:right;">Reference no: <?=$model->id?></p>
    
    </div>
    
<div style="clear:both"></div>
    
    <table width="100%" border="1" cellpadding="0" cellspacing="0" class="table table-striped table-bordered" id="yw0">
    <thead>
    	<tr>
        	
            
            <th width="18%">Hotel</th>
            <th width="19%" height="50">Check-In</th>
            <th width="18%">Check-Out</th>
            <th width="7%">Room(s)</th>
            <th width="13%">Room Type</th>
            <th width="8%">Meals</th>
            <th width="17%">City</th>
        	<th width="17%">Purchase Price</th>
        </tr>
    
    </thead>
    
    
	<tbody>
    
    <?php
                    $sql = "SELECT SUM(br.room_rate) AS rate, br.no_of_room,hr.`title`,br.breakfast,br.lunch,br.dinner,br.hbase_price FROM `booking_rooms` br  INNER JOIN `hotel_rooms` hr ON br.`room_id` = hr.`id` WHERE br.`booking_id` = ".$model->id." GROUP BY br.`room_id`;";
					$data = Yii::app()->db->createCommand($sql)->queryAll();
					
					$totalPayment = 0;
					
					foreach($data as $room){


						$meal = "";
						$mealPrice = 0;
						$MealRate = BookingMealRate::model()->findAll(["condition"=>'booking_room_id = "'.$room["booking_room_id"].'"']);


						foreach($MealRate as $meal){

							$mealKey = $meal["meal_type"];
							$mealPrice = $meal["price"];

							if($mealPrice <= 0){
								continue;
							}
							$mealArr = explode("_",$mealKey);

							$mealMsg = "";

							foreach($mealArr as $name){

								$msg .= strtoupper($name);

							}

							$mealMsg .= $msg."/";
							$mealPrice += $mealPrice;


						}



						/*if (isset($room["breakfast"]) && $room["breakfast"] >=0){
							$meal .= "BB/";
							$mealPrice += $room["breakfast"] * $model->noofnight ;
						}

						if (isset($room["lunch"]) && $room["lunch"] >=0){
							$meal .= "L/";
							$mealPrice += $room["lunch"] * $model->noofnight;
						}

						if (isset($room["dinner"]) && $room["dinner"] >=0){
							$meal .= "D/";
							$mealPrice += $room["dinner"] * $model->noofnight;
						}*/

						$meal = substr($mealMsg,0,-1);

						if ($meal == ""){
							$meal = "Room Only";
						}


						$price = $room["rate"] * $room["no_of_room"];

						$totalPayment += $price + $mealPrice;
					
					?>
                    
        
        
                        <tr>
                            <td align="center"><?=$model->hotel->name?></td>
                            <td height="50" align="center"><?= date("d F, Y",strtotime($model->checkin))?></td>
                            <td align="center"><?= date("d F, Y",strtotime($model->checkout))?></td>
                            <td align="center"><?=$room["no_of_room"]?></td>
                            <td align="center"><?=$room["title"]?></td>
                            <td align="center"><?=$meal?></td>
                            <td align="center"><?=$model->hotel->city?></td>
                        	<td align="center"><?=$room["hbase_price"]?></td>
                        </tr>
        
        
         <?php
					}
					?>
		

	</tbody>
</table>

<br />


	<p><strong>Best Regards,</strong></p>
	
    <p style="font-family:Arial; font-size:15px;"><strong style="font-weight:bold;">Cancellation policy applied:</strong>  <?=$message;?></p>
    
	<p>Note: This is an automatic system generated document. There is no need for any signature</p>
	
</body>
</html>