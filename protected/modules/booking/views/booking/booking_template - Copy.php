<?php

?><html>
<head>
</head>
<body>
	<img src="http://clickurhotel.ideabox.online/themes/admin/img/logo.png" class="logo" />
	<p style="font-family:Arial; font-size:15px;">Dear <?= $model->full_guest_name?>,</p>
	<p style="font-family:Arial; font-size:15px;">Please find the booking details as follow</p>
	<br />
	
    
    <table class="table table-striped table-bordered" id="yw0">
	<tbody>
    	
		<tr class="even"><th>Booking Ref#</th><td><?=$model->id?></td></tr>
        <tr class="odd"><th>Booking Type</th><td><?= ucwords($model->btype)?></td></tr>
        <tr class="even"><th>Booking Name</th><td><?=$model->full_guest_name?></td></tr>
        <tr class="odd"><th>Guest Nationality</th><td><?=$model->nationality?></td></tr>
        <tr class="even"><th>Guest Phone</th><td><?=$model->phone?></td></tr>
        <tr class="odd"><th>Guest Email</th><td><?=$model->email?></td></tr>

		<tr class="even"><th colspan="2" style="margin:15px 0;height: 50px;vertical-align: middle;"><h3>Hotel Detail</h3></th></tr>


        <tr class="even"><th>Hotel Name</th><td><?=$model->hotel->name?></td></tr>
        <tr class="odd"><th>City Name</th><td><?=$model->hotel->city?></td></tr>
        <tr class="even"><th>Booking Date</th><td><span class="null"><?=date("d F, Y",strtotime($model->created))?></span></td></tr>
        <tr class="odd"><th>Booking Checkin Date</th><td><?=date("d F, Y",strtotime($model->checkin))?></td></tr>
        <tr class="even"><th>Booking Checkout Date</th><td><span class="null"><?=date("d F, Y",strtotime($model->checkout))?></span></td></tr>
        <tr class="odd"><th># of Night</th><td><?=$model->noofnight?></td></tr>

		<tr class="even"><th colspan="2" style="margin:15px 0;height: 50px;vertical-align: middle;"><h3>Booking Details</h3></th></tr>


        <tr class="even">
        
            <table class="table table-striped table-bordered" id="yw0">
                <thead>
                    <tr >
                        <th>Room Name</th>
                        <th># of Room Book</th>
                        <th>Room Book Price</th>
                    </tr>
                </thead>
                
                
                <tbody>
                
                	<?php
                    $sql = "SELECT SUM(br.room_rate) AS rate, br.no_of_room,hr.`title` FROM `booking_rooms` br  INNER JOIN `hotel_rooms` hr ON br.`room_id` = hr.`id` WHERE br.`booking_id` = ".$model->id." GROUP BY br.`room_id`;";
					$data = Yii::app()->db->createCommand($sql)->queryAll();
					
					$totalPayment = 0;
					
					foreach($data as $room){
						
					
						
						$price = $room["rate"] * $room["no_of_room"];
						
						$totalPayment += $price;
					
					?>
                    <tr >
                        <td><?=$room["title"]?></td>
                        <td><?=$room["no_of_room"]?></td>
                        <td><?=$price?></td>
                    </tr>
                    
                    <?php
					}
					?>
                    
                    
                    
                    <tr >
                        <th colspan="2">Total Booking Price</th>
                        
                        <td><?=$totalPayment?></td>
                    </tr>
                </tbody>
                
            </table>
            
        
        </tr>

	</tbody>
</table>

<br />

	<p style="font-family:Arial; font-size:15px;"><br><br>
    
    Thank you,<br />
    
	Team ClickUrHotel.com</p>
</body>
</html>