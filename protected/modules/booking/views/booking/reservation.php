<?php
/* @var $this BookingController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Reservation';
$this->breadcrumbs = array(
	'Bookings',
    'Reservation'
);
?>

<style>

/* DivTable.com */
.divTable{
	display: table;
	width: 100%;
}
.divTableRow {
	display: table-row;
}
.divTableHeading {
	background-color: #EEE;
	display: table-header-group;
}
.divTableCell, .divTableHead {
	border: 1px solid #999999;
	display: table-cell;
	padding: 3px 10px;
}
.divTableHeading {
	background-color: #EEE;
	display: table-header-group;
	font-weight: bold;
}
.divTableFoot {
	background-color: #EEE;
	display: table-footer-group;
	font-weight: bold;
}
.divTableBody {
	display: table-row-group;
}

.border{
	border: 1px solid #999999;
	padding: 3px 10px;
}

</style>


<div class="box-body table-responsive no-padding">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'hotel-room-rate-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />
    
    <div class="form-group col-lg-4">
        <label for="Hotel_city" class="required">City <span class="required">*</span></label>
        <select class="form-control" name="Reservation[city]" id="city">
    		<option value="Makkah" <?php if (isset($_POST["Reservation"]["city"]) && $_POST["Reservation"]["city"] == "Makkah"){ echo " selected";}?>>Makkah</option>
    		<option value="Madina" <?php if (isset($_POST["Reservation"]["city"]) && $_POST["Reservation"]["city"] == "Madina"){ echo " selected";}?>>Madina</option>
    	</select>
    </div>

	<?php
	$cityName = (isset($_POST["Reservation"]["city"])) ? $_POST["Reservation"]["city"] : 'Makkah';
	?>

	<div class="form-group col-lg-4">
		<label for="Hotel_city" class="required">Hotel <span class="required"></span></label>
		<?= $form->dropDownList($model, 'hotel_id', CHtml::listData(Hotel::model()->findAll(array("condition"=>"deleted=0 and city='".$cityName."' ","order"=>" `sort_order` asc")), 'id', 'name'), array('empty'=>'--Any Hotel--','class' => 'form-control mselect'));?>
	</div>


	<div class="form-group col-lg-4">
		<label for="Hotel_city" class="required">Room <span class="required"></span></label>
		<select id="HotelRoomRate_room_id" name="Reservation[room_id]" class="form-control mselect" tabindex="-1" aria-hidden="true">
			<option value="">--Any Room--</option>
		</select>
	</div>
    
    <div class="form-group col-lg-4">
		<label for="checkin" class="required">Check-in Date <span class="required">*</span></label>
        <?php
		$dt = strtotime("+3 day");
		if (isset($_POST["Reservation"]["checkin"])){
			$checkinArr = explode("/",$_POST["Reservation"]["checkin"]);			
			$checkin = $checkinArr[1] . "/" . $checkinArr[0] . "/" . $checkinArr[2];
			
			$value=Yii::app()->dateFormatter->format("d/M/y",strtotime($_POST["Reservation"]["checkin"]));
		}else{
			
			$value=Yii::app()->dateFormatter->format("d/M/y",$dt);
		}
		 
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'Reservation[checkin]',
								
                                'id'=>'checkin',
                            	'value'=>$value,
                                'options'=>array(	
								'dateFormat'=>'dd/mm/yy',							
                                'showAnim'=>'fold',
								'minDate'=>date("d/m/Y",$dt),
                                ),
								
                                'htmlOptions'=>array(
								
                                'display'=>'block;',
								'class' => 'form-control',
                                ),
                        ));
		?>
		
	</div>
    
    
    <div class="form-group col-lg-4">
		<label for="checkout" class="required">Check-out Date <span class="required">*</span></label>
        <?php
		$dt = strtotime("+4 day");
		if (isset($_POST["Reservation"]["checkout"])){
			
			//$checkoutArr = explode("/",$_POST["Reservation"]["checkout"]);			
			//$checkout = $checkoutArr[1] . "/" . $checkoutArr[0] . "/" . $checkoutArr[2];
			
			$value=Yii::app()->dateFormatter->format("d/M/y",strtotime($_POST["Reservation"]["checkout"]));
		}else{
			
			$value=Yii::app()->dateFormatter->format("d/M/y",$dt);
		}
		 
		
		 
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'Reservation[checkout]',
								
                                'id'=>'checkout',
                            	'value'=>$value,
                                'options'=>array(	
								
								'dateFormat'=>'dd/mm/yy',								
                                'showAnim'=>'fold',
								'minDate'=>date("d/m/Y",$dt),
                                ),
								
                                'htmlOptions'=>array(
                                'display'=>'block;',
								'class' => 'form-control',
                                ),
                        ));
		?>
		
	</div>

	<div class="form-group col-lg-4">
		<?= $form->labelEx($model,'nationality'); ?>

		<?php

		if(isset($_COOKIE["nationality"])){

			$model->nationality = $_COOKIE["nationality"];
		}

		?>
		<?= $form->dropDownList($model, 'nationality', CHtml::listData(Countries::model()->findAll(array("condition"=>"deleted=0","order"=>" `order` asc")), 'country', 'country'), array('empty'=>'--Select a Nationality--','class' => 'form-control mselect', 'required'=>true));?>
	</div>
    
    
    <div class="box-footer  col-lg-12">
            <input class="btn btn-primary pull-right" type="submit" name="yt0" value="Search">
    </div>
    
    <?php $this->endWidget(); ?>
    
    <div class="clearfix"></div>
    
    <?php
    
	if ($output){
		
		foreach($output as $hotel){
			
			if(count($hotel["rooms"])==0)
				continue;
		
		?>
    
            <!-- Hotel Start -->
            
            <form id="hotel_<?=$hotel["id"]?>" action="create" method="post" class="booking">
            
            	<input type="hidden" name="checkin" value="<?=$_POST["Reservation"]["checkin"]?>" />
                <input type="hidden" name="checkout" value="<?=$_POST["Reservation"]["checkout"]?>" />
                <input type="hidden" name="city" value="<?=$_POST["Reservation"]["city"]?>" />
            
                <div class="col-lg-12 border">
                
                    <div class="col-lg-2 hidden">
                        <img src="/hotel_images/13/Dallah-Taibah-2.jpg" alt="<?=$hotel["name"]?>" width="90" height="90" border="0" />
                    </div>
                    
                    <div class="col-lg-12">
                        <h3><?=$hotel["name"]?> <?php for($i=1; $i<=$hotel["hotel_star"]; $i++){?><img src="/images/start.png" alt="" /><?php }?></h3>
                                    <p><?=$hotel["short_desc"]?></p>
                                    
                    </div>
                    
                    
                    <div class="col-lg-2 hidden"></div>
                    
                    <div class="col-lg-12">
                    
                        <?php
						/*LOAD A Fonts*/
						//debug($hotel["rooms"] ,true);
						
                    foreach($hotel["rooms"] as $room){
						$booking = false;
						
                        ?>
                        
                         <?php
						 
						 			$checkin = date("Y-m-d",strtotime($_POST["Reservation"]["checkin"]));
						 			$checkout = date("Y-m-d",strtotime($_POST["Reservation"]["checkout"]));

									$bookRequestDays =Yii::app()->functions->getDays(date("Y-m-d"),$checkin);


									
									
									$roomBook = ($room["maxBooking"] > 9) ? 9 : $room["maxBooking"];
									
									$bookingAva = "true";
									
									if ($roomBook <=0){
										$bookingAva = "false";
										$roomBook = 15;
									}else{
										$booking = true;	
									}
									
									
									if ($bookRequestDays <=1){

										$booking = false;	
									}elseif($checkin == $checkout){
										$booking = false;
									}
                                	
									
								?>
                        
                        
                        <div class="col-lg-12" style="margin:10px 0">
                        
                            <div class="col-lg-4"><?=$room["title"]?><?php if($bookingAva == "false"){ echo " **";}?></div>
                            <div class="col-lg-2"><?php for($i=1; $i<=$room["max_user"]; $i++){?><img src="/images/persons.jpg" alt="" width="10" height="10" /><?php }?> </div>
                            <div class="col-lg-3">For <?=$room["totalDays"]?> night will be SR. <?=$room["price"]//*$room["totalDays"]?></div>
                            <div class="col-lg-3">
                            	
                               
                               <div id="priceBreakup-<?=$room["id"]?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">
                                
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">(<?=$room["title"]?>) Price Breakdown</h4>
                                      </div>
                                      <div class="modal-body">
                                        
                                        <div class="box-body table-responsive no-padding">
                                        	<div id="partners-grid" class="grid-view">
                                        
                                                <table class="table table-condensed table-striped table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                        	<th id="" width="33%">Date</th>
                                                        	<th id=""  width="33%">Day</th>
                                                            <th id="" width="33%">Price</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
													if(isset($room["roomRate"])){
														foreach($room["roomRate"] as $bookDate => $bookRate){
														?>
															<tr>
																<td><?=date("d F, Y",strtotime($bookDate))?></td>
																<td><?= $bookRate["day"]?></td>
																<td><?= number_format($bookRate["price"],2)?></td>
															</tr>
													   <?php
														}
													}
												   ?>
                                                    </tbody>
                                                </table>
                                            <div class="keys" style="display:none" title="/partner/partners/index"></div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                
                                  </div>
                                </div>
                            
                            	<input type="hidden" name="totalDays" value="<?=$room["totalDays"]?>" />
                                <input id="roomBPricer[]" name="roomBPricer[]" type="hidden" value="<?=$room["bprice"]?>" />
                                
                                
                                <input id="roomPricer[]" name="roomPricer[]" type="hidden" value="<?=$room["price"]?>" />
                                <input id="roomBy[]" name="roomBy[]" type="hidden" value="<?=$room["createBy"]?>" />
                                                            
                                <input name="roomr[]" type="hidden" value="<?=$room["id"]?>" />
                                 <input name="bookingAva[]" type="hidden" class="bookingAva" value="<?=$bookingAva?>" />



                                <select name="noofroomr[]" id="noofroomr" class="noofroomr">
                                    <option value="">Select no of Room</option>
                                    
                                    <?php
									
									
                                    
                                    
                                    for($c=1; $c <= $roomBook; $c++){
                                    ?>
                                        <option value="<?=$c?>"><?=$c?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <br /> <a class="link_loginlinks" style="cursor:pointer;" data-toggle="modal" data-target="#priceBreakup-<?=$room["id"]?>"><strong>View Price Breakup</strong></a>
                            </div>
                        
                        
                        </div>
                    <?php
                    }
                    ?>

						<input name="nationality" type="hidden" class="bookingAva" value="<?=$model->nationality?>" />
                    
                    </div>
                    
                    
                    <?php

					if (isset($model->nationality)){

						$country = Countries::model()->findByAttributes(["country" => $model->nationality]);

						$countryId = $country->id;

						$marketType = MarketType::model()->findAll(["condition"=>"countries like '%".$countryId.",%'"]);
						$nationality = [];

						foreach($marketType as $ke => $va){
								$nationality[] = $va->id;
						}


						$marketType = MarketType::model()->findAll(["condition"=>"countries IS NULL"]);
						$nationalityAll = [];

						foreach($marketType as $ke => $va){
							$nationalityAll[] = $va->id;
						}

					}
                    
					$marketTypeArr = explode(",",$hotel["market_type"]);


					$match=array_intersect($nationality,$marketTypeArr);

					if (count($match)>0){
						$markets = $match;
					}else{

						$match=array_intersect($nationalityAll,$marketTypeArr);
						if (count($match)>0){
							$markets = $match;
						}else{
							$markets = [];
						}
					}

					$meal_option = $hotel["meal_option"];
					$meal_type = $hotel["meal_type"];
					

					
					if ($meal_option == "2"){ ?>

                    	<div class="col-lg-12 meal_rate" style="margin-bottom:10px;">

						<?php
                           //$markets = explode(",",$market_type);



                            $hotel_breakfast = $hotel["breakfast_rate"];
                            $hotel_lunch = $hotel["lunch_rate"];
                            $hotel_dinner = $hotel["dinner_rate"];


                            foreach($markets as $market ){

                                $marketData = MarketType::model()->findByPk($market);

                        ?>


                                <div class="clearfix"></div>
                                <h3><?=$marketData->title?></h3>
                                <div class="col-lg-12 text-left" style="margin-bottom:10px;">

                                    <?php

									//debug($meal_type);
                                    $meals = explode(",",$meal_type);



                                    $HotelMealRate = HotelMealRate::model()->find(array("condition"=>' deleted=0 and hotel_id='.$hotel["id"].' and market_id='.$market.' and "'.$checkin.'" between date_from and date_to',"order"=>"id desc"));

									$title2 = "";


                                    foreach($meals as $meal){




										$mealType = Mealstype::model()->find(["condition" =>"id = '".$meal."'"]);
                                        if ($HotelMealRate){



											if ($meal == "1" || $meal == 11){
                                                $price = "Free";
                                                $title = strtolower(str_replace(" ","_",$mealType->title));//"room_only";
                                                $id = $meal;

                                            }else{

												//debug($HotelMealRate);

												$HotelMealRateMeal = HotelMealRateMeal::model()->find(array("condition"=>' hotel_meal_rate='.$HotelMealRate->id.' and meal_type_id='.$meal.'',"order"=>"id desc"));
												$price = $HotelMealRateMeal["rate"];

												$title = strtolower(str_replace(" ","_",$mealType->title));
												$id = $meal;

											}

											//debug($meal . " " . $title . " " . $price);


											if ($meal == "1"  || $meal == 11){
												$price = "Free";
												$title = strtolower(str_replace(" ","_",$mealType->title));//"room_only";
												$title2= " ( Free )";
												$id = $meal;

											}elseif ($meal == "2" && $price <=0 ){

												$price = $hotel_breakfast;
												$title = strtolower(str_replace(" ","_",$mealType->title));//"breakfast";
												$id = $meal;

											}elseif ($meal == "3" && $price <=0 ){

												$price = $hotel_lunch;
												$title = strtolower(str_replace(" ","_",$mealType->title));//"lunch";
												$id = $meal;

											}elseif ($meal == "4" && $price <=0 ){

												$price = $hotel_dinner;
												$title = strtolower(str_replace(" ","_",$mealType->title));//"dinner";
												$id = $meal;

											}


											

                                            if($price != "Free")
                                                $title2= " ( SR. ".$price." )";


                                        }else{

                                            if ($meal == "1"  || $meal == 11){
                                                $price = "Free";
                                                $title = strtolower(str_replace(" ","_",$mealType->title));//"room_only";
                                                $title2= " ( Free )";
                                                $id = $meal;

                                            }elseif ($meal == "2" ){

                                                $price = $hotel_breakfast;
                                                $title = strtolower(str_replace(" ","_",$mealType->title));//"breakfast";
                                                $id = $meal;

                                            }elseif ($meal == "3" ){

                                                $price = $hotel_lunch;
                                                $title = strtolower(str_replace(" ","_",$mealType->title));//"lunch";
                                                $id = $meal;

                                            }elseif ($meal == "4" ){

                                                $price = $hotel_dinner;
                                                $title = strtolower(str_replace(" ","_",$mealType->title));//"dinner";
                                                $id = $meal;

                                            }else{

												continue;
											}

                                            if($price != "Free")
                                                $title2= " ( SR. ".$price." )";

                                        }


										if (($price != "Free" && $price <=0)){ // && trim(strtolower($title)) != "room only"
											continue;
										}


                                    ?>

                                    <div class="col-lg-3 text-left meals">
                                        <input type="checkbox" id="<?=$title?>" value="<?=$price?>" class="MealRadio" name="Meal[<?=$title?>]"  /> <?=strtoupper(str_replace("_"," ",$title)). " " . $title2?>
                                    </div>


                                    <?php
                                    }
                                    ?>

                                </div>




                        <?php

                            }
						?>
                        </div>
					<?php
					}

					?>
                    
                    
                   
                    
                
                	<div class="col-lg-4 text-left" style="margin-bottom:10px;"><input type="submit" class="request" name="request" value="Request now"/></div>
                    <div class="col-lg-4 text-left" style="margin-bottom:10px;"><input type="reset" class="reset" name="reset" value="Reset Booking"/></div>
                    <?php

                    if($booking){
					?>
                    <div class="col-lg-4 text-right" style="margin-bottom:10px;"><input type="submit" class="book"  name="book" value="Book now"/></div>
                    <?php
					}
					?>
                    
                     <div class="col-lg-12">** Not available for booking</div>
                    
                
                </div>
            
            </form>
            <!-- Hotel Ends -->

    
    <?php
	
		}
	}
	?>
    
<script>

$("#city").change(function(e) {
		$val = $(this).val();
		if ($val != ""){


			url  = "<?= $this->createUrl('/hotel/hotel/hotelByCity/city/') ?>/"+$val;
			$("#Booking_hotel_id").select2('destroy');
			$("#Booking_hotel_id option").each(function() {
				$(this).remove();
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){
					data = $.parseJSON(result);
					if(data){
						$('#Booking_hotel_id').append('<option value="">Any Hotel</option>');
						$.each(data,function(index,value){

							$('#Booking_hotel_id').append('<option value="'+value.id+'">'+value.name+'</option>');

						});
						$("#Booking_hotel_id").select2({
							placeholder: "Any  Hotel"
						});
						//$(".roomId").removeClass("hidden");
					}

				}
			});

		}else{
			$("#Booking_hotel_id").select2('destroy');
			$("#Booking_hotel_id option").each(function() {
				$(this).remove();
			});
			$("#Booking_hotel_id").select2({
				placeholder: "Any Hotel"
			});
		}



	});

$("#Booking_hotel_id").change(function(e) {
		$val = $(this).val();
		if ($val != ""){


			url  = "<?= $this->createUrl('/hotel/hotel/hotelRooms/id/') ?>/"+$val;
			$("#HotelRoomRate_room_id").select2('destroy');
			$("#HotelRoomRate_room_id option").each(function() {
				$(this).remove();
			});
			$.ajax({
				url: url,
				method: "GET",
				success: function(result){
					data = $.parseJSON(result);
					if(data){
						$('#HotelRoomRate_room_id').append('<option value="">Any Room</option>');
						$.each(data,function(index,value){

							$('#HotelRoomRate_room_id').append('<option value="'+value.id+'">'+value.title+'</option>');

						});

						<?php


						if(isset($_REQUEST["Reservation"]["room_id"]) && $_REQUEST["Reservation"]["room_id"] != ""){

						?>


						$("#HotelRoomRate_room_id").val('<?=$_REQUEST["Reservation"]["room_id"]?>');

						$


						<?php
						}
						?>



						$("#HotelRoomRate_room_id").select2({
							placeholder: "Any  Room"
						});
						//$(".roomId").removeClass("hidden");
					}

				}
			});

		}else{
			$("#HotelRoomRate_room_id").select2('destroy');
			$("#HotelRoomRate_room_id option").each(function() {
				$(this).remove();
			});
			$("#HotelRoomRate_room_id").select2({
				placeholder: "Any Room"
			});
		}



	});


$(".book").click(function(e) {
	
	hotel_id = $(this).parent("div").parents("form").attr("id");
	
	error = false;
	message = "";
	
	$("#"+hotel_id + " select").each(function(index, element) {
		
		bookingAva = $(this).parent("div").children(".bookingAva").val();
		
		roomName = $(this).parent("div").parent("div").children("div:first-child").html();
		
		
        
		if ($(this).val() != "" && bookingAva == "false" ){
			message += "Room " + roomName + " is not available for booking \n";
			
			error = true;
		}
  	});
	
	
	
	
	

	
	if (error){
		
		message += '\n You may send booking request by pressing "request now".'
		alert(message);
		return false;	
	}
	
	
	
	
	
});

$("#hotel-room-rate-form").submit(function(e){

	checkin_date = $("#checkin").val();
	checkout_date = $("#checkout").val();

	if(new Date(checkout_date) <= new Date(checkin_date))
	{
		alert ("Checkout date must be greater than checkin date");
		return false;
	}


})



$(".booking").submit(function(e) {
	
	$found = false
	
	
	hotel_id = $(this).attr("id");

	$("#"+hotel_id + " select").each(function(index, element) {
        
		if ($(this).val() != "")
			$found = true;
  	});
	if (!$found){
		$message = "Please select atlease one room.";	
	}else{
		$message = "";	
	}
    
	if($("#"+hotel_id + " .meal_rate").length){
		
		
		
		if ($("#"+hotel_id + " .meal_rate #room_only").length){
			
			/*$efound = false;
			
			if ($("#"+hotel_id + " .meal_rate #room_only").is(':checked')){
				$efound = true;;
			}
			
			
			if ($("#"+hotel_id + " .meal_rate #breakfast").is(':checked')){
				
				$efound = true;;	
			}
			
			
			if ($("#"+hotel_id + " .meal_rate #lunch").is(':checked')){
				$efound = true;;	
					
			}
			
			if ($("#"+hotel_id + " .meal_rate #dinner").is(':checked')){
				$efound = true;
			}	*/


			$efound = false;

			$(".meals > input").each(function(){

				$mealName = $(this).attr("id");

				if ($("#"+hotel_id + " .meal_rate #"+$mealName).is(':checked')){

					$efound = true;

				}


			})
			
			if (!$efound){
				
				$found = false;
				$message += "\nPlease select Room Only or atleast one Meal from list";
					
			}
			
		}else{
			/*$efound = false;
			if ($("#"+hotel_id + " .meal_rate #breakfast").is(':checked')){
				
				$efound = true;;	
			}
			
			
			if ($("#"+hotel_id + " .meal_rate #lunch").is(':checked')){
				$efound = true;;	
					
			}
			
			if ($("#"+hotel_id + " .meal_rate #dinner").is(':checked')){
				$efound = true;
			}*/

			$efound = false;

			$(".meals > input").each(function(){

				$mealName = $(this).attr("id");

				if ($("#"+hotel_id + " .meal_rate #"+$mealName).is(':checked')){

					$efound = true;

				}


			})

			if (!$efound){
				
				$found = false;
				$message += "\nPlease select atleast one Meal from list";
					
			}
		}
		
			
	}
	
	//if ()
	
	if (!$found){
		alert($message);
		return false;
	}
	
	
});


$(".request").submit(function(e) {
	
	$found = false
	
	
	hotel_id = $(this).attr("id");

	$("#"+hotel_id + " select").each(function(index, element) {
        
		if ($(this).val() != "")
			$found = true;
  	});
	
	if (!$found){
		$message = "Please select atlease one room.";	
	}else{
		$message = "";	
	}
	
	
    
	if($("#"+hotel_id + " .meal_rate").length){
		
		
		
		if ($("#"+hotel_id + " .meal_rate #room_only").length){

			$efound = false;

			$(".meals > input").each(function(){

				$mealName = $(this).attr("id");

				if ($("#"+hotel_id + " .meal_rate #"+$mealName).is(':checked')){

					$efound = true;

				}


			})

			/*$efound = false;
			
			if ($("#"+hotel_id + " .meal_rate #room_only").is(':checked')){
				$efound = true;;
			}
			
			
			if ($("#"+hotel_id + " .meal_rate #breakfast").is(':checked')){
				
				$efound = true;;	
			}
			
			
			if ($("#"+hotel_id + " .meal_rate #lunch").is(':checked')){
				$efound = true;;	
					
			}
			
			if ($("#"+hotel_id + " .meal_rate #dinner").is(':checked')){
				$efound = true;
			}	*/
			
			if (!$efound){
				
				$found = false;
				$message += "\nPlease select Room Only or atleast one Meal from list";
					
			}
			
		}else{

			$efound = false;

			$(".meals > input").each(function(){

				$mealName = $(this).attr("id");

				if ($("#"+hotel_id + " .meal_rate #"+$mealName).is(':checked')){

					$efound = true;

				}


			})


			/*if ($("#"+hotel_id + " .meal_rate #breakfast").is(':checked')){
				
				$efound = true;;	
			}
			
			
			if ($("#"+hotel_id + " .meal_rate #lunch").is(':checked')){
				$efound = true;;	
					
			}
			
			if ($("#"+hotel_id + " .meal_rate #dinner").is(':checked')){
				$efound = true;
			}	*/
			
			if (!$efound){
				
				$found = false;
				$message += "\nPlease select atleast one Meal from list";
					
			}
		}
		
			
	}
	
	//if ()
	
	if (!$found){
		alert( $message);
		return false;
	}





});

(function(){
	$('.MealRadio').on("click",function(){

		alert("aa");

	});
});

$(document).ready(function(){

	<?php


	if(isset($_REQUEST["Booking"]["hotel_id"]) && $_REQUEST["Booking"]["hotel_id"] != ""){

	?>

		//$("#Booking_hotel_id").select2('destroy');

		$("#Booking_hotel_id").val('<?=$_REQUEST["Booking"]["hotel_id"]?>');

		$("#Booking_hotel_id").select2({
			placeholder: "Any Hotel"
		});

		$("#HotelRoomRate_room_id").select2({
			placeholder: "Any Room"
		});

		$("#Booking_hotel_id").trigger("change");


	<?php
	}
	?>





})

</script>
    
    



