<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->pageTitle = 'Bookings - View';
$this->breadcrumbs=array(
	'Bookings' => array('index'),
	'View',
);
?>

<form id="form1" name="form1" method="post" action="">

<table class="table table-striped table-bordered" id="yw0">
	<tbody>
    	<?php
        if($model->btype == "book" && Yii::app()->user->profile['child_id'] != $model->staff_id){
		?>
		<tr class="even"><th>Booking Confirmation#</th><td><input type="text" name="ref_number" value="<?=$model->ref_number?>" /></td></tr>
        <?php
		}elseif($model->btype == "book" && Yii::app()->user->profile['child_id'] == $model->staff_id){
		?>
        <tr class="even"><th>Booking Confirmation#</th><td><?=$model->ref_number?></td></tr>
        <?php
		}
		?>
        <tr class="even"><th>Booking Ref#</th><td><?=$model->id?></td></tr>
        <tr class="odd"><th>Booking Type</th><td><?= ($model->deleted == 1) ? "Cancelled " . (($model->btype == "book") ? "Definite Booking " : "Request") : (($model->btype == "book") ? "Definite" : "Request");?></td></tr>
        <tr class="even"><th>Booking Name</th><td><?=$model->full_guest_name?></td></tr>
        <tr class="odd"><th>Guest Nationality</th><td><?=$model->nationality?></td></tr>
        <tr class="even"><th>Guest Phone</th><td><?=$model->phone?></td></tr>
        <tr class="odd"><th>Guest Email</th><td><?=$model->email?></td></tr>

		<tr class="even"><th colspan="2" style="margin:15px 0;height: 50px;vertical-align: middle;"><h3>Hotel Detail</h3></th></tr>


        <tr class="even"><th>Hotel Name</th><td><?=$model->hotel->name?></td></tr>
        <tr class="odd"><th>City Name</th><td><?=$model->hotel->city?></td></tr>
        <tr class="even"><th>Booking Date</th><td><span class="null"><?=date("d F, Y",strtotime($model->created))?></span></td></tr>
        <tr class="odd"><th>Booking Checkin Date</th><td><?=date("d F, Y",strtotime($model->checkin))?></td></tr>
        <tr class="even"><th>Booking Checkout Date</th><td><span class="null"><?=date("d F, Y",strtotime($model->checkout))?></span></td></tr>
        <tr class="odd"><th># of Night</th><td><?=$model->noofnight?></td></tr>

		<tr class="even"><th colspan="2" style="margin:15px 0;height: 50px;vertical-align: middle;"><h3>Booking Details</h3></th></tr>


        <tr class="even">
        
            <table class="table table-striped table-bordered" id="yw0">
                <thead>
                    <tr >
                        <th>Room Name</th>
                        <th># of Room Book</th>
                        <th>Room Book Price</th>
                        <th>Meals</th>
                        <th>Meal Price</th>
                        <th>Room#</th>
                    </tr>
                </thead>
                
                
                <tbody>
                
                	<?php
                    $sql = "SELECT SUM(br.room_rate) AS rate,COUNT(br.room_rate) AS totalRoom, br.no_of_room,hr.`title`,br.id as booking_room_id,br.breakfast,br.lunch,br.dinner,br.`id` as bRoomId,br.`room_no` FROM `booking_rooms` br  INNER JOIN `hotel_rooms` hr ON br.`room_id` = hr.`id` WHERE br.`booking_id` = ".$model->id." GROUP BY br.`room_id`;";
					$data = Yii::app()->db->createCommand($sql)->queryAll();
					
					$totalPayment = 0;
					
					foreach($data as $room){

                        $meal = "";
                        $mealPrice = 0;
                        $MealRate = BookingMealRate::model()->findAll(["condition"=>'booking_room_id = "'.$room["booking_room_id"].'"']);


                        $mealMsg = "";
                        foreach($MealRate as $meals){

                            $mealKey = $meals["meal_type"];
                            $priceMeal = $meals["price"] * $room["totalRoom"];


                            if($priceMeal <= 0){
                                continue;
                            }
                            $mealArr = explode("_",$mealKey);

                            $msg = "";

                            foreach($mealArr as $name){

                                $msg .= strtoupper($name);

                            }

                            $mealMsg .= $msg."/";
                            $mealPrice += $priceMeal;


                        }



                        /*if (isset($room["breakfast"]) && $room["breakfast"] >=0){
                            $meal .= "BB/";
                            $mealPrice += $room["breakfast"] * $model->noofnight ;
                        }

                        if (isset($room["lunch"]) && $room["lunch"] >=0){
                            $meal .= "L/";
                            $mealPrice += $room["lunch"] * $model->noofnight;
                        }

                        if (isset($room["dinner"]) && $room["dinner"] >=0){
                            $meal .= "D/";
                            $mealPrice += $room["dinner"] * $model->noofnight;
                        }*/

                        $meal = substr($mealMsg,0,-1);
						
						if ($meal == ""){
							$meal = "Room Only";	
						}
						
						
						$price = $room["rate"] * $room["no_of_room"];
						
						$totalPayment += $price + $mealPrice;
					
					?>
                    <tr >
                        <td><?=$room["title"]?></td>
                        <td><?=$room["no_of_room"]?></td>
                        <td><?=$price?></td>
                        <td><?=$meal?></td>
                        <td><?=($mealPrice>0)? number_format($mealPrice,2) : "N/A"?></td>
                        <?php
                        if($model->btype == "book" && Yii::app()->user->profile['child_id'] != $model->staff_id){
						?>
                        <td><input type="text" name="roomNo[]" value="<?=$room["room_no"]?>" /><input type="hidden" name="roomId[]" value="<?=$room["bRoomId"]?>" /></td>
                        <?php
						}elseif($model->btype == "book" && Yii::app()->user->profile['child_id'] == $model->staff_id){
						?>
                        <td><?=$room["room_no"]?></td>
                        <?php
						}?>
                    </tr>
                    
                    <?php
					}
					?>
                    
                    
                    
                    
                    <?php
                    if($model->extra_charge > 0){
						
						$totalPayment += $model->extra_charge;
					?>
                    
                        <tr >
                            <th colspan="2"></th>
                            <th colspan="2">Extra Charges</th>
                            
                            <td><?=$model->extra_charge?></td>
                             <td></td>
                        </tr>
                    
                    <?php
					}
					?>
                    
                    
                     <?php
                    if($model->discount > 0){
						
						$totalPayment -= $model->discount;
					?>
                    
                        <tr >
                            <th colspan="2"></th>
                            <th colspan="2">Discount</th>
                            
                            <td><?=$model->discount?></td>
                             <td></td>
                        </tr>
                    
                    <?php
					}
					?>
                    
                    
                    <tr >
                    	<th colspan="2"></th>
                        <th colspan="2">Total Booking Price</th>
                        
                        <td><?=$totalPayment?></td>
                        <td></td>
                    </tr>
                    
                    
                    <?php
                    
					if ($model->staff->parent_id ==  Yii::app()->user->profile['child_id']){
					?>
                    
                    
                     <tr >
                    	<th colspan="6"></th>
                       
                    </tr>
                    
                     <tr >
                    	<th colspan="6">
                        
                        	<span class="txt_bototherlinks">Apply Discount</span>
            
                        
                        </th>
                       
                    </tr>
                    
                    
                     <tr >
                    	<td colspan="6">
                        
                        	
                          <table class="table table-striped table-bordered" >
                           
                           
                            <tr>
                              <td width="19%" height="30" class="blueColorTd"><div align="left"><span class="txt_copyright">Extra Cost </span>&nbsp; </div></td>
                              <td width="81%" class="whiteColorTd"><div class="">
							  
							  <input name="extra_charge_old" type="hidden" class="forms" id="extra_charge_old" value="<?php  echo $model->extra_charge?>" />
                                <input name="Booking[extra_charge]" type="text" class="forms" id="extra_charge" value="<?php  echo $model->extra_charge?>" />
                              </div></td>
                            </tr>
                            <tr>
                              <td  class="blueColorTd"><div align="left"><span class="txt_copyright">Discout</span>&nbsp; </div></td>
                              <td class="whiteColorTd"><div class="">
							  <input name="discount_old" type="hidden" class="forms" id="discount_old" value="<?php  echo $model->discount?>" />
                                <input name="Booking[discount]" type="text" class="forms" id="discount" value="<?php  echo $model->discount?>" />
                              </div></td>
                            </tr>
                            <tr>
                              <td lass="blueColorTd">&nbsp;</td>
                              <td class="whiteColorTd">
                                <input type="submit" value="Apply" name="discount" class="btn btn-default" /></td>
                            </tr>
                           
                          </table>
                          
                          
                          
            
                        
                        </td>
                       
                    </tr>
                    
                    
                    <?php
                    
					}
					
					
					
					$days =Yii::app()->functions->getDays(date("Y-m-d"),$model->checkin);
					
			
		
					
					
					if ( $model->btype == "book" && $model->deleted == 0){ //$days > 5 &&
						
					?>
                    
                    
                    
                     <tr >
                    	<th colspan="6" class="text-center">
                        <input type="submit" value="Cancel Booking" name="cancel" class="btn btn-default" onclick="return confirm('Cancellation Policy will apply. Are you sure to cancel?');" />
                        
                        
                        </th>
                         
                    </tr>
                 <?php
					}elseif ($days >= 0 && $model->btype == "request" && $model->deleted == 0){
						
					?>
                    
                    
                    
                     <tr >
                    	<th colspan="6" class="text-center">
                        <input type="submit" value="Cancel Request" name="cancel" class="btn btn-default" onclick="return confirm('Are you sure to Cancel Request?');" />
                        
                        
                      </th>
                       
                    </tr>
                 <?php
					}
					
					if($model->btype == "book" && $model->deleted == 0 && Yii::app()->user->profile['child_id'] != $model->staff_id){
				 ?>   
                 
                 <tr >
                    	<th colspan="6" class="text-center">
                        <input type="submit" value="Update Booking Detail" name="updateBooking" class="btn btn-default"  />
                        
                        
                      </th>
                       
                    </tr>
                <?php
					}
				?>    
                    
                </tbody>
                
            </table>
            
        
        </tr>
		
        <tr>
        	<td>
            <h3>Cancellation Policy</h3>
            
            <span>
            	
                <?php
                $hotel = Hotel::model()->findByPk($model->hotel_id);
				
				
				
				
				$one_night_start 	= $hotel["one_night_start"]; 
				$one_night_end 		= $hotel["one_night_end"];
				
				$cancel_50_start 	= $hotel["cancel_50_start"];
				$cancel_50_end 		= $hotel["cancel_50_end"];
				
				$cancel_100_start 	= $hotel["cancel_100_start"];
				$cancel_100_end 	= $hotel["cancel_100_end"];
				
				$free_cancel_start 	= $hotel["free_cancel_start"];
				$free_cancel_end 	= $hotel["free_cancel_end"];
				
				$currentDate = date("Y-m-d");
				
				$day = Yii::app()->functions->getDatesToDays($currentDate, $model->checkin);

				$totalDay = $day->days;
				
				
				if ($totalDay >= $cancel_100_start  && $totalDay <= $cancel_100_end || $totalDay<=0){
				
					$fix  = ($model->amount);
					$message = "100% cancellation charges";
					
				}elseif ($totalDay >= $cancel_50_start  && $totalDay <= $cancel_50_end){
					
					$message = "50% cancellation charges";
					$fix  = ($model->amount * .5);
					
				}elseif ($totalDay >= $one_night_start  && $totalDay <= $one_night_end){
					
					$message = "one night cancellation charges";
					$fix  = ($model->amount / $model->noofnight);
					
				}elseif ($totalDay >= $free_cancel_start  && $totalDay <= $free_cancel_end){
					
					$message = "Free cancellation";
					$fix  = 0;
					
				}
				
				
				
				
				?>
                
                
                <ul>
                	<li>
                    	Free Cancellation when cancelled before <strong><?=date("d F, Y",strtotime($model->checkin . " -".$free_cancel_end." days"))?></strong> <!-- and  <strong><?=date("d F, Y",strtotime($model->checkin . " -".$free_cancel_start." days"))?></strong> -->
                    </li>
                    <li>
                    	One Night charge for Cancellation when cancelled  between <strong><?=date("d F, Y",strtotime($model->checkin . " -".$one_night_end." days"))?></strong> and  <strong><?=date("d F, Y",strtotime($model->checkin . " -".$one_night_start." days"))?></strong>
                    </li>
                    <li>
                    	50% Cancellation charges when cancelled  between <strong><?=date("d F, Y",strtotime($model->checkin . " -".$cancel_50_start." days"))?></strong> and  <strong><?=date("d F, Y",strtotime($model->checkin . " -".$cancel_50_end." days"))?></strong>
                    </li>
                    <li>
                    	100% Cancellation charges when cancelled  after <strong><?=date("d F, Y",strtotime($model->checkin . " -".$cancel_100_end." days"))?></strong> 
                    </li>
                    
                    <li>
                    	<strong><?= (isset($message))?$message : "";?> applied for your booking.</strong>
                    </li>
                </ul>
                
            
            </span>
            
            </td>
        
        </tr>
        
        
	</tbody>
</table>


</form>



<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>