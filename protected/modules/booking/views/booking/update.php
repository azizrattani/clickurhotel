<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->pageTitle = 'Bookings - Update';
$this->breadcrumbs = array(
	'Bookings' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_formUpdate', compact('model')); ?>