<?php
/* @var $this BookingController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Manage Bookings';
$this->breadcrumbs = array(
	'Bookings',
    'Manage'
);

$myId = Yii::app()->user->profile['child_id'];


unset($_SESSION["partnerChild"]);		
Yii::app()->functions->childIdHierarchy($myId);

if(isset($_SESSION["partnerChild"]))		
	$childs = implode(",",$_SESSION["partnerChild"]).",".$myId;
else
	$childs = $myId;

?>

<style>
.ui-datepicker-year,.ui-datepicker-month{
	color:#000000;	
}
</style>

<?php if($createAllowed) { ?>
<p class="text-right">
    <a href="<?php echo $this->createUrl('reservation'); ?>" class="btn btn-social btn-instagram">
        <i class="fa fa-plus"></i> Add New
    </a>
</p>
<?php } 


	?>

<div class="box-body table-responsive no-padding">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'booking-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
		'afterAjaxUpdate' => 'reinstallDatePicker', //call function to reinstall date picker after filter 
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
		
		
		
		
        'columns' => array(
    		'id',
		array('name'=>'btype','value'=>'($data->btype == "book") ? "Definite" : "Request"','filter' => CHtml::listData(array(array("id"=>"book","title"=>"Definite"),array("id"=>"request","title"=>"Request"),array("id"=>"cancelled","title"=>"Cancelled")),"id","title")),
		array('name'=>'hotel_id','value'=>'$data->hotel->name','filter' => CHtml::listData(Hotel::model()->findAll(), 'id', 'name')),
		array('name'=>'staff_id','value'=>'$data->staff->name','filter' => CHtml::listData(Partners::model()->findAll(array("condition"=>"id in (".$childs.")")), 'id', 'name')),
		
		'full_guest_name',
		  array(
            'name' => 'checkin',
            'value'=>'date("d F, Y",strtotime($data->checkin))',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'checkin',
            'options'=>array(
            'dateFormat'=>'yy-mm-dd',
            'changeYear'=>'true',
            'changeMonth'=>'true',
            'showAnim' =>'slide',
            'yearRange'=>'2000:'.(date('Y')+1),
            ),
            'htmlOptions'=>array(
			'style'=>'color:#000000',
			'autocomplete'=>'off',
            'id'=>'checkin',
            ),

            ),
            true),

            ),
			
			
			array(
            'name' => 'checkout',
            'value'=>'date("d F, Y",strtotime($data->checkout))',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'checkout',
            'options'=>array(
            'dateFormat'=>'yy-mm-dd',
            'changeYear'=>'true',
            'changeMonth'=>'true',
            'showAnim' =>'slide',
            'yearRange'=>'2000:'.(date('Y')+1),
            ),
            'htmlOptions'=>array(
			'style'=>'color:#000000',
			'autocomplete'=>'off',
            'id'=>'checkout',
            ),

            ),
            true),

            ),
		
		
		
		array('name'=>'amount','value'=>'$data->getMyRateTotal()'),
		array("header"=>"@ Per night","value"=>'number_format($data->getMyRateTotal("return")/($data->noofnight?$data->noofnight:1),2)'),
		'noofnight',
		/*
		'btype',
		'full_guest_name',
		'smooking',
		'special_request',
		'nationality',
		'meal_plan',
		'email',
		'phone',
		'adult',
		'child',
		'exp_arrive',
		'extra_charge',
		'discount',
		'book_request',
		'request_email',
		'created',
		*/
            array(
                'htmlOptions' => array('style' => 'width: 100px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '{invoice}{voucher}{view}{update}{convert}',
                'buttons' => array(
                    'view' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Detail', 'class' => 'fa fa-info', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$viewAllowed"
                    ),
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Edit', 'class' => 'fa fa-pencil-square-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$updateAllowed"
                    ),
                    'delete' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Delete', 'class' => 'fa fa-trash', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$deleteAllowed"
                    ),
					'invoice' => array(
                        'label' => '',
                        'imageUrl' => '',
						'url'=>'Yii::app()->createUrl("/invoice/invoice/duesDetail/", array("id"=>$data->id))',
                        'options' => array('title' => 'Invoice', 'class' => 'fa fa-file-invoice', 'style' => 'margin: 0px 3px;'),
                       'visible' => '$data->btype == "book" ? true : false',
                    ),
					'voucher' => array(
                        'label' => '',
                        'imageUrl' => '',
						'url'=>'Yii::app()->createUrl("/invoice/invoice/voucher/", array("id"=>$data->id))',
                        'options' => array('title' => 'Voucher', 'class' => 'fa fa-tags	', 'style' => 'margin: 0px 3px;'),
						'visible' => '$data->btype == "book" ? true : false',
                        
                    ),
					'convert' => array(
                        'label' => '',
                        'imageUrl' => '',
						'url'=>'Yii::app()->createUrl("/booking/booking/convert/", array("id"=>$data->id))',
                        'options' => array('title' => 'Covert to Booking', 'class' => 'fa fa-exchange-alt', 'style' => 'margin: 0px 3px;'),
						'visible' => '(($data->btype == "request") && '."$convertAllowed".') ? true : false',
                        
                    )
                )
            )
        ),
    )); ?>
</div>

<?php
/* function to re install date picker after filter the result. if you don't use it then after filter the result calendar will not shown in filter box */
Yii::app()->clientScript->registerScript('re-install-date-picker',"function reinstallDatePicker(id, data) {
    jQuery('#checkin,#checkout').datepicker({
        changeMonth: true,
        changeYear: true,
		dateFormat:'yy-mm-dd',
    });
}");
?>