<?php
/* @var $this BookingController */
/* @var $model Booking */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'booking-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model,'staff_id'); ?>
		<?= $form->textField($model,'staff_id'); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'checkin'); ?>
		<?= $form->textField($model,'checkin'); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'checkout'); ?>
		<?= $form->textField($model,'checkout'); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'amount'); ?>
		<?= $form->textField($model,'amount'); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'noofnight'); ?>
		<?= $form->textField($model,'noofnight'); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'btype'); ?>
		<?= $form->textField($model, 'btype', array('class' => 'form-control', 'size' => 7, 'maxlength' => 7)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'full_guest_name'); ?>
		<?= $form->textField($model, 'full_guest_name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'smooking'); ?>
		<?= $form->textField($model, 'smooking', array('class' => 'form-control', 'size' => 3, 'maxlength' => 3)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'special_request'); ?>
		<?= $form->textArea($model,'special_request',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'nationality'); ?>
		<?= $form->textField($model, 'nationality', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'meal_plan'); ?>
		<?= $form->textField($model, 'meal_plan', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'email'); ?>
		<?= $form->textField($model, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'phone'); ?>
		<?= $form->textField($model, 'phone', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'adult'); ?>
		<?= $form->textField($model, 'adult', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'child'); ?>
		<?= $form->textField($model, 'child', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'exp_arrive'); ?>
		<?= $form->textField($model, 'exp_arrive', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'extra_charge'); ?>
		<?= $form->textField($model,'extra_charge'); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'discount'); ?>
		<?= $form->textField($model,'discount'); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'book_request'); ?>
		<?= $form->textField($model, 'book_request', array('class' => 'form-control', 'size' => 5, 'maxlength' => 5)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model,'request_email'); ?>
		<?= $form->textField($model, 'request_email', array('class' => 'form-control', 'size' => 3, 'maxlength' => 3)); ?>
	</div>

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>
