<?php

class BookingController extends AdminController
{
    public function filters()
    {
        return array(
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function actionIndex()
    {
        $model = new Booking('search');
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['Booking']))
        {
            $model->attributes = $_GET['Booking'];
        }else{

            if (Yii::app()->user->profile['role_id'] == 2  ){
                //$model->parent_id = 0 ;
            }else{
                $model->staff_id = Yii::app()->user->profile['child_id'] ;
            }

        }

        $model->status = 0;



        $createAllowed 	= $this->isAllowed('create');
        $updateAllowed 	= $this->isAllowed('update');
        $deleteAllowed 	= $this->isAllowed('delete');
        $viewAllowed   	= $this->isAllowed('view');
        $convertAllowed = $this->isAllowed('convert');


        if (!$convertAllowed || $convertAllowed == ""){
            $convertAllowed = 0;
        }




        $this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed','convertAllowed'));
    }


    public function actionCancel()
    {
        $model = new Booking('search');
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['Booking']))
        {
            $model->attributes = $_GET['Booking'];
        }else{
            if (Yii::app()->user->profile['role_id'] == 2  ){
                //$model->parent_id = 0 ;
            }else{
                $model->staff_id = Yii::app()->user->profile['child_id'] ;
            }
        }

        $model->status = 2;
        $model->deleted = 1;
        $model->btype = 'cancelled';


        $createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

        $this->render('cancelIndex', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
    }

    public function actionReservation()
    {
        $model = new Booking('search');
        $model->unsetAttributes();  // clear any default values

        $output = array();



        if(isset($_POST['Reservation'])) {


            if(isset($_POST['Booking']["nationality"])){
                $model->nationality = $_POST['Booking']["nationality"];

                setcookie("nationality",$model->nationality,strtotime("+1 year"));

                $_COOKIE["nationality"] = $_POST['Booking']["nationality"];
            }




            unset($_SESSION["partnerPrice"]);
            unset($_SESSION["agentRate"]);


            Yii::app()->functions->partnerHierarchy(Yii::app()->user->profile['child_id']);


            $price_set = ($_SESSION["partnerPrice"]);
            krsort($price_set);


            /*echo "<pre>";
            print_r($price_set);
            die();*/

            $agentRate = array();

            foreach ($price_set as $agentPrice) {
                if ($agentPrice["staff_id"] == "")
                    continue;

                $agentRate[$agentPrice["staff_id"]]["price_type"] = $agentPrice["price_type"];
                $agentRate[$agentPrice["staff_id"]]["fix_price"] = $agentPrice["fix_price"];
                $agentRate[$agentPrice["staff_id"]]["floating_price"] = $agentPrice["floating_price"];
                $agentRate[$agentPrice["staff_id"]]["hybrid_condition"] = $agentPrice["hybrid_condition"];
            }

            $_SESSION["agentRate"] = json_encode($agentRate);


            $checkinArr = explode("/",$_POST["Reservation"]["checkin"]);
            $checkin = $checkinArr[1] . "/" . $checkinArr[0] . "/" . $checkinArr[2];
            $_POST["Reservation"]["checkin"] = $checkin;

            $checkoutArr = explode("/",$_POST["Reservation"]["checkout"]);
            $checkout = $checkoutArr[1] . "/" . $checkoutArr[0] . "/" . $checkoutArr[2];
            $_POST["Reservation"]["checkout"] = $checkout;

            $checkin = date("Y-m-d", strtotime($checkin));
            $checkout = date("Y-m-d", strtotime($checkout));





            $city = $_POST["Reservation"]["city"];

            $start_date = $checkin;
            $end_date = $checkout;

            $day = Yii::app()->functions->getDatesToDays($start_date, $end_date);



            $totalDay = $day->days;


            $sqlHotel = "SELECT * FROM (
			  (SELECT
			  *
			FROM
			  (SELECT
				'package' as list_type,
				COUNT(pr.id) AS totalDays,
				h.*
			  FROM
				hotel h
				INNER JOIN hotel_rooms hr
				  ON hr.hotel_id = h.`id`
				INNER JOIN package_rates pr
				  ON pr.`room_id` = hr.id
				INNER JOIN package p
				  ON p.`id` = pr.`package_id`
			  WHERE (
				  '" . $checkin . "' BETWEEN p.`date_start`
				  AND p.`date_end`
				)
				AND (
				  '" . $checkout . "' BETWEEN p.`date_start`
				  AND p.`date_end`
				)
				AND city = '" . $city . "'
				AND h.`deleted` = 0
				AND hr.`deleted` = 0
				AND p.`deleted` = 0
				AND pr.`deleted` = 0
			  GROUP BY h.`id`,
				hr.`id`) data1
			Order BY sort_order,
			  id)
			UNION All

			(SELECT
			  *
			FROM
			  (SELECT
			   	'hotel' as list_type,
				COUNT(hrr.id) AS totalDays,
				h.*
			  FROM
				hotel h
				INNER JOIN hotel_rooms hr
				  ON hr.hotel_id = h.`id`
				INNER JOIN hotel_room_rate hrr
				  ON hrr.`room_id` = hr.id
			  WHERE hrr.`bookdate` BETWEEN '" . $checkin . "'
				AND '" . $checkout . "'
				AND city = '" . $city . "'
				AND h.`deleted` = 0
				AND hr.`deleted` = 0
				AND hrr.`deleted` = 0
			  GROUP BY hotel_id,
				hr.`id`) data1
			Order BY sort_order,
			  id)) hotelResults GROUP BY hotelResults.id Order BY sort_order
			;
			";


            //echo $sqlHotel;

            //die ();


            $hotels = Yii::app()->db->createCommand($sqlHotel)->queryAll();



            if ($hotels) {

                foreach ($hotels as $hkey => $hotel) {

                    $hdata = array();

                    $hdata = $hotel;


                    if ($hotel["list_type"] == "hotel"){


                        $sqlMaxRoom = "SELECT
							hrr.room_id ,
							 ((no_of_room-room_book)) AS totRoom,
							 MAX(price) AS price,
							  created_by
							FROM
							  hotel_room_rate hrr
							WHERE room_id IN
							  (SELECT
								id
							  FROM
								hotel_rooms
							  WHERE hotel_id = '" . $hotel["id"] . "' and deleted=0)
							  AND `bookdate` BETWEEN '" . $checkin . "' AND '" . $checkout . "'
							  AND deleted = 0
							GROUP BY hrr.created_by,hrr.`room_id`
							ORDER BY totRoom DESC,price ASC ;";



                        $maxRoom = Yii::app()->db->createCommand($sqlMaxRoom)->queryAll();

                        $rdata = [];

                        foreach($maxRoom as $rm){

                            if(!isset($rdata[$rm["room_id"]])){
                                $rdata[$rm["room_id"]] = $rm["created_by"];
                            }

                        }

                        //debug($rdata,true);

                        //$created_by = $maxRoom["created_by"];


                        $sqlHotelRooms = "SELECT
							  *
							FROM
							  hotel_rooms
							WHERE id IN
							  (SELECT
								room_id
							  FROM
								hotel_room_rate hrr
							  WHERE room_id IN
								(SELECT
								  id
								FROM
								  hotel_rooms
								WHERE hotel_id = '" . $hotel["id"] . "'  and deleted=0)
								AND `bookdate` BETWEEN '" . $checkin . "' AND '" . $checkout . "'
								AND deleted = 0
								) and deleted =0 ;";


                    }else{

                        $sqlMaxRoom = "SELECT
							 ((no_of_room-room_book)) AS totRoom,
							 MAX(price) AS price,
							pr.room_id as room_id,
							  pr.created_by
							FROM
							  package_rates pr, package p
							WHERE

							p.id = pr.package_id and


							room_id IN
							  (SELECT
								id
							  FROM
								hotel_rooms
							  WHERE hotel_id = '" . $hotel["id"] . "' and deleted=0)
							  AND

							  ('" . $checkin . "' BETWEEN p.`date_start` AND p.`date_end`) AND ('" . $checkout . "' BETWEEN p.`date_start` AND p.`date_end` )


							  AND pr.deleted = 0  and p.deleted = 0
							GROUP BY pr.created_by, pr.room_id
							ORDER BY totRoom DESC, price ASC";



                        $maxRoom = Yii::app()->db->createCommand($sqlMaxRoom)->query();
                        //$created_by = $maxRoom["created_by"];




                        $rdata = [];

                        foreach($maxRoom as $rm){

                            if(!isset($rdata[$rm["room_id"]])){
                                $rdata[$rm["room_id"]] = $rm["created_by"];
                            }

                        }



                        $sqlHotelRooms = "SELECT
							  *
							FROM
							  hotel_rooms
							WHERE id IN
							  (SELECT
								room_id
							  FROM
								 package_rates pr, package p
							  WHERE
							  p.id = pr.package_id and
							  room_id IN
								(SELECT
								  id
								FROM
								  hotel_rooms
								WHERE hotel_id = '" . $hotel["id"] . "'  and deleted=0)
								and
								('" . $checkin . "' BETWEEN p.`date_start` AND p.`date_end`) AND ('" . $checkout . "' BETWEEN p.`date_start` AND p.`date_end` )
								AND p.deleted = 0 and pr.deleted = 0
								) and deleted =0 ;";

                    }




                    $hotelRooms = Yii::app()->db->createCommand($sqlHotelRooms)->queryAll();


                    foreach ($hotelRooms as $key => $room) {
                        $hdata["rooms"][$key] = $room;




                        if ($hotel["list_type"] == "hotel"){

                            $created_by = $rdata[ $room["id"]];

                            $maxRooms = "SELECT
							 min((no_of_room-room_book)) AS totRoom

							FROM
							  hotel_room_rate hrr
							WHERE room_id  = " . $room["id"] . "
							  AND `bookdate` BETWEEN '" . $checkin . "' AND '" . $checkout . "'
							  and hrr.created_by = " . $created_by . "
							  AND deleted = 0
							";


                        }else{



                            $created_by = $rdata[ $room["id"]];

                            $maxRooms = "SELECT
							 min((no_of_room-room_book)) AS totRoom

							FROM
							  package_rates pr, package p
							WHERE room_id  = " . $room["id"] . " and

							pr.package_id = p.id and

							('" . $checkin . "' BETWEEN p.`date_start` AND p.`date_end`) AND ('" . $checkout . "' BETWEEN p.`date_start` AND p.`date_end` )



							  and pr.created_by = '" . $created_by . "'
							  AND p.deleted = 0 AND pr.deleted = 0
							";



                        }



                        //echo "<br><Br> " . $maxRooms . "<br><Br> ";

                        $maximum = Yii::app()->db->createCommand($maxRooms)->queryRow();

                        $maxBooking = $maximum["totRoom"];




                        $dayCount = 0;
                        $totalPrice = $totalBPrice = 0;


                        if ($totalDay < 1){
                            $totalDay = 1;
                        }

                        for ($i = 0; $i < $totalDay; $i++) {

                            $price = 0;

                            $bookDate = date("Y-m-d", strtotime($start_date . "+ " . ($i) . " day"));
                            $day = date("D", strtotime($bookDate));

                            $sql4 = "SELECT
								pr.*,
								p.date_start, p.date_end
							  FROM
								package_rates pr

								inner join package p on p.id = pr.package_id

							  WHERE room_id = '" . $room["id"] . "'  and '" . $bookDate . "' BETWEEN p.`date_start` AND p.`date_end` and p.deleted = 0 and pr.deleted = 0

			  				";




                            $data4 = Yii::app()->db->createCommand($sql4)->queryRow();

                            if ($data4){

                                $price = $data4["price"];


                                $bprice = $price;

                                $totalBPrice += $bprice;

                                $price = Yii::app()->functions->agentPrice($price, $price_set);

                                $totalPrice += $price;

                                $hdata["rooms"][$key]["roomRate"][$bookDate]["bprice"] = $bprice;
                                $hdata["rooms"][$key]["roomRate"][$bookDate]["price"] = $price;
                                $hdata["rooms"][$key]["roomRate"][$bookDate]["day"] = "Package from " . date("d F, Y",strtotime($data4["date_start"])) . " - " . date("d F, Y",strtotime($data4["date_end"]));

                                $hdata["rooms"][$key]["bookType"] = "package";

                                $hdata["rooms"][$key]["bprice"] = $totalBPrice;
                                $hdata["rooms"][$key]["price"] = $totalPrice;
                                $hdata["rooms"][$key]["totalDays"] = $totalDay;
                                $hdata["rooms"][$key]["maxBooking"] = $maxBooking;
                                $hdata["rooms"][$key]["createBy"] =  $data4["created_by"];

                                $day2 = Yii::app()->functions->getDatesToDays($bookDate, $data4["date_end"]);
                                $totalDay2 = $day2->days;

                                $i += $totalDay2;



                            }else{

                                $sql4 = "SELECT
								*
							  FROM
								hotel_room_rate
							  WHERE room_id = '" . $room["id"] . "' and created_by = " . $created_by . " and bookdate ='" . $bookDate . "' AND `deleted` = 0";


                                $data4 = Yii::app()->db->createCommand($sql4)->queryRow();


                                if ($hotel["rate_type"] == 1) {
                                    $price = $data4["price"];
                                } elseif ($hotel["rate_type"] == 3 && $totalDay >= 4) {
                                    $price = $data4["four_days"];
                                } elseif (in_array($day, array("Thu","Fri"))) {
                                    $price = $data4["price_weekend"];
                                } else {
                                    $price = $data4["price"];
                                }

                                $bprice = $price;

                                if($bprice > 0){
                                    $totalBPrice += $bprice;
                                }else{
                                    $totalBPrice = 0;
                                    break;
                                }



                                $price = Yii::app()->functions->agentPrice($price, $price_set);

                                $totalPrice += $price;

                                $hdata["rooms"][$key]["roomRate"][$bookDate]["bprice"] = $bprice;
                                $hdata["rooms"][$key]["roomRate"][$bookDate]["price"] = $price;
                                $hdata["rooms"][$key]["roomRate"][$bookDate]["day"] = $day;
                                $hdata["rooms"][$key]["bookType"] = "normal";
                                $hdata["rooms"][$key]["maxBooking"] = $maxBooking;
                                $hdata["rooms"][$key]["createBy"] = $created_by;

                            }





                        }

                        if($totalBPrice > 0){
                            $hdata["rooms"][$key]["bprice"] = $totalBPrice;
                            $hdata["rooms"][$key]["price"] = $totalPrice;
                            $hdata["rooms"][$key]["totalDays"] = $totalDay;
                            //$hdata["rooms"][$key]["maxBooking"] = $maxBooking;
                            //$hdata["rooms"][$key]["createBy"] = $created_by;

                        }else{

                            unset($hdata["rooms"][$key]);


                        }



                    }

                    $output[$hkey] = $hdata;



                }


            }

            if(isset($_REQUEST["Booking"]["hotel_id"]) && $_REQUEST["Booking"]["hotel_id"] != ""){

                $hotelKey = Yii::app()->functions->array_search_inner($output, 'id', $_REQUEST["Booking"]["hotel_id"]);
                //echo $hotelKey;


                $hotelArray = $output[$hotelKey];
                unset($output[$hotelKey]);



                if(isset($_REQUEST["Reservation"]["room_id"]) && $_REQUEST["Reservation"]["room_id"] != ""){


                    echo $_REQUEST["Reservation"]["room_id"];

                    $roomKey = Yii::app()->functions->array_search_inner($hotelArray["rooms"], 'id', $_REQUEST["Reservation"]["room_id"]);

                    if(isset($hotelArray["rooms"][$roomKey])){

                        $roomsArray = $hotelArray["rooms"][$roomKey];
                        unset($hotelArray["rooms"][$roomKey]);

                        array_unshift($hotelArray["rooms"] , $roomsArray);

                    }



                }


                array_unshift($output , $hotelArray);

            }



            //debug($output,1);

        }




        $this->render('reservation', compact('model','output'));
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id);

        if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
            $this->render('view', compact('model'));
        }
    }

    public function actionCreate()
    {
        $model = new Booking;

        $booking = "";

        if(isset($_POST['Booking']))
        {
            $booking = 	$_POST['Booking'];
        }

        $output = array ();

        if (!isset($_POST["noofroomr"]) && isset($_SESSION["reservation"])){
            $_POST = $_SESSION["reservation"];
            $_POST["Booking"] = $booking;
        }


        $totalAmount = 0;

        if (isset($_POST["noofroomr"])){

            $_SESSION["reservation"] = $_POST;

            if (isset($_POST["request"])){
                $output["btype"] = "request";
            }else{
                $output["btype"] = "book";
            }


            $output["checkin"] 		= $_POST["checkin"];
            $output["checkout"] 	= $_POST["checkout"];
            $output["city"] 		= $_POST["city"];
            $output["totalDays"] 	= $_POST["totalDays"];




            foreach($_POST["noofroomr"] as $key => $roomData){

                if ($roomData){

                    $roomid = $_POST["roomr"][$key];
                    $roomDetail = HotelRooms::model()->findByPk($roomid);
                    $hotel_id = $roomDetail["hotel_id"];
                    $hotel = Hotel::model()->findByPk($hotel_id);

                    $meal_message = "";

                    $meal_price = 0;



                    if (isset($_POST["Meal"])){
                        foreach($_POST["Meal"] as $mealType => $meal){

                            if($meal <= 0){
                                continue;
                            }

                            //debug($mealType,1);
                            $mealArr = explode("_",$mealType);

                            $msg = "";

                            foreach($mealArr as $name){

                                $msg .= strtoupper($name);

                            }

                            $meal_message .= $msg."/";
                            $meal_price += $meal;


                        }
                    }



                    /*if (isset($_POST["breakfast"])){
                        $meal_message .= "B/";
                        $meal_price += $_POST["breakfast"];
                    }


                    if (isset($_POST["lunch"])){
                        $meal_message .= "L/";
                        $meal_price += $_POST["lunch"];
                    }


                    if (isset($_POST["dinner"])){
                        $meal_message .= "D/";
                        $meal_price += $_POST["dinner"];
                    }*/

                    $meal_price = $meal_price * $roomDetail["max_user"] * $roomData * $output["totalDays"];

                    $meal_message = substr($meal_message,0,-1);

                    if ($meal_message == ""){
                        $meal_message = "Room Only";
                    }

                    $output["hotel"] = $hotel;
                    $output["rooms"][$key]["data"] = $roomDetail;
                    $output["rooms"][$key]["roomPricer"] = $_POST["roomPricer"][$key];
                    $output["rooms"][$key]["noofroomr"] = $roomData;
                    $output["rooms"][$key]["roomBy"] = $_POST["roomBy"][$key];

                    $output["rooms"][$key]["mealMessage"] = $meal_message;
                    $output["rooms"][$key]["mealPrice"] = $meal_price;



                    $totalAmount += ($_POST["roomPricer"][$key] * $roomData) + $meal_price;

                }


            }

        }else{
            Yii::app()->user->setFlash('error', 'Reservation data not found');
            $this->redirect(array('reservation'));
        }

        if(isset($_POST['Booking']))
        {


            $model->attributes = $_POST['Booking'];

            $model->checkin= date("Y-m-d",strtotime($_POST["checkin"]));
            $model->checkout= date("Y-m-d",strtotime($_POST["checkout"]));
            $model->amount = $totalAmount;
            $model->noofnight = $output["totalDays"];
            $model->staff_id = Yii::app()->user->profile["child_id"];
            $model->hotel_id = $output["hotel"]["id"];


            if ($model->save())
            {

                unset($_SESSION["partnerPrice"]);
                Yii::app()->functions->partnerHierarchy(Yii::app()->user->profile['child_id']);
                $price_set = ($_SESSION["partnerPrice"]);
                krsort($price_set);



                foreach($output["rooms"] as $room){


                    $hotel_price = 0;

                    $roomId = $room["data"]["id"];
                    $roomBy = $room["roomBy"];
                    $totalDay = $output["totalDays"];
                    $start_date = $model->checkin;

                    $plus = 0;

                    if ($totalDay == 0){
                        $plus = 1;
                    }



                    for($i = 0; $i < ($totalDay + $plus); $i++) {



                        $price = 0;

                        $bookDate = date("Y-m-d", strtotime($start_date . "+ " . ($i) . " day"));
                        $day = date("D", strtotime($bookDate));

                        $sql4 = "SELECT
 								((no_of_room-room_book)) AS totRoom,
								pr.*,
								p.date_start, p.date_end,p.id as packageID
							  FROM
								package_rates pr

								inner join package p on p.id = pr.package_id

							  WHERE room_id = '" . $roomId . "'  and '" . $bookDate . "' BETWEEN p.`date_start` AND p.`date_end`
								
								and p.deleted = 0								
								and pr.deleted = 0

								GROUP BY pr.created_by, pr.room_id
							ORDER BY totRoom DESC, price ASC
								
			  				";




                        $data4 = Yii::app()->db->createCommand($sql4)->queryRow();

                        if ($data4){


                            $packageID = $data4["packageID"];

                            $price = $data4["price"];
                            $bprice = $price;
                            $hbprice = $data4["base_price"];
                            $price = Yii::app()->functions->agentPrice($price, $price_set);


                            //$bprice = $price;

                            //$price = Yii::app()->functions->agentPrice($price,$price_set);



                            $meal_message = "";

                            $meal_price = $breakfast = $lunch = $dinner = 0;


                            $max_user = (int) $roomDetail["max_user"];
                            $roomData = (int) $room["noofroomr"];
                            $totalDays = (int) $output["totalDays"];

                            $roomData = $room["noofroomr"];

                            if (isset($_POST["Meal"])){
                                foreach($_POST["Meal"] as $mealKey => $meal){
                                    $meal = (float) $meal;

                                    if($meal <= 0){
                                        continue;
                                    }
                                    $mealArr = explode("_",$mealKey);

                                    $msg = "";

                                    foreach($mealArr as $name){

                                        $msg .= strtoupper($name);

                                    }

                                    $meal_message .= $msg."/";
                                    $meal_price += $meal;


                                } 
                            }
                            

                            /*if (isset($_POST["breakfast"])){
                                $meal_message .= "B/";
                                $breakfast = $_POST["breakfast"];
                                $meal_price += $_POST["breakfast"];
                            }else{
                                $breakfast = -1;
                            }


                            if (isset($_POST["lunch"])){
                                $meal_message .= "L/";
                                $lunch = $_POST["lunch"];
                                $meal_price += $_POST["lunch"];
                            }else{
                                $lunch = -1;
                            }


                            if (isset($_POST["dinner"])){
                                $meal_message .= "D/";
                                $dinner = $_POST["dinner"];
                                $meal_price += $_POST["dinner"];
                            }else{
                                $dinner = -1;
                            }*/



                            //$meal_price = $meal_price * $max_user * $roomData * $totalDays;

                            $meal_message = substr($meal_message,0,-1);

                            if ($meal_message == ""){
                                $meal_message = "Room Only";
                            }


                            Yii::app()->db->createCommand("SET FOREIGN_KEY_CHECKS = 0 ")->execute();

                            $bookingRooms = new BookingRooms;

                            $bookingRooms->booking_id = $model->id;
                            $bookingRooms->booking_date = $bookDate;
                            $bookingRooms->room_id = $roomId;
                            $bookingRooms->no_of_room = $room["noofroomr"];
                            $bookingRooms->room_rate = $price;
                            $bookingRooms->room_rate_id = 0;




                            /*$bookingRooms->breakfast = $breakfast * $roomDetail["max_user"] * $roomData;
                            $bookingRooms->lunch = $lunch * $roomDetail["max_user"] * $roomData;
                            $bookingRooms->dinner = $dinner * $roomDetail["max_user"] * $roomData;

                            $bookingRooms->hbase_price = $hbprice;*/




                            $bookingRooms->base_price = $bprice;
                            $bookingRooms->rate_level = $_SESSION["agentRate"];


                            $bookingRooms->save(false);





                            if (isset($_POST["Meal"])){
                                foreach($_POST["Meal"] as $mealKey => $meal){


                                    $meal = (float) $meal;

                                    $bookingMealRate = new BookingMealRate;

                                    $bookingMealRate->booking_room_id = $bookingRooms->id;
                                    $bookingMealRate->meal_type = $mealKey;
                                    $bookingMealRate->price = $meal  * $roomData;

                                    $bookingMealRate->save(false);


                                }
                            }
                            



                            if ($model->btype == "book"){

                                $sql = "update package_rates set room_book= room_book + ".$room["noofroomr"]." where room_id='".$roomId."' and package_id='".$packageID."'";
                                $rateData = Yii::app()->db->createCommand($sql)->execute();

                            }





                            $day2 = Yii::app()->functions->getDatesToDays($bookDate, $data4["date_end"]);
                            $totalDay2 = $day2->days;

                            $i += $totalDay2;



                        }else{

                            $sql4 = "SELECT
								((no_of_room-room_book)) AS totRoom,id,base_price,price
							  FROM
								hotel_room_rate
							  WHERE room_id = '" . $roomId . "'  and bookdate ='" . $bookDate . "' and deleted = 0

							  GROUP BY created_by,`room_id`
							ORDER BY totRoom DESC,price ASC
							  ";




                            $data4 = Yii::app()->db->createCommand($sql4)->queryRow();


                            if ($hotel["rate_type"] == 1) {
                                $price = $data4["price"];
                            } elseif ($hotel["rate_type"] == 3 && $totalDay >= 4) {
                                $price = $data4["four_days"];
                            } elseif (in_array($day, array("Thu", "Fri"))) {
                                $price = $data4["price_weekend"];
                            } else {
                                $price = $data4["price"];
                            }

                            $bprice = $price;

                            $bprice = $price;

                            $hbprice = $data4["base_price"];

                            $price = Yii::app()->functions->agentPrice($price,$price_set);



                            $meal_message = "";

                            $meal_price = $breakfast = $lunch = $dinner = 0;

                            /*if (isset($_POST["breakfast"])){
                                $meal_message .= "B/";
                                $breakfast = $_POST["breakfast"];
                                $meal_price += $_POST["breakfast"];
                            }else{
                                $breakfast = -1;
                            }


                            if (isset($_POST["lunch"])){
                                $meal_message .= "L/";
                                $lunch = $_POST["lunch"];
                                $meal_price += $_POST["lunch"];
                            }else{
                                $lunch = -1;
                            }


                            if (isset($_POST["dinner"])){
                                $meal_message .= "D/";
                                $dinner = $_POST["dinner"];
                                $meal_price += $_POST["dinner"];
                            }else{
                                $dinner = -1;
                            }*/


                            if(isset($_POST["Meal"])){
                                foreach($_POST["Meal"] as $mealKey => $meal){

                                    $meal = (float) $meal;
                                    if($meal <= 0){
                                        continue;
                                    }
                                    $mealArr = explode("_",$mealKey);

                                    $msg = "";

                                    foreach($mealArr as $name){

                                        $msg .= strtoupper($name);

                                    }

                                    $meal_message .= $msg."/";
                                    $meal_price += $meal;


                                }
                            }


                            $max_user = (int) $roomDetail["max_user"];
                            $roomData = (int) $room["noofroomr"];
                            $totalDays = (int) $output["totalDays"];

                            $roomData = $room["noofroomr"];

                            //$meal_price = $meal_price * $max_user * $roomData * $totalDays;

                            $meal_message = substr($meal_message,0,-1);

                            if ($meal_message == ""){
                                $meal_message = "Room Only";
                            }



                            $bookingRooms = new BookingRooms;

                            $bookingRooms->booking_id = $model->id;
                            $bookingRooms->booking_date = $bookDate;
                            $bookingRooms->room_id = $roomId;
                            $bookingRooms->no_of_room = $room["noofroomr"];
                            $bookingRooms->room_rate = $price;
                            $bookingRooms->room_rate_id = $data4["id"];



                            /*$bookingRooms->breakfast = $breakfast * $roomDetail["max_user"] * $roomData;
                            $bookingRooms->lunch = $lunch * $roomDetail["max_user"] * $roomData;
                            $bookingRooms->dinner = $dinner * $roomDetail["max_user"] * $roomData;*/


                            $bookingRooms->base_price = $bprice;
                            $bookingRooms->hbase_price = $hbprice;
                            $bookingRooms->rate_level = $_SESSION["agentRate"];


                            $bookingRooms->save(false);

                            if(isset($_POST["Meal"])){
                                foreach($_POST["Meal"] as $mealKey => $meal){

                                    $meal = (float) $meal;

                                    $bookingMealRate = new BookingMealRate;

                                    $bookingMealRate->booking_room_id = $bookingRooms->id;
                                    $bookingMealRate->meal_type = $mealKey;
                                    $bookingMealRate->price = $meal * $room["data"]["max_user"] * $roomData;


                                    $bookingMealRate->save(false);


                                }
                            }
                            

                            if ($model->btype == "book"){

                                $sql = "update hotel_room_rate set room_book= room_book + ".$room["noofroomr"]." where id='".$data4["id"]."'";
                                $rateData = Yii::app()->db->createCommand($sql)->execute();

                            }

                        }





                    }




                }


                $sqlcu = "insert into payment set uid='".Yii::app()->user->profile['child_id']."', pay_date='".date("Y-m-d")."', amount = '-".$totalAmount."', payment_date='".date("Y-m-d")."', reference_number='".$model->id."', `narration`='Booking Online', bid='".$model->id."' ";
                //$rscu = mysql_query($sqlcu) or die (mysql_error());


                if ($model->btype == "book"){

                    $body = $this->renderPartial('booking_voucher', compact('model'),true);



                    /* PDF cde start*/

                    $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                    require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                        'default_font' => '',

                        'orientation' => 'P']);
                    $mpdf->SetDisplayMode('fullpage');



                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    /*LOAD A Fonts*/
                    ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                    ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                    $mpdf->WriteHTML($body);

                    $basePAth = dirname(Yii::app()->request->scriptFile) ;

                    $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                    $mpdf->Output($pdfFile,'F');



                    /* PDF code End*/





                    $to = $subject = $body =  $from_email =  $from_name = '';

                    $body="Hi, <Br><Br>
					
					
					Please find booking voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";



                    $subject = "Booking Voucher for " . $model->full_guest_name . " against ref#".$model->id ;

                    $from_email = "no-reply@clickurhotel.com";
                    $from_name = "Booking Department ";


                    $customer_email = $model->email ;

                    if ($customer_email != "")
                        Yii::app()->functions->email($customer_email, $subject , $body, $from_email , $from_name,$pdfFile);





                    $agent_email = $model->staff->email ;

                    $agent_cc_email = $model->staff->ccEmail ;

                    //echo $agent_email . "<br>";


                    Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);


                    $partner = Partners::model()->findByPk($model->staff->parent_id);


                    if($partner){

                        $agent_email = $partner->email ;

                        $agent_cc_email = $partner->ccEmail ;

                        //echo $agent_email . "<br>";


                        Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

                    }




                    $body = $this->renderPartial('booking_voucher_nologo', compact('model'),true);


                    /* PDF cde start*/

                    $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                    require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                        'default_font' => '',

                        'orientation' => 'P']);
                    $mpdf->SetDisplayMode('fullpage');



                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    /*LOAD A Fonts*/
                    ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                    ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                    $mpdf->WriteHTML($body);

                    $basePAth = dirname(Yii::app()->request->scriptFile) ;

                    $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                    $mpdf->Output($pdfFile,'F');


                    $body="Hi, <Br><Br>
					
					
					Please find booking voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";



                    unset($_SESSION["partnerEmail"]);
                    unset($_SESSION["partnerCcEmail"]);

                    Yii::app()->functions->partnerEmailHierarchyEx($model->staff->parent_id);

                    if(isset($_SESSION["partnerEmail"])){

                        foreach($_SESSION["partnerEmail"] as $key=>$partnerEmail){

                            if (isset($_SESSION["partnerEmail"][$key])){
                                $agent_cc_email = $_SESSION["partnerEmail"][$key];
                            }else{
                                $agent_cc_email = "";
                            }

                            //echo $partnerEmail . "<br>";
                            if ($partnerEmail != "")
                                Yii::app()->functions->email($partnerEmail, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

                        }

                    }







                    //Hotel Email


                    $body = $this->renderPartial('booking_voucher_hotel', compact('model'),true);


                    /* PDF cde start*/

                    $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                    require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                        'default_font' => '',

                        'orientation' => 'P']);
                    $mpdf->SetDisplayMode('fullpage');



                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    /*LOAD A Fonts*/
                    ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                    ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                    $mpdf->WriteHTML($body);

                    $basePAth = dirname(Yii::app()->request->scriptFile) ;

                    $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                    $mpdf->Output($pdfFile,'F');



                    $hotel_email = $model->hotel->email ;


                    $body="M/S ".$model->hotel->name.", <Br><Br>
					
					
					Please find booking voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";

                    Yii::app()->functions->email($hotel_email, $subject , $body, $from_email , $from_name,$pdfFile);




                }elseif ($model->btype == "request"){



                    $body = $this->renderPartial('request_voucher', compact('model'),true);


                    /* PDF cde start*/

                    $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                    require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                        'default_font' => '',

                        'orientation' => 'P']);
                    $mpdf->SetDisplayMode('fullpage');



                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    /*LOAD A Fonts*/
                    ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                    ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                    $mpdf->WriteHTML($body);

                    $basePAth = dirname(Yii::app()->request->scriptFile) ;

                    $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                    $mpdf->Output($pdfFile,'F');



                    /* PDF code End*/





                    $to = $subject = $body =  $from_email =  $from_name = '';

                    $body="Hi, <Br><Br>
					
					
					Please find request voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";



                    $subject = "Request Voucher for " . $model->full_guest_name . " against ref#".$model->id ;

                    $from_email = "no-reply@clickurhotel.com";
                    $from_name = "Booking Department ";


                    $customer_email = $model->email ;

                    if ($customer_email != "")
                        Yii::app()->functions->email($customer_email, $subject , $body, $from_email , $from_name,$pdfFile);





                    $agent_email = $model->staff->email ;

                    $agent_cc_email = $model->staff->ccEmail ;

                    //echo $agent_email . "<br>";


                    Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);


                    $partner = Partners::model()->findByPk($model->staff->parent_id);


                    if($partner){

                        $agent_email = $partner->email ;

                        $agent_cc_email = $partner->ccEmail ;

                        //echo $agent_email . "<br>";


                        Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

                    }




                    $body = $this->renderPartial('request_voucher_nologo', compact('model'),true);


                    /* PDF cde start*/

                    $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                    require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                        'default_font' => '',

                        'orientation' => 'P']);
                    $mpdf->SetDisplayMode('fullpage');



                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    /*LOAD A Fonts*/
                    ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                    ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                    $mpdf->WriteHTML($body);

                    $basePAth = dirname(Yii::app()->request->scriptFile) ;

                    $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                    $mpdf->Output($pdfFile,'F');


                    $body="Hi, <Br><Br>
					
					
					Please find request voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";



                    unset($_SESSION["partnerEmail"]);
                    unset($_SESSION["partnerCcEmail"]);

                    Yii::app()->functions->partnerEmailHierarchyEx($model->staff->parent_id);

                    if(isset($_SESSION["partnerEmail"])){

                        foreach($_SESSION["partnerEmail"] as $key=>$partnerEmail){

                            if (isset($_SESSION["partnerEmail"][$key])){
                                $agent_cc_email = $_SESSION["partnerEmail"][$key];
                            }else{
                                $agent_cc_email = "";
                            }

                            //echo $partnerEmail . "<br>";
                            if ($partnerEmail != "")
                                Yii::app()->functions->email($partnerEmail, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

                        }

                    }







                    //Hotel Email


                    $body = $this->renderPartial('request_voucher_hotel', compact('model'),true);


                    /* PDF cde start*/

                    $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                    require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                        'default_font' => '',

                        'orientation' => 'P']);
                    $mpdf->SetDisplayMode('fullpage');



                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    /*LOAD A Fonts*/
                    ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                    ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                    // LOAD a stylesheet
                    $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                    $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                    $mpdf->WriteHTML($body);

                    $basePAth = dirname(Yii::app()->request->scriptFile) ;

                    $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                    $mpdf->Output($pdfFile,'F');



                    $hotel_email = $model->hotel->email ;


                    $body="M/S ".$model->hotel->name.", <Br><Br>
					
					
					Please find request voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";

                    Yii::app()->functions->email($hotel_email, $subject , $body, $from_email , $from_name,$pdfFile);



                }


                Yii::app()->user->setFlash('success', 'Booking saved successfully');
                $this->redirect(array('index'));
            }
        }

        $bookingLimit = Yii::app()->functions->getAgentLimit(Yii::app()->user->profile['child_id']);

        //debug($output,1);


        $this->render('create', compact('model','output','bookingLimit'));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        /*if(!$model || $model->deleted == 1)
        {
            //Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {*/
        if(isset($_POST['Booking']) && !isset($_POST["updateBooking"]))
        {
            $model->attributes = $_POST['Booking'];

            if ($model->save())
            {

                Yii::app()->user->setFlash('success', 'Booking updated successfully');
                $this->redirect(array('index'));
            }
        }
        elseif(isset($_POST['cancel']))
        {

            $hotel = Hotel::model()->findByPk($model->hotel_id);


            $one_night_start 	= $hotel["one_night_start"];
            $one_night_end 		= $hotel["one_night_end"];

            $cancel_50_start 	= $hotel["cancel_50_start"];
            $cancel_50_end 		= $hotel["cancel_50_end"];

            $cancel_100_start 	= $hotel["cancel_100_start"];
            $cancel_100_end 	= $hotel["cancel_100_end"];

            $free_cancel_start 	= $hotel["free_cancel_start"];
            $free_cancel_end 	= $hotel["free_cancel_end"];

            $currentDate = date("Y-m-d");

            $day = Yii::app()->functions->getDatesToDays($currentDate, $model->checkin);

            $totalDay = $day->days;


            if ($totalDay >= $cancel_100_start  && $totalDay <= $cancel_100_end){

                $fix  = ($model->amount);
                $message = "100 % cancellation charges";

            }elseif ($totalDay >= $cancel_50_start  && $totalDay <= $cancel_50_end){

                $message = "50 % cancellation charges";
                $fix  = ($model->amount * .5);

            }elseif ($totalDay >= $one_night_start  && $totalDay <= $one_night_end){

                $message = "one night cancellation charges";
                $fix  = ($model->amount / $model->noofnight);

            }else{ //if ($totalDay >= $free_cancel_start  && $totalDay <= $free_cancel_end)

                $message = "Free cancellation";
                $fix  = 0;

            }


            $sqlcu = "insert into partner_payment set partner_id='".$model->staff_id."', pay_date='".date("Y-m-d")."', amount = '-".$fix."', payment_date='".date("Y-m-d")."', reference_number='".$model->id."', `narration`='".$message."', bid='".$model->id."' ";
            Yii::app()->db->createCommand($sqlcu)->execute();






            $model->deleted = 1;
            $model->status = 2;
            $model->btype = 'cancelled';
            $model->save(false);




            /*Email Code Start*/


            if ($model->btype == "book"){

                $body = $this->renderPartial('booking_voucher_cancel', compact('model','message'),true);



                /* PDF cde start*/

                $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                    'default_font' => '',

                    'orientation' => 'P']);
                $mpdf->SetDisplayMode('fullpage');



                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                /*LOAD A Fonts*/
                ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                $mpdf->WriteHTML($body);

                $basePAth = dirname(Yii::app()->request->scriptFile) ;

                $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                $mpdf->Output($pdfFile,'F');



                /* PDF code End*/





                $to = $subject = $body =  $from_email =  $from_name = '';

                $body="Hi, <Br><Br>
					
					
					Please find cancel  booking voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";



                $subject = "Cancel Booking Voucher for " . $model->full_guest_name . " against ref#".$model->id ;

                $from_email = "no-reply@clickurhotel.com";
                $from_name = "Booking Department ";


                $customer_email = $model->email ;

                if ($customer_email != "")
                    Yii::app()->functions->email($customer_email, $subject , $body, $from_email , $from_name,$pdfFile);





                $agent_email = $model->staff->email ;

                $agent_cc_email = $model->staff->ccEmail ;

                //echo $agent_email . "<br>";


                Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);


                $partner = Partners::model()->findByPk($model->staff->parent_id);


                if($partner){

                    $agent_email = $partner->email ;

                    $agent_cc_email = $partner->ccEmail ;

                    //echo $agent_email . "<br>";


                    Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

                }




                $body = $this->renderPartial('booking_voucher_cancel_nologo', compact('model','message'),true);


                /* PDF cde start*/

                $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                    'default_font' => '',

                    'orientation' => 'P']);
                $mpdf->SetDisplayMode('fullpage');



                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                /*LOAD A Fonts*/
                ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                $mpdf->WriteHTML($body);

                $basePAth = dirname(Yii::app()->request->scriptFile) ;

                $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                $mpdf->Output($pdfFile,'F');


                $body="Hi, <Br><Br>
					
					
					Please find cancel  booking voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";



                unset($_SESSION["partnerEmail"]);
                unset($_SESSION["partnerCcEmail"]);

                Yii::app()->functions->partnerEmailHierarchyEx($model->staff->parent_id);

                if(isset($_SESSION["partnerEmail"])){

                    foreach($_SESSION["partnerEmail"] as $key=>$partnerEmail){

                        if (isset($_SESSION["partnerEmail"][$key])){
                            $agent_cc_email = $_SESSION["partnerEmail"][$key];
                        }else{
                            $agent_cc_email = "";
                        }

                        //echo $partnerEmail . "<br>";
                        if ($partnerEmail != "")
                            Yii::app()->functions->email($partnerEmail, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

                    }

                }







                //Hotel Email


                $body = $this->renderPartial('booking_voucher_cancel_hotel', compact('model','message'),true);


                /* PDF cde start*/

                $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                    'default_font' => '',

                    'orientation' => 'P']);
                $mpdf->SetDisplayMode('fullpage');



                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                /*LOAD A Fonts*/
                ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                $mpdf->WriteHTML($body);

                $basePAth = dirname(Yii::app()->request->scriptFile) ;

                $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                $mpdf->Output($pdfFile,'F');



                $hotel_email = $model->hotel->email ;


                $body="M/S ".$model->hotel->name.", <Br><Br>
					
					
					Please find cancel  booking voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";

                Yii::app()->functions->email($hotel_email, $subject , $body, $from_email , $from_name,$pdfFile);




            }elseif ($model->btype == "request"){



                $body = $this->renderPartial('request_voucher_cancel', compact('model','message'),true);


                /* PDF cde start*/

                $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                    'default_font' => '',

                    'orientation' => 'P']);
                $mpdf->SetDisplayMode('fullpage');



                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                /*LOAD A Fonts*/
                ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                $mpdf->WriteHTML($body);

                $basePAth = dirname(Yii::app()->request->scriptFile) ;

                $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                $mpdf->Output($pdfFile,'F');



                /* PDF code End*/





                $to = $subject = $body =  $from_email =  $from_name = '';

                $body="Hi, <Br><Br>
					
					
					Please find cancel request voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";



                $subject = "Cancel Request Voucher for " . $model->full_guest_name . " against ref#".$model->id ;

                $from_email = "no-reply@clickurhotel.com";
                $from_name = "Booking Department ";


                $customer_email = $model->email ;

                if ($customer_email != "")
                    Yii::app()->functions->email($customer_email, $subject , $body, $from_email , $from_name,$pdfFile);





                $agent_email = $model->staff->email ;

                $agent_cc_email = $model->staff->ccEmail ;

                //echo $agent_email . "<br>";


                Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);


                $partner = Partners::model()->findByPk($model->staff->parent_id);


                if($partner){

                    $agent_email = $partner->email ;

                    $agent_cc_email = $partner->ccEmail ;

                    //echo $agent_email . "<br>";


                    Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

                }




                $body = $this->renderPartial('request_voucher_cancel_nologo', compact('model','message'),true);


                /* PDF cde start*/

                $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                    'default_font' => '',

                    'orientation' => 'P']);
                $mpdf->SetDisplayMode('fullpage');



                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                /*LOAD A Fonts*/
                ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                $mpdf->WriteHTML($body);

                $basePAth = dirname(Yii::app()->request->scriptFile) ;

                $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                $mpdf->Output($pdfFile,'F');


                $body="Hi, <Br><Br>
					
					
					Please find cancel request voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";



                unset($_SESSION["partnerEmail"]);
                unset($_SESSION["partnerCcEmail"]);

                Yii::app()->functions->partnerEmailHierarchyEx($model->staff->parent_id);

                if(isset($_SESSION["partnerEmail"])){

                    foreach($_SESSION["partnerEmail"] as $key=>$partnerEmail){

                        if (isset($_SESSION["partnerEmail"][$key])){
                            $agent_cc_email = $_SESSION["partnerEmail"][$key];
                        }else{
                            $agent_cc_email = "";
                        }

                        //echo $partnerEmail . "<br>";
                        if ($partnerEmail != "")
                            Yii::app()->functions->email($partnerEmail, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

                    }

                }







                //Hotel Email


                $body = $this->renderPartial('request_voucher_cancel_hotel', compact('model','message'),true);


                /* PDF cde start*/

                $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

                require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




                $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                    'default_font' => '',

                    'orientation' => 'P']);
                $mpdf->SetDisplayMode('fullpage');



                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                /*LOAD A Fonts*/
                ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
                ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

                // LOAD a stylesheet
                $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
                $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



                $mpdf->WriteHTML($body);

                $basePAth = dirname(Yii::app()->request->scriptFile) ;

                $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
                $mpdf->Output($pdfFile,'F');



                $hotel_email = $model->hotel->email ;


                $body="M/S ".$model->hotel->name.", <Br><Br>
					
					
					Please find cancel request voucher in attachment.<Br><Br>
					
					
					Regards,<Br><Br>
					
					Admin";

                Yii::app()->functions->email($hotel_email, $subject , $body, $from_email , $from_name,$pdfFile);



            }




            /*Email Code Ends*/




            Yii::app()->user->setFlash('success', 'Booking cancelled successfully');
            $this->redirect(array('index'));
        }elseif(isset($_POST["updateBooking"])){

            $model->ref_number = $_POST["ref_number"];
            $model->save(false);

            foreach($_POST["roomId"] as $key => $roomId){

                $bookingRoom = BookingRooms::model()->findByPk($roomId);
                $bookingRoom->room_no = $_POST["roomNo"][$key];
                $bookingRoom->save(false);

            }



            /* Send email to customer */

            $body = $this->renderPartial('booking_voucher', compact('model'),true);


            /* PDF cde start*/

            $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

            require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




            $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                'default_font' => '',

                'orientation' => 'P']);
            $mpdf->SetDisplayMode('fullpage');



            // LOAD a stylesheet
            $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
            $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

            /*LOAD A Fonts*/
            ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
            ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

            // LOAD a stylesheet
            $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
            $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

            // LOAD a stylesheet
            $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
            $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



            $mpdf->WriteHTML($body);

            $basePAth = dirname(Yii::app()->request->scriptFile) ;

            $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
            $mpdf->Output($pdfFile,'F');



            /* PDF code End*/





            $to = $subject = $body =  $from_email =  $from_name = '';

            $body="Hi, <Br><Br>


			Please find updated booking voucher in attachment.<Br><Br>


			Regards,<Br><Br>

			Admin";



            $subject = "Updated Booking Voucher for " . $model->full_guest_name . " against ref#".$model->id . " " ;

            $from_email = "no-reply@clickurhotel.com";
            $from_name = "Booking Department ";

            



            $customer_email = $model->email ;

            if ($customer_email != "")
                Yii::app()->functions->email($customer_email, $subject , $body, $from_email , $from_name,$pdfFile);


            /*Agent Email starts*/

            $agent_email = $model->staff->email ;

            $agent_cc_email = $model->staff->ccEmail ;

            //echo $agent_email . "<br>";


            Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);


            $partner = Partners::model()->findByPk($model->staff->parent_id);


            if($partner){

                $agent_email = $partner->email ;

                $agent_cc_email = $partner->ccEmail ;

                //echo $agent_email . "<br>";


                Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile,$agent_cc_email);

            }


           //die();

            /* Agent Email ends */



            /* Send email tp customer Ends*/

            Yii::app()->user->setFlash('success', 'Booking details updated successfully');
            $this->redirect(array('index'));

        }

        $this->render('update', compact('model'));
        //}
    }

    public function actionConvert($id){


        $model = Booking::model()->findByPk($id);

        $model->btype = "book";
        $model->save(false);


        if ($model->btype == "book"){

            $body = $this->renderPartial('booking_voucher', compact('model'),true);


            /* PDF cde start*/

            $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

            require_once Yii::app()->basePath . '/../mpdf/vendor/autoload.php';




            $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','default_font_size' => 0,
                'default_font' => '',

                'orientation' => 'P']);
            $mpdf->SetDisplayMode('fullpage');



            // LOAD a stylesheet
            $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/bootstrap/css/bootstrap.min.css');
            $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

            /*LOAD A Fonts*/
            ##$stylesheet = file_get_contents('http://fonts.googleapis.com/css?family=Open+Sans');
            ##$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

            // LOAD a stylesheet
            $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/AdminLTE.css');
            $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

            // LOAD a stylesheet
            $stylesheet = file_get_contents(Yii::app()->basePath . '/../themes/admin/css/custom.css');
            $mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text



            $mpdf->WriteHTML($body);

            $basePAth = dirname(Yii::app()->request->scriptFile) ;

            $pdfFile = $basePAth."/tmp/".preg_replace('/[^A-Za-z0-9\-]/', '', $model->full_guest_name).'_'.$model->id."_".date("YmdHis").".pdf";
            $mpdf->Output($pdfFile,'F');



            /* PDF code End*/





            $to = $subject = $body =  $from_email =  $from_name = '';

            $body="Hi, <Br><Br>
			
			
			Please find booking voucher in attachment.<Br><Br>
			
			
			Regards,<Br><Br>
			
			Admin";



            $subject = "Booking Voucher for " . $model->full_guest_name . " against ref#".$model->id . " " ;

            $from_email = "no-reply@clickurhotel.com";
            $from_name = "Booking Department ";


            $customer_email = $model->email ;

            if ($customer_email != "")
                Yii::app()->functions->email($customer_email, $subject , $body, $from_email , $from_name,$pdfFile);





            $agent_email = $model->staff->email ;

            //echo $agent_email . "<br>";


            Yii::app()->functions->email($agent_email, $subject , $body, $from_email , $from_name,$pdfFile);

            Yii::app()->functions->partnerEmailHierarchy($model->staff->parent_id);


            foreach($_SESSION["partnerEmail"] as $partnerEmail){

                //echo $partnerEmail . "<br>";
                if ($partnerEmail != "")
                    Yii::app()->functions->email($partnerEmail, $subject , $body, $from_email , $from_name,$pdfFile);

            }







            Yii::app()->user->setFlash('success', 'Request converted to Definate successfully');
            $this->redirect(array('index'));


        }


    }



    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->deleted = 1;
        $model->status = 2;
        $model->save(false);

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
        {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function loadModel($id)
    {
        $model = Booking::model()->findByPk($id);

        if($model === null)
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        return $model;
    }
}
