<?php

/**
 * This is the model class for table "booking".
 *
 * The followings are the available columns in table 'booking':
 * @property integer $id
 * @property integer $staff_id
 * @property string $checkin
 * @property string $checkout
 * @property double $amount
 * @property integer $noofnight
 * @property integer $hotel_id
 * @property string $btype
 * @property string $full_guest_name
 * @property string $smooking
 * @property string $special_request
 * @property string $nationality
 * @property string $meal_plan
 * @property string $email
 * @property string $phone
 * @property string $adult
 * @property string $child
 * @property string $exp_arrive
 * @property double $extra_charge
 * @property double $discount
 * @property string $book_request
 * @property string $request_email
 * @property string $created
 * @property string $modified
 * @property string $deleted
 *
 * The followings are the available model relations:
 * @property Hotel $hotel
 * @property Partners $staff
 * @property BookingDetails[] $bookingDetails
 * @property BookingRooms[] $bookingRooms
 */
class Booking extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'booking';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('full_guest_name, nationality, book_request', 'required'), // email, phone
			array('staff_id, noofnight, hotel_id', 'numerical', 'integerOnly'=>true),
			array('amount, extra_charge, discount', 'numerical'),
			array('btype', 'length', 'max'=>7),
			array('full_guest_name, nationality, meal_plan, email, phone, adult, child, exp_arrive', 'length', 'max'=>255),
			array('smooking, request_email', 'length', 'max'=>3),
			array('book_request', 'length', 'max'=>5),
			array('deleted', 'length', 'max'=>1),
			array('checkin, checkout, special_request, created, modified,ref_number', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, staff_id, checkin, checkout, amount, noofnight, hotel_id, btype, full_guest_name, smooking, special_request, nationality, meal_plan, email, phone, adult, child, exp_arrive, extra_charge, discount, book_request, request_email, created, modified, deleted,ref_number', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hotel' => array(self::BELONGS_TO, 'Hotel', 'hotel_id'),
			'staff' => array(self::BELONGS_TO, 'Partners', 'staff_id'),
			'bookingDetails' => array(self::HAS_MANY, 'BookingDetails', 'booking_id'),
			'bookingRooms' => array(self::HAS_MANY, 'BookingRooms', 'booking_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ref_number' => 'Booking Reference',
			'staff_id' => 'Staff',
			'checkin' => 'Checkin',
			'checkout' => 'Checkout',
			'amount' => 'Amount',
			'noofnight' => 'Noofnight',
			'hotel_id' => 'Hotel',
			'btype' => 'Booking Type',
			'full_guest_name' => 'Full Guest Name',
			'smooking' => 'Smooking',
			'special_request' => 'Special Request',
			'nationality' => 'Nationality',
			'meal_plan' => 'Meal Plan',
			'email' => 'Email',
			'phone' => 'Phone',
			'adult' => 'Adult',
			'child' => 'Child',
			'exp_arrive' => 'Exp Arrive',
			'extra_charge' => 'Extra Charge',
			'discount' => 'Discount',
			'book_request' => 'Book Request',
			'request_email' => 'Request Email',
			'created' => 'Created',
			'modified' => 'Modified',
			'deleted' => 'Deleted',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($deleted =false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;
		
		
		$myId = Yii::app()->user->profile['child_id'];
		
		unset($_SESSION["partnerChild"]);		
		Yii::app()->functions->childIdHierarchy($myId);
		
		if (isset($_SESSION["partnerChild"])){
			$childId = implode(",",$_SESSION["partnerChild"]).",".$myId;
		}else{
			$childId = $myId;	
		}
		

		$criteria->compare('id', $this->id);
		$criteria->addCondition('staff_id in ('.$childId.')');
		$criteria->compare('checkin', $this->checkin, true);
		$criteria->compare('staff_id', $this->staff_id, true);
		$criteria->compare('checkout', $this->checkout, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('noofnight', $this->noofnight);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('btype', $this->btype, true);
		$criteria->compare('full_guest_name', $this->full_guest_name, true);
		$criteria->compare('smooking', $this->smooking, true);
		$criteria->compare('special_request', $this->special_request, true);
		$criteria->compare('nationality', $this->nationality, true);
		$criteria->compare('meal_plan', $this->meal_plan, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('adult', $this->adult, true);
		$criteria->compare('child', $this->child, true);
		$criteria->compare('exp_arrive', $this->exp_arrive, true);
		$criteria->compare('extra_charge', $this->extra_charge);
		$criteria->compare('discount', $this->discount);
		$criteria->compare('book_request', $this->book_request, true);
		$criteria->compare('request_email', $this->request_email, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);
		if(!$deleted)
			$criteria->compare('deleted', 0);
		else
			$criteria->compare('deleted', 1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function myInvoiceSearch($date="")
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		$myId = Yii::app()->user->profile['child_id'];
		
		unset($_SESSION["partnerChild"]);		
		Yii::app()->functions->childIdHierarchy($myId);
		
		if (isset($_SESSION["partnerChild"])){
			$childId = implode(",",$_SESSION["partnerChild"]);
		}else{
			$childId = 0;	
		}
		
		
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->addCondition('staff_id in ('.$childId.')');
		$criteria->compare('checkin', $this->checkin, true);
		$criteria->compare('checkout', $this->checkout, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('noofnight', $this->noofnight);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('btype', "book");
		$criteria->compare('full_guest_name', $this->full_guest_name, true);
		$criteria->compare('smooking', $this->smooking, true);
		$criteria->compare('special_request', $this->special_request, true);
		$criteria->compare('nationality', $this->nationality, true);
		$criteria->compare('meal_plan', $this->meal_plan, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('adult', $this->adult, true);
		$criteria->compare('child', $this->child, true);
		$criteria->compare('exp_arrive', $this->exp_arrive, true);
		$criteria->compare('extra_charge', $this->extra_charge);
		$criteria->compare('discount', $this->discount);
		$criteria->compare('book_request', $this->book_request, true);
		$criteria->compare('request_email', $this->request_email, true);
		
		if($date == "")
			$criteria->compare('created', $this->created, true);
		else
			$criteria->addCondition('created <= "'.$date.'"');	
			
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('deleted', 0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			
		));
	}
	
	
	public function myInvoiceSearchAll($date="",$aid="")
	{
		// @todo Please modify the following code to remove attributes that should not be searched.


		if($aid != ""){
			$myId = $aid;
		}else{

			$myId = Yii::app()->user->profile['child_id'];



		}


		unset($_SESSION["partnerChild"]);
		Yii::app()->functions->childIdHierarchy($myId);

		if (isset($_SESSION["partnerChild"])){
			$childId = 'select id from partners where parent_id="'.$myId.'"';
		}else{
			$childId = 0;
		}
		

		
		
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->addCondition('staff_id in ('.$childId.')');
		$criteria->compare('checkin', $this->checkin, true);
		$criteria->compare('checkout', $this->checkout, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('noofnight', $this->noofnight);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('btype', "book");
		$criteria->compare('full_guest_name', $this->full_guest_name, true);
		$criteria->compare('smooking', $this->smooking, true);
		$criteria->compare('special_request', $this->special_request, true);
		$criteria->compare('nationality', $this->nationality, true);
		$criteria->compare('meal_plan', $this->meal_plan, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('adult', $this->adult, true);
		$criteria->compare('child', $this->child, true);
		$criteria->compare('exp_arrive', $this->exp_arrive, true);
		$criteria->compare('extra_charge', $this->extra_charge);
		$criteria->compare('discount', $this->discount);
		$criteria->compare('book_request', $this->book_request, true);
		$criteria->compare('request_email', $this->request_email, true);
		
		if($date == "")
			$criteria->compare('created', $this->created, true);
		else
			$criteria->addCondition('created <= "'.$date.'"');	
			
		$criteria->compare('modified', $this->modified, true);
		$criteria->compare('deleted', 0);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
		));
	}
	
	public function dueInvoiceSearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		$myId = Yii::app()->user->profile['child_id'];

		$criteria=new CDbCriteria;       
		//custom query
		$criteria->select=array('t.*');
		$criteria->join = 'LEFT JOIN `booking_rooms` br  ON t.`id` = br.`booking_id` '; 
		//Add Different Condition
		
		$criteria->compare('checkin', $this->checkin, true);
		$criteria->compare('checkout', $this->checkout, true);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('noofnight', $this->noofnight);
		$criteria->compare('hotel_id', $this->hotel_id);
		$criteria->compare('btype', "book");
		$criteria->compare('full_guest_name', $this->full_guest_name, true);
		$criteria->compare('smooking', $this->smooking, true);
		$criteria->compare('special_request', $this->special_request, true);
		$criteria->compare('nationality', $this->nationality, true);
		$criteria->compare('meal_plan', $this->meal_plan, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('phone', $this->phone, true);
		$criteria->compare('adult', $this->adult, true);
		$criteria->compare('child', $this->child, true);
		$criteria->compare('exp_arrive', $this->exp_arrive, true);
		$criteria->compare('extra_charge', $this->extra_charge);
		$criteria->compare('discount', $this->discount);
		$criteria->compare('book_request', $this->book_request, true);
		$criteria->compare('request_email', $this->request_email, true);
		
		//$criteria->compare('userid',$this->userid,true);
		$criteria->addCondition("br.rate_level LIKE '%\"".$myId."\":%' ");
		//$criteria->addBetweenCondition('createdon',$this->datefrom,$this->dateto);
		// group by , if need
		$criteria->group='br.booking_id';
		//return new CActiveDataProvider($this, array(			'criteria'=>$criteria,		));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
		
		
	}
	
	
	public function getRateTotal(){
		
		$sql ="SELECT * FROM `booking_rooms` WHERE booking_id = ".$this->id;
		
		$bookingRate = Yii::app()->db->createCommand($sql)->queryAll();
		
		$totalPrice = $totalPrice2 = 0;
		
		$myId = Yii::app()->user->profile['child_id'];
		
		unset($_SESSION["partnerParent"]);		
		Yii::app()->functions->partnerIdHierarchy($myId);
		
		
		
		$parents = ($_SESSION["partnerParent"]);
		krsort($parents);
		
	
	
		
		foreach($bookingRate as $rate){
			
			if ($rate["breakfast"] < 0)
				$rate["breakfast"] = 0;
			
			if ($rate["lunch"] < 0)
				$rate["lunch"] = 0;
				
			if ($rate["dinner"] < 0)
				$rate["dinner"] = 0;	
			
			$meal_price = $rate["breakfast"] + $rate["lunch"] + $rate["dinner"];
			
			$rate_level = json_decode($rate["rate_level"],true);
			$final_price = $rate["base_price"] ;
			//echo $final_price . " ==> ";
			$no_of_room = $rate["no_of_room"];
			
			$totalPrice = 0;
			
			
			foreach($parents as $parent_id){
				
				if ($parent_id == 0)
					continue;
					
					
			
					
				if (isset($rate_level[$parent_id])){
					
					$priceRate = $rate_level[$parent_id];
				
					
					
					if ($priceRate["price_type"] == 1){
						$final_price += $priceRate["fix_price"];
					}elseif ($priceRate["price_type"] == 2){
						$final_price += ($final_price * ($priceRate["floating_price"]/100));
					}elseif ($priceRate["price_type"] == 3){						
						$fixprice = $final_price + $priceRate["fix_price"];
						
						$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
						
						
						if ($priceRate["hybrid_condition"] == 1){
							if($fixprice < $floating_price){
								$final_price = $fixprice;	
							}else{
								$final_price = $floating_price;	
							}
						}else{
							
							if($fixprice > $floating_price){
								$final_price = $fixprice ;	
							}else{
								$final_price = $floating_price;	
							}
							
						}						
					}
					
				}else{
						
				}
			}
			
			
			//echo $final_price . " ==> ";
			
			
		
			$priceRate = $rate_level[$myId];
			
			if ($priceRate["price_type"] == 1){
				$final_price += $priceRate["fix_price"];
			}elseif ($priceRate["price_type"] == 2){
				$final_price += ($final_price * ($priceRate["floating_price"]/100));
			}elseif ($priceRate["price_type"] == 3){
				
				
				$fixprice = $final_price + $priceRate["fix_price"];
				
				
				$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
				
				
				
				if ($priceRate["hybrid_condition"] == 1){
					if($fixprice < $floating_price){
						$final_price = $fixprice;	
					}else{
						$final_price = $floating_price;	
					}
				}else{
					
					if($fixprice > $floating_price){
						$final_price = $fixprice ;	
					}else{
						$final_price = $floating_price;	
					}
					
				}
				
			}
			
			$totalPrice2 += ($final_price * $no_of_room)  + $meal_price;
			
			//echo $final_price * $no_of_room . "<br>";
			
			//echo $rate["base_price"];
			
		} 
		
		
		
		echo $totalPrice2   + $this->extra_charge - $this->discount  ;
		
	}
	
	
	public function getMyRateTotal($output = "echo"){
		
		$sql ="SELECT * FROM `booking_rooms` WHERE booking_id = ".$this->id;
		
		$bookingRate = Yii::app()->db->createCommand($sql)->queryAll();
		
		$totalPrice = $totalPrice2 = 0;
		
		$myId = Yii::app()->user->profile['child_id'];
		
		unset($_SESSION["partnerParent"]);		
		Yii::app()->functions->partnerIdHierarchy($myId);
		
		
		
		$parents = ($_SESSION["partnerParent"]);
		krsort($parents);
		
		
		unset($_SESSION["partnerChild"]);		
		Yii::app()->functions->childIdHierarchy($myId);
		
		if (isset($_SESSION["partnerChild"]))		
			$childs = ($_SESSION["partnerChild"]);
		else
			$childs = array();
		//krsort($childs);
		
	
	
		
		foreach($bookingRate as $rate){
			
			
			if ($rate["breakfast"] < 0)
				$rate["breakfast"] = 0;
			
			if ($rate["lunch"] < 0)
				$rate["lunch"] = 0;
				
			if ($rate["dinner"] < 0)
				$rate["dinner"] = 0;	
			

			$meal_price = $rate["breakfast"] + $rate["lunch"] + $rate["dinner"];
			
			$rate_level = json_decode($rate["rate_level"],true);
			$final_price = $rate["base_price"] ;
			//echo $final_price . " ==> ";
			$no_of_room = $rate["no_of_room"];
			
			$totalPrice = 0;
			if (isset(Yii::app()->user->partner) && Yii::app()->user->partner['parent_id'] == 0){
				if(isset($rate_level))
					$child_key = key($rate_level);
				else
					$child_key = 0;
				
			}elseif (isset(Yii::app()->user->partner) && isset(Yii::app()->user->partner['parent_id'])){
				
				if(isset($rate_level)){
					$keys = array_keys($rate_level);
					if(isset($keys[array_search($myId,$keys)+1]))
						$child_key = $keys[array_search($myId,$keys)+1];
					elseif(isset($keys[array_search($myId,$keys)]))
						$child_key = $keys[array_search($myId,$keys)];
				}else{
					$child_key = 0;
				}
			}else{
				$child_key = 0;
			}
			
			
			//echo $myId . " == " . $child_key . "<br>";
			
			
			if ($myId != $child_key){
			
				foreach($parents as $parent_id){
					
					if ($parent_id == 0)
						continue;
						
						
					if (isset($rate_level[$parent_id])){
						
						$priceRate = $rate_level[$parent_id];
						
						if ($priceRate["price_type"] == 1){
							$final_price += $priceRate["fix_price"];
						}elseif ($priceRate["price_type"] == 2){
							$final_price += ($final_price * ($priceRate["floating_price"]/100));
						}elseif ($priceRate["price_type"] == 3){						
							$fixprice = $final_price + $priceRate["fix_price"];
							
							$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
							
							
							if ($priceRate["hybrid_condition"] == 1){
								if($fixprice < $floating_price){
									$final_price = $fixprice;	
								}else{
									$final_price = $floating_price;	
								}
							}else{
								
								if($fixprice > $floating_price){
									$final_price = $fixprice ;	
								}else{
									$final_price = $floating_price;	
								}
								
							}						
						}
						
					}else{
							
					}
					
					//echo $final_price . "<br>";
				}
				
				
				if(isset($rate_level[$myId])){
				
				
				$priceRate = $rate_level[$myId];
				
				if ($priceRate["price_type"] == 1){
					$final_price += $priceRate["fix_price"];
				}elseif ($priceRate["price_type"] == 2){
					$final_price += ($final_price * ($priceRate["floating_price"]/100));
				}elseif ($priceRate["price_type"] == 3){
					
					
					$fixprice = $final_price + $priceRate["fix_price"];
					
					
					$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
					
					
					
					if ($priceRate["hybrid_condition"] == 1){
						if($fixprice < $floating_price){
							$final_price = $fixprice;	
						}else{
							$final_price = $floating_price;	
						}
					}else{
						
						if($fixprice > $floating_price){
							$final_price = $fixprice ;	
						}else{
							$final_price = $floating_price;	
						}
						
					}
					
				}
				
				
			}else{
				//$final_price = $rate["base_price"];
			}
			
			
			
				if ($child_key  != $this->staff_id || true){
				
					
					//echo "Child Key = " . $child_key . " == staff_id = " . $this->staff_id . "<br> <pre>" ;
					
					//print_r($rate_level);
					
					//die ();
					
					if(!isset($rate_level[$child_key])){
						//$final_price += $rate["room_rate"];
						$totalPrice2 += ($final_price * $no_of_room) + $meal_price ;
						continue;
					}
				
					$priceRate = $rate_level[$child_key];
				
					
					
					if ($priceRate["price_type"] == 1){
						$final_price += $priceRate["fix_price"];
					}elseif ($priceRate["price_type"] == 2){
						$final_price += ($final_price * ($priceRate["floating_price"]/100));
					}elseif ($priceRate["price_type"] == 3){						
						$fixprice = $final_price + $priceRate["fix_price"];
						
						$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
						
						
						//echo "Fix = " .$fixprice . " === Floating = " . $floating_price . "<br>";
						
						if ($priceRate["hybrid_condition"] == 1){
							if($fixprice < $floating_price){
								$final_price = $fixprice;	
							}else{
								$final_price = $floating_price;	
							}
						}else{
							
							if($fixprice > $floating_price){
								$final_price = $fixprice ;	
							}else{
								$final_price = $floating_price;	
							}
							
						}						
					}
					//echo $final_price . "<br>";
					
				}
				
				
				
			}else{
				
				
				$final_price = $rate["room_rate"] ;;

				
			}
			
			
			
			
			
			
			
			
			
			
			$totalPrice2 += ($final_price * $no_of_room) + $meal_price ;
			
			//echo $final_price * $no_of_room . "<br>";
			
			//echo $rate["base_price"];
			
		} 
		
		
		
		$totalPrice2 +=  $this->extra_charge - $this->discount;
		
		if($output == "echo")
			echo $totalPrice2  ;
		else
			return $totalPrice2 ;
	}
	
	
	public function getMyRoomRateTotal($room_id,$output = ""){
		
		$sql ="SELECT * FROM `booking_rooms` WHERE booking_id = ".$this->id . " and room_id='".$room_id."'";
		
		$bookingRate = Yii::app()->db->createCommand($sql)->queryAll();
		
		$totalPrice = $totalPrice2 = 0;
		
		$myId = Yii::app()->user->profile['child_id'];
		
		unset($_SESSION["partnerParent"]);		
		Yii::app()->functions->partnerIdHierarchy($myId);
		
		
		
		$parents = ($_SESSION["partnerParent"]);
		krsort($parents);
		
		
		unset($_SESSION["partnerChild"]);		
		Yii::app()->functions->childIdHierarchy($myId);
		
		if (isset($_SESSION["partnerChild"]))		
			$childs = ($_SESSION["partnerChild"]);
		else
			$childs = array();
		//krsort($childs);
		
	
	
		
		foreach($bookingRate as $rate){
			
			
			if ($rate["breakfast"] < 0)
				$rate["breakfast"] = 0;
			
			if ($rate["lunch"] < 0)
				$rate["lunch"] = 0;
				
			if ($rate["dinner"] < 0)
				$rate["dinner"] = 0;	
			

			$meal_price = $rate["breakfast"] + $rate["lunch"] + $rate["dinner"];
			
			$rate_level = json_decode($rate["rate_level"],true);
			$final_price = $rate["base_price"] ;
			//echo $final_price . " ==> ";
			$no_of_room = $rate["no_of_room"];
			
			$totalPrice = 0;
			if (isset(Yii::app()->user->partner) && Yii::app()->user->partner['parent_id'] == 0){
				if(isset($rate_level))
					$child_key = key($rate_level);
				else
					$child_key = 0;
				
			}elseif (isset(Yii::app()->user->partner) && isset(Yii::app()->user->partner['parent_id'])){
				
				if(isset($rate_level)){
					$keys = array_keys($rate_level);
					if(isset($keys[array_search($myId,$keys)+1]))
						$child_key = $keys[array_search($myId,$keys)+1];
					elseif(isset($keys[array_search($myId,$keys)]))
						$child_key = $keys[array_search($myId,$keys)];
				}else{
					$child_key = 0;
				}
			}else{
				$child_key = 0;
			}
			
			
			//echo $myId . " == " . $child_key . "<br>";
			
			
			if ($myId != $child_key){
			
				foreach($parents as $parent_id){
					
					if ($parent_id == 0)
						continue;
						
						
					if (isset($rate_level[$parent_id])){
						
						$priceRate = $rate_level[$parent_id];
						
						if ($priceRate["price_type"] == 1){
							$final_price += $priceRate["fix_price"];
						}elseif ($priceRate["price_type"] == 2){
							$final_price += ($final_price * ($priceRate["floating_price"]/100));
						}elseif ($priceRate["price_type"] == 3){						
							$fixprice = $final_price + $priceRate["fix_price"];
							
							$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
							
							
							if ($priceRate["hybrid_condition"] == 1){
								if($fixprice < $floating_price){
									$final_price = $fixprice;	
								}else{
									$final_price = $floating_price;	
								}
							}else{
								
								if($fixprice > $floating_price){
									$final_price = $fixprice ;	
								}else{
									$final_price = $floating_price;	
								}
								
							}						
						}
						
					}else{
							
					}
					
					//echo $final_price . "<br>";
				}
				
				
				if(isset($rate_level[$myId])){
				
				
				$priceRate = $rate_level[$myId];
				
				if ($priceRate["price_type"] == 1){
					$final_price += $priceRate["fix_price"];
				}elseif ($priceRate["price_type"] == 2){
					$final_price += ($final_price * ($priceRate["floating_price"]/100));
				}elseif ($priceRate["price_type"] == 3){
					
					
					$fixprice = $final_price + $priceRate["fix_price"];
					
					
					$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
					
					
					
					if ($priceRate["hybrid_condition"] == 1){
						if($fixprice < $floating_price){
							$final_price = $fixprice;	
						}else{
							$final_price = $floating_price;	
						}
					}else{
						
						if($fixprice > $floating_price){
							$final_price = $fixprice ;	
						}else{
							$final_price = $floating_price;	
						}
						
					}
					
				}
				
				
			}else{
				//$final_price = $rate["base_price"];
			}
			
			
			
				if ($child_key  != $this->staff_id || true){
				
					
					//echo "Child Key = " . $child_key . " == staff_id = " . $this->staff_id . "<br> <pre>" ;
					
					//print_r($rate_level);
					
					//die ();
					
					if(!isset($rate_level[$child_key])){
						//$final_price += $rate["room_rate"];
						$totalPrice2 += ($final_price * $no_of_room) + $meal_price ;
						continue;
					}
				
					$priceRate = $rate_level[$child_key];
				
					
					
					if ($priceRate["price_type"] == 1){
						$final_price += $priceRate["fix_price"];
					}elseif ($priceRate["price_type"] == 2){
						$final_price += ($final_price * ($priceRate["floating_price"]/100));
					}elseif ($priceRate["price_type"] == 3){						
						$fixprice = $final_price + $priceRate["fix_price"];
						
						$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
						
						
						//echo "Fix = " .$fixprice . " === Floating = " . $floating_price . "<br>";
						
						if ($priceRate["hybrid_condition"] == 1){
							if($fixprice < $floating_price){
								$final_price = $fixprice;	
							}else{
								$final_price = $floating_price;	
							}
						}else{
							
							if($fixprice > $floating_price){
								$final_price = $fixprice ;	
							}else{
								$final_price = $floating_price;	
							}
							
						}						
					}
					//echo $final_price . "<br>";
					
				}
				
				
				
			}else{
				
				
				$final_price = $rate["room_rate"] ;;

				
			}
			
			
			
			
			
			
			
			
			
			
			$totalPrice2 += ($final_price * $no_of_room) + $meal_price;
			
			//echo $final_price * $no_of_room . "<br>";
			
			//echo $rate["base_price"];
			
		} 
		
		
		
		$totalPrice2 +=  $this->extra_charge - $this->discount;
		
		if($output == "echo")
			echo $totalPrice2  ;
		else
			return $totalPrice2 ;
	}
	
	public function getMyRoomRateTotal2($room_id){
		
		$sql = "SELECT * FROM `booking_rooms` WHERE booking_id = ".$this->id. " and room_id = ".$room_id;
		
		$bookingRate = Yii::app()->db->createCommand($sql)->queryAll();
		
		$totalPrice = $totalPrice2 = 0;
		
		$myId = Yii::app()->user->profile['child_id'];
		
		unset($_SESSION["partnerParent"]);		
		Yii::app()->functions->partnerIdHierarchy($myId);
		
		
		
		$parents = ($_SESSION["partnerParent"]);
		krsort($parents);
		
		
		unset($_SESSION["partnerChild"]);		
		Yii::app()->functions->childIdHierarchy($myId);
		
		$childs = ($_SESSION["partnerChild"]);
		//krsort($childs);
		
	
	
		
		foreach($bookingRate as $rate){
			
			if ($rate["breakfast"] < 0)
				$rate["breakfast"] = 0;
			
			if ($rate["lunch"] < 0)
				$rate["lunch"] = 0;
				
			if ($rate["dinner"] < 0)
				$rate["dinner"] = 0;	
			
			$meal_price = $rate["breakfast"] + $rate["lunch"] + $rate["dinner"];
			
			$rate_level = json_decode($rate["rate_level"],true);
			$final_price = $rate["base_price"] ;
			//echo $final_price . " ==> ";
			$no_of_room = $rate["no_of_room"];
			
			$totalPrice = 0;
			if (isset(Yii::app()->user->partner) && Yii::app()->user->partner['parent_id'] == 0){
				
				$child_key = key($rate_level);
				
			}elseif (isset(Yii::app()->user->partner) && isset(Yii::app()->user->partner['parent_id'])){
				if (isset($rate_level)){
					$keys = array_keys($rate_level);
					$child_key = $keys[array_search($myId,$keys)+1];
				}else{
					$child_key = 0;	
				}
			}else{
				$child_key = key($rate_level);
			}
			
			
			
			
			
			foreach($parents as $parent_id){
				
				if ($parent_id == 0)
					continue;
					
					
				if (isset($rate_level[$parent_id])){
					
					$priceRate = $rate_level[$parent_id];
					
					if ($priceRate["price_type"] == 1){
						$final_price += $priceRate["fix_price"];
					}elseif ($priceRate["price_type"] == 2){
						$final_price += ($final_price * ($priceRate["floating_price"]/100));
					}elseif ($priceRate["price_type"] == 3){						
						$fixprice = $final_price + $priceRate["fix_price"];
						
						$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
						
						
						if ($priceRate["hybrid_condition"] == 1){
							if($fixprice < $floating_price){
								$final_price = $fixprice;	
							}else{
								$final_price = $floating_price;	
							}
						}else{
							
							if($fixprice > $floating_price){
								$final_price = $fixprice ;	
							}else{
								$final_price = $floating_price;	
							}
							
						}						
					}
					
				}else{
						
				}
				
				
			}
			
			
			
			
			//echo $final_price . " ==> ";
			
			
			
			
			if(isset($rate_level[$myId])){
				
				
				$priceRate = $rate_level[$myId];
				
				if ($priceRate["price_type"] == 1){
					$final_price += $priceRate["fix_price"];
				}elseif ($priceRate["price_type"] == 2){
					$final_price += ($final_price * ($priceRate["floating_price"]/100));
				}elseif ($priceRate["price_type"] == 3){
					
					
					$fixprice = $final_price + $priceRate["fix_price"];
					
					
					$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
					
					
					
					if ($priceRate["hybrid_condition"] == 1){
						if($fixprice < $floating_price){
							$final_price = $fixprice;	
						}else{
							$final_price = $floating_price;	
						}
					}else{
						
						if($fixprice > $floating_price){
							$final_price = $fixprice ;	
						}else{
							$final_price = $floating_price;	
						}
						
					}
					
				}
				
				
			}else{
				//$final_price = $rate["base_price"];
			}
			
			
			//echo $final_price . "<br>";
			
			
			if ($child_key  != $this->staff_id ){
				
				
				//echo $child_key . " == " . $this->staff_id . "<br>";
			
				$priceRate = $rate_level[$child_key];
			
				
				
				if ($priceRate["price_type"] == 1){
					$final_price += $priceRate["fix_price"];
				}elseif ($priceRate["price_type"] == 2){
					$final_price += ($final_price * ($priceRate["floating_price"]/100));
				}elseif ($priceRate["price_type"] == 3){						
					$fixprice = $final_price + $priceRate["fix_price"];
					
					$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
					
					
					if ($priceRate["hybrid_condition"] == 1){
						if($fixprice < $floating_price){
							$final_price = $fixprice;	
						}else{
							$final_price = $floating_price;	
						}
					}else{
						
						if($fixprice > $floating_price){
							$final_price = $fixprice ;	
						}else{
							$final_price = $floating_price;	
						}
						
					}						
				}
				
				
			}
			
			
			
			
			$totalPrice2 += ($final_price * $no_of_room)  + $meal_price ;
			
			//echo $final_price * $no_of_room . "<br>";
			
			//echo $rate["base_price"];
			
		} 
		
		
		$totalPrice2 +=  $this->extra_charge - $this->discount;
		return $totalPrice2  ;
		
	}
	
	
	public function getRoomRateTotal($room_id){
		
		$sql ="SELECT * FROM `booking_rooms` WHERE booking_id = ".$this->id . " and room_id = ".$room_id  ;
		
		$bookingRate = Yii::app()->db->createCommand($sql)->queryAll();
		
		$totalPrice = $totalPrice2 = 0;
		
		$myId = Yii::app()->user->profile['child_id'];
		
		unset($_SESSION["partnerParent"]);		
		Yii::app()->functions->partnerIdHierarchy($myId);
		
		
		
		$parents = ($_SESSION["partnerParent"]);
		krsort($parents);
	
		
		foreach($bookingRate as $rate){
			
			if ($rate["breakfast"] < 0)
				$rate["breakfast"] = 0;
			
			if ($rate["lunch"] < 0)
				$rate["lunch"] = 0;
				
			if ($rate["dinner"] < 0)
				$rate["dinner"] = 0;	
			
			$meal_price = $rate["breakfast"] + $rate["lunch"] + $rate["dinner"];
			
			$rate_level = json_decode($rate["rate_level"],true);
			$final_price = $rate["base_price"] ;
			$no_of_room = $rate["no_of_room"];
			
			
		
			
			$totalPrice = 0;
			
			
			foreach($parents as $parent_id){
				
				if ($parent_id == 0)
					continue;
					
					
			
					
				if (isset($rate_level[$parent_id])){
					
					$priceRate = $rate_level[$parent_id];
				
					
					
					if ($priceRate["price_type"] == 1){
						$final_price += $priceRate["fix_price"];
					}elseif ($priceRate["price_type"] == 2){
						$final_price += ($final_price * ($priceRate["floating_price"]/100));
					}elseif ($priceRate["price_type"] == 3){
						
						
						$fixprice = $final_price + $priceRate["fix_price"];
						
						$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
						
						
						if ($priceRate["hybrid_condition"] == 1){
							if($fixprice < $floating_price){
								$final_price = $fixprice;	
							}else{
								$final_price = $floating_price;	
							}
						}else{
							
							if($fixprice > $floating_price){
								$final_price = $fixprice ;	
							}else{
								$final_price = $floating_price;	
							}
							
						}
						
						
						
						
					}
					
				
					
					
					
				}else{
					
					
					//$totalPrice += $final_price ;
						
				}
					
				
				
				
				
			}
			
			
			
			
			
			
			
			if (!isset($rate_level[$myId])){
				
				$value = reset($rate_level);				
				$priceRate = $value;				
		
			}else{
				$priceRate = $rate_level[$myId];
			}
			
			
			if ($priceRate["price_type"] == 1){
				$final_price += $priceRate["fix_price"];
			}elseif ($priceRate["price_type"] == 2){
				$final_price += ($final_price * ($priceRate["floating_price"]/100));
			}elseif ($priceRate["price_type"] == 3){
				
				
				$fixprice = $final_price + $priceRate["fix_price"];
				
				$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
				
				
				if ($priceRate["hybrid_condition"] == 1){
					if($fixprice < $floating_price){
						$final_price = $fixprice;	
					}else{
						$final_price = $floating_price;	
					}
				}else{
					
					if($fixprice > $floating_price){
						$final_price = $fixprice ;	
					}else{
						$final_price = $floating_price;	
					}
					
				}
				
				
				
				
			}
		
			
			
			
			$totalPrice2 += ($final_price * $no_of_room)  + $meal_price ;
			
			//echo $rate["base_price"];
			
		} 
		$totalPrice2 +=  $this->extra_charge - $this->discount;
		
		return $totalPrice2;
		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Booking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
