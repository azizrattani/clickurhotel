<?php

/**
 * This is the model class for table "booking_meal_rate".
 *
 * The followings are the available columns in table 'booking_meal_rate':
 * @property integer $id
 * @property integer $booking_room_id
 * @property string $meal_type
 * @property double $price
 *
 * The followings are the available model relations:
 * @property BookingRooms $bookingRoom
 */
class BookingMealRate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'booking_meal_rate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('booking_room_id', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('meal_type', 'length', 'max'=>255),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, booking_room_id, meal_type, price', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bookingRoom' => array(self::BELONGS_TO, 'BookingRooms', 'booking_room_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'booking_room_id' => 'Booking Room',
			'meal_type' => 'Meal Type',
			'price' => 'Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('booking_room_id', $this->booking_room_id);
		$criteria->compare('meal_type', $this->meal_type, true);
		$criteria->compare('price', $this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BookingMealRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        //->created = new CDbExpression('NOW()');
	    }

       // $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
