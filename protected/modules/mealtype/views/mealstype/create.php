<?php
/* @var $this MealsTypeController */
/* @var $model MealsType */

$this->pageTitle = 'Meals Types - Create';
$this->breadcrumbs = array(
	'Meals Types' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>