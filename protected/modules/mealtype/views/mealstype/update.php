<?php
/* @var $this MealsTypeController */
/* @var $model MealsType */

$this->pageTitle = 'Meals Types - Update';
$this->breadcrumbs = array(
	'Meals Types' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>