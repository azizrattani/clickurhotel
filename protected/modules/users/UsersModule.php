<?php

class UsersModule extends CWebModule
{
    public function init()
    {
        $this->setImport(array(
            'users.models.*'
        ));
    }

    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}