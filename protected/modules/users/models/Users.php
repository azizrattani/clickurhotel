<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $username
 * @property string $password
 * @property integer $role_id
 * @property integer $child_id
 * @property string $photo
 * @property integer $status
 * @property integer $deleted
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property Roles $role
 */
class Users extends CActiveRecord
{
	public $_identity;
    public $rememberMe;
    public $repassword;
    public $role;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, email, username, password, repassword', 'required', 'on' => 'create'),
            array('first_name, last_name, email, username', 'required', 'on' => array('update')),
			array('first_name, username', 'required', 'on' => array('profile')),
            
            array('username, password', 'required', 'on' => 'login'),
            array('password', 'authenticate', 'on' => 'login'),
            array('rememberMe', 'boolean', 'on' => 'login'),

            array('username', 'validateUsername', 'on' => array('create', 'update')),
            array('email', 'validateEmail', 'on' => array('create', 'update')),
            array('repassword', 'validatePassword', 'on' => array('create', 'update', 'profile')),

            array('role_id, child_id, status, deleted', 'numerical', 'integerOnly'=>true),
            array('first_name, last_name, email, photo, username', 'length', 'max' => 255),
            array('password', 'length', 'max' => 35),
			array('created, modified', 'safe'),

            array('email', 'email'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, last_name, email, username, password, role_id, child_id, photo, status, deleted, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'role' => array(self::BELONGS_TO, 'Roles', 'role_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'username' => 'Username',
			'password' => 'Password',
			'role_id' => 'Role',
			'child_id' => 'Child',
			'photo' => 'Photo',
			'status' => 'Status',
			'deleted' => 'Deleted',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.first_name', $this->first_name, true);
        $criteria->compare('t.last_name', $this->last_name, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.username', $this->username, true);
        $criteria->compare('t.password', $this->password, true);
        $criteria->compare('t.role_id', $this->role_id);
        $criteria->compare('t.child_id', $this->child_id);
        $criteria->compare('t.photo', $this->photo, true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.deleted', 0);
        $criteria->compare('t.created', $this->created, true);
        $criteria->compare('t.modified', $this->modified, true);

        $criteria->select = "t.*, r.`title` AS `role`";
        $criteria->join = "INNER JOIN `roles` r ON r.`id` = t.`role_id`";

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}

	/**
     * Validate unique username.
     */
    public function validateUsername($attribute, $params)
    {
    	$user = Users::model()->find('LOWER(username) = "' . $this->{$attribute} .'" AND deleted = 0');

        if ($this->isNewRecord && $user !== null)
        {
            $this->addError($attribute, 'Username already registered.');
        }
        elseif ($user !== null && !$this->isNewRecord && $this->id != $user->id)
        {
            $this->addError($attribute, 'Username already registered.');
        }
    }

    /**
     * Validate unique email.
     */
    public function validateEmail($attribute, $params)
    {
        $user = Users::model()->find('LOWER(email) = "' . $this->{$attribute} .'" AND deleted = 0');

        if ($this->isNewRecord && $user !== null)
        {
            $this->addError($attribute, 'Email address already registered.');
        }
        elseif ($user !== null && !$this->isNewRecord && $this->id != $user->id)
        {
            $this->addError($attribute, 'Email address already registered.');
        }
    }

    /**
     * Validate both passwords.
     */
    public function validatePassword($attribute, $params)
    {
        if ($this->password != $this->repassword)
        {
            $this->addError($attribute, 'Password and Retype Password must be matched.');
        }
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $this->_identity = new UserIdentity($this->username, $this->password);
            if (!$this->_identity->authenticate())
                $this->addError('password', 'Incorrect username or password.');
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_identity === null)
        {
            $this->_identity = new UserIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE)
        {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0;
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        else
            return false;
    }

}