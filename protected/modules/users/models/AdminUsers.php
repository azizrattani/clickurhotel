<?php

/**
 * This is the model class for table "admin_users".
 *
 * The followings are the available columns in table 'admin_users':
 * @property integer $id
 * @property string $user_name
 * @property string $email_add
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property integer $is_admin
 * @property integer $is_active
 * @property string $added_on
 */
class AdminUsers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email_add, password, first_name, added_on', 'required'),
			array('is_admin, is_active', 'numerical', 'integerOnly'=>true),
			array('user_name, email_add, password', 'length', 'max'=>100),
			array('first_name, last_name', 'length', 'max'=>60),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_name, email_add, password, first_name, last_name, is_admin, is_active, added_on', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_name' => 'User Name',
			'email_add' => 'Email Add',
			'password' => 'Password',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'is_admin' => 'Is Admin',
			'is_active' => 'Is Active',
			'added_on' => 'Added On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_name', $this->user_name, true);
		$criteria->compare('email_add', $this->email_add, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('first_name', $this->first_name, true);
		$criteria->compare('last_name', $this->last_name, true);
		$criteria->compare('is_admin', $this->is_admin);
		$criteria->compare('is_active', $this->is_active);
		$criteria->compare('added_on', $this->added_on, true);

		return new CActiveDataProvider($this, array('criteria'=>$criteria,
		  'pagination' => array(
			  'pageSize' => 100,
			),      
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
