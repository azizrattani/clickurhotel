<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 * @property integer $id
 * @property integer $type
 * @property string $first_name
 * @property string $last_name
 * @property string $company_name
 * @property string $slug
 * @property string $desc
 * @property string $web_address
 * @property string $image
 * @property string $email
 * @property string $contact
 * @property integer $country_id
 * @property integer $city_id
 * @property string $city
 * @property string $address
 * @property string $created_on
 * @property string $contact_count
 * @property integer $has_book_button
 * @property string $area
 * @property integer $IATA
 * @property integer $HOAP
 * @property integer $TAAP
 * @property integer $IS_PAID
 * @property integer $is_active
 */
class Clients extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, email, company_name, contact, desc, country_id, web_address, city_id', 'required'),
			array('type, country_id, city_id, has_book_button, IATA, HOAP, TAAP, IS_PAID, is_active', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, contact', 'length', 'max'=>100),
			array('web_address', 'length', 'max'=>256),
			array('email, city', 'length', 'max'=>150),
			array('contact_count', 'length', 'max'=>20),
			array('address, created_on, area, commission', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, first_name, last_name, company_name, slug, desc, web_address, image, email, contact, country_id, city_id, city, address, created_on, contact_count, has_book_button, area, IATA, HOAP, TAAP, IS_PAID, is_active,commission', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'company_name' => 'Company Name',
			'slug' => 'Slug',
			'desc' => 'Description',
			'web_address' => 'Web Address',
			'image' => 'Image',
			'email' => 'Email',
			'contact' => 'Contact',
			'country_id' => 'Country',
			'city_id' => 'City',
			'city' => 'City',
			'address' => 'Address',
			'created_on' => 'Created On',
			'contact_count' => 'Contact Count',
			'has_book_button' => 'Has Book Button',
			'area' => 'Area',
			'IATA' => 'Iata',
			'HOAP' => 'Hoap',
			'TAAP' => 'Taap',
			'IS_PAID' => 'Is Paid',
			'is_active' => 'Is Active',
			'commission' => 'Commission',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('type', $this->type);
		$criteria->compare('first_name', $this->first_name, true);
		$criteria->compare('last_name', $this->last_name, true);
		$criteria->compare('company_name', $this->company_name, true);
		$criteria->compare('slug', $this->slug, true);
		$criteria->compare('desc', $this->desc, true);
		$criteria->compare('web_address', $this->web_address, true);
		$criteria->compare('image', $this->image, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('contact', $this->contact, true);
		$criteria->compare('country_id', $this->country_id);
		$criteria->compare('city_id', $this->city_id);
		$criteria->compare('city', $this->city, true);
		$criteria->compare('address', $this->address, true);
		$criteria->compare('created_on', $this->created_on, true);
		$criteria->compare('contact_count', $this->contact_count, true);
		$criteria->compare('has_book_button', $this->has_book_button);
		$criteria->compare('area', $this->area, true);
		$criteria->compare('IATA', $this->IATA);
		$criteria->compare('HOAP', $this->HOAP);
		$criteria->compare('TAAP', $this->TAAP);
		$criteria->compare('IS_PAID', $this->IS_PAID);
		$criteria->compare('is_active', $this->is_active);

		return new CActiveDataProvider($this, array('criteria'=>$criteria,
		  'pagination' => array(
			  'pageSize' => 100,
			),      
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
