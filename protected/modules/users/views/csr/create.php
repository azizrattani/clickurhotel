<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle = 'CSR Users - Create';
$this->breadcrumbs = array(
	'CSR Users' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>