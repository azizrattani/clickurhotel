<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle = 'CSR Users - Update';
$this->breadcrumbs = array(
	'CSR Users' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>