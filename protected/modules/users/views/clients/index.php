<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Manage Clients Users';
$this->breadcrumbs = array(
	'Clients Users',
    'Manage'
);
?>

<div class="box-body table-responsive no-padding">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'users-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'itemsCssClass' => 'table table-condensed table-striped table-hover table-bordered',
        'filterCssClass' => 'filters',
        'pagerCssClass' => 'pull-right',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'htmlOptions' => array('class' => 'pagination')
        ),
        'columns' => array(
    		array(
                'name' => 'id',
                'htmlOptions' => array('style' => 'width: 50px; text-align: center;'),
            ),
    		'first_name',
    		'last_name',
    		'email',
    		'username',
            array(
                'name' => 'status',
                'value' => '$data->status ? "Active" : "Inactive"',
                'filter' => array('1' => 'Active', '0' => 'Inactive')
            ),
            array(
                'htmlOptions' => array('style' => 'width: 75px; text-align: center;'),
                'class' => 'CButtonColumn',
                'template' => '{update}',
                'buttons' => array(
                    'update' => array(
                        'label' => '',
                        'imageUrl' => '',
                        'options' => array('title' => 'Update', 'class' => 'fa fa-pencil-square-o', 'style' => 'margin: 0px 3px;'),
                        'visible' => "$updateAllowed"
                    )
                )
            )
        ),
    )); ?>
</div>