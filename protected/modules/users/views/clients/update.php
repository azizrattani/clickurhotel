<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle = 'Clients Users - Update';
$this->breadcrumbs = array(
	'Clients Users' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>