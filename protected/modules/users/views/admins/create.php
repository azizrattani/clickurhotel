<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle = 'Admin Users - Create';
$this->breadcrumbs = array(
	'Admin Users' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>