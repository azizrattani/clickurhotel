<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle = 'Admin Users - Update';
$this->breadcrumbs = array(
	'Admin Users' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>