<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle = 'Users - View';
$this->breadcrumbs=array(
	'Users' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'first_name',
		'last_name',
		'email',
		'username',
        array(
            'name' => 'role_id',
            'value' => Roles::model()->findByPk($model->role_id)->title,
        ),
        array(
            'name' => 'status',
            'value' => $model->status ? "Active" : "Inactive",
        ),	
		array(
			'name' => 'created',
			'value' => date('d/M/Y h:i a', strtotime($model->created))
		),
		array(
			'name' => 'modified',
			'value' => date('d/M/Y h:i a', strtotime($model->modified))
		),
	),
)); ?>

<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
    <a href="<?= $this->createUrl('update'); ?>/id/<?=$model->id?>" class="btn btn-default pull-right">update</a>
</div>
