<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle = 'Users - Update';
$this->breadcrumbs = array(
	'Users' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>