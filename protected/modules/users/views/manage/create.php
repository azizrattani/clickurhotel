<?php
/* @var $this UsersController */
/* @var $model Users */

$this->pageTitle = 'Users - Create';
$this->breadcrumbs = array(
	'Users' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>