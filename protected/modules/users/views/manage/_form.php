<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'users-form',
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?= $form->errorSummary($model, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

	<br />

	<div class="form-group">
		<?= $form->labelEx($model, 'first_name'); ?>
		<?= $form->textField($model, 'first_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'last_name'); ?>
		<?= $form->textField($model, 'last_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'email'); ?>
		<?= $form->textField($model, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'username'); ?>
		<?= $form->textField($model, 'username', array('class' => 'form-control', 'size' => 25, 'maxlength' => 25)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'password'); ?>
		<?= $form->passwordField($model, 'password', array('class' => 'form-control', 'size' => 35, 'maxlength' => 35)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'repassword'); ?>
		<?= $form->passwordField($model, 'repassword', array('class' => 'form-control', 'size' => 35, 'maxlength' => 35)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'role_id'); ?>
		<?= $form->dropDownList($model, 'role_id', CHtml::listData(Roles::model()->findAllByAttributes(array('deleted' => 0)), 'id', 'title'), array('class' => 'form-control')); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($model, 'status'); ?>
		<?= $form->dropDownList($model, 'status', array('1' => 'Active', '0' => 'Inactive'), array('class' => 'form-control')); ?>
	</div>

	<div class="box-footer">
		<a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
		<?= CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>