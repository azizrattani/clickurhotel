<?php
/* @var $this RolesController */
/* @var $model Roles */

$this->pageTitle = 'Roles - Create';
$this->breadcrumbs = array(
	'Roles' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>