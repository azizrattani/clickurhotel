<?php
/* @var $this RolesController */
/* @var $model Roles */

$this->pageTitle = 'Roles - View';
$this->breadcrumbs=array(
	'Roles' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'title',
		array(
			'name' => 'created',
			'value' => date('d/M/Y h:i a', strtotime($model->created))
		),
		array(
			'name' => 'modified',
			'value' => date('d/M/Y h:i a', strtotime($model->modified))
		),
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
    <a href="<?= $this->createUrl('update'); ?>/id/<?=$model->id?>" class="btn btn-default pull-right">update</a>
</div>