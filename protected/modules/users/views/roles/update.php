<?php
/* @var $this RolesController */
/* @var $model Roles */

$this->pageTitle = 'Roles - Update';
$this->breadcrumbs = array(
	'Roles' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>