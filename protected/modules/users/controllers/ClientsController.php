<?php

class ClientsController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new Users('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Users']))
		{
			$model->attributes = $_GET['Users'];
		}

		$model->role_id = Yii::app()->user->client_role;

        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');

		$this->render('index', compact('model', 'updateAllowed', 'deleteAllowed'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $model->scenario = 'update';
        
		if(isset($_POST['Users']))
		{
			$model->attributes = $_POST['Users'];
			$model->role_id    = Yii::app()->user->client_role;

			// Just to bypass email validation in users
			$model->email = time().'abc@abc.com';

			if ($model->validate())
            {
            	$clientModel = Clients::model()->findByPk($model->child_id);

            	$clientModel->email      = $_POST['Users']['email'];
            	$clientModel->first_name = $model->first_name;
            	$clientModel->last_name  = $model->last_name;
            	$clientModel->is_active  = $model->status;
                $clientModel->save(false);

            	// Update only username & password in users
                $userModel = $this->loadModel($id);
                $userModel->username = $_POST['Users']['username'];

                if(isset($_POST['Users']['password']) && $_POST['Users']['password'] != '')
                {
                    $userModel->password = MD5($_POST['Users']['password']);
                }
                
                $userModel->save(false);

                Yii::app()->user->setFlash('success', 'Client user updated successfully!');
                $this->redirect(array('index'));
            }
		}
        else
        {
            unset($model->password);
        }

		$this->render('update', compact('model'));
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = Users::model()->findByPk($id);
		
		if($model === null || $model->deleted == 1 || $model->role_id != Yii::app()->user->client_role)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
