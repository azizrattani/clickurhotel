<?php

class ManageController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new Users('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Users']))
		{
			$model->attributes = $_GET['Users'];
		}

        $createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');
        $viewAllowed   = $this->isAllowed('view');

		$this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed', 'viewAllowed'));
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);

		if($model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to view is not found');
            $this->redirect(array('index'));
        }
        else
        {
			$this->render('view', compact('model'));
		}
	}

	public function actionCreate()
	{
		$model = new Users('create');

		if(isset($_POST['Users']))
		{
			$model->attributes = $_POST['Users'];

			if ($model->validate())
            {
                $model->password = MD5($model->password);
                $model->save(false);

                Yii::app()->user->setFlash('success', 'Users saved successfully');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

        $model->scenario = 'update';
        
		if(!$model || $model->deleted == 1)
        {
            Yii::app()->user->setFlash('error', 'Record you are trying to update is not found');
            $this->redirect(array('index'));
        }
        else
        {
			if(isset($_POST['Users']))
			{
				$model->attributes = $_POST['Users'];

				if ($model->validate())
	            {
                    if(isset($_POST['Users']['password']) && $_POST['Users']['password'] != '')
                    {
                        $model->password = MD5($_POST['Users']['password']);
                    }
                    else
                    {
                        unset($model->password);
                    }

                    $model->save(false);

	                Yii::app()->user->setFlash('success', 'Users updated successfully');
	                $this->redirect(array('index'));
	            }
			}
            else
            {
                unset($model->password);
            }

			$this->render('update', compact('model'));
		}
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->deleted = 1;
        $model->save(false);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = Users::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
