<?php

class AdminsController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = new Users('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Users']))
		{
			$model->attributes = $_GET['Users'];
		}

		$model->role_id = Yii::app()->user->admin_role;

        $createAllowed = $this->isAllowed('create');
        $updateAllowed = $this->isAllowed('update');
        $deleteAllowed = $this->isAllowed('delete');

		$this->render('index', compact('model', 'createAllowed', 'updateAllowed', 'deleteAllowed'));
	}

	public function actionCreate()
	{
		$model = new Users('create');

		if(isset($_POST['Users']))
		{
			$model->attributes = $_POST['Users'];
			$model->role_id    = Yii::app()->user->admin_role;

			if ($model->validate())
            {
            	$adminModel = new AdminUsers;
            	$adminModel->user_name  = $model->username;
            	$adminModel->email_add  = $model->email;
            	$adminModel->password   = MD5($model->password);
            	$adminModel->first_name = $model->first_name;
            	$adminModel->last_name  = $model->last_name;
            	$adminModel->is_admin   = 1;
            	$adminModel->is_active  = $model->status;
            	$adminModel->added_on   = new CDbExpression('NOW()');
            	$adminModel->save(false);
            	
                Yii::app()->user->setFlash('success', 'Admin user added successfully!');
                $this->redirect(array('index'));
            }
		}

		$this->render('create', compact('model'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $model->scenario = 'update';
        
		if(isset($_POST['Users']))
		{
			$model->attributes = $_POST['Users'];
			$model->role_id    = Yii::app()->user->admin_role;

			if ($model->validate())
            {
            	$adminModel = AdminUsers::model()->findByPk($model->child_id);

            	$adminModel->user_name  = $model->username;
            	$adminModel->email_add  = $model->email;
            	$adminModel->first_name = $model->first_name;
            	$adminModel->last_name  = $model->last_name;
            	$adminModel->is_admin   = 1;
            	$adminModel->is_active  = $model->status;

                if(isset($_POST['Users']['password']) && $_POST['Users']['password'] != '')
                {
                    $adminModel->password = MD5($_POST['Users']['password']);
                }
                
                $adminModel->save(false);

                Yii::app()->user->setFlash('success', 'Admin user updated successfully!');
                $this->redirect(array('index'));
            }
		}
        else
        {
            unset($model->password);
        }

		$this->render('update', compact('model'));
	}

	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		AdminUsers::model()->findByPk($model->child_id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = Users::model()->findByPk($id);
		
		if($model === null || $model->deleted == 1 || $model->role_id != Yii::app()->user->admin_role)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
