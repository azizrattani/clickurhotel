<?php

/**
 * This is the model class for table "market_type".
 *
 * The followings are the available columns in table 'market_type':
 * @property integer $id
 * @property string $title
 * @property string $countries
 * @property string $description
 * @property string $deleted
 * @property integer $created_by
 * @property string $created
 * @property string $modified
 *
 * The followings are the available model relations:
 * @property HotelMarket[] $hotelMarkets
 * @property HotelMealRate[] $hotelMealRates
 */
class MarketType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'market_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('deleted', 'length', 'max'=>1),
			array('countries, description, created, modified', 'safe'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, countries, description, deleted, created_by, created, modified', 'safe', 'on' => 'search')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hotelMarkets' => array(self::HAS_MANY, 'HotelMarket', 'market_id'),
			'hotelMealRates' => array(self::HAS_MANY, 'HotelMealRate', 'market_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'countries' => 'Countries',
			'description' => 'Description',
			'deleted' => 'Deleted',
			'created_by' => 'Created By',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('countries', $this->countries, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('deleted', 0);
		$criteria->compare('created_by', $this->created_by);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('modified', $this->modified, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarketType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
	    if ($this->isNewRecord)
	    {
	        $this->created = new CDbExpression('NOW()');
	    }

        $this->modified = new CDbExpression('NOW()');

	    return parent::beforeSave();
	}
}
