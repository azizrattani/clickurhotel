<?php
/* @var $this MarketTypeController */
/* @var $model MarketType */

$this->pageTitle = 'Market Types - View';
$this->breadcrumbs=array(
	'Market Types' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'title',
		'countries',
		'description',
		'deleted',
		'created_by',
		'created',
		'modified',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>