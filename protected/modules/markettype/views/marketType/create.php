<?php
/* @var $this MarketTypeController */
/* @var $model MarketType */

$this->pageTitle = 'Market Types - Create';
$this->breadcrumbs = array(
	'Market Types' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>