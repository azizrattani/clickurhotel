<?php
/* @var $this MarketTypeController */
/* @var $model MarketType */

$this->pageTitle = 'Market Types - Update';
$this->breadcrumbs = array(
	'Market Types' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>