<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" rel="stylesheet">

<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
<?php
/* @var $this TrainersController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Checkin Report';
$this->breadcrumbs = array(
	'Reports',
    'Checkin Report'
);
?>

 <?php
        //$event = Query::model()->findAll(array('condition'=>' deleted =0','order' => 'event_name ASC'));
		?>

<div class="row">

	<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	
        <form method="post" >
        
        
        	<div class="col-lg-4">
            	
                <div class="col-lg-12"><strong>Report Type</strong></div>
                <div class="col-lg-12"><select name="report_type" id="report_type" class="form-control mselect">
                	<option value="checkin">Checkin</option>
                  	<option value="checkout">Checkout</option>
                
                </select></div>
                
            
            </div>
            
            <?php
            $hotel = Hotel::model()->findAll(array('condition'=>' deleted =0','order' => 'name ASC'));
            ?>
        	
            <div class="col-lg-4">
            	
                <div class="col-lg-12"><strong>Hotel</strong></div>
                <div class="col-lg-12"><select name="hotel" id="hotel" class="form-control mselect">
                	<option value="">All Hotel</option>
                    <?php 
						
						foreach ($hotel as $val){
					
					?>
                    
                    		<option value="<?=$val->id?>"><?=$val->name?></option>
                    
                    <?php
						}
					?>
                    
                
                </select></div>
                
            
            </div>
        
        	
            
            
            <div class="col-lg-4 ">
            	
                <div class="col-lg-12"><strong>Date Range</strong></div>
                <div class="col-lg-6">
                	<input class="form-control" id="start_date" type="text" value="<?= isset($_POST["start_date"])? $_POST["start_date"] : "" ?>" name="start_date" autocomplete="off">
                </div>
                <div class="col-lg-6">
                	<input class="form-control" id="end_date" type="text" value="<?= isset($_POST["end_date"])? $_POST["end_date"] : "" ?>" name="end_date" autocomplete="off">
                </div>
                
            
            </div>
            
            
            
            
            
            
            
          
            
            <br />
            <div class="clearfix"></div>
            <br />
            
            <div class="col-lg-12">
            	
                <input type="submit" name="search" class="btn btn-primary pull-right" />
            
            </div>
        
        
        </form>
        
        
    
    </div>
	

</div>


<div class="box-body table-responsive no-padding" style="margin-top:15px;">

	<?php
		if ($output){
			
			
			
			
	?>
    
    		<table id="myTable" class="table table-striped">
                <thead>
                <tr>
                    
                          
                    <th width="50px">Conf #</th>
                    <th width="50px">Book #</th>
                    <th width="100px">Checkin</th>
                    
                    <th width="100px">Checkout</th>
                    <th width="120px">Guest Name</th>
                    <th width="120px">Hotel Name</th>
                   
                    <th width="80px">Room Type</th>
                    
                    <th width="80px">View Type</th>
                    
                    <th width="80px">Meal Type</th>
                    <th width="50px"># Rooms</th>
                    <th width="30px">Room Number</th>
                    
                    
                </tr>
                </thead>
                <tbody>
                <?php
                
                foreach ($output as $m) { 
				
				
					//$hotel= Hotel::model()->findByPk($m["hotel_id"]);
					//$partner= Partners::model()->findByPk($m["staff_id"]);
					
					$bookRoom = BookingRooms::model()->findAll(array("condition"=> "booking_id=".$m["id"]."","group"=>'room_id'))
				
				?>
                
                	<?php
                    foreach($bookRoom as $room){
						
						$mealType = "";
						
						if ($room["breakfast"] != "")
							$mealType .= "BB/";
							
						if ($room["lunch"] != "")
							$mealType .= "L/";
							
								
						if ($room["dinner"] != "")
							$mealType .= "D/";	
							
						$mealType = substr($mealType,0,-1);
						
						if ($mealType == "")
							$mealType = "Room Only";
						?>
                        <tr>
                            
                            
                            <td><?= ($m["ref_number"])?></td>
                            <td><?= ($m["id"])?></td>
                            <td><?= date("d F, Y", strtotime($m["checkin"]))?></td>                        
                            <td><?= date("d F, Y", strtotime($m["checkout"]))?></td>   
                            
                            
                            <td><?= $m["full_guest_name"]?></td>
                                                 
                            <td><?= $m->hotel->name?></td>
                            
                            
                            <td><?= $room->room->roomType->title?></td>
                                              
                            <td><?= $room->room->views0->title?></td>
                            
                            <td><?= $mealType;?></td>
                            <td><?= ($room["no_of_room"])?></td>   
                            <td><?= ($room["room_no"])?></td>
                            
                            
                    	</tr>
                <?php }
				}
                ?>
                </tbody>
            </table>
    
    
    
    <?php
    
		}
	?>
    
</div>


<script>

$(document).ready(function(e) {
    
	jQuery('#start_date').datepicker({'showAnim':'fold','class':'form-control'});
	jQuery('#end_date').datepicker({'showAnim':'fold','class':'form-control'});
	
	//jQuery('#expire_start_date').datepicker({'showAnim':'fold','class':'form-control'});
	//jQuery('#expire_end_date').datepicker({'showAnim':'fold','class':'form-control'});
	
});


$(document).ready(function () {
		$('#myTable').DataTable( {
        	"order": [[ 2, "asc" ]],
			"pageLength": 100,
		 	dom: 'Bfrtip',
			buttons: [
				{
				   extend: 'pdf',
				   footer: true,
				   /*exportOptions: {
						columns: [0,1,2,3,4,5,6,7,8,9,10,11]
					}*/
			   },/*
			   {
				   extend: 'csv',
				   footer: false,
				  
				  
			   },*/
			   {
				   extend: 'excel',
				   footer: false,
				   
			   }   
			]
    	});
	});

</script>