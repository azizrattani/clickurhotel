<link href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css" rel="stylesheet">

<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
<?php
/* @var $this TrainersController */
/* @var $dataProvider CActiveDataProvider */

$this->pageTitle = 'Agent Statment Report';
$this->breadcrumbs = array(
	'Reports',
    'Agent Statment Report'
);
?>

 <?php
        //$event = Query::model()->findAll(array('condition'=>' deleted =0','order' => 'event_name ASC'));
		?>

<div class="row">

	<div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	
        <form method="post" >
        
        
        	
            
            <?php
			
			if (isset(Yii::app()->user->partner))
				$parent_id = (Yii::app()->user->partner["parent_id"]);
			else
				$parent_id = 0;
			$otherCondition = "";
			//if ($parent_id != 0){ 
			//
			
			if (isset(Yii::app()->user->partner)){
				$profileId = Yii::app()->user->partner["id"];
			}else{
				$profileId = 0;	
			}
				
				$otherCondition = " and parent_id='".$profileId."'";
		//}
			
			$partners = Partners::model()->findAll(array("condition"=>' id != '.$parent_id.' and deleted=0' . $otherCondition));

			$agentName = "All Agents"

            ?>
        	
            <div class="col-lg-4">
            	
                <div class="col-lg-12"><strong>Agent Name</strong></div>
                <div class="col-lg-12"><select name="agent" id="agent" class="form-control mselect">
                	<option value="">All Agent</option>
                    <?php
                    if($parent_id != 0){?>
                    <option value="<?=Yii::app()->user->partner["id"]?>"><?=Yii::app()->user->partner["name"]?></option>
                    <?php
					}?>
                    <?php 
						
						foreach ($partners as $val){
							
							
							$select = "";
							
							if (isset($_POST["agent"]) && $val->id == $_POST["agent"]){
								$select = " selected";
								$agentName = $val->name;
							}
							
					?>
                    
                    		<option value="<?=$val->id?>" <?=$select?>><?=$val->name?></option>
                    
                    <?php
						}
					?>
                    
                
                </select></div>
                
            
            </div>
        
        	
            
            
            <div class="col-lg-4 ">
            	
                <div class="col-lg-12"><strong>Date Range</strong></div>
                <div class="col-lg-6">
                	<input class="form-control" id="start_date" type="text" value="<?= isset($_POST["start_date"])? $_POST["start_date"] : "" ?>" name="start_date" autocomplete="off">
                </div>
                <div class="col-lg-6">
                	<input class="form-control" id="end_date" type="text" value="<?= isset($_POST["end_date"])? $_POST["end_date"] : "" ?>" name="end_date" autocomplete="off">
                </div>
                
            
            </div>
            
            
            
            
            
            
            
          
            
            <br />
            <div class="clearfix"></div>
            <br />
            
            <div class="col-lg-12">
            	
                <input type="submit" name="search" class="btn btn-primary pull-right" />
            
            </div>
        
        
        </form>
        
        
    
    </div>
	

</div>


<div class="box-body table-responsive no-padding" style="margin-top:15px;">

	<?php
		if ($output){
			
			
			
			
	?>
    
    		<table id="myTable" class="table table-striped">
                <thead>
                <tr>
                    
                          
                    <th width="50px">Activity</th>
                    <th width="150px">Booking Ref</th>
                    <th width="100px">Date</th>
                    
                    <th width="250px">Particular</th>
                    <th width="100px">Receiveable</th>
                    <th width="100px">Payment</th>
                   
                    <th width="100px">Balance</th>
                    <th width="100px">Booking Limit</th>
                    
                    
                </tr>
                </thead>
                <tbody>

				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td ><strong>Reports for <?=$agentName?></strong></td>
					<td></td>

					<td></td>


					<td></td>
					<td></td>

				</tr>
                
                <tr>
                            
                            
                            <td></td>
                            <td></td>
                            <td></td>                        
                            <td>Opening Balance</td>   
                            
                            
                            <td><?=$receivable?></td>
                                                 
                            <td></td>
                            
                            
                            <td><?=$receivable?></td>
                            <td><?=$openingLimit?></td> 
                            
                            
                    	</tr>
                
                
                <?php
                $balance = $receivable;
				$bookingLimit = $openingLimit;
				
                foreach ($output as $m) { 
				
					$rp = $pay = 0;
					
					if ($m["activity"] == "LC"){
						
						$bookingLimit = $m["payment"];
						
					}elseif ($m["activity"] == "P"){
						
						$bookingLimit += $m["payment"];
						$balance -= $m["payment"];
						$pay = $m["payment"];
						
					}elseif ($m["activity"] == "B"){
						
						$partner_id = Yii::app()->user->profile['child_id'];
						
						$bookingDetail = Booking::model()->findByPk($m["ref"]);
						
						$amount = Yii::app()->functions->getMyRateTotalReport($m["ref"],$partner_id);
						$amount = $bookingDetail->getMyRateTotal("return");
						
						$rp = $amount;
						
						$bookingLimit -= $amount;
						$balance += $amount;
					}
				
				
					
				?>
                
                	<?php
                    
						
					
						?>
                        <tr>
                            
                            
                            <td><?= ($m["activity"])?></td>
                            <td><?= ($m["ref"])?></td>
                            <td><?= date("d F, Y", strtotime($m["date"]))?></td>                        
                            <td><?= $m["particular"]?></td>   
                            
                            
                            <td><?=$rp == 0 ? "-" : $rp?></td>
                                                 
                            <td><?=$pay == 0 ? "-" : $pay?></td>
                            
                            
                            <td><?=$balance?></td>
                            <td><?=$bookingLimit?></td> 
                            
                            
                    	</tr>
                <?php 
				}
                ?>
                </tbody>
            </table>
    
    
    
    <?php
    
		}
	?>
    
</div>


<script>

$(document).ready(function(e) {
    
	jQuery('#start_date').datepicker({'showAnim':'fold','class':'form-control'});
	jQuery('#end_date').datepicker({'showAnim':'fold','class':'form-control'});
	
	//jQuery('#expire_start_date').datepicker({'showAnim':'fold','class':'form-control'});
	//jQuery('#expire_end_date').datepicker({'showAnim':'fold','class':'form-control'});
	
});


$(document).ready(function () {
		$('#myTable').DataTable( {
        	"order": [[ 2, "asc" ]],
			"pageLength": 100,
		 	dom: 'Bfrtip',
			buttons: [
				{
				   extend: 'pdf',
				   footer: true,
				   /*exportOptions: {
						columns: [0,1,2,3,4,5,6]
					}*/
			   },/*
			   {
				   extend: 'csv',
				   footer: false,
				  
				  
			   },*/
			   {
				   extend: 'excel',
				   footer: false,
				   
			   }   
			]
    	});
	});

</script>