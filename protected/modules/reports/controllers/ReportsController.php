<?php

class ReportsController extends AdminController
{
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actioncheckinReport()
	{
		
		

		
		$where = " 1 = 1 ";


		
		
		
		if (isset($_POST["report_type"])){
			
			$q= "";
			
			if (isset($_POST["start_date"]) && $_POST["start_date"] != ""){
				$date = date("Y-m-d",strtotime($_POST["start_date"]));
				$q .= " and " . $_POST["report_type"] . " >= '".$date ."'" ;
			}
			
			
			if (isset($_POST["end_date"]) && $_POST["end_date"] != ""){
				$date = date("Y-m-d",strtotime($_POST["end_date"]));
				$q .=  " and " . $_POST["report_type"] . " <= '".$date ."'" ;
			}
			
			
			if (isset($_POST["hotel"]) && $_POST["hotel"] != ""){
				
				
					$q .=  " and hotel_id='".$_POST["hotel"]."'";
			}
			
			if (Yii::app()->user->profile['role_id'] == 2  ){
				//$model->parent_id = 0 ;
			}else{
				
				 
				unset($_SESSION["partnerChild"]);
				
				$myId = Yii::app()->user->profile['child_id'];
				
				Yii::app()->functions->childIdHierarchy($myId);
				
			
				if (isset($_SESSION["partnerChild"]))
					$child_ids = implode(",",$_SESSION["partnerChild"]);
				else
					$child_ids = "";
					
				if ($child_ids)
					$ids = $myId.",".$child_ids;
				else
					$ids = $myId;
					
				$q .= " and staff_id in (".$ids.")";
				
			}
			
			
			
			$output = Booking::model()->findAll(array("condition"=>'deleted = 0 '.$q));
			
			
				
			
		}elseif (isset($_GET["report_type"])){
			
			$q= "";
			
			$date = date("Y-m-d");
			
			$q .= " and " . $_GET["report_type"] . " = '".$date ."'" ;
			
			if (Yii::app()->user->profile['role_id'] == 2  ){
				//$model->parent_id = 0 ;
			}else{
				
				 
				unset($_SESSION["partnerChild"]);
				
				$myId = Yii::app()->user->profile['child_id'];
				
				Yii::app()->functions->childIdHierarchy($myId);
				
			
				if (isset($_SESSION["partnerChild"]))
					$child_ids = implode(",",$_SESSION["partnerChild"]);
				else
					$child_ids = "";
					
				if ($child_ids)
					$ids = $myId.",".$child_ids;
				else
					$ids = $myId;
					
				$q .= " and staff_id in (".$ids.")";
				
			}
			
			
			$output = Booking::model()->findAll(array("condition"=>'deleted = 0 '.$q));
			
			
				
			
		}else{
			
			$output = $eventsVenue = "";
			
			if (!isset($q)){
				$q= "";	
			}
			
			if (Yii::app()->user->profile['role_id'] == 2  ){
				//$model->parent_id = 0 ;
			}else{
				
				 $ids = "";
				unset($_SESSION["partnerChild"]);
				
				$myId = Yii::app()->user->profile['child_id'];
				
				Yii::app()->functions->childIdHierarchy($myId);
				
			
				if (isset($_SESSION["partnerChild"]))
					$child_ids = implode(",",$_SESSION["partnerChild"]);
				else
					$child_ids = "";
					
				if ($child_ids)
					$ids = $myId.",".$child_ids;
				else
					$ids = $myId;
					
				$q = " and staff_id in (".$ids.")";
				
			}
			
			
			
			$output = Booking::model()->findAll(array("condition"=>'checkin >="'.date("Y-m-d").'" and deleted = 0 '.$q));
			
			
		}
		
		
		

        $this->render('checkinReport', compact('output'));
	}
	
	
	public function actionagentStatement()
	{


		
		unset($_SESSION["partnerChild"]);
		
		if (isset($_POST["agent"]) && $_POST["agent"] != ""){
			$myId = $_POST["agent"];			
		}else{
			$myId = Yii::app()->user->profile['child_id'];			
		}


		$receivable = $openingLimit = 0;
		
		
		Yii::app()->functions->childIdHierarchy($myId);
		
	
		if (isset($_SESSION["partnerChild"]))
			$child_ids = implode(",",$_SESSION["partnerChild"]);
		else
			$child_ids = "";
			
		if ($child_ids)
			$ids = $myId.",".$child_ids;
		else
			$ids = $myId;
		
		
	
		$sdate = $output = "";
		
		
		$where = " 1 = 1 ";
		
		
		
		if (isset($_POST["agent"])){
			
			$bq= $pq = $lq = "";
			
			if (isset($_POST["start_date"]) && $_POST["start_date"] != ""){
				$date = date("Y-m-d",strtotime($_POST["start_date"]));
			
				
				$bq .= " and b.created >= '".$date ."'" ;
				$pq .= " and created >= '".$date ."'" ;
				$lq .= " and date_start >= '".$date ."'" ;
				
				$sdate = $_POST["start_date"];
			}else{
				$date = date("Y-m-d");
				$bq .= " and b.created >= '".$date ."'" ;
				$pq .= " and created >= '".$date ."'" ;
				$lq .= " and date_start >= '".$date ."'" ;
				
				$sdate = $date;
			}
			
			
			if (isset($_POST["end_date"]) && $_POST["end_date"] != ""){
				$date = date("Y-m-d",strtotime($_POST["end_date"]));
				
				$bq .= " and b.created <= '".$date ."'" ;
				$pq .= " and created <= '".$date ."'" ;
				$lq .= " and date_start <= '".$date ."'" ;
				
				if ($sdate == ""){
					$sdate = $_POST["end_date"];	
				}
			}
			
			
			//if (isset($_POST["agent"]) && $_POST["agent"] != ""){
//				
//				$bq .=  " and b.staff_id='".$_POST["agent"]."'";
//				$pq .=  " and partner_id='".$_POST["agent"]."'";
//				$lq .=  " and staff_id='".$_POST["agent"]."'";
//				
//				
//			}else{
				
				if (!$ids){
					$bq .=  " and b.staff_id='0'";
					$pq .=  " and partner_id='0'";
					$lq .=  " and staff_id='0'";
				}else{
					$bq .=  " and b.staff_id in (".$ids.")";
					$pq .=  " and partner_id in (".$ids.")";
					$lq .=  " and staff_id in (".$ids.")";
				}
			//}

			if ($_POST["agent"] != ""){
				$obid = $myId;
			}else{
				$obid = $_POST["agent"];
			}
			
			if($sdate == "")
				$sdate = date("Y-m-d 23:59:59");
			else
				$sdate = date("Y-m-d 23:59:59",strtotime($sdate));
		
			$receivable = Yii::app()->functions->getOpeningReceiveable($sdate);
			$openingLimit = Yii::app()->functions->getOpeningLimit($obid,$sdate);
			
			
			$sql = "SELECT 
					  * 
					FROM
					  (
						(SELECT 
						  'B' as activity,
						  b.id AS 'ref',
						  checkin AS 'date',
						  CONCAT(
							full_guest_name,
							', ',
							noofnight,
							' night, ',
							h.name
						  ) AS 'particular',
						  amount AS 'payment' 
						FROM
						  booking b 
						  INNER JOIN hotel h 
							ON h.id = b.hotel_id 
						WHERE b.deleted = 0 
						  ".$bq." order by b.id asc) 
						UNION
						ALL 
						(SELECT 
						  'P' as activity,
						  id AS 'ref',
						  pay_date AS 'date',
						  narration AS 'particular',
						  amount AS 'payment' 
						FROM
						  partner_payment 
						WHERE deleted = 0 
						  ".$pq." order by id asc) 
						UNION
						ALL 
						(SELECT 
						  'LC' as activity,
						  created AS 'ref',
						  created AS 'date',
						  'Limit changed on approval of CEO,' AS 'particular',
						  booking_limit AS 'payment' 
						FROM
						  `partner_limit` 
						WHERE 1=1 ".$lq." order by id asc)
					  ) b 
					ORDER BY `date` ASC ;
					";
	
				$output = Yii::app()->db->createCommand($sql)->queryAll();
			
			
				
			
		}
		
		
		

        $this->render('agentStatement', compact('output','receivable','openingLimit'));
		
		
		
		
		
	}


	public function actioninventoryReport()
	{




		$where = " 1 = 1 ";




		$output =[];

		if (isset($_POST["room_type"])){

			//$q= " and room.room_type='".$_POST["room_type"]."'";

			/*if (isset($_POST["start_date"]) && $_POST["start_date"] != ""){
				$date = date("Y-m-d",strtotime($_POST["start_date"]));
				$q .= " and bookdate >= '".$date ."'" ;
			}


			if (isset($_POST["end_date"]) && $_POST["end_date"] != ""){
				$date = date("Y-m-d",strtotime($_POST["end_date"]));
				$q .=  " and bookdate <= '".$date ."'" ;
			}*/

			$q = "";


			if (isset($_POST["hotel"]) && $_POST["hotel"] != ""){


				$q .=  " and t.id='".$_POST["hotel"]."'";
			}

			if (isset($_POST["view_type"]) && $_POST["view_type"] != ""){

				$q .=  " and hotelRooms.views='".$_POST["view_type"]."'";
			}





			$output = Hotel::model()->with("hotelRooms")->findAll(array("condition"=>'t.deleted = 0 '.$q));




			debug($output);




		}




		$this->render('inventoryReport', compact('output'));
	}


	public function loadModel($id)
	{
		$model = Trainers::model()->findByPk($id);
		
		if($model === null)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
}
