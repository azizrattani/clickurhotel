<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $seo_url
 * @property string $created
 * @property string $modified
 * @property integer $deleted
 */
class Pages extends CActiveRecord
{
	
	public function GetStudioRegistration()
	{               
		
		
		$sqlCount = 'SELECT 
 count(*) 
FROM
  `gyms_studios` gs 
WHERE deleted = 0 ;
';
		
		
		$sql = 'SELECT 
		gs.id,
  gs.name,
  gs.`status`,
  (SELECT 
    start_date 
  FROM
    `gyms_studios_package` 
  WHERE `gym_studio_id` = gs.`id` 
  ORDER BY start_date ASC 
  LIMIT 1) AS register_date,
  (SELECT 
    end_date 
  FROM
    `gyms_studios_package` 
  WHERE `gym_studio_id` = gs.`id` 
  ORDER BY end_date DESC 
  LIMIT 1) AS expire_date,
  (SELECT 
    title 
  FROM
    `gyms_studios_packages` 
  WHERE id = 
    (SELECT 
      packageid 
    FROM
      `gyms_studios_package` 
    WHERE `gym_studio_id` = gs.`id` 
    ORDER BY end_date DESC 
    LIMIT 1)) AS current_package 
FROM
  `gyms_studios` gs 
WHERE deleted = 0 ;
';
		
		$count=Yii::app()->db->createCommand($sqlCount)->queryScalar();
		
		$dataProvider=new CSqlDataProvider($sql, array(
						'totalItemCount'=>$count,
						'sort'=>array(
										'attributes'=>array(
														'name', 'status', 'register_date', 'expire_date','current_package',
										),
						),
						'pagination'=>array(
										'pageSize'=>15,
						),
		));
		
		return $dataProvider;
	}
}
