<?php
/* @var $this ViewTypeController */
/* @var $model ViewType */

$this->pageTitle = 'View Types - Update';
$this->breadcrumbs = array(
	'View Types' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>