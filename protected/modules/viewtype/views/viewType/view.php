<?php
/* @var $this ViewTypeController */
/* @var $model ViewType */

$this->pageTitle = 'View Types - View';
$this->breadcrumbs=array(
	'View Types' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'title',
		'create_by',
		'created',
		'modified',
		'deleted',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>