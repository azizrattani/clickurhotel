<?php
/* @var $this ViewTypeController */
/* @var $model ViewType */

$this->pageTitle = 'View Types - Create';
$this->breadcrumbs = array(
	'View Types' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>