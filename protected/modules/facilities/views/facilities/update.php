<?php
/* @var $this FacilitiesController */
/* @var $model Facilities */

$this->pageTitle = 'Facilities - Update';
$this->breadcrumbs = array(
	'Facilities' => array('index'),
	'Update',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>