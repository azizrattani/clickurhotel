<?php
/* @var $this FacilitiesController */
/* @var $model Facilities */

$this->pageTitle = 'Facilities - Create';
$this->breadcrumbs = array(
	'Facilities' => array('index'),
	'Create',
);
?>

<?= $this->renderPartial('_form', compact('model')); ?>