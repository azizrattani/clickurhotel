<?php

abstract class MyActiveRecord extends CActiveRecord
{
    public static function fetch($sql, $params = array())
    {
        $row = Yii::app()->db->createCommand($sql)->queryAll(true, $params);
        $row = isset($row[0]) ? $row[0] : false;    
    
        return $row;
    }
    
    public static function fetchAll($sql, $params = array())
    {
        return Yii::app()->db->createCommand($sql)->queryAll(true, $params);
    }
}