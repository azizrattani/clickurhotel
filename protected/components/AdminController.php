<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AdminController extends CController {

    public $layout = '//layouts/main';
    
    public $breadcrumbs = array();

    public function init()
    {
        Yii::app()->functions->getHost();

        if(Yii::app()->user->isGuest)
        {
            $this->redirect($this->createUrl('/site/login'));
        }
    }

    protected function beforeAction($action)
    {
        $valid = false;

        $module     = strtolower($this->module->id);
        $controller = strtolower($this->id);
        $action     = strtolower($action->id);

        $permissions = Yii::app()->user->permissions;

        foreach($permissions as $per)
        {
            $valid = ($per['module'] == $module && $per['controller'] == $controller && $per['action'] == $action) ? true : $valid;
        }

        if(!$valid)
        {
            Yii::app()->user->setFlash('error', 'You are not allowed to perform this action');
            $this->redirect($this->createUrl('/dashboard'));
        }

        return parent::beforeAction($action);
    }

    protected function isAllowed($action = '', $metacode = '', $controller = '', $module = '')
    {
        $module     = $module     ? strtolower($module)     : strtolower($this->module->id);
        $controller = $controller ? strtolower($controller) : strtolower($this->id);
        $action     = $action     ? strtolower($action)     : '';
        $metacode   = $metacode   ? strtolower($metacode)   : '';

        $valid  = false;

        $permissions = Yii::app()->user->permissions;

        foreach($permissions as $per)
        {
            $valid = ($per['module'] == $module && $per['controller'] == $controller && $per['action'] == $action && $per['meta_code'] == $metacode) ? true : $valid;
        }

        return $valid;
    }
}
