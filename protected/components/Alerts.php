<?php

class Alerts extends CWidget {

    /**
     * @var array the keys for which to get flash messages.
     */
    public $keys = array('success', 'info', 'warning', 'error', /* or */ 'danger');

    /**
     * @var string the template to use for displaying flash messages.
     */
    public $template = '<div class="alert alert-block alert-{key}{class}"><a class="close" data-dismiss="alert">&times;</a>{message}</div>';

    /**
     * @var string[] the JavaScript event handlers.
     */
    public $events = array();

    /**
     * @var array the HTML attributes for the widget container.
     */
    public $htmlOptions = array();

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        if (!isset($this->htmlOptions['id']))
            $this->htmlOptions['id'] = $this->getId();
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        $id = $this->id;

        if (is_string($this->keys))
            $this->keys = array($this->keys);

        echo CHtml::openTag('div', $this->htmlOptions);

        foreach ($this->keys as $key)
        {
            if (Yii::app()->user->hasFlash($key))
            {
                $key2 = $key == 'error' ? 'danger' : $key;

                echo strtr($this->template, array(
                    '{class}' => ' fade in',
                    '{key}' => $key2,
                    '{message}' => Yii::app()->user->getFlash($key),
                ));
            }
        }

        echo '</div>';

        $selector = "#{$id} .alert";
        $id .= '_' . uniqid(true, true);

        /** @var CClientScript $cs */
        $cs = Yii::app()->getClientScript();
        $cs->registerScript(__CLASS__ . '#' . $id, "jQuery('{$selector}').alert();");

        foreach ($this->events as $name => $handler)
        {
            $handler = CJavaScript::encode($handler);
            $cs->registerScript(__CLASS__ . '#' . $id . '_' . $name, "jQuery('{$selector}').on('" . $name . "', {$handler});");
        }
    }

}
