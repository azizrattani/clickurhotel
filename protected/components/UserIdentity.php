<?php

class UserIdentity extends CUserIdentity {

    private $_id;

    public function authenticate()
    {
        $criteria = new CDbCriteria;  
        $criteria->compare('username', $this->username);
		$criteria->addCondition('role_id in (1,2,3,4,6)', 'AND');
        $criteria->compare('deleted', 0);
        $record = Users::model()->find($criteria);

        if ($record === null)
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else if ($record->password !== md5($this->password))
        {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else
        {
            $this->_id = $record->id;
            $this->setState('profile', $record);
			
			
			if ($record->role_id == 3){
				$partner = Partners::model()->findByPk($record["child_id"]);
				$this->setState('partner', $partner);
				
				$partnerLimit = PartnerPriceType::model()->find(array("condition"=>' staff_id="'.$record["child_id"].'" and "'.date("Y-m-d").'" >= date_from and (date_to is null or date_to <= "'.date("Y-m-d").'") and deleted = 0'));
				$this->setState('partnerLimit', $partnerLimit);
				
				
				$sql = "update partner_login_logs set logout_date='".date("Y-m-d H:i:s")."' where staff_id='".$record->child_id."'  AND logout_date IS NULL";
				$data=Yii::app()->db->createCommand($sql)->execute();
				
				$sql = "insert into partner_login_logs set login_date='".date("Y-m-d H:i:s")."', staff_id='".$record->child_id."', created='".date("Y-m-d H:i:s")."', modified='".date("Y-m-d H:i:s")."' ";
				$data=Yii::app()->db->createCommand($sql)->execute();
				
					
			}
            
            // Role permission
            $permissions = RolePermissions::getPermissionsByRole($record->role_id);
            $this->setState('permissions', $permissions);

            // System Setting
            $settings = Settings::model()->findAll();
            foreach($settings as $s)
            {
                $key = str_replace(" ", "_", $s->meta_key);
                $key = strtolower($key);
                $this->setState($key, $s->meta_value);
            }
            
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}