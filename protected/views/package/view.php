<?php
/* @var $this PackageController */
/* @var $model Package */

$this->pageTitle = 'Packages - View';
$this->breadcrumbs=array(
	'Packages' => array('index'),
	'View',
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
    'htmlOptions' => array('class' => 'table table-striped table-bordered'),
	'attributes' => array(
		'id',
		'title',
		'day_condition',
		'idate_start',
		'idate_end',
		'date_start',
		'date_end',
		'created',
		'modified',
		'created_by',
		'deleted',
	),
)); ?>


<br />

<div class="box-footer">
    <a href="<?= $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
</div>