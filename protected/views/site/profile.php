<?php

$this->pageTitle = 'Update Profile';

$this->breadcrumbs = array(
    $model->username,
    'Update Profile',
);

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'inventory-form',
    'enableAjaxValidation' => false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php $arr = $client ? compact('model', 'client') : $model ?>
<?php echo $form->errorSummary($arr, 'Please fix the following errors:', '', array('class' => 'alert alert-danger')); ?>

<br />

<?php if(Yii::app()->user->profile['role_id'] == 3) { ?>

<div class="form-group">
		<?= $form->labelEx($client,'name'); ?>
		<?= $form->textField($client, 'name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($client,'cell'); ?>
		<?= $form->textField($client, 'cell', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($client,'cell2'); ?>
		<?= $form->textField($client, 'cell2', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($client,'email'); ?>
		<?= $form->textField($client, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>
    
    <div class="form-group">
		<?= $form->labelEx($client,'ccEmail'); ?> (user emails with , separated)
		<?= $form->textArea($client,'ccEmail',array('class' => 'form-control','rows'=>5, 'cols'=>50,"tabindex"=>"3")); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($client,'address'); ?>
		<?= $form->textField($client, 'address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="form-group">
		<?= $form->labelEx($client,'image'); ?>
        <?= $form->fileField($client, 'image', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
         <?php
        if(!$client->isNewRecord)
    	{
			$path = Yii::app()->baseUrl . '/images/partner/'.$client->id.'/';
			
			echo '<img src="'.$path.$client->image.'" style= "max-height:200px;" />';
		}
		?>
		
	</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'username'); ?>
    <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'disabled' => 'disabled')); ?>
</div>

<div class="row">
	<div class="col-md-6 col-xs-12">
		<div class="form-group">
		    <?php echo $form->labelEx($model, 'password'); ?>
		    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'maxlength' => 100)); ?>
		</div>
	</div>
	<div class="col-md-6 col-xs-12">
		<div class="form-group">
		    <?php echo $form->labelEx($model, 'repassword'); ?>
		    <?php echo $form->passwordField($model, 'repassword', array('class' => 'form-control', 'maxlength' => 100)); ?>
		</div>
	</div>
</div>

<?php } else { ?>

<div class="form-group">
    <?php echo $form->labelEx($model, 'email'); ?>
    <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'disabled' => 'disabled')); ?>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'username'); ?>
    <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'disabled' => 'disabled')); ?>
</div>

<div class="form-group">
    <?php echo $form->labelEx($model, 'password'); ?>
    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'maxlength' => 100)); ?>
</div>

<div class="form-group">
    <?php echo $form->label($model, 'repassword'); ?>
    <?php echo $form->passwordField($model, 'repassword', array('class' => 'form-control', 'maxlength' => 100)); ?>
</div>

<?php } ?>

<div class="box-footer">
    <a href="<?php echo $this->createUrl('index'); ?>" class="btn btn-default">Back</a>
    <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
</div>

<?php $this->endWidget(); ?>