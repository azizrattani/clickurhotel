<?php
$this->pageTitle = 'Dashboard';

$this->breadcrumbs = array(
    $this->pageTitle
);

$themeUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl . '/plugins/morris/morris.min.js', CClientScript::POS_END);
$cs->registerCssFile($themeUrl . '/plugins/morris/morris.css');
$cs->registerScriptFile($themeUrl . '/plugins/flot/jquery.flot.js', CClientScript::POS_END);
$cs->registerScriptFile($themeUrl . '/plugins/flot/jquery.flot.pie.min.js', CClientScript::POS_END);
$cs->registerScriptFile('//canvasjs.com/assets/script/canvasjs.min.js', CClientScript::POS_END);


?>


<div class="col-md-6">

    <!-- My Performance -->
    <div class="box box-info">
    	<div class="box-header with-border">
    		<h3 class="box-title">My Performance</h3>
    	</div>
    	<div class="box-body chart-responsive">
    		<table class="table table-condensed table-striped table-hover table-bordered">
            	<thead>
                	<tr>
                    	<th width="70%">Partner Performance</th>
                    	<th width="30%">Season</th>
                    </tr>                	
                </thead>
                <tbody>
                	<tr>
                    	<td>Total Request(s)</td>
                        <td><?=number_format($performance["totalRequestConfirm"])?></td>
                    </tr> 
                    <tr>
                    	<td>Cancelled Request(s)</td>
                        <td><?=number_format($performance["totalRequestCancell"])?></td>
                    </tr> 
                    <tr>
                    	<td>Request Cancellation Percentage</td>
                        <?php if ($performance["totalRequestConfirm"]==0){ $percentage =  0;}else{ $percentage =  (($performance["totalRequestCancell"]/$performance["totalRequestConfirm"])*100);}?>
                        <td style="background-color:<?=Yii::app()->functions->getColor($percentage,"request")?>"><?=number_format($percentage,2) ;?>%</td>
                    </tr> 
                    <tr>
                    	<td>Definite Booking (s)</td>
                        <td><?=number_format($performance["totalBookingConfirm"])?></td>
                    </tr>     
                    <tr>
                    	<td>Cancelled Bookings (s)</td>
                        <td><?=number_format($performance["totalBookingCancell"])?></td>
                    </tr>    
                    <tr>
                    	<td>Booking Cancellation Percentage</td>
                        <?php if ($performance["totalBookingConfirm"]==0){ $percentage =  0;}else{ $percentage =  (($performance["totalBookingCancell"]/$performance["totalBookingConfirm"])*100);}?>
                        <td style="background-color:<?=Yii::app()->functions->getColor($percentage,"booking")?>"><?=number_format($percentage,2)?>%</td>
                    </tr>               	
                </tbody>
            </table>
    	</div>
    <!-- /.box-body -->
	</div>
<!-- /.box -->
</div>


<div class="col-md-6">

    <!-- My Performance -->
    <div class="box box-info">
    	<div class="box-header with-border">
    		<h3 class="box-title">My Limits</h3>
    	</div>
    	<div class="box-body chart-responsive">
    		<table class="table table-condensed table-striped table-hover table-bordered">
            	
                <tbody>
                	<tr>
                    	<td>Assigned Booking Limit</td>
                        <td><?=number_format($myLimit["assignedBookingLimit"])?></td>
                    </tr> 
                    <tr>
                    	<td>Consumed Booking Limit</td>
                        <td><?=number_format($myLimit["consumedBookingLimit"])?></td>
                    </tr> 
                    <tr>
                    	<td>% Booking Limit Used</td>
                        <?php if ($myLimit["assignedBookingLimit"]==0){ $percentage = 0;}else{ $percentage =  (($myLimit["consumedBookingLimit"]/$myLimit["assignedBookingLimit"])*100);}?>
                        <td style="background-color:<?=Yii::app()->functions->getColor($percentage)?>"><?=number_format($percentage,2)?>%</td>
                    </tr> 
                    <tr>
                    	<td>Assigned Checkin Limit</td>
                        <td><?=number_format($myLimit["assignedCheckinLimit"])?></td>
                    </tr>     
                    <tr>
                    	<td>Consumed Checkin Limit</td>
                        <td><?=number_format($myLimit["consumedCheckinLimit"])?></td>
                    </tr>    
                    <tr>
                    	<td>% Checkin Limit Used</td>
                        <?php if ($myLimit["assignedCheckinLimit"]==0){ $percentage = 0;}else{ $percentage =  (($myLimit["consumedCheckinLimit"]/$myLimit["assignedCheckinLimit"])*100);}?>
                        <td style="background-color:<?=Yii::app()->functions->getColor($percentage)?>"><?=number_format($percentage,2) ?>%</td>
                    </tr>    
                    <tr>
                    	<td>Consumed Checkin Limit (5 day forecast)</td>
                        <td><?=number_format($myLimit["consumedCheckinLimit5"])?></td>
                    </tr>    
                    <tr>
                    	<td>% Checkin Limit Used (5 day forecast)</td>
                        <?php if ($myLimit["assignedCheckinLimit"]==0){ $percentage = 0;}else{ $percentage =  (($myLimit["consumedCheckinLimit5"]/$myLimit["assignedCheckinLimit"])*100);}?>
                        <td style="background-color:<?=Yii::app()->functions->getColor($percentage)?>"><?=number_format($percentage,2)?>%</td>
                    </tr>               	
                </tbody>
            </table>
    	</div>
    <!-- /.box-body -->
	</div>
<!-- /.box -->
</div>

<div class="col-md-6">

    <!-- My Performance -->
    <div class="box box-info">
    	<div class="box-header with-border">
    		<h3 class="box-title">Red Zone Agents (Checkin Limit)</h3>
    	</div>
    	<div class="box-body chart-responsive">
    		<table class="table table-condensed table-striped table-hover table-bordered">
            	<thead>
                	<tr>
                    	<th width="30%">Agent Name</th>
                    	<th width="30%">Checkin Limit</th>
                        <th width="30%">Checkin Limit (5 day forecast)</th>
                    </tr>                	
                </thead>
                <tbody>
                	<?php
                    foreach($agentData as $agent){
					?>
                        <tr>
                            <td><?=$agent["name"]?></td>
                            <?php if ($agent["checkin"]==0){ $percentage = 0;}else{ $percentage = (($agent["checkinuse"]/$agent["checkin"])*100);}?>
                            <td style="background-color:<?=Yii::app()->functions->getColor($percentage)?>"><?=number_format($percentage,2)?>%</td>
                            <?php if ($agent["checkin"]==0){ $percentage = 0;}else{ $percentage = (($agent["checkin5day"]/$agent["checkin"])*100);}?>
                            <td style="background-color:<?=Yii::app()->functions->getColor($percentage)?>"><?=number_format($percentage,2)?>%</td>
                        </tr> 
                    <?php
					}
					?>
                    	
                </tbody>
            </table>
    	</div>
    <!-- /.box-body -->
	</div>
<!-- /.box -->
</div>

<div class="col-md-6">
        	
            <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
    
                  <h3 class="box-title">Agent Contribution</h3>
    
                  <div class="box-tools pull-right">
                    
                  </div>
                </div>
                <div class="box-body">
                  <div id="agentContribution" style="height: 300px;"></div>
                </div>
                <!-- /.box-body-->
                
              </div>
              
        
        
        </div>

<div class="col-md-6">

    <!-- My Performance -->
    <div class="box box-info">
    	<div class="box-header with-border">
    		<h3 class="box-title">My today's checkins</h3>
    	</div>
    	<div class="box-body chart-responsive">
    		<table class="table table-condensed table-striped table-hover table-bordered">
            	<thead>
                	<tr>
                    	<th width="20%">Conf #</th>
                        <th width="20%">Book #</th>
                    	<th width="30%">Guest Name</th>
                        <th width="30%">Hotel Name</th>
                    </tr>                	
                </thead>
                <tbody>
                	<?php
                    foreach($todaysChecking as $checkin){
					?>
                        <tr>
                            <td><?=$checkin["ref_number"]?></td>
                            <td><?=$checkin["id"]?></td>
                            <td><?=$checkin["full_guest_name"]?></td>                           
                            <td><?=$checkin->hotel->name?></td>
                        </tr> 
                    <?php
					}
					?>
                    
                    <tr>
                            <td colspan="4" class="text-center"><a href="/reports/reports/checkinReport?report_type=checkin">View Report</a></td>
                        </tr> 
                    	
                </tbody>
            </table>
    	</div>
    <!-- /.box-body -->
	</div>
<!-- /.box -->
</div>


<div class="col-md-6">

    <!-- My Performance -->
    <div class="box box-info">
    	<div class="box-header with-border">
    		<h3 class="box-title">My today's checkouts</h3>
    	</div>
    	<div class="box-body chart-responsive">
    		<table class="table table-condensed table-striped table-hover table-bordered">
            	<thead>
                	<tr>
                    	<th width="20%">Conf #</th>
                        <th width="20%">Book #</th>
                    	<th width="30%">Guest Name</th>
                        <th width="30%">Hotel Name</th>
                    </tr>                	
                </thead>
                <tbody>
                	<?php
                    foreach($todaysCheckout as $checkin){
					?>
                        <tr>
                            <td><?=$checkin["ref_number"]?></td>
                            <td><?=$checkin["id"]?></td>
                            <td><?=$checkin["full_guest_name"]?></td>                           
                            <td><?=$checkin->hotel->name?></td>
                        </tr> 
                    <?php
					}
					?>
                    
                    <tr>
                            <td colspan="4" class="text-center"><a href="/reports/reports/checkinReport?report_type=checkout">View Report</a></td>
                        </tr> 
                    	
                </tbody>
            </table>
    	</div>
    <!-- /.box-body -->
	</div>
<!-- /.box -->
</div>

<div class="col-md-6">
          <!-- LINE CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Booking Trend</h3>

             
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="bookingTrend" style="height: 300px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
</div>






<script>

$(document).ready(function(e) {
    
	// booking Trend
   
	var chart = new CanvasJS.Chart("bookingTrend",
	{
	  title:{
		text: "Booking Trend"
	  },
	
	  axisX:{
		title: "",
		gridThickness: 0,
		interval:2, 
		intervalType: "",        
		
		labelAngle: -20
	  },
	  axisY:{
		title: "distance"
	  },
	  data: [
	  {        
		type: "line",
		dataPoints: [//array
		<?php
		foreach($bookingTrend as $trandDate => $trendAmt){
		?>
		{ x: new Date(<?=$trandDate?>), y: <?=$trendAmt?>},
        <?php
		}
		?>
		]
	  }
	  ]
	});
	
	chart.render();
   
   
   // booking Trend
   
   
   //Agent Contribution
   
  	<?php
	if(isset($agentContribution) && !empty($agentContribution)){
		
		
	?>
		var donutData = [
		 <?php
		 foreach($agentContribution as $contribution){
		 ?>
		  { label: '<?=$contribution["name"]?>', data:<?=$contribution["receivablee"]?> , color: '<?=$contribution["color"]?>' },
		  <?php
		 }
		  ?>
		]
		$.plot('#agentContribution', donutData, {
		  series: {
			pie: {
			  show       : true,
			  radius     : 1,
			  innerRadius: 0.5,
			  label      : {
				show     : true,
				radius   : 2 / 2.7,
				formatter: labelFormatter,
				threshold: 0.1
			  }
	
			}
		  },
		  legend: {
			show: true
		  }
		}) 
   <?php
	}
   ?>
   
   
   //Agent contribution
	
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }
	
});

</script>

