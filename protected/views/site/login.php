<div class="login-box-body">
    <div id="login-div" <?= $showForgot ? 'style="display:none"' : '' ?>>
        <p class="login-box-msg">Sign in to access your account</p>

        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'login-form')); ?>

            <?php $showForgot ? '' : $this->widget('Alerts'); ?>

            <?= $form->errorSummary($model, NULL, NULL, array('class' => 'alert alert-danger')); ?>

            <div class="form-group has-feedback">
                <?= $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => 'Username')); ?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
                  
            <div class="form-group has-feedback">
                <?= $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label><?= $form->checkBox($model,'rememberMe'); ?> Stay Logged In </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        Login <i class="fa fa-arrow-right"></i>
                    </button>
                </div>
            </div>

            <hr />
            <p>Forgot Password? Click <a href="javascript:void(0);" onclick="showReset();">Here</a> to reset it.</p>
        <?php $this->endWidget(); ?>
    </div>

    <div id="forgot-pwd-div" <?= $showForgot ? '' : 'style="display:none"' ?>>
        <p class="login-box-msg">Enter your email or username to reset your password</p>

        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'forgot-form')); ?>

            <?php $showForgot ? $this->widget('Alerts') : ''; ?>

            <div class="form-group has-feedback">
                <?= $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => 'Email/Username')); ?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
                  
            <div class="row">
                <div class="col-xs-6">
                    <a href="javascript:void(0);" onclick="showLogin();">Back To Login</a>
                </div>
                <div class="col-xs-6">
                    <?= $form->hiddenField($model, 'forgot_pwd', array('value' => '1')); ?>
                    <button type="submit" class="btn btn-primary btn-flat">Reset My Password</button>
                </div>
            </div>
        <?php $this->endWidget(); ?>
    </div>
</div>