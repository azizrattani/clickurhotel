<?php $this->pageTitle = $error['code'] . ' Error'; ?>

<br />

<?php if($error['code'] == 500) { ?>
<div class="error-page text-center">
    <h2 class="headline text-red">500</h2>

    <div class="error-content">
        <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
        <p>We will work on fixing that right away.</p>
    </div>
</div>
<?php } ?>


<?php if($error['code'] == 404) { ?>
<div class="error-page text-center">
    <h2 class="headline text-yellow">404</h2>

    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
        <p>We could not find the page you were looking for.</p>
    </div>
</div>
<?php } ?>

<br />