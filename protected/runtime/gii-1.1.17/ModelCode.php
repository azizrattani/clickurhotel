<?php
return array (
  'template' => 'custom',
  'connectionId' => 'db',
  'tablePrefix' => '',
  'modelPath' => 'application.modules.packages.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '1',
);
