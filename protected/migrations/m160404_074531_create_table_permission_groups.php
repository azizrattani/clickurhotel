<?php

class m160404_074531_create_table_permission_groups extends CDbMigration
{
	public function up()
	{
		$this->createTable('permission_groups', array(
            'id'          => 'pk',
            'title'       => 'VARCHAR(255) NOT NULL',
            'deleted'     => 'TINYINT(1) DEFAULT 0',
            'created'     => 'datetime',
            'modified'    => 'datetime',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
	}

	public function down()
	{
		$this->dropTable('permission_groups');
	}
}