<?php

class m160328_073423_create_roles_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('roles', array(
            'id'          => 'pk',
            'title'       => 'VARCHAR(255) NOT NULL',
            'deleted'     => 'TINYINT(1) DEFAULT 0',
            'created'     => 'datetime',
            'modified'    => 'datetime',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
        
        $this->insert('roles', array(
            "id"         => 1,
            "title"      => "Super Admin",
            "created"    => new CDbExpression('NOW()'),
            "modified"   => new CDbExpression('NOW()')
        ));
	}

	public function down()
	{
		$this->dropTable('roles');
	}
}