<?php

class m160404_074602_create_table_permission_actions extends CDbMigration
{
	public function up()
	{
		$this->createTable('permission_actions', array(
            'id'          		   => 'pk',
            'title'       		   => 'VARCHAR(255) NOT NULL',
            'permission_entity_id' => 'INT NOT NULL',
            'module'       	   	   => 'VARCHAR(255)',
            'controller'       	   => 'VARCHAR(255)',
            'action'       	       => 'VARCHAR(255)',
        	'meta_code'       	   => 'VARCHAR(255)',
            'parent'               => 'INT',
            'deleted'     		   => 'TINYINT(1) DEFAULT 0',
            'created'     		   => 'datetime',
            'modified'    		   => 'datetime',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

        $this->addForeignKey('FK_permission_entity_id', 'permission_actions', 'permission_entity_id', 'permission_entities', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropTable('permission_actions');
	}
}