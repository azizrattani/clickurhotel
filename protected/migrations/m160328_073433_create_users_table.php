<?php

class m160328_073433_create_users_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('users', array(
            'id'          => 'pk',
            'first_name'  => 'VARCHAR(50)',
            'last_name'   => 'VARCHAR(50)',
            'email'       => 'VARCHAR(255) NOT NULL',
            'username'    => 'VARCHAR(25) NOT NULL',
            'password'    => 'VARCHAR(35) NOT NULL',
            'role_id'     => 'INT',
            'photo'		  => 'VARCHAR(255)',
            'status'      => 'TINYINT(1) DEFAULT 1',
            'deleted'     => 'TINYINT(1) DEFAULT 0',
            'created'     => 'datetime',
            'modified'    => 'datetime',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
        
        $this->addForeignKey('FK_user_role', 'users', 'role_id', 'roles', 'id', 'CASCADE', 'CASCADE');

        $this->insert('users', array(
            "first_name" => "Super",
            "last_name"  => "Admin",
            "email"      => "biqbal@eplanetcom.com",
            "username"   => "superadmin",
            "password"   => MD5("xyz123"),
            "role_id"    => 1,
            "created"    => new CDbExpression('NOW()'),
            "modified"   => new CDbExpression('NOW()')
        ));
	}

	public function down()
	{
		$this->dropTable('users');
	}
}