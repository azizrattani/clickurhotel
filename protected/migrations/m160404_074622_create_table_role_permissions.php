<?php

class m160404_074622_create_table_role_permissions extends CDbMigration
{
	public function up()
	{
		$this->createTable('role_permissions', array(
            'id'          		   => 'pk',
            'role_id'       	   => 'INT NOT NULL',
            'permission_action_id' => 'INT NOT NULL',
            'deleted'     		   => 'TINYINT(1) DEFAULT 0',
            'created'     		   => 'datetime',
            'modified'    		   => 'datetime',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

        $this->addForeignKey('FK_rp_role_id', 'role_permissions', 'role_id', 'roles', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_rp_action_id', 'role_permissions', 'permission_action_id', 'permission_actions', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropTable('role_permissions');
	}
}