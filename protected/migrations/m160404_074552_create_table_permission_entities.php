<?php

class m160404_074552_create_table_permission_entities extends CDbMigration
{
	public function up()
	{
		$this->createTable('permission_entities', array(
            'id'          		  => 'pk',
            'title'       		  => 'VARCHAR(255) NOT NULL',
            'permission_group_id' => 'INT NOT NULL',
            'deleted'     		  => 'TINYINT(1) DEFAULT 0',
            'created'     		  => 'datetime',
            'modified'    		  => 'datetime',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');

        $this->addForeignKey('FK_permission_group_id', 'permission_entities', 'permission_group_id', 'permission_groups', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropTable('permission_entities');
	}
}