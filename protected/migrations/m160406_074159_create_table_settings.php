<?php

class m160406_074159_create_table_settings extends CDbMigration
{
	public function up()
	{
		$this->createTable('settings', array(
            'id'          => 'pk',
            'meta_key'    => 'VARCHAR(255) NOT NULL',
            'meta_value'  => 'TEXT NOT NULL',
            'created'     => 'datetime',
            'modified'    => 'datetime',
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');
	}

	public function down()
	{
		$this->dropTable('settings');
	}
}