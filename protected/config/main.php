<?php
return array(
	'timeZone' => 'Asia/Karachi',
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' 	   => 'Admin',
	'preload'=>array(
        'log',       // preloading 'log' component
      
        //'less', // preload the component to allow for automatic compilation        
    ),
	'theme'    => 'admin',
    'defaultController' => 'site',
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
        'application.modules.users.models.*',
        'application.modules.permissions.models.*',
        'application.modules.booking.models.*',
        'application.modules.facilities.models.*',
		'application.modules.hotel.models.*',
		'application.modules.viewtype.models.*',
		'application.modules.roomtype.models.*',
		'application.modules.mealtype.models.*',
		'application.modules.markettype.models.*',
		'application.modules.packages.models.*',
		'application.modules.partner.models.*',
		'application.modules.invoice.models.*',
		'application.modules.reports.models.*',
		'application.modules.misc.models.*',
		
	),
	'modules' => array(
		'gii' => array(
			'class'	    => 'system.gii.GiiModule',
			'password'  => 'xyz123',
			'ipFilters' => array('127.0.0.1','::1', '172.16.102.90'),
			'generatorPaths'=>array(
                'application.gii', // This means we have a custom templates in protected/gii folder
                'bootstrap.gii', // since 0.9.1
				'ext.giitemplates',
            ),
			
		),
        'users',
        'permissions',
        'booking',
		'facilities',
		'hotel',
		'viewtype',
		'roomtype',
		'mealtype',
		'markettype',
		'partner',
		'invoice',
		'reports',
		'packages',
		'misc',
		'sitedetails',

	),
	'components'=>array(
		'functions' => array(
            'class' => 'ext.Functions',
        ),
        'Smtpmail' => array(
            'class' => 'application.extensions.smtpmail.PHPMailer',
            'Host' => "smtp.gmail.com",
            'Username' => 'test-crm@gmail.com',
            'Password' => 'test123',
            'Mailer' => 'smtp',
            'Port' => 587,
            'SMTPAuth' => true, 
        ),
        'user' => array(
            'allowAutoLogin' => true,
        ),
        'session' => array(
            'timeout' => 3600,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'login'     => 'site/login',
                'logout'    => 'site/logout',
                'dashboard' => 'site/dashboard',
                'profile'   => 'site/profile',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
		'db' => require(dirname(__FILE__).'/database.php'),
		'errorHandler' => array(
			'errorAction' => YII_DEBUG ? null : 'site/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
		'bootstrap'=>array(
            'class'=>'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
        ),

	),
	
	'params' => require(dirname(__FILE__).'/params.php'),
);
