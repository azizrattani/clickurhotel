<?php
return array(
    'from_email' => 'no-reply@oshoot.com.pk',
    'from_name'  => 'Oshoot',
    'sms_username' => 'oshoto',
    'sms_password' => '123456',
	'order_status' => array(
    	0 => array('title' => 'New/Pending', 'color' => 'blue'),
		1 => array('title' => 'Waiting for Approval', 'color' => 'blue'),
		2 => array('title' => 'Request Changes', 'color' => 'blue'),
    	3 => array('title' => 'Completed', 'color' => 'black'),
	),
    
);