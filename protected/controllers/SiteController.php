<?php

class SiteController extends Controller {

    public $showCrmTitle = 1;
    public $layout = 'login';

    public function actionIndex()
    {
        $this->pageTitle = 'Login';
        $showForgot = 0;

        if (!Yii::app()->user->isGuest)
        {
            $this->redirect('dashboard');
        }

        $model = new Users('login');

        if (isset($_POST['Users']['forgot_pwd']))
        {
            $email = $_POST['Users']['email'];

            $user = Users::model()->find(array("condition" => "`email` = '$email' OR `username` = '$email'"));

            if ($user)
            {
                $newPassword = Yii::app()->functions->randomString(8);
                $user->password = MD5($newPassword);
                $user->save(false);

                $message  = "Dear " . $user->first_name . ",";
                $message .= "<br /><br />";
                $message .= "Your new password is: <b>" . $newPassword . '</b>';
                $message .= "<br /><br />";
                $message .= "Thank you.";

                Yii::app()->functions->email($user->email, 'Password Reset', $message);
                Yii::app()->user->setFlash('success', 'Password Reset Successfully! Kindly Check Your Email.');   
            }
            else
            {
                Yii::app()->user->setFlash('error', 'Email/Username not found!');
                $showForgot = 1;
            }
        }
        else if (isset($_POST['Users']))
        {
            $model->attributes = $_POST['Users'];
            if ($model->validate() && $model->login())
            {
                $this->redirect('dashboard');
            }
        }

        $this->render('login', compact('model', 'showForgot'));
    }

    public function actionLogin()
    {
        $this->actionIndex();
    }
  
    public function actionLogout()
    {
		
		if (Yii::app()->user->profile['role_id'] == 3){

			$sql = "update partner_login_logs set logout_date='".date("Y-m-d H:i:s")."' where staff_id='".Yii::app()->user->profile['child_id']."'  AND logout_date IS NULL";
			$data=Yii::app()->db->createCommand($sql)->execute();



		}
		
		
		Yii::app()->user->logout();
        Yii::app()->session->open();
        Yii::app()->user->setFlash('success', 'Successfully logout...');
      
        $this->redirect(Yii::app()->homeUrl);
    }
  
    public function actionDashboard()
    {
        $this->loggedInCheck();

        $userRole   = Yii::app()->user->profile['role_id'];
        $adminRole  = Yii::app()->user->admin_role;
        $csrRole    = Yii::app()->user->csr_role;
        $clientRole = Yii::app()->user->client_role;

        if($userRole == 2)
        {
            $this->adminDashboard();
        }elseif($userRole == 3)
		{
			$this->staffDashboard();
		}elseif($userRole == $clientRole)
        {
            $this->clientDashboard();
        }
        else
        {
            $this->render('dashboard');
        }
    }

    private function adminDashboard()
    {
		

		

		
		
		

	

        $this->render('admin_dashboard');
		

        
    }   
	
	
	
	private function staffDashboard()
    {
        
		unset($_SESSION["partnerChild"]);
		
		$myId = Yii::app()->user->profile['child_id'];
		
		Yii::app()->functions->childIdHierarchy($myId);
		
	
		if (isset($_SESSION["partnerChild"]))
			$child_ids = implode(",",$_SESSION["partnerChild"]);
		else
			$child_ids = "";
			
		if ($child_ids)
			$ids = $myId.",".$child_ids;
		else
			$ids = $myId;
		
			
		//My Performance	
		
				
		$sql = "select count(id) as total from booking where staff_id in (".$ids.") and btype='request'" ;
		$q = Yii::app()->db->createCommand($sql)->queryRow();
		
		$performance["totalRequestConfirm"] = $q["total"];
		
		
		$sql = "select count(id) as total from booking where staff_id in (".$ids.") and btype='request' and status=2" ;
		$q = Yii::app()->db->createCommand($sql)->queryRow();
		
		$performance["totalRequestCancell"] = $q["total"];
		
		
		$sql = "select count(id) as total from booking where staff_id in (".$ids.") and btype='book' and status=0" ;
		$q = Yii::app()->db->createCommand($sql)->queryRow();
		
		$performance["totalBookingConfirm"] = $q["total"];
		
		$sql = "select count(id) as total from booking where staff_id in (".$ids.") and btype='book' and status=2" ;
		$q = Yii::app()->db->createCommand($sql)->queryRow();
		
		$performance["totalBookingCancell"] = $q["total"];
		
		
		
		
		
		
		//My Limits	
		$staffLimit = PartnerLimit::model()->find(array("condition"=>" deleted = 0 and staff_id='".$myId."'"));
		$limitUsed = Yii::app()->functions->getAgentLimit($myId);
		
		$myLimit["assignedBookingLimit"] = $staffLimit["booking_limit"];		
		$myLimit["consumedBookingLimit"] = $staffLimit["booking_limit"] - $limitUsed;	
		
		
		$myLimit["assignedCheckinLimit"] = ($staffLimit["booking_limit"] * ($staffLimit["checkin_limit"]/100));		
		
		$sql = "select sum(amount) as total from booking where staff_id in (".$ids.") and btype='book' and status=0 and checkin <='".date("Y-m-d")."'" ;
		$q = Yii::app()->db->createCommand($sql)->queryRow();
		
		$myLimit["consumedCheckinLimit"] = $q["total"];	
		
		
		$sql = "select sum(amount) as total from booking where staff_id in (".$ids.") and btype='book' and status=0 and checkin >'".date("Y-m-d")."' and checkin <='".date("Y-m-d",strtotime("+5 days"))."'" ;
		
		$q = Yii::app()->db->createCommand($sql)->queryRow();
		
		$myLimit["consumedCheckinLimit5"] = $q["total"] > 0 ? $q["total"] : 0;	
		
		
		
		//Red Zone Agents (Checkin Limit)		
		
		$agentData = array();
		
		if ($child_ids != ""){
		
			$partners = Partners::model()->findAll(array("condition"=>' deleted = 0 and id in (select id from partners where parent_id="'.$myId.'")'));
			
			$agentData = array ();
			
			foreach($partners as $partner){
				
				
				unset($_SESSION["partnerChild"]);
				
				$agentId = $partner['id'];
				
				Yii::app()->functions->childIdHierarchy($agentId);
				
			
				if (isset($_SESSION["partnerChild"]))
					$agentChildIds = implode(",",$_SESSION["partnerChild"]);
				else
					$agentChildIds = "";
					
				if ($agentChildIds)
					$agentIds = $agentId.",".$agentChildIds;
				else
					$agentIds = $agentId;
					
					
				$staffLimit = PartnerLimit::model()->find(array("condition"=>" deleted = 0 and staff_id='".$agentId."'"));
				$limitUsed = Yii::app()->functions->getAgentLimit($agentId);
				
				
				$checkinLimit = ($staffLimit["booking_limit"] * ($staffLimit["checkin_limit"]/100));
				
				$sql = "select sum(amount) as total from booking where staff_id in (".$agentIds.") and btype='book' and status=0 and checkin <='".date("Y-m-d")."'" ;
				$q = Yii::app()->db->createCommand($sql)->queryRow();
			
				$checkinLimitConsume = $q["total"];	
				
				
			
				
				
				
				$sql = "select sum(amount) as total from booking where staff_id in (".$agentIds.") and btype='book' and status=0 and checkin >'".date("Y-m-d")."' and checkin <='".date("Y-m-d",strtotime("+5 days"))."'" ;
				
				$q = Yii::app()->db->createCommand($sql)->queryRow();
				
				$checkinLimitConsume5day = $q["total"] > 0 ? $q["total"] : 0;	
				
				
				
				$agentData[$agentId]["name"] 		= $partner["name"];
				$agentData[$agentId]["checkin"] 	= $checkinLimit;
				$agentData[$agentId]["checkinuse"]	= $checkinLimitConsume;
				$agentData[$agentId]["checkin5day"]	= $checkinLimitConsume5day;
				
				
				
				
			}
			
		}
		
		
		//Agent Contribution
		
		$agentContribution = array();
		$receivable = Booking::model()->myInvoiceSearchAll();
		
		foreach($receivable->getData() as $key=>$data){
			
			if(isset($agentContribution[$data["staff_id"]])){
				
				$agentContribution[$data["staff_id"]]["receivablee"] += $data->getMyRateTotal("return");
				
			}else{
				$agentContribution[$data["staff_id"]]["name"] = $data->staff->name;
				$agentContribution[$data["staff_id"]]["receivablee"] = $data->getMyRateTotal("return");
				$agentContribution[$data["staff_id"]]["color"] = $this->rand_color();
			}
			
			
		}
	

		// My today's checkins		
		
		
		$todaysChecking = Booking::model()->findAll(array("condition"=>'deleted = 0 and staff_id in ('.$ids.') and checkin="'.date("Y-m-d").'"'));
		
		// My today's checkins		
		
		
		$todaysCheckout = Booking::model()->findAll(array("condition"=>'deleted = 0 and staff_id in ('.$ids.') and checkout="'.date("Y-m-d").'"'));
		
		
		//Booking Trend
		
		
		$bookingTrend = Booking::model()->findAll(array("condition"=>'status <> 2 and deleted =0 and btype="book" and staff_id="'.$myId.'" and created between "'.date("Y-m-d",strtotime("-30 days")).'" and "'.date("Y-m-d").'"',"order"=>" checkin asc"));
		
		$trends = array();
		
		foreach($bookingTrend as $trend){
			
			$dt = date("Y, m, d",strtotime($trend["checkin"]));
			if(isset($trends[$dt])){
				
				$trends[$dt] += $trend["amount"];
					
			}else{
				$trends[$dt] = $trend["amount"];	
			}
		}
		
		$bookingTrend = $trends;
		
	

        $this->render('staff_dashboard',compact('performance','myLimit','agentData','todaysChecking','todaysCheckout','bookingTrend','agentContribution'));
		

        
    }    

    private function clientDashboard()
    {
       

        $this->render('staff_dashboard', compact('leadsChartData', 'statusChartData', 'leadsList', 'updateAllowed', 'statusKey', 'statusLabel', 'stats'));
    }

    public function actionProfile()
    {
        $this->loggedInCheck();

        $model = Users::model()->findByPk(Yii::app()->user->id);
        $model->scenario = 'profile';
       
        $client = array();
        if(Yii::app()->user->profile['role_id'] == 3)
        {
            $client = Partners::model()->findByPk($model->child_id);
			$image = $client->image;
        }

        if(isset($_POST['Users']))
        {
            if(Yii::app()->user->profile['role_id'] == 3)
            {
                $client->attributes = $_POST['Partners'];
				
				
				
				if (isset($_FILES["Partners"]["name"]["image"]) && $_FILES["Partners"]["name"]["image"] != ""){
					$client->image=CUploadedFile::getInstance($client,'image');
				}
            }

            $model->password   = $_POST['Users']['password'];
            $model->repassword = $_POST['Users']['repassword'];
            $model->email      = $_POST['Partners']['email'];

            $valid = $model->validate();
            $valid = $client ? $client->validate() && $valid : $valid;

            if($valid)
            {
                if($_POST['Users']['password'])
                {
                    $model->password = $model->repassword = md5($_POST['Users']['password']);
                    $model->save(); 
                }
				
				
				
                
                if(Yii::app()->user->profile['role_id'] == 3)
                {
                    if($client->save()){
						
						$path = Yii::app()->basePath . '/../images/partner/'.$client->id;
						
						if(!is_dir($path)){
							mkdir($path,0777);
						}
						$path .='/';
						
						if (isset($_FILES["Partners"]["name"]["image"]) && $_FILES["Partners"]["name"]["image"] != ""){						
							$client->image->saveAs($path.$client->image);
						}else{
							$client->image = $image;
							$client->save(false);
						}
					
						
					}
                }

                Yii::app()->user->setFlash('success', 'Profile updated successfully.');

                $this->redirect(array('site/dashboard'));
            }
        }
        else
        {
            $model->password = '';
        }
        
        $this->render('profile', compact('model', 'client'));
    }
    

    public function actionError()
    {
        $this->layout = 'error';

        $error = Yii::app()->errorHandler->error;

        $this->render('error', compact('error'));
    }

    private function loggedInCheck()
    {
        $this->layout = 'main';

        if (Yii::app()->user->isGuest)
        {
            $this->redirect('login');
        }
    }
	
	private function rand_color() {
		return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
	}
	
	
	
	
}