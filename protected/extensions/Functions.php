<?php

class Functions extends CApplicationComponent {
   	
	public function getSidebarMenu()
    {
        $pkgTitle = Yii::app()->user->profile['role_id'] == Yii::app()->user->client_role ? 'My Packages' : 'Manage Packages';
		
		if (Yii::app()->user->profile['role_id'] == 3){
			
			
		
			$menu = array(

				'partners' => array(
					'title'  => 'Partners Management',
					'icon'   => 'fa-users',
					'module' => 'partner',
					'childs' => array(
						array('title' => 'Add Partner', 'controller' => 'partners', 'action' => 'create'),
						array('title' => 'Manage Partners', 'controller' => 'partners', 'action' => 'index'),
						array('title' => 'Add Partner Price Type', 'controller' => 'partnerPriceType', 'action' => 'create'),
						array('title' => 'Manage Partners Price Types', 'controller' => 'partnerPriceType', 'action' => 'index'),
						array('title' => 'Add Partner Limit', 'controller' => 'partnerLimit', 'action' => 'create'),
						array('title' => 'Manage Partners Limit', 'controller' => 'partnerLimit', 'action' => 'index'),
						array('title' => 'Add Partner Payment', 'controller' => 'partnerPayment', 'action' => 'create'),
						array('title' => 'Manage Partners Payment', 'controller' => 'partnerPayment', 'action' => 'index'),
					)
				),
				
				
				'Booking' => array(
					'title'  => 'Booking Management',
					'icon'   => 'fa-users',
					'module' => 'booking',
					'childs' => array(
						array('title' => 'Reservation', 'controller' => 'booking', 'action' => 'reservation'),
						array('title' => 'Manage Booking ', 'controller' => 'booking', 'action' => 'index'),
						array('title' => 'Manage Cancel Booking', 'controller' => 'booking', 'action' => 'cancel'),
						
					)
				),
				
				
				
				'Invoice' => array(
					'title'  => 'Invoice Management',
					'icon'   => 'fa-users',
					'module' => 'invoice',
					'childs' => array(
						array('title' => 'My Invoices', 'controller' => 'invoice', 'action' => 'my'),
						array('title' => 'My Dues', 'controller' => 'invoice', 'action' => 'dues'),
						
						
					)
				),
				
				'reports' => array(
					'title'  => 'Reports',
					'icon'   => 'fa-pie-chart',
					'module' => 'reports',
					'childs' => array(

						array('title' => 'Checkin Report', 'controller' => 'reports', 'action' => 'checkinReport'),
						array('title' => 'Agent Statement', 'controller' => 'reports', 'action' => 'agentStatement'),

					)
				),


				
				/*'event' => array(
					'title'  => 'Event Management',
					'icon'   => 'fa-star',
					'module' => 'event',
					'childs' => array(
						
						array('title' => 'My Event', 'controller' => 'event', 'action' => 'staffIndex'),
						array('title' => 'Event Pictures', 'controller' => 'eventPicture', 'action' => 'index'),
						array('title' => 'My Task', 'controller' => 'eventTask', 'action' => 'index'),
					)
				),
				
				'meetings' => array(
					'title'  => 'Meeting Request',
					'icon'   => 'fa-star',
					'module' => 'meetings',
					'childs' => array(
						
						array('title' => 'Create Request', 'controller' => 'meetings', 'action' => 'index'),
					)
				),*/
				
				
			);
			
		}elseif (Yii::app()->user->profile['role_id'] == 2){
			
			$menu = array(
			
				
				'Misc' => array(
					'title'  => 'Misc Management',
					'icon'   => 'fa-film',
					'module' => 'misc',
					'childs' => array(
						array('title' => 'Add Country', 'controller' => 'countries', 'action' => 'create'),
						array('title' => 'Manage Countries', 'controller' => 'countries', 'action' => 'index'),
						array('title' => 'Add City', 'controller' => 'city', 'action' => 'create'),
						array('title' => 'Manage City', 'controller' => 'city', 'action' => 'index'),
					)
				),

				
				'facilities' => array(
					'title'  => 'Facilities Management',
					'icon'   => 'fa-film',
					'module' => 'facilities',
					'childs' => array(
						array('title' => 'Add Facilities', 'controller' => 'facilities', 'action' => 'create'),
						array('title' => 'Manage Facilities', 'controller' => 'facilities', 'action' => 'index'),
					)
				),

				'hotel' => array(
					'title'  => 'Hotel Management',
					'icon'   => 'fa-film',
					'module' => 'hotel',
					'childs' => array(
						array('title' => 'Add Hotel', 'controller' => 'hotel', 'action' => 'create'),
						array('title' => 'Manage Hotel', 'controller' => 'hotel', 'action' => 'index'),
						array('title' => 'Manage Hotel Priority', 'controller' => 'hotel', 'action' => 'priority'),
						array('title' => 'Add Hotel Room', 'controller' => 'hotelRooms', 'action' => 'create'),
						array('title' => 'Manage Hotel Room', 'controller' => 'hotelRooms', 'action' => 'index'),
						array('title' => 'Add Hotel Room Rate', 'controller' => 'hotelRoomRate', 'action' => 'create'),
						array('title' => 'Update Hotel Room Rate', 'controller' => 'hotelRoomRate', 'action' => 'update'),
						array('title' => 'Manage Hotel Room Rate', 'controller' => 'hotelRoomRate', 'action' => 'index'),
						array('title' => 'Add Hotel Meal Rate', 'controller' => 'hotelMealRate', 'action' => 'create'),
						array('title' => 'Manage Hotel Meal Rate', 'controller' => 'hotelMealRate', 'action' => 'index'),
					)
				),
				
				'packages' => array(
					'title'  => 'Packages Management',
					'icon'   => 'fa-film',
					'module' => 'packages',
					'childs' => array(
						array('title' => 'Add Package', 'controller' => 'package', 'action' => 'create'),
						array('title' => 'Manage Packages', 'controller' => 'package', 'action' => 'index'),
						array('title' => 'Add Package Rate', 'controller' => 'packageRates', 'action' => 'create'),
						array('title' => 'Manage Package Rate', 'controller' => 'packageRates', 'action' => 'index'),
						
					)
				),

				'mealtype' => array(
					'title'  => 'Meal Type',
					'icon'   => 'fa-film',
					'module' => 'mealtype',
					'childs' => array(
						array('title' => 'Add Meal Type', 'controller' => 'mealstype', 'action' => 'create'),
						array('title' => 'Manage Meal Type', 'controller' => 'mealstype', 'action' => 'index'),
					)
				),

				'roomtype' => array(
					'title'  => 'Room Type',
					'icon'   => 'fa-film',
					'module' => 'roomtype',
					'childs' => array(
						array('title' => 'Add Room Type', 'controller' => 'roomType', 'action' => 'create'),
						array('title' => 'Manage Room Type', 'controller' => 'roomType', 'action' => 'index'),
					)
				),

				'viewtype' => array(
					'title'  => 'View Type',
					'icon'   => 'fas fa-kabba',
					'module' => 'viewtype',
					'childs' => array(
						array('title' => 'Add View Type', 'controller' => 'viewType', 'action' => 'create'),
						array('title' => 'Manage View Type', 'controller' => 'viewType', 'action' => 'index'),
					)
				),

				'markettype' => array(
					'title'  => 'Market Type',
					'icon'   => 'fa-film',
					'module' => 'markettype',
					'childs' => array(
						array('title' => 'Add Market Type', 'controller' => 'marketType', 'action' => 'create'),
						array('title' => 'Manage Market Type', 'controller' => 'marketType', 'action' => 'index'),
					)
				),




				'partners' => array(
					'title'  => 'Partners Management',
					'icon'   => 'fa-users',
					'module' => 'partner',
					'childs' => array(
						array('title' => 'Add Partner', 'controller' => 'partners', 'action' => 'create'),
						array('title' => 'Manage Partners', 'controller' => 'partners', 'action' => 'index'),
					)
				),
				
				'Booking' => array(
					'title'  => 'Booking Management',
					'icon'   => 'fa-users',
					'module' => 'booking',
					'childs' => array(
						
						array('title' => 'Manage Booking', 'controller' => 'booking', 'action' => 'index'),
						array('title' => 'Manage Cancel Booking', 'controller' => 'booking', 'action' => 'cancel'),
						
						
					)
				),

				
				'reports' => array(
					'title'  => 'Reports',
					'icon'   => 'fa-pie-chart',
					'module' => 'reports',
					'childs' => array(
					
						array('title' => 'Checkin Report', 'controller' => 'reports', 'action' => 'checkinReport'),
						array('title' => 'Agent Statement', 'controller' => 'reports', 'action' => 'agentStatement'),
						array('title' => 'Inventory Report', 'controller' => 'reports', 'action' => 'inventoryReport'),

					)
				),
				
				
				
				'Invoice' => array(
					'title'  => 'Invoice Management',
					'icon'   => 'fa-users',
					'module' => 'invoice',
					'childs' => array(
						array('title' => 'My Invoices', 'controller' => 'invoice', 'action' => 'my'),
						//array('title' => 'My Dues', 'controller' => 'invoice', 'action' => 'dues'),
						
						
					)
				),


				/*'sitedetails' => array(
					'title'  => 'Whitelabel Sites',
					'icon'   => 'fa-pie-chart',
					'module' => 'sitedetails',
					'childs' => array(

						array('title' => 'Listing', 'controller' => 'sitedetails', 'action' => 'index'),
						array('title' => 'Create', 'controller' => 'sitedetails', 'action' => 'create'),

					)
				),*/
				
				

				
				
			);
				
		}elseif (Yii::app()->user->profile['role_id'] == 6){ //reservation company

			$menu = array(




				'hotel' => array(
					'title'  => 'Hotel Management',
					'icon'   => 'fa-film',
					'module' => 'hotel',
					'childs' => array(

						array('title' => 'Add Hotel Room Rate', 'controller' => 'hotelRoomRate', 'action' => 'create'),
						array('title' => 'Update Hotel Room Rate', 'controller' => 'hotelRoomRate', 'action' => 'update'),
						array('title' => 'Manage Hotel Room Rate', 'controller' => 'hotelRoomRate', 'action' => 'index'),

					)
				),

				'packages' => array(
					'title'  => 'Packages Management',
					'icon'   => 'fa-film',
					'module' => 'packages',
					'childs' => array(
						array('title' => 'Add Package', 'controller' => 'package', 'action' => 'create'),
						array('title' => 'Manage Packages', 'controller' => 'package', 'action' => 'index'),
						array('title' => 'Add Package Rate', 'controller' => 'packageRates', 'action' => 'create'),
						array('title' => 'Manage Package Rate', 'controller' => 'packageRates', 'action' => 'index'),

					)
				),








			);

		}elseif (Yii::app()->user->profile['role_id'] == 4){
			
			$menu = array(
			
				
				

				'hotel' => array(
					'title'  => 'Hotel Management',
					'icon'   => 'fa-film',
					'module' => 'hotel',
					'childs' => array(
						
						array('title' => 'Update Hotel', 'controller' => 'hotel', 'action' => 'index'),
						array('title' => 'Add Hotel Room', 'controller' => 'hotelRooms', 'action' => 'create'),
						array('title' => 'Manage Hotel Room', 'controller' => 'hotelRooms', 'action' => 'index'),
						array('title' => 'Add Hotel Room Rate', 'controller' => 'hotelRoomRate', 'action' => 'create'),
						array('title' => 'Update Hotel Room Rate', 'controller' => 'hotelRoomRate', 'action' => 'index'),
						array('title' => 'Add Hotel Meal Rate', 'controller' => 'hotelMealRate', 'action' => 'create'),
						array('title' => 'Manage Hotel Meal Rate', 'controller' => 'hotelMealRate', 'action' => 'index'),
					)
				),
				
				
				

				
				
			);
				
		}else{
			
			$menu = array(
				
				
				'users' => array(
					'title'  => 'User Management',
					'icon'   => 'fa-user',
					'module' => 'users',
					'childs' => array(
						array('title' => 'Manage Admin',   'controller' => 'admins',  'action' => 'index'),
						
						array('title' => 'Manage Clients', 'controller' => 'clients', 'action' => 'index'),
						array('title' => 'Users',     	   'controller' => 'manage',  'action' => 'index'),
						array('title' => 'Roles',          'controller' => 'roles',   'action' => 'index'),
					)
				),
				
				'permissions' => array(
					'title'  => 'Permission & Settings',
					'icon'   => 'fa-cogs',
					'module' => 'permissions',
					'childs' => array(
						array('title' => 'Permission Groups', 'controller' => 'groups',   'action' => 'index'),
						array('title' => 'Roles Permission',  'controller' => 'manage',   'action' => 'index'),
						array('title' => 'Settings',          'controller' => 'settings', 'action' => 'index'),
					)
				)
			);
				
		}

       

        return $menu;
    }

	public  function sendQuotation($event){

		$basePAth = dirname(Yii::app()->request->scriptFile) ;

		$subject = "A Quotation from Oshoot.com.pk";

		if($event->event_category == 1){
			$file = $basePAth."/pdf/O'Shoot-Birthday-Services-Quotation.pdf";
		}else{
			$file = $basePAth."/pdf/O'Shoot-Wedding-Services-Quotation.pdf";
		}

		$to = $event->client->email;

		$message = "Hello ".$event->client->name.",<Br><Br>

				Please find quotation in attachedment.<Br><Br>

				Thansk,<Br>

				Oshoot.com.pk

		";

		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = 'ssl://smtp.gmail.com';
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		//$mail->SMTPSecure = 'tls';
		$mail->Username = 'client.services@oshootphotography.com';
		$mail->Password = 'Admin123';
		$mail->SetFrom("client.services@oshootphotography.com", "Oshoot.com.pk");
		$mail->Subject = $subject;
		if($file)
			$mail->addAttachment($file);
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message);
		$mail->AddAddress($to);
		$sendStatus =$mail->Send();

	}

    public function email($to = '', $subject = '', $message = '', $from_email = '', $from_name = '',$file="",$ccEmail="")
    {
		
		
		if ($to == ""){
			return false;	
		}
		
		if ($ccEmail != ""){			
			$cc = explode(",",$ccEmail);	
		}else{
			$cc = array();	
		}

		//$to = "azizrattani@gmail.com";


		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->isSMTP();
		//$mail->Host = 'smtp.gmail.com';
		$mail->Host = 'bl.creativeon.net';
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		//$mail->Username = 'clickurhotel@gmail.com';
		$mail->Username = 'no-reply@clickhotel.online';
		//$mail->Password = 'Admin@123';
		$mail->Password = 'i%;z1@5(g)$b';
		$mail->SetFrom($from_email, $from_name);
		$mail->Subject = $subject;
		if($file)
			$mail->addAttachment($file);
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message);
		$mail->AddAddress($to);
		$mail->AddBcc("aburayyan92@gmail.com");
		$mail->AddBcc("azizrattani@gmail.com");

		//echo $from_email;
		
		foreach($cc as $email){
			$mail->AddCC(trim($email));
		}
		
		
		$sendStatus =$mail->Send();
		
		//die($sendStatus);




    }


	public function emailICS($event_task)
	{
		$basePAth = dirname(Yii::app()->request->scriptFile) ;

		$staffId = $event_task->staff_id;
		$staffDetail = Staff::model()->findByPk($staffId);


		$ics = "BEGIN:VCALENDAR
				VERSION:2.0
				PRODID:".$event_task->title."
				BEGIN:VEVENT
				UID:http://oshoot.com.pk/backend/event/eventTask/staff/id/".$event_task->id."
				DTSTAMP:".date("Ymd")."TO".date("His")."z
				DTSTART:".date("Ymd",strtotime($event_task->staff_start))."TO".date("His",strtotime($event_task->staff_start))."z
				DTEND:".date("Ymd",strtotime($event_task->staff_deadline))."TO".date("His",strtotime($event_task->staff_deadline))."z
				END:VEVENT
				END:VCALENDAR";

		include $basePAth.'/ICS.php';


		$ics = new ICS(array(
			'location' => "Oshoot Offices",
			'description' => $event_task->title,
			'dtstart' => date("m/d/Y",strtotime($event_task->staff_start)),
			'dtend' => date("m/d/Y",strtotime($event_task->staff_deadline)),
			'summary' => $event_task->title,
			'url' => "http://oshoot.com.pk/backend/event/eventTask/staff/id/".$event_task->id
		));

		$ics = $ics->to_string();


		if ($ics != ""){



			$file_name = date("YmdHis").".ics";
			$file = $basePAth."/tmp/ics".$file_name;
			fopen($file, 'w+');
			if (is_writable($file)) {


				if (!$handle = fopen($file, 'w')) {
					echo "error ";
				}

				// Write $somecontent to our opened file.
				if (fwrite($handle, $ics) === FALSE) {
					echo "error ";
				}



				fclose($handle);

			}




		}


		$from_email         = 'client.services@oshootphotography.com';//'info@oshoot.com.pk'; //from mail, sender email addrress
		$recipient_email    = $staffDetail["email"]; //recipient email addrress

		//Load POST data from HTML form
		$sender_name    = "Oshoot"; //sender name
		$reply_to_email = $from_email; //sender email, it will be used in "reply-to" header
		$subject        = 'Event Task Assigned'; //subject for the email
		$message        = 'Hi, <Br><Br>

		A new Task assigned to you.<Br><Br>

		Thanks,<Br><Br>

		Support Oshoot.

		'; //body of the email

		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = 'ssl://smtp.gmail.com';
		$mail->Port = 465;
		$mail->SMTPAuth = true;
		//$mail->SMTPSecure = 'tls';
		$mail->Username = 'client.services@oshootphotography.com';
		$mail->Password = 'Admin123';
		$mail->SetFrom($from_email, $sender_name);
		$mail->Subject = $subject;
		$mail->addAttachment($file);
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message );
		$mail->AddAddress($recipient_email);
		$sendStatus =$mail->Send();

		echo $recipient_email . " --- " . $sendStatus . "<br>";



	}

    public function sms($to = '', $message = '')
    {       
		//return;
		if($message)
        {
            $to = str_replace(array('+', '-', ' '), '', $to);

            if(!$this->isValidNumber($to))
            {
                $to = '92' . ltrim($to, '0');
            }

            if($this->isValidNumber($to))
            {
                $url   = "http://sms.its.com.pk/api/?";
                $url  .= 'username=' . Yii::app()->params['sms_username'];
                $url  .= '&password=' . Yii::app()->params['sms_password'];
                $url  .= '&receiver=' . urlencode($to);
                $url  .= '&msgdata=' . urlencode($message);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                $output = curl_exec($ch);
                curl_close($ch);
            }
        }
    }

    public function isValidNumber($number)
    {
        return (strlen($number) == 12 && substr($number, 0, 3) == '923') ? true : false;
    }

    public function randomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
    
    public function getLeadType($type)
    {
        return Yii::app()->params['leads_type'][$type];
    }

    public function getLeadStatusLabel($status, $type = 'leads_status')
    {
        $statusList = Yii::app()->params[$type];

        return '<i class="fa fa-circle text-' . $statusList[$status]['color'] . '" title="' . $statusList[$status]['title'] .'" rel="tooltip"></i>';
    }

    public function friendlyTime($date)
    {
        $etime = time() - strtotime($date);

        if ($etime < 1)
        {
            return 'Just now';
        }

        $a = array( 365 * 24 * 60 * 60  =>  'year',
                     30 * 24 * 60 * 60  =>  'month',
                          24 * 60 * 60  =>  'day',
                               60 * 60  =>  'hour',
                                    60  =>  'minute',
                                     1  =>  'second'
                    );
        $a_plural = array( 'year'   => 'years',
                           'month'  => 'months',
                           'day'    => 'days',
                           'hour'   => 'hours',
                           'minute' => 'minutes',
                           'second' => 'seconds'
                    );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

    public function getAirports()
    {
        $sql = "SELECT `iata_code`, CONCAT(`name`, ' ', `municipality`) AS `name`
                FROM `airport`
                WHERE `iso_country` = 'PK' AND `type` != 'small_airport' AND `scheduled_service` = 'yes'";

        $records = MyActiveRecord::fetchAll($sql);

        $result = array();

        /*foreach ($records as $r)
        {
            $result[$r['name']] = $r['name'];
        }
*/

		$result["Karachi"] = "Karachi";
		$result["Lahore"] = "Lahore";
		$result["Islamabad"] = "Islamabad";
		$result["Peshawar"] = "Peshawar";

		
        return $result;
    }

    public function calcExpireDate($date = '')
    {
        $date = $date ? $date : date('Y-m-d H:i:s');
        $expire = date('Y-m-d H:i:s', strtotime($date . ' +1 day'));

        $holidays = CHtml::listData(Holidays::model()->findAll(), 'date', 'date');

        while (in_array(date('N', strtotime($expire)), array(7)) || in_array(date('Y-m-d', strtotime($expire)), $holidays))
        {
            $expire = date('Y-m-d H:i:s', strtotime($expire . ' +1 day'));
        }
        
        return $expire;
    }

    public function calcExpiryProgress($start = '', $expiry = '', $status = '')
    {
        $start   = $start ? strtotime($start) : time();
        $expiry  = $expiry ? strtotime($expiry) : time();
        $current = time();

        $remaining = $expiry - $start;
        $elapsed   = $current - $start;

        $per = $remaining > 0 ? 100 - round(($elapsed / $remaining) * 100) : 0;
        $per = $per > 0 ? $per : 0;

        $diff = ($expiry - $current) / 3600;

        if($diff <= 0)
        {
            if(in_array($status, array(1,8)))
            {
                $txt = 'Expired';
            }
            elseif($status == 7)
            {
                $txt = 'Booked by other agent';
            }
            else
            {
                $txt = 'Submitted';
            }
        }
        elseif($diff > 24)
        {
            $diff = $diff / 24;

            $day = (int) $diff;
            $hrs = round(($diff - $day) * 24);

            $txt = "{$day} day {$hrs} hr";
        }
        else
        {
            $hrs  = (int) $diff;
            $mins = round(($diff - $hrs) * 60);

            $txt = "{$hrs} hr {$mins} min";
        }

        $barColor = $per <= 75 ? 'progress-bar-yellow' : 'progress-bar-green';
        $barColor = $per <= 25 ? 'progress-bar-red'    : $barColor;
        
        $txtColor = $per <= 5 ? 'text-red' : 'text-black';

        $txtColor = !in_array($status, array(1,7,8)) && $per == 0 ? 'text-green' : $txtColor;

        $html = '<div class="progress">
                    <div class="progress-bar '.$barColor.'" role="progressbar" aria-valuenow="'.$per.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$per.'%"><span class="'.$txtColor.'">'.$txt.'</span></div>
                </div>';

        return $html;
    }

    public function getLowestPrice($model)
    {
        $price = 0;

        $price = $model->fivbed ? $model->fivbed : $price;
        $price = $model->forbed ? $model->forbed : $price;
        $price = $model->tribed ? $model->tribed : $price;
        $price = $model->doubed ? $model->doubed : $price;
        $price = $model->shabed ? $model->shabed : $price;
        
        return $price;
    }

    public function getHighestPrice($model)
    {
        $price = 0;

        $price = $model->shabed ? $model->shabed : $price;
        $price = $model->doubed ? $model->doubed : $price;
        $price = $model->tribed ? $model->tribed : $price;
        $price = $model->forbed ? $model->forbed : $price;
        $price = $model->fivbed ? $model->fivbed : $price;
                
        return $price;

    }

    public function getTotalPrice($package, $model)
    {
        $adultPrice = 0;

        $adultPrice = $package->shabed ? $package->shabed : $adultPrice;
        $adultPrice = $package->doubed ? $package->doubed : $adultPrice;
        $adultPrice = $package->tribed ? $package->tribed : $adultPrice;
        $adultPrice = $package->forbed ? $package->forbed : $adultPrice;
        $adultPrice = $package->fivbed ? $package->fivbed : $adultPrice;

        $totalPrice = ($adultPrice * $model->no_adults) +
                      ($package->chibed * $model->no_children) + 
                      ($package->airline_price_adult * $model->no_adults) + 
                      ($package->airline_price_child * $model->no_children);
                
        return $totalPrice;
    }			

    public function getClientLeadStatus($status)
    {
        $statusList = Yii::app()->params['client_leads_status'];
        
        return '<i class="fa fa-circle text-' . $statusList[$status]['color'] . '" title="' . $statusList[$status]['title'] .'" rel="tooltip"></i>';
    }

    public function getHotelsStar($id)
    {
        $hotel = Hotels::model()->findByPk($id);
        $stars = '';

        if(isset($hotel->stars) && $hotel->stars)
        {
            for($i = 0; $i < $hotel->stars; $i++)
            {
                $stars .= '<img src="http://travelpackagecompare.com/assets/images/icon/star_icon.png" style="width:12px; height: 12px;" />';
            }
        }

        if($hotel->hotel_distance)
        {
            $stars .= "<br />Distance: " . $hotel->hotel_distance;    
        }
        
        return $stars;
    }
	
	public function getDateTime ($hours = 0){
		
		$tm = date('Gi', strtotime('+'. $hours .' hours'));
		$ndt = date('Y-m-d H:i:s', strtotime('+'. $hours .' hours'));
		
		$nday = date ("D",strtotime($ndt));
		
		if ($nday == "Sun"){
			$ndt = date('Y-m-d H:i:s', strtotime('+1 day',strtotime($ndt)));	
		}
							
		if($tm > 1800)
		{
			$ndt = date('Y-m-d H:i:s', strtotime('+1 day',strtotime($ndt)));
			
			$nday = date ("D",strtotime($ndt));
			
			if ($nday == "Sun"){
				$ndt = date('Y-m-d H:i:s', strtotime('+1 day',strtotime($ndt)));	
			}
			
			$datetime = date('Y-m-d 09:00:00', strtotime($ndt));	
			
		}
		else
		{
			$datetime = $ndt;	
			
		}
		
		return $datetime;
		
	}
	
	public function getClientCity($clientId){
		
		$clientMod = Clients::model()->findByPK($clientId);
		$clientCityID = $clientMod->city_id;
		
		$cityMod = Cities::model()->findByPK($clientCityID);
		
		return $cityMod->name;
		
	}
	
	public function get_next_business_date($from, $days) {
		$workingDays = [1, 2, 3, 4, 5, 6]; # date format = N (1 = Monday, ...)
		$holidayDays = ['*-12-25', '*-01-01', '2013-12-24']; # variable and fixed holidays
	
		$from = new DateTime($from);
		while ($days) {
			$from->modify('+1 day');
			if (!in_array($from->format('N'), $workingDays)) continue;
			if (in_array($from->format('Y-m-d'), $holidayDays)) continue;
			if (in_array($from->format('*-m-d'), $holidayDays)) continue;
			$days--;
		}
		return $from->format('Y-m-d'); #  or just return DateTime object
	}

	public function getChargeableService($event_id){

		$eventsVenue = EventVenues::model()->findAll(array('condition'=>' deleted =0 and clash=0 and event_id = '.$event_id.'','order' => 'title ASC'));
		$totalCost = 0;
		foreach($eventsVenue as $key=>$data) {
			$services = EventVenuesServices::model()->findAll(array('condition' => ' event_venues_id="' . $data["id"] . '" ', 'order' => 'id ASC'));

			$serviceCost = 0;
			foreach($services as $k => $d) {

				if ($d->services->chargeable == 1) {
					$serviceCost += $d->services->price;
				}

			}

			if ($data["location_id"]!= 0 && $data->location->fees) {

				$totalCost += $data->location->fees;

			}

			if ($data["location_id"]!= 0 && $data->location->service_charges) {

				$totalCost += $data->location->service_charges;

			}

			if ($data["location_id"]!= 0 && $data->location->tax_rate && $serviceCost) {

				$taxrate = $serviceCost * ($data->location->tax_rate / 100);

				$totalCost += $taxrate;

			}

		}

		return $totalCost;

	}
	
	public function encode($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		for ($i = 0; $i < $strLen; $i++) {
			$ordStr = ord(substr($string,$i,1));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
		}
		return $hash;
	}
	
	public function decode($string,$key) {
		$key = sha1($key);
		$strLen = strlen($string);
		$keyLen = strlen($key);
		for ($i = 0; $i < $strLen; $i+=2) {
			$ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
			if ($j == $keyLen) { $j = 0; }
			$ordKey = ord(substr($key,$j,1));
			$j++;
			$hash .= chr($ordStr - $ordKey);
		}
		return $hash;
	}
	
	public function partnerHierarchy($partner_id){
		
		$partnerPrice = PartnerPriceType::model()->find(array('condition'=>' staff_id ="'.$partner_id.'" and date_from <="'.date("Y-m-d").'" and deleted=0 and date_to is null'));
		
		$_SESSION["partnerPrice"][] = $partnerPrice;
		
		$partner = Partners::model()->findByPk($partner_id);
		
		$partner_id = $partner["parent_id"];
		
		if ($partner_id > 0){
			$this->partnerHierarchy($partner_id);
		}
		
		
	}
	
	public function partnerEmailHierarchy($partner_id){
		
		$partner = Partners::model()->findByPk($partner_id);
		
		$partner_id = $partner["parent_id"];
		
		$_SESSION["partnerEmail"][] =  $partner["email"];
		$_SESSION["partnerCcEmail"][] =  $partner["ccEmail"];
		
		if ($partner_id > 0){
			$this->partnerEmailHierarchy($partner_id);
		}
		
		
	}
	
	
	public function partnerEmailHierarchyEx($partner_id){
		
		$partner = Partners::model()->findByPk($partner_id);
		
		$partner_id = $partner["parent_id"];
		
		//$_SESSION["partnerEmail"][] =  $partner["email"];
		//$_SESSION["partnerCcEmail"][] =  $partner["ccEmail"];
		
		if ($partner_id > 0){
			$this->partnerEmailHierarchy($partner_id);
		}
		
		
	}
	
	
	public function partnerIdHierarchy($partner_id){
		
		$partner = Partners::model()->findByPk($partner_id);
		
		$partner_id = $partner["parent_id"];
		
		$_SESSION["partnerParent"][] =  $partner_id;
		
		if ($partner_id > 0){
			$this->partnerIdHierarchy($partner_id);
		}
		
		
	}
	
	public function childIdHierarchy($partner_id){
		
		$partner = Partners::model()->findAll(array("condition"=>" parent_id='".$partner_id."'"));
		
		foreach($partner as $staff){
			
			$child_id = $staff["id"];
		
			$_SESSION["partnerChild"][] =  $child_id;
			
			
			$this->childIdHierarchy($child_id);
			
			
		}
		
		
		
		
	}
	
	public function agentPrice($price,$price_set){
		
		$final_price = $price ;
	
		
		
		foreach($price_set as $priceRate){
			
			if($priceRate){
				
				if ($priceRate["price_type"] == 1){
					$final_price += $priceRate["fix_price"];
				}elseif ($priceRate["price_type"] == 2){
					$final_price += ($final_price * ($priceRate["floating_price"]/100));
				}elseif ($priceRate["price_type"] == 3){
					
					
					$fixprice = $final_price + $priceRate["fix_price"];
					
					
					
					$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
					
					
					if ($priceRate["hybrid_condition"] == 1){
						if($fixprice < $floating_price){
							$final_price = $fixprice;	
						}else{
							$final_price = $floating_price;	
						}
					}else{
						
						
						
						if($fixprice > $floating_price){
							$final_price = $fixprice ;	
						}else{
							$final_price = $floating_price;	
						}
						
					}
					
					
					
					
				}
				
			}
			
			
				
		}
		
		return $final_price;
		
	}
	
	
	public function getAgentLimit($partnerId){
		
		$partnerLimit = PartnerLimit::model()->find(array("condition"=>" staff_id = '".$partnerId."' and date_start <= '".date("Y-m-d H:i:s")."' and date_end is null and deleted =0"));
		
	
		
		$sql = "SELECT SUM(booking_limit) AS total FROM `partner_limit` WHERE staff_id IN (SELECT id FROM `partners` WHERE parent_id='".$partnerId."') and deleted=0;";
		$childLimit = Yii::app()->db->createCommand($sql)->queryRow();
		
		
		
		$bookingLimit = $partnerLimit["booking_limit"];
		$checkinLimit = $partnerLimit["checkin_limit"];
		$date_start = $partnerLimit["date_start"];
		
		$used = $this->getUsedAmt($partnerId);
		
		
		$val = $bookingLimit - $used - $childLimit["total"];
		
		return ($val);
		
	}
	
	
	public function getUsedAmt($partnerId){
		
		
		$sql = "SELECT SUM(amount) AS total, SUM(extra_charge) AS extra_charge, SUM(discount) AS discount FROM booking WHERE staff_id='".$partnerId."' and `status` <> 2";
		$bookingAmt = Yii::app()->db->createCommand($sql)->queryRow();
		
		$sql = "SELECT SUM(amount) AS total FROM partner_payment WHERE partner_id='".$partnerId."'";
		$payments = Yii::app()->db->createCommand($sql)->queryRow();
		
		$used = ($bookingAmt["total"] + $bookingAmt["extra_charge"] - $bookingAmt["discount"]) - $payments["total"];
		
		return $used;
		
	}
	
	public function getCheckinLimit($partnerId){
		$partnerLimit = PartnerLimit::model()->find(array("condition"=>" staff_id = '".$partnerId."' and date_start <= '".date("Y-m-d H:i:s")."' and date_end is null and deleted =0"));
		
		$checkinLimit = $partnerLimit["checkin_limit"];
		
		return $checkinLimit;
		
		
	}
	
	public function getCheckinLimitMin($partnerId){
		$sql = "SELECT max(checkin_limit) AS total FROM `partner_limit` WHERE staff_id IN (SELECT id FROM `partners` WHERE parent_id='".$partnerId."') and deleted=0;";
		$childLimit = Yii::app()->db->createCommand($sql)->queryRow();
		
		$checkinLimit = $childLimit["total"];
		
		return $checkinLimit;
		
		
	}
	
	public function getCheckinLimitAva($partnerId){
		$partnerLimit = PartnerLimit::model()->find(array("condition"=>" staff_id = '".$partnerId."' and date_start <= '".date("Y-m-d H:i:s")."' and date_end is null and deleted =0"));
		
		$checkinLimit = $partnerLimit["checkin_limit"];
		
		return $checkinLimit;
		
		
	}

	
	
	public function getAgentLimitMin($partnerId){
		
		
		$sql = "SELECT SUM(booking_limit) AS total FROM `partner_limit` WHERE staff_id IN (SELECT id FROM `partners` WHERE parent_id='".$partnerId."') and deleted=0;";
		$childLimit = Yii::app()->db->createCommand($sql)->queryRow();
		
		
		$used = $this->getUsedAmt($partnerId);
		
		
		$val =  $childLimit["total"] - $used;
		
		return ($val);
		
	}
	
	public function getDatesToDays($date1,$date2){
		
		$date1=date_create($date1);
		$date2=date_create($date2);
		
		$diff=date_diff($date1,$date2);

		
		return $diff;
			
	}
	
	public function getDays($date1,$date2){
		
		$days = $this->getDatesToDays($date1,$date2);	
	
		
		if ($days->invert == 1)	
			return "-".$days->days;
		else
			return $days->days;
		
	}
	
	public function getColor ($percentage,$type="other"){
		
		$color = "";
		
		if ($type == "request"){
			
			if($percentage >= 21)
				$color = "#ffc7ce";
			elseif($percentage >= 11 && $percentage <=20)
				$color = "#ffeb9c";
			elseif($percentage <= 10)
				$color = "#c6efce";
			
			
		}elseif ($type == "booking"){
			
			if($percentage >= 11) //Red
				$color = "#ffc7ce";
			elseif($percentage >= 6 && $percentage <=10) // Yellow
				$color = "#ffeb9c";
			elseif($percentage <= 5) // Green
				$color = "#c6efce";
		
			
		}else{
			
			if($percentage >= 67)
				$color = "#ffc7ce";
			elseif($percentage >= 34 && $percentage <=66)
				$color = "#ffeb9c";
			elseif($percentage <= 33)
				$color = "#c6efce";
			
			
		}
		
		
		
		
		return $color;
		
	}
	
	public function getOpeningReceiveable($date,$aid=""){
		
		$balance = 0;
		$receivable = Booking::model()->myInvoiceSearchAll($date,$aid);
		
	
		foreach($receivable->getData() as $key=>$data){
			
			$val = $data->getMyRateTotal("return");
			
			$balance =  $balance + $val;
		}
		
		
		
		
		
		return $balance;
		
	}
	
	public function getOpeningLimit($partnerId,$date=""){
		
		if ($date == "")
			$date = date("Y-m-d H:i:s");
		
		$partnerLimit = PartnerLimit::model()->find(array("condition"=>" staff_id = '".$partnerId."' and date_start <= '".$date."' and date_end is null and deleted =0"));
		
	
		
		$sql = "SELECT SUM(booking_limit) AS total FROM `partner_limit` WHERE staff_id IN (SELECT id FROM `partners` WHERE parent_id='".$partnerId."') and deleted=0 and date_start <= '".$date."' ORDER BY id DESC;";
		$childLimit = Yii::app()->db->createCommand($sql)->queryRow();
		
		
		
		$bookingLimit = $partnerLimit["booking_limit"];
		$checkinLimit = $partnerLimit["checkin_limit"];
		$date_start = $partnerLimit["date_start"];
		
		
		$sql = "SELECT SUM(amount) AS total, SUM(extra_charge) AS extra_charge, SUM(discount) AS discount FROM booking WHERE staff_id='".$partnerId."' and `status` <> 2 and created <='".$date."'";
		$bookingAmt = Yii::app()->db->createCommand($sql)->queryRow();
		
		$sql = "SELECT SUM(amount) AS total FROM partner_payment WHERE partner_id='".$partnerId."' and payment_date <='".date("Y-m-d",strtotime($date))."'";
		$payments = Yii::app()->db->createCommand($sql)->queryRow();
		
		$used = ($bookingAmt["total"] + $bookingAmt["extra_charge"] - $bookingAmt["discount"]) - $payments["total"];
		
		
		$val = $bookingLimit - $used - $childLimit["total"];
		
		return ($val);
			
	}
	
	
	public function getMyRateTotalReport($booking_id,$partner_id){
		
		$sql ="SELECT * FROM `booking_rooms` WHERE booking_id = ".$booking_id;
		
		$bookingRate = Yii::app()->db->createCommand($sql)->queryAll();
		
		$totalPrice = $totalPrice2 = 0;
		
		$myId = $partner_id;
		
		unset($_SESSION["partnerParent"]);		
		Yii::app()->functions->partnerIdHierarchy($myId);
		
		
		
		$parents = ($_SESSION["partnerParent"]);
		krsort($parents);
		
		
		unset($_SESSION["partnerChild"]);		
		Yii::app()->functions->childIdHierarchy($myId);
		
		if(isset($_SESSION["partnerChild"]))		
			$childs = ($_SESSION["partnerChild"]);
		else
			$childs = array();
		//krsort($childs);
		
		
		
	
		
		foreach($bookingRate as $rate){
			
			$rate_level = json_decode($rate["rate_level"],true);
			$final_price = $rate["base_price"] ;
			//echo $final_price . " ==> ";
			$no_of_room = $rate["no_of_room"];
			
			$totalPrice = 0;
			if (isset(Yii::app()->user->partner) && Yii::app()->user->partner['parent_id'] == 0){
				if(isset($rate_level))
					$child_key = key($rate_level);
				else
					$child_key = 0;
				
			}elseif (isset(Yii::app()->user->partner)){
				
				
				
				if(isset($rate_level)){
					$keys = array_keys($rate_level);
					
					
					if(isset($keys[array_search($myId,$keys)]))
						$child_key = $keys[array_search($myId,$keys)];
					else
						$child_key = 0;
					
				}else{
					$child_key = 0;
				}
			}else{
				$child_key = key($rate_level);
			}
			
			
			//echo $myId . " == " . $child_key . "<br>";
			
			
			
			
			foreach($parents as $parent_id){
				
				if ($parent_id == 0)
					continue;
					
					
				if (isset($rate_level[$parent_id])){
					
					$priceRate = $rate_level[$parent_id];
					
					if ($priceRate["price_type"] == 1){
						$final_price += $priceRate["fix_price"];
					}elseif ($priceRate["price_type"] == 2){
						$final_price += ($final_price * ($priceRate["floating_price"]/100));
					}elseif ($priceRate["price_type"] == 3){						
						$fixprice = $final_price + $priceRate["fix_price"];
						
						$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
						
						
						if ($priceRate["hybrid_condition"] == 1){
							if($fixprice < $floating_price){
								$final_price = $fixprice;	
							}else{
								$final_price = $floating_price;	
							}
						}else{
							
							if($fixprice > $floating_price){
								$final_price = $fixprice ;	
							}else{
								$final_price = $floating_price;	
							}
							
						}						
					}
					
				}else{
						
				}
				
				//echo $final_price . "<br>";
			}
			
			
			
			
			
			
			
			
			
			
			
		
			
			
			if(isset($rate_level[$myId])){
				
				
				$priceRate = $rate_level[$myId];
				
				if ($priceRate["price_type"] == 1){
					$final_price += $priceRate["fix_price"];
				}elseif ($priceRate["price_type"] == 2){
					$final_price += ($final_price * ($priceRate["floating_price"]/100));
				}elseif ($priceRate["price_type"] == 3){
					
					
					$fixprice = $final_price + $priceRate["fix_price"];
					
					
					$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
					
					
					
					if ($priceRate["hybrid_condition"] == 1){
						if($fixprice < $floating_price){
							$final_price = $fixprice;	
						}else{
							$final_price = $floating_price;	
						}
					}else{
						
						if($fixprice > $floating_price){
							$final_price = $fixprice ;	
						}else{
							$final_price = $floating_price;	
						}
						
					}
					
				}
				
				
			}else{
				//$final_price = $rate["base_price"];
			}
				//echo $final_price . " ==> ";
				
				
				
				
			if ( true){
				
				
				//echo "Child Key = " . $child_key . " == staff_id = " . $this->staff_id . "<br>" ;
				if (isset($rate_level[$child_key]))
					$priceRate = $rate_level[$child_key];
				else{
					return;
				}
				
				
				if ($priceRate["price_type"] == 1){
					$final_price += $priceRate["fix_price"];
				}elseif ($priceRate["price_type"] == 2){
					$final_price += ($final_price * ($priceRate["floating_price"]/100));
				}elseif ($priceRate["price_type"] == 3){						
					$fixprice = $final_price + $priceRate["fix_price"];
					
					$floating_price = $final_price +  ($final_price * ($priceRate["floating_price"]/100));
					
					
					//echo "Fix = " .$fixprice . " === Floating = " . $floating_price . "<br>";
					
					if ($priceRate["hybrid_condition"] == 1){
						if($fixprice < $floating_price){
							$final_price = $fixprice;	
						}else{
							$final_price = $floating_price;	
						}
					}else{
						
						if($fixprice > $floating_price){
							$final_price = $fixprice ;	
						}else{
							$final_price = $floating_price;	
						}
						
					}						
				}
				//echo $final_price . "<br>";
				
			}
			
			
			
			
			$totalPrice2 += ($final_price * $no_of_room) ;
			
			//echo $final_price * $no_of_room . "<br>";
			
			//echo $rate["base_price"];
			
		} 
		
		$booking = Booking::model()->findByPk($booking_id);
		$totalPrice2 +=  $booking->extra_charge - $booking->discount;
		return $totalPrice2 ;
	}

	public  function  getHost(){

		$host = getHost();

		$settings = Settings::model()->findAll();


		foreach($settings as $s){

			$key = str_replace(" ", "_", $s->meta_key);
			$key = strtolower($key);
			Yii::app()->user->setState($key, $s->meta_value);

		}


		if ($host != ""){

			$partners = Partners::model()->findByAttributes(["base_url"=>$host,"is_whitelabale"=>"1"]);

			if($partners){

				Yii::app()->user->setState("site_name", $partners["name"]);
				Yii::app()->user->setState("cell", $partners["cell"]);
				Yii::app()->user->setState("email", $partners["email"]);
				Yii::app()->user->setState("cc_email", $partners["ccEmail"]);
				Yii::app()->user->setState("address", $partners["address"]);
				Yii::app()->user->setState("logo", "images/partner/".$partners["id"]."/".$partners["image"]);
				Yii::app()->user->setState("mail_host", $partners["mail_host"]);
				Yii::app()->user->setState("mail_username", $partners["mail_username"]);
				Yii::app()->user->setState("mail_password", $partners["mail_password"]);

				if($partners["important_note"] != "")
					Yii::app()->user->setState("important_note", $partners["important_note"]);

				if($partners["note"] != "")
					Yii::app()->user->setState("note", $partners["note"]);


			}
		}

	}

	function array_search_inner ($array, $attr, $val, $strict = FALSE) {
		// Error is input array is not an array
		if (!is_array($array)) return FALSE;
		// Loop the array
		foreach ($array as $key => $inner) {
			// Error if inner item is not an array (you may want to remove this line)
			if (!is_array($inner)) return FALSE;
			// Skip entries where search key is not present
			if (!isset($inner[$attr])) continue;
			if ($strict) {
				// Strict typing
				if ($inner[$attr] === $val) return $key;
			} else {
				// Loose typing
				if ($inner[$attr] == $val) return $key;
			}
		}
		// We didn't find it
		return NULL;
	}
	
	
}