<?php
/**
 * BootListView class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 */

Yii::import('zii.widgets.CListView');

/**
 * Bootstrap list view.
 * Used to enable the bootstrap pager.
 */
class BootListView extends CListView
{
	/**
	 * @var string the CSS class name for the pager container. Defaults to 'pagination'.
	 */
	public $pagerCssClass = 'pagination';
	/**
	 * @var array the configuration for the pager.
	 */
	public $pager = array('class'=>'bootstrap.widgets.BootPager');
	/**
	 * @var string the URL of the CSS file used by this detail view.
	 * Defaults to false, meaning that no CSS will be included.
	 */
	public $cssFile = false;

	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		parent::init();

		$popover = Yii::app()->bootstrap->popoverSelector;
		$tooltip = Yii::app()->bootstrap->tooltipSelector;

    // Added by Raheel Dharolia to Customize the Pager for Clistview
    // This is CLinkPager Class. See http://www.yiiframework.com/doc/api/1.1/CLinkPager for reference of properties which can be set here.
    $this->pager = array( 
      'class'          => 'MyLinkPager', 
      'header'         => '',
      'footer'         => '',
      'firstPageLabel' => '', 
      'firstPageCssClass' => 'hidden',
      'prevPageLabel'  => 'Previous', 
      'nextPageLabel'  => 'Next', 
      'lastPageLabel'  => '',
      'lastPageCssClass' => 'hidden',
      'maxButtonCount' => Yii::app()->params['PAGER_NO_OF_PAGES'],
      'cssFile'=>false, //  Setting this to False means do not apply any default styles. null means apply default styles. giving a path to a css file here means use custom css for pager links
      'pages' => array(
        'class' => 'MyPagination',
      )
     );
                       
		$afterAjaxUpdate = "js:function() {
			jQuery('.popover').remove();
			jQuery('{$popover}').popover();
			jQuery('.tooltip').remove();
			jQuery('{$tooltip}').tooltip();
		}";

		if (isset($this->afterAjaxUpdate))
			$this->afterAjaxUpdate .= ' '.$afterAjaxUpdate;
		else
			$this->afterAjaxUpdate = $afterAjaxUpdate;
	}


}
