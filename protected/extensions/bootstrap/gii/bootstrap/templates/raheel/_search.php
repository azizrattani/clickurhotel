<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl(\$this->route),
	'method'=>'get',
)); ?>\n"; ?>

<?php foreach($this->tableSchema->columns as $column): ?>
<?php
	$field=$this->generateInputField($this->modelClass,$column);
	if(strpos($field,'password')!==false)
		continue;

    // Skip These Columns to be shown on Advanced Search Form
    if (in_array($column->name, Yii::app()->params['GII_SKIP_COLUMNS']['SEARCH'])) {
        continue;    
    }
    
    // Generate DropDown for all Fields having dataType tinyint(1) such as 'active', 'verified' etc.
    if ($column->dbType=='tinyint(1)') {        
        print "<?php print \$form->dropDownListRow(\$model,'".$column->name."',Yii::app()->params['YES_NO_LIST'], array('class'=>'span2')); ?>";
        continue;
    }
    
?>
    
	<?php echo "<?php echo ".$this->generateActiveRow($this->modelClass,$column)."; ?>\n"; ?>

<?php endforeach; ?>
	<div class="form-actions">
		<?php echo "<?php \$this->widget('bootstrap.widgets.BootButton', array(
			'buttonType'=>'submit',
            'type'=>'primary',
			'label'=>'Search',
		)); ?>\n"; ?>
	</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>