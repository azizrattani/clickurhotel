<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.BootActiveForm',array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'class'=>'well',
    ),
)); ?>\n"; ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<?php
foreach($this->tableSchema->columns as $column)
{
        
    // Skip These Columns to be shown on create/update forms
    if (in_array($column->name, Yii::app()->params['GII_SKIP_COLUMNS']['FORM'])) {
        continue;    
    }
    
    // Generate DropDown for all Fields having dataType tinyint(1) such as 'active', 'verified' etc.
    //if ($column->name=='active') {
    if ($column->dbType=='tinyint(1)') {        
        print "<?php print \$form->dropDownListRow(\$model,'".$column->name."',Yii::app()->params['YES_NO_LIST'], array('class'=>'span2')); ?>";
        continue;
    }    

	if($column->autoIncrement)
		continue;
?>
	<?php echo "<?php echo ".$this->generateActiveRow($this->modelClass,$column)."; ?>\n"; ?>

<?php
}
?>
	<div class="form-actions">
		<?php echo "<?php \$this->widget('bootstrap.widgets.BootButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
            'icon'=>'icon-ok icon-white',
			'label'=>\$model->isNewRecord ? 'Create' : 'Save',
		)); ?>\n"; ?>

        <?php echo "<?php \$this->widget('bootstrap.widgets.BootButton', array(
			'buttonType'=>'link',
			'url'=>array('admin'),
            'type'=>'primary',
            'icon'=>'icon-arrow-left icon-white',
			'label'=>'Go back',
		)); ?>\n"; ?>
                
	</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
