<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Manage',
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->modelClass; ?>','url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert');
?>

<h1>Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?></h1>
<?php /*?>
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo "<?php echo CHtml::link('<i class=\"icon-search\"></i> Advanced Search','#',array('class'=>'search-button btn')); ?>"; ?>

<div class="search-form" style="display:none">
<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->
<?php */ ?>
<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.BootButton', array(
    'label'=>'Add new <?php print $this->class2name($this->modelClass); ?>',
    'url'=>array('create'),
    'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'icon'=>'icon-plus icon-white',
    'size'=>'small', // '', 'large', 'small' or 'mini'
)); ?>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.BootGridView',array(
	'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider'=>$model->search(),
    'type'=>'striped bordered condensed',
	'filter'=>$model,
	'columns'=>array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";

    // Skip These Columns to be shown on create/update forms
    if (in_array($column->name, Yii::app()->params['GII_SKIP_COLUMNS']['LIST'])) {
        continue;    
    }
    
    // Handle "active" column differently
    if ($column->dbType=='tinyint(1)') {
        echo "\t\tarray(\n";
        echo "\t\t\t'name'=>'".$column->name."',\n";
        echo "\t\t\t'type'=>'raw',\n";
        echo "\t\t\t'value'=>'\$data->".$column->name."==0?CHtml::encode(\"No\"):CHtml::encode(\"Yes\")',\n";
        echo "\t\t),\n\n";

        continue;
    }
	echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
		array(
			'class'=>'bootstrap.widgets.BootButtonColumn',
		),
	),
)); ?>
