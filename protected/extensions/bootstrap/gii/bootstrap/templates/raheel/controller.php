<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass."\n"; ?>
{

	<?php if (strpos($this->controllerFile,'\cms\controllers/wpanel/') !== FALSE) { ?>
	
			/**
			 * @return array action filters
			*/
			public function filters()
			{
		 		return array(
		 			'accessControl', // perform access control for CRUD operations
		 		);
			}
		
			/**
			 * Specifies the access control rules.
			 * This method is used by the 'accessControl' filter.
			 * @return array access control rules
			 */
			public function accessRules()
			{ 
		 		if($_SESSION['fr_user_shop']['uid'])
		 		{
		  			$myClass=get_class($this);    
		         	$actions=AdminController::isAllowed($_SESSION['fr_user_shop']['uid'],$myClass);
		 		}
		        
		 		return array(
		  			array('allow', 
		   				'actions'=>explode(',',$actions[$myClass]),
		   				'users'=>''.$_SESSION['fr_user_shop']['NAME'].'',
		                'redirect'=>array('/wpanel/default/notallowed'),
		  			),
		  
		  			array('deny',  // deny all users
		   				'users'=>array('*'),
		  			),
		 		);
			}
	<?php } ?>
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	// Commented by Raheel
    //public $layout='//layouts/column2';

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
    
    $this->render('view',compact('model'));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new <?php echo $this->modelClass; ?>;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];
			if($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Yo!</strong> Record has been saved successfully!');
				//$this->redirect(array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>));
                $this->redirect(array('admin'));
            }
		}

		$this->render('create',compact('model'));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];
			if($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Yo!</strong> Record has been updated successfully!');
                $this->redirect(array('admin'));			 
			}
		}

		$this->render('update',compact('model'));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if ($this->loadModel($id)->delete()) {
                Yii::app()->user->setFlash('warning', '<strong>Ahh Don\'t Regret!</strong> Record has been deleted!');			 
			} else {
                Yii::app()->user->setFlash('error', '<strong>Oops!</strong> Can\'t delete the record, try again my dear...');			 
			}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));			 
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new <?php echo $this->modelClass; ?>('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['<?php echo $this->modelClass; ?>']))
			$model->attributes=$_GET['<?php echo $this->modelClass; ?>'];

		$this->render('admin',compact('model'));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=<?php echo $this->modelClass; ?>::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='<?php echo $this->class2id($this->modelClass); ?>-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
