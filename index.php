<?php
set_time_limit(0);
function debug($data = array(),$die=false)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";

    if($die){
        die();
    }
}

function getHost(){

    //$_SERVER["HTTP_REFERER"] = "http://www.taibahonline.com";
    if (isset($_SERVER["HTTP_REFERER"])){

        $url = $_SERVER["HTTP_REFERER"];
        $parse = parse_url($url);

        return str_replace("www.","",$parse['host']);

    }else{
        $url = $_SERVER["HTTP_HOST"];
        //$parse = parse_url($url);

        return str_replace("www.","",$url);
    }
}

// change the following paths if necessary
$yii = dirname(__FILE__).'/yii_fw/yii.php';
$config = dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);
Yii::createWebApplication($config)->run();
