/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.20 : Database - clickurhotel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `admin_users` */

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  `email_add` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `added_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_emailAdd` (`email_add`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `admin_users` */

insert  into `admin_users`(`id`,`user_name`,`email_add`,`password`,`first_name`,`last_name`,`is_admin`,`is_active`,`added_on`) values (1,'admin','aziz@ideabox.com.pk','0192023a7bbd73250516f069df18b500','Aziz','Rattani',1,1,'2014-10-08 13:14:26');
insert  into `admin_users`(`id`,`user_name`,`email_add`,`password`,`first_name`,`last_name`,`is_admin`,`is_active`,`added_on`) values (17,'aziz','azizrattani@hotmail.com2','b85dc795ba17cb243ab156f8c52124e1','Aziz','Rattani',1,1,'2018-06-06 16:09:12');
insert  into `admin_users`(`id`,`user_name`,`email_add`,`password`,`first_name`,`last_name`,`is_admin`,`is_active`,`added_on`) values (18,'admin2','admin2@aa.com','e00cf25ad42683b3df678c61f42c6bda','admin2','admin2',1,1,'2018-07-21 07:12:40');

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `checkin` date DEFAULT NULL,
  `checkout` date DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `noofnight` int(11) DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `btype` enum('book','request') DEFAULT 'book',
  `full_guest_name` varchar(255) NOT NULL,
  `smooking` enum('yes','no') DEFAULT 'no',
  `special_request` text,
  `nationality` varchar(255) NOT NULL,
  `meal_plan` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `adult` varchar(255) DEFAULT NULL,
  `child` varchar(255) DEFAULT NULL,
  `exp_arrive` varchar(255) DEFAULT NULL,
  `extra_charge` float(10,2) DEFAULT '0.00',
  `discount` float(10,2) DEFAULT '0.00',
  `book_request` varchar(5) NOT NULL,
  `request_email` enum('yes','no') DEFAULT 'no',
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `staff_booking` (`staff_id`),
  KEY `hotel_id` (`hotel_id`),
  CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  CONSTRAINT `staff_booking` FOREIGN KEY (`staff_id`) REFERENCES `partners` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `booking` */

/*Table structure for table `booking_details` */

DROP TABLE IF EXISTS `booking_details`;

CREATE TABLE `booking_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `passport_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `details_booking` (`booking_id`),
  CONSTRAINT `details_booking` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `booking_details` */

/*Table structure for table `booking_rooms` */

DROP TABLE IF EXISTS `booking_rooms`;

CREATE TABLE `booking_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_rate_id` int(11) DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `booking_date` date DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `no_of_room` int(11) DEFAULT NULL,
  `room_rate` float(10,2) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `bookingRooms_booking` (`booking_id`),
  KEY `bookingRooms_rooms` (`room_rate_id`),
  CONSTRAINT `bookingRooms_booking` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`),
  CONSTRAINT `bookingRooms_rooms` FOREIGN KEY (`room_rate_id`) REFERENCES `hotel_room_rate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `booking_rooms` */

/*Table structure for table `countries` */

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) DEFAULT NULL,
  `drive_type` tinytext,
  `sort_order` int(11) DEFAULT NULL,
  `inspection` float(10,2) DEFAULT NULL,
  `inspection2` float(10,2) DEFAULT '0.00',
  `inspection3` float(10,2) DEFAULT '0.00',
  `inspection4` float(10,2) DEFAULT '0.00',
  `inspection5` float(10,2) DEFAULT '0.00',
  `vanning` float(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=latin1;

/*Data for the table `countries` */

insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (1,'AFGHANISTAN','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (2,'ANGOLA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (3,'ANGUILLA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (4,'ANTIGUA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (5,'ARUBA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (6,'AUSTRALIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (7,'BAHAMAS','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (8,'BANGKOK','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (9,'BANGLADESH','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (10,'BARBADOS','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (11,'BELGIUM','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (12,'BELIZE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (13,'BERMUDA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (14,'BOLIVIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (15,'BOTSWANA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (16,'BRAZIL','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (17,'BURUNDI','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (18,'CAMEROON','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (19,'CANADA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (20,'CAYMAN  ISLANDS','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (21,'CHILE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (22,'CHINA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (23,'CHRISTMAS ISLAND','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (24,'DOMINICA, COMMONWEALTH OF','LHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (25,'CONGO','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (26,'COTE DIVOIRE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (27,'CUBA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (28,'CYPRUS','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (29,'DEMO. REP OF CONGO','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (30,'DOMINICAN REPUBLIC','LHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (31,'DOUALA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (32,'EAST TIMOR','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (33,'ECUADOR','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (34,'EGYPT','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (35,'ETHIOPIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (36,'FIJI','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (37,'FREE PORT','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (38,'FREETOWN','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (39,'GABON','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (40,'GAMBIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (41,'GEORGIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (42,'GERMANY','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (43,'GHANA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (44,'GIBRALTAR','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (45,'GREECE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (46,'GRENADA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (47,'GUAM','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (48,'GUINEA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (49,'GUYANA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (50,'HAITI','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (51,'HONDURAS','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (52,'HONG KONG','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (53,'ICELAND','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (55,'INDONESIA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (56,'IRELAND','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (59,'JAMAICA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (60,'JAPAN','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (61,'KENYA','RHD',NULL,250.00,400.00,500.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (62,'KIRIBATI','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (63,'KUWAIT','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (64,'LESOTHO','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (65,'LIBERIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (66,'LIBYAN ARAB JAMAHIRIYA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (67,'MADAGASCAR','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (68,'MALAWI','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (69,'MALAYSIA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (70,'MALDIVES','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (71,'MALI','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (72,'MALTA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (73,'MANGOLIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (74,'MARITIUS','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (75,'MARSHALL ISLANDS','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (76,'MAURITANIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (77,'MICRONESIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (78,'MONTSERRAT','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (79,'MOZAMBIQUE','RHD',NULL,100.00,100.00,100.00,100.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (80,'NAMIBIA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (81,'NAURU','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (82,'NETHERLANDS','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (83,'NEW CALEDONIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (84,'NEW ZEALAND','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (85,'NIGER','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (86,'NIGERIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (87,'NORFOLK ISLAND','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (88,'NORTHLAND ANTILLES','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (89,'NORWAY','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (90,'NOUE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (91,'NUKUALOFA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (92,'OMAN','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (93,'PAKISTAN','RHD',NULL,0.00,0.00,0.00,0.00,0.00,350.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (94,'PALAU','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (95,'PANAMA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (96,'PAPUA NEW GUINEA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (97,'PERU','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (98,'PHILIPPINES','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (99,'PHILIPSBURG','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (100,'POLAND','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (101,'PORTUGAL','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (102,'QATAR','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (103,'RUSSIAN FEDERATION','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (104,'RWANDA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (105,'SAMOA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (106,'SAO TOME AND PRINCIPE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (107,'SAUDI ARABIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (108,'SENEGAL','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (109,'SIERRA LEONE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (110,'SINGAPORE','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (111,'SOLOMON ISLANDS','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (112,'SOUTH AFRICA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (113,'SPAIN','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (114,'SRI LANKA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (115,'ST KITTS','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (116,'ST LUCIA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (117,'ST VINCENT','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (118,'SUDAN','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (119,'SURINAME','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (120,'SWAZILAND','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (121,'TANZANIA','RHD',NULL,250.00,400.00,500.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (122,'THAILAND','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (123,'TOGO','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (124,'TONGA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (125,'TRINIDAD TOBAGO','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (126,'TUNISIA','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (127,'TURKEY','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (128,'TUVALU','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (129,'UAE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (130,'UGANDA','RHD',NULL,250.00,400.00,500.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (131,'UNITED KINGDOM','RHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (132,'VANUATU','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (133,'VIETNAM','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (134,'VIRGIN ISLANDS US','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (135,'ZAIRE','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (136,'ZAMBIA','RHD',NULL,250.00,400.00,500.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (137,'ZIMBABWE','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (138,'BENIN','LHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (139,'INDIA','RHD',NULL,NULL,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (140,'CHAD','LHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (141,'UNITED STATES OF AMERICA','LHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (142,'S.KOREA','LHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (143,'KOREA','LHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (144,'TAIWAN','LHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (145,'ST MAARTEN','LHD',NULL,0.00,0.00,0.00,0.00,0.00,0.00);
insert  into `countries`(`id`,`country`,`drive_type`,`sort_order`,`inspection`,`inspection2`,`inspection3`,`inspection4`,`inspection5`,`vanning`) values (146,'','',NULL,0.00,0.00,0.00,0.00,0.00,0.00);

/*Table structure for table `facilities` */

DROP TABLE IF EXISTS `facilities`;

CREATE TABLE `facilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `facilities` */

insert  into `facilities`(`id`,`title`,`description`,`deleted`,`created`,`modified`) values (1,'Breakfast and dinner','',0,'2018-12-24 19:38:09','2018-12-24 19:38:09');
insert  into `facilities`(`id`,`title`,`description`,`deleted`,`created`,`modified`) values (2,'Guest Houses ','',0,'2018-12-24 19:38:23','2018-12-24 19:38:23');
insert  into `facilities`(`id`,`title`,`description`,`deleted`,`created`,`modified`) values (3,'Serviced Apartments ','',0,'2018-12-24 19:38:41','2018-12-24 19:38:41');
insert  into `facilities`(`id`,`title`,`description`,`deleted`,`created`,`modified`) values (4,'Boutique','',0,'2018-12-24 19:38:59','2018-12-24 19:38:59');

/*Table structure for table `hotel` */

DROP TABLE IF EXISTS `hotel`;

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `short_desc` text NOT NULL,
  `description` text,
  `hotel_star` int(11) NOT NULL,
  `langitude` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` int(5) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `phone1` varchar(100) NOT NULL,
  `phone2` varchar(100) DEFAULT NULL,
  `phone3` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `hotel_policies` text,
  `pet` char(1) NOT NULL DEFAULT '0',
  `liquor` char(1) NOT NULL DEFAULT '0',
  `smoking` char(1) NOT NULL DEFAULT '0',
  `child_age_from` int(3) DEFAULT NULL,
  `child_age_to` int(3) DEFAULT NULL,
  `checkin_time` time NOT NULL,
  `checkout_time` time NOT NULL,
  `total_room` int(11) NOT NULL,
  `total_floor` int(11) NOT NULL,
  `build_year` int(4) NOT NULL,
  `mealOption` int(11) DEFAULT NULL,
  `rate_type` int(11) DEFAULT NULL,
  `breakfast_rate` float(10,2) DEFAULT '0.00',
  `lunch_rate` float(10,2) DEFAULT '0.00',
  `dinner_rate` float(10,2) DEFAULT '0.00',
  `one_night_start` int(11) NOT NULL,
  `one_night_end` int(11) NOT NULL,
  `cancel_50_start` int(11) NOT NULL,
  `cancel_50_end` int(11) NOT NULL,
  `cancel_100_start` int(11) NOT NULL,
  `cancel_100_end` int(11) NOT NULL,
  `free_cancel_start` int(11) NOT NULL,
  `free_cancel_end` int(11) NOT NULL,
  `market_type` varchar(255) NOT NULL,
  `meal_option` varchar(255) DEFAULT NULL,
  `meal_type` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hotel` */

insert  into `hotel`(`id`,`name`,`username`,`password`,`short_desc`,`description`,`hotel_star`,`langitude`,`latitude`,`address`,`city`,`state`,`country`,`zipcode`,`phone1`,`phone2`,`phone3`,`email`,`fax`,`website`,`hotel_policies`,`pet`,`liquor`,`smoking`,`child_age_from`,`child_age_to`,`checkin_time`,`checkout_time`,`total_room`,`total_floor`,`build_year`,`mealOption`,`rate_type`,`breakfast_rate`,`lunch_rate`,`dinner_rate`,`one_night_start`,`one_night_end`,`cancel_50_start`,`cancel_50_end`,`cancel_100_start`,`cancel_100_end`,`free_cancel_start`,`free_cancel_end`,`market_type`,`meal_option`,`meal_type`,`status`,`active`,`deleted`,`created`,`modified`) values (1,'Dar Al-Eiman Royal','dar_al-eiman','123456','Dar Al-Eiman Royal',NULL,5,'5.254555','5.11555','test','Makka',NULL,NULL,'','65455454','','','dar-aleiman-royal@yahoo.com',NULL,'',NULL,'0','0','0',NULL,NULL,'12:00:00','14:00:00',10,11,2010,2,3,10.00,15.00,0.00,0,0,0,0,0,0,0,0,'',NULL,'','1',1,0,'2019-01-02 19:44:21','2019-01-02 19:44:21');
insert  into `hotel`(`id`,`name`,`username`,`password`,`short_desc`,`description`,`hotel_star`,`langitude`,`latitude`,`address`,`city`,`state`,`country`,`zipcode`,`phone1`,`phone2`,`phone3`,`email`,`fax`,`website`,`hotel_policies`,`pet`,`liquor`,`smoking`,`child_age_from`,`child_age_to`,`checkin_time`,`checkout_time`,`total_room`,`total_floor`,`build_year`,`mealOption`,`rate_type`,`breakfast_rate`,`lunch_rate`,`dinner_rate`,`one_night_start`,`one_night_end`,`cancel_50_start`,`cancel_50_end`,`cancel_100_start`,`cancel_100_end`,`free_cancel_start`,`free_cancel_end`,`market_type`,`meal_option`,`meal_type`,`status`,`active`,`deleted`,`created`,`modified`) values (2,'Hotel 2','hotel 2','123456','asdsad',NULL,3,'12231232132','122312','B-09 Alyabad Center, Alyabad Colony Block 8 F.B Area Karachi, Near Tabba Heart Hospital.','Makka',NULL,NULL,'75950','03213812859','','','azizrattani@hotmail.com',NULL,'',NULL,'0','0','0',NULL,NULL,'14:00:00','00:00:00',222,23,2015,2,2,22.00,22.00,0.00,0,0,0,0,0,0,0,0,'',NULL,'','1',1,0,'2019-01-03 00:15:18','2019-01-03 00:16:41');
insert  into `hotel`(`id`,`name`,`username`,`password`,`short_desc`,`description`,`hotel_star`,`langitude`,`latitude`,`address`,`city`,`state`,`country`,`zipcode`,`phone1`,`phone2`,`phone3`,`email`,`fax`,`website`,`hotel_policies`,`pet`,`liquor`,`smoking`,`child_age_from`,`child_age_to`,`checkin_time`,`checkout_time`,`total_room`,`total_floor`,`build_year`,`mealOption`,`rate_type`,`breakfast_rate`,`lunch_rate`,`dinner_rate`,`one_night_start`,`one_night_end`,`cancel_50_start`,`cancel_50_end`,`cancel_100_start`,`cancel_100_end`,`free_cancel_start`,`free_cancel_end`,`market_type`,`meal_option`,`meal_type`,`status`,`active`,`deleted`,`created`,`modified`) values (3,'Aziz Rattani','admin','admin123','assa',NULL,1,'12132','1212','B-09 Alyabad Center, Alyabad Colony Block 8 F.B Area Karachi, Near Tabba Heart Hospital.','Makka',NULL,NULL,'75950','03213812859','','','azizrattani@hotmail.com',NULL,'',NULL,'0','0','0',NULL,NULL,'14:00:00','12:00:00',13,12,5000,NULL,1,100.00,0.00,100.00,10,123,12,123,123,12,12,12,'3,4,5','2','2,4','1',1,0,'2019-01-03 01:50:11','2019-01-03 01:52:42');

/*Table structure for table `hotel_cancellation_policy` */

DROP TABLE IF EXISTS `hotel_cancellation_policy`;

CREATE TABLE `hotel_cancellation_policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `one_night_start` int(11) NOT NULL,
  `one_night_end` int(11) NOT NULL,
  `cancel_50_start` int(11) NOT NULL,
  `cancel_50_end` int(11) NOT NULL,
  `cancel_100_start` int(11) NOT NULL,
  `cancel_100_end` int(11) NOT NULL,
  `free_cancel_start` int(11) NOT NULL,
  `free_cancel_end` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_cancellation_policy` */

/*Table structure for table `hotel_contact` */

DROP TABLE IF EXISTS `hotel_contact`;

CREATE TABLE `hotel_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `contact_type` enum('contact','reservation','account') NOT NULL DEFAULT 'contact',
  `person_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `deleted` char(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_contact` (`hotel_id`),
  CONSTRAINT `hotel_contact` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_contact` */

/*Table structure for table `hotel_facilities` */

DROP TABLE IF EXISTS `hotel_facilities`;

CREATE TABLE `hotel_facilities` (
  `hotel_id` int(11) NOT NULL,
  `facilities_id` int(11) DEFAULT NULL,
  KEY `hotelFacilities_hotel` (`hotel_id`),
  KEY `hotelFacilities_facilities` (`facilities_id`),
  CONSTRAINT `hotelFacilities_facilities` FOREIGN KEY (`facilities_id`) REFERENCES `facilities` (`id`),
  CONSTRAINT `hotelFacilities_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_facilities` */

/*Table structure for table `hotel_market` */

DROP TABLE IF EXISTS `hotel_market`;

CREATE TABLE `hotel_market` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `market_id` int(11) NOT NULL,
  `price` float(10,2) DEFAULT NULL COMMENT 'price is per person',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `market_hotel` (`hotel_id`),
  KEY `market_market` (`market_id`),
  CONSTRAINT `market_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  CONSTRAINT `market_market` FOREIGN KEY (`market_id`) REFERENCES `market_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_market` */

/*Table structure for table `hotel_market_type` */

DROP TABLE IF EXISTS `hotel_market_type`;

CREATE TABLE `hotel_market_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `market_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_market_type` */

/*Table structure for table `hotel_meal_rate` */

DROP TABLE IF EXISTS `hotel_meal_rate`;

CREATE TABLE `hotel_meal_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `bookDate` date NOT NULL,
  `breakfast` float(10,2) DEFAULT NULL,
  `lunch` float(10,2) DEFAULT NULL,
  `dinner` float(10,2) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `hotel_meal_rate_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  CONSTRAINT `hotel_meal_rate_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_meal_rate` */

/*Table structure for table `hotel_meal_type` */

DROP TABLE IF EXISTS `hotel_meal_type`;

CREATE TABLE `hotel_meal_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `meal_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `meal_type_id` (`meal_type_id`),
  CONSTRAINT `hotel_meal_type_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  CONSTRAINT `hotel_meal_type_ibfk_2` FOREIGN KEY (`meal_type_id`) REFERENCES `meals_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_meal_type` */

/*Table structure for table `hotel_picture` */

DROP TABLE IF EXISTS `hotel_picture`;

CREATE TABLE `hotel_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_pictures` (`hotel_id`),
  CONSTRAINT `hotel_pictures` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_picture` */

/*Table structure for table `hotel_room_rate` */

DROP TABLE IF EXISTS `hotel_room_rate`;

CREATE TABLE `hotel_room_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `bookdate` date DEFAULT NULL,
  `price` float(10,2) NOT NULL,
  `price_weekend` float(10,2) DEFAULT NULL,
  `four_days` float(10,2) DEFAULT NULL,
  `no_of_room` int(11) DEFAULT NULL,
  `room_book` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_room_rate` (`room_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `hotel_room_rate_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `room_room_rate` FOREIGN KEY (`room_id`) REFERENCES `hotel_rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hotel_room_rate` */

/*Table structure for table `hotel_rooms` */

DROP TABLE IF EXISTS `hotel_rooms`;

CREATE TABLE `hotel_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) NOT NULL,
  `title` tinytext NOT NULL,
  `description` text,
  `max_user` int(11) NOT NULL,
  `no_of_room` int(5) NOT NULL,
  `room_type` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`),
  KEY `room_type` (`room_type`),
  KEY `views` (`views`),
  CONSTRAINT `hotel_rooms_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  CONSTRAINT `hotel_rooms_ibfk_2` FOREIGN KEY (`room_type`) REFERENCES `room_type` (`id`),
  CONSTRAINT `hotel_rooms_ibfk_3` FOREIGN KEY (`views`) REFERENCES `view_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `hotel_rooms` */

insert  into `hotel_rooms`(`id`,`hotel_id`,`title`,`description`,`max_user`,`no_of_room`,`room_type`,`views`,`deleted`,`created`,`modified`) values (1,2,'12313','12',123,12,4,1,0,'2019-01-03 00:47:36','2019-01-03 00:47:36');

/*Table structure for table `market_type` */

DROP TABLE IF EXISTS `market_type`;

CREATE TABLE `market_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `deleted` char(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `market_type` */

insert  into `market_type`(`id`,`title`,`description`,`deleted`,`created_by`,`created`,`modified`) values (2,'All Market','','0',NULL,'2018-12-24 18:00:34','2018-12-24 18:00:34');
insert  into `market_type`(`id`,`title`,`description`,`deleted`,`created_by`,`created`,`modified`) values (3,'Far-East','','0',NULL,'2018-12-24 18:01:26','2018-12-24 18:01:26');
insert  into `market_type`(`id`,`title`,`description`,`deleted`,`created_by`,`created`,`modified`) values (4,'Turkey','','0',NULL,'2018-12-24 18:01:32','2018-12-24 18:01:32');
insert  into `market_type`(`id`,`title`,`description`,`deleted`,`created_by`,`created`,`modified`) values (5,'UK','','0',NULL,'2018-12-24 18:01:37','2018-12-24 18:01:37');

/*Table structure for table `meals_type` */

DROP TABLE IF EXISTS `meals_type`;

CREATE TABLE `meals_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `create_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `meals_type` */

insert  into `meals_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (1,'Room Only',0,'2018-12-24 17:56:35','2018-12-24 17:56:35','0');
insert  into `meals_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (2,'Breakfast',0,'2018-12-24 17:56:40','2018-12-24 17:56:40','0');
insert  into `meals_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (3,'Lunch',0,'2018-12-24 17:56:47','2018-12-26 20:06:18','0');
insert  into `meals_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (4,'Dinner',0,'2018-12-24 17:56:52','2018-12-26 20:06:26','0');

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `url` text,
  `user_id` int(11) NOT NULL,
  `read` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `notifications` */

/*Table structure for table `partner_limit` */

DROP TABLE IF EXISTS `partner_limit`;

CREATE TABLE `partner_limit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL DEFAULT '0',
  `booking_limit` float(10,2) NOT NULL,
  `checkin_limit` float(10,2) NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL COMMENT 'if null mean continue',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `limit_staff` (`staff_id`),
  CONSTRAINT `limit_staff` FOREIGN KEY (`staff_id`) REFERENCES `partners` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `partner_limit` */

/*Table structure for table `partner_login_logs` */

DROP TABLE IF EXISTS `partner_login_logs`;

CREATE TABLE `partner_login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `staff_login` (`staff_id`),
  CONSTRAINT `staff_login` FOREIGN KEY (`staff_id`) REFERENCES `partners` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `partner_login_logs` */

/*Table structure for table `partner_payment` */

DROP TABLE IF EXISTS `partner_payment`;

CREATE TABLE `partner_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) NOT NULL,
  `pay_date` date DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `reference_number` varchar(255) DEFAULT NULL,
  `narration` text,
  `bid` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partner_id` (`partner_id`),
  CONSTRAINT `partner_payment_ibfk_1` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `partner_payment` */

/*Table structure for table `partner_price_type` */

DROP TABLE IF EXISTS `partner_price_type`;

CREATE TABLE `partner_price_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `price_type` char(1) NOT NULL DEFAULT '1' COMMENT '1=flat,2=percentage,3=hybrid',
  `fix_price` float(10,2) DEFAULT NULL,
  `floating_price` float(10,2) DEFAULT NULL,
  `hybrid_condition` char(1) DEFAULT NULL COMMENT '1= which ever is low, 2= which ever is high',
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `partner_price_type_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `partners` (`id`),
  CONSTRAINT `partner_price_type_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `partner_price_type` */

/*Table structure for table `partners` */

DROP TABLE IF EXISTS `partners`;

CREATE TABLE `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '0 mean Sales office',
  `name` varchar(255) NOT NULL,
  `cell` varchar(255) NOT NULL,
  `cell2` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `joining_date` date NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `self_relation` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `partners` */

/*Table structure for table `permission_actions` */

DROP TABLE IF EXISTS `permission_actions`;

CREATE TABLE `permission_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_entity_id` int(11) NOT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_permission_entity_id` (`permission_entity_id`),
  CONSTRAINT `FK_permission_entity_id` FOREIGN KEY (`permission_entity_id`) REFERENCES `permission_entities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_actions` */

insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (1,'List',1,'permissions','Groups','Index','',NULL,0,'2016-04-10 21:35:29','2016-04-10 21:35:29');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (2,'Add',1,'permissions','Groups','Create','',1,0,'2016-04-10 21:35:45','2016-04-10 21:36:37');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (3,'Edit',1,'permissions','Groups','Update','',1,0,'2016-04-10 21:36:03','2016-04-10 21:36:32');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (4,'Delete',1,'permissions','Groups','Delete','',1,0,'2016-04-10 21:36:19','2016-04-10 21:36:19');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (5,'List',2,'permissions','Entities','Index','',NULL,0,'2016-04-10 21:37:42','2016-04-10 21:37:42');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (6,'Add',2,'permissions','Entities','Create','',5,0,'2016-04-10 21:37:54','2016-04-10 21:37:54');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (7,'Edit',2,'permissions','Entities','Update','',5,0,'2016-04-10 21:38:23','2016-04-10 21:38:23');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (8,'Delete',2,'permissions','Entities','Delete','',5,0,'2016-04-10 21:38:40','2016-04-10 21:38:40');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (9,'List',3,'permissions','Actions','Index','',NULL,0,'2016-04-10 21:39:35','2016-04-10 21:39:35');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (10,'Add',3,'permissions','Actions','Create','',9,0,'2016-04-10 21:40:11','2016-04-10 21:40:11');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (11,'Edit',3,'permissions','Actions','Update','',9,0,'2016-04-10 21:40:23','2016-04-10 21:40:23');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (12,'Delete',3,'permissions','Actions','Delete','',9,0,'2016-04-10 21:40:40','2016-04-10 21:40:40');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (13,'Ajax - Get Parents',3,'permissions','Actions','GetParents','',9,0,'2016-04-10 21:41:34','2016-04-10 21:41:34');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (14,'List',4,'permissions','Manage','Index','',NULL,0,'2016-04-10 21:42:16','2016-04-10 21:42:16');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (15,'Edit',4,'permissions','Manage','Index','update',NULL,0,'2016-04-10 21:42:42','2016-04-10 21:42:42');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (16,'List',5,'permissions','Settings','Index','',NULL,0,'2016-04-10 21:43:17','2016-04-10 21:43:17');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (17,'Add',5,'permissions','Settings','Create','',16,0,'2016-04-10 21:43:32','2016-04-10 21:43:32');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (18,'Edit',5,'permissions','Settings','Update','',16,0,'2016-04-10 21:43:53','2016-04-10 21:43:53');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (19,'List',6,'users','Admins','Index','',NULL,0,'2016-04-10 21:45:39','2016-04-10 21:45:39');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (20,'Add',6,'users','Admins','Create','',19,0,'2016-04-10 21:45:50','2016-04-10 21:45:50');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (21,'Edit',6,'users','Admins','Update','',19,0,'2016-04-10 21:46:01','2016-04-10 21:46:01');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (22,'Delete',6,'users','Admins','Delete','',19,0,'2016-04-10 21:46:13','2016-04-10 21:46:13');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (23,'List',7,'users','Csr','Index','',NULL,0,'2016-04-10 21:46:53','2016-04-10 21:46:53');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (24,'Add',7,'users','Csr','Create','',23,0,'2016-04-10 21:47:15','2016-04-10 21:47:15');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (25,'Edit',7,'users','Csr','Update','',23,0,'2016-04-10 21:47:37','2016-04-10 21:47:37');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (26,'Delete',7,'users','Csr','Delete','',23,0,'2016-04-10 21:47:50','2016-04-10 21:47:50');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (27,'List',8,'users','Clients','Index','',NULL,0,'2016-04-10 21:48:16','2016-04-10 21:48:16');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (28,'Edit',8,'users','Clients','Update','',27,0,'2016-04-10 21:48:33','2016-04-10 21:48:33');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (29,'List',9,'users','Manage','Index','',NULL,0,'2016-04-10 21:49:18','2016-04-10 21:49:18');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (30,'Add',9,'users','Manage','Create','',29,0,'2016-04-10 21:49:40','2016-04-10 21:49:40');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (31,'Edit',9,'users','Manage','Update','',29,0,'2016-04-10 21:49:53','2016-04-10 21:49:53');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (32,'Delete',9,'users','Manage','Delete','',29,0,'2016-04-10 21:50:07','2016-04-10 21:50:07');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (33,'List',10,'users','Roles','Index','',NULL,0,'2016-04-10 21:51:01','2016-04-10 21:51:01');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (34,'Add',10,'users','Roles','Create','',33,0,'2016-04-10 21:51:15','2016-04-10 21:51:15');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (35,'Edit',10,'users','Roles','Update','',33,0,'2016-04-10 21:51:29','2016-04-10 21:51:29');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (36,'Delete',10,'users','Roles','Delete','',33,0,'2016-04-10 21:51:44','2016-04-10 21:51:44');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (37,'Index',11,'facilities','Facilities','Index','',NULL,0,'2018-12-23 06:36:15','2018-12-23 06:36:15');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (38,'View',11,'facilities','Facilities','View','',NULL,0,'2018-12-23 06:36:28','2018-12-23 06:36:28');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (39,'Create',11,'facilities','Facilities','Create','',NULL,0,'2018-12-23 06:36:43','2018-12-23 06:36:43');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (40,'Update',11,'facilities','Facilities','Update','',NULL,0,'2018-12-23 06:36:53','2018-12-23 06:36:53');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (41,'Delete',11,'facilities','Facilities','Delete','',NULL,0,'2018-12-23 06:37:24','2018-12-23 06:37:24');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (42,'Index',12,'hotel','Hotel','Index','',NULL,0,'2018-12-23 06:40:04','2018-12-23 06:40:04');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (43,'View',12,'hotel','Hotel','View','',NULL,0,'2018-12-23 06:40:14','2018-12-23 06:40:14');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (44,'Create',12,'hotel','Hotel','Create','',NULL,0,'2018-12-23 06:40:25','2018-12-23 06:40:25');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (45,'Update',12,'hotel','Hotel','Update','',NULL,0,'2018-12-23 06:40:36','2018-12-23 06:40:36');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (46,'Delete',12,'hotel','Hotel','Delete','',NULL,0,'2018-12-23 06:40:44','2018-12-23 06:40:44');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (47,'Index',13,'markettype','Markettype','Index','',NULL,0,'2018-12-23 06:41:19','2018-12-23 06:41:19');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (48,'View',13,'markettype','Markettype','View','',NULL,0,'2018-12-23 06:41:28','2018-12-23 06:41:28');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (49,'Create',13,'markettype','Markettype','Create','',NULL,0,'2018-12-23 06:41:38','2018-12-23 06:41:38');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (50,'Update',13,'markettype','Markettype','Update','',NULL,0,'2018-12-23 06:41:46','2018-12-23 06:41:46');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (51,'Delete',13,'markettype','Markettype','Delete','',NULL,0,'2018-12-23 06:41:53','2018-12-23 06:41:53');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (52,'Index',14,'partner','Partners','Index','',NULL,0,'2018-12-23 06:43:19','2018-12-23 06:43:19');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (53,'View',14,'partner','Partners','View','',NULL,0,'2018-12-23 06:43:27','2018-12-23 06:43:27');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (54,'Create',14,'partner','Partners','Create','',NULL,0,'2018-12-23 06:43:35','2018-12-23 06:43:35');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (55,'Update',14,'partner','Partners','Update','',NULL,0,'2018-12-23 06:43:43','2018-12-23 06:43:43');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (56,'Delete',14,'partner','Partners','Delete','',NULL,0,'2018-12-23 06:43:52','2018-12-23 06:43:52');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (57,'Index',15,'mealtype','Mealstype','Index','',NULL,0,'2018-12-24 16:26:15','2018-12-24 16:26:15');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (58,'View',15,'mealtype','Mealstype','View','',NULL,0,'2018-12-24 16:26:27','2018-12-24 16:26:27');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (59,'Create',15,'mealtype','Mealstype','Create','',NULL,0,'2018-12-24 16:26:38','2018-12-24 16:26:38');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (60,'Update',15,'mealtype','Mealstype','Update','',NULL,0,'2018-12-24 16:26:48','2018-12-24 16:26:48');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (61,'Delete',15,'mealtype','Mealstype','Delete','',NULL,0,'2018-12-24 16:26:58','2018-12-24 16:26:58');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (62,'Index',16,'roomtype','RoomType','Index','',NULL,0,'2018-12-24 16:27:39','2018-12-24 16:27:39');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (63,'View',16,'roomtype','RoomType','View','',NULL,0,'2018-12-24 16:27:50','2018-12-24 16:27:50');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (64,'Create',16,'roomtype','RoomType','Create','',NULL,0,'2018-12-24 16:28:02','2018-12-24 16:28:02');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (65,'Update',16,'roomtype','RoomType','Update','',NULL,0,'2018-12-24 16:28:11','2018-12-24 16:28:11');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (66,'Delete',16,'roomtype','RoomType','Delete','',NULL,0,'2018-12-24 16:28:21','2018-12-24 16:28:21');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (67,'Index',17,'viewtype','ViewType','Index','',NULL,0,'2018-12-24 16:28:48','2018-12-24 16:28:48');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (68,'View',17,'viewtype','ViewType','View','',NULL,0,'2018-12-24 16:28:59','2018-12-24 16:28:59');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (69,'Create',17,'viewtype','ViewType','Create','',NULL,0,'2018-12-24 16:29:13','2018-12-24 16:29:13');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (70,'Update',17,'viewtype','ViewType','Update','',NULL,0,'2018-12-24 16:29:24','2018-12-24 16:29:24');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (71,'Delete',17,'viewtype','ViewType','Delete','',NULL,0,'2018-12-24 16:29:31','2018-12-24 16:29:31');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (72,'Index',18,'hotel','HotelRooms','Index','',NULL,0,'2018-12-25 14:43:13','2018-12-25 14:43:13');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (73,'View',18,'hotel','HotelRooms','View','',NULL,0,'2018-12-25 14:43:25','2018-12-25 14:43:25');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (74,'Create',18,'hotel','HotelRooms','Create','',NULL,0,'2018-12-25 14:43:37','2018-12-25 14:43:37');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (75,'Update',18,'hotel','HotelRooms','Update','',NULL,0,'2018-12-25 14:43:46','2018-12-25 14:43:46');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (76,'Delete',18,'hotel','HotelRooms','Delete','',NULL,0,'2018-12-25 14:43:59','2018-12-25 14:43:59');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (77,'Index',19,'hotel','HotelRoomRate','Index','',NULL,0,'2018-12-25 17:31:36','2018-12-25 17:31:36');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (78,'View',19,'hotel','HotelRoomRate','View','',NULL,0,'2018-12-25 17:31:49','2018-12-25 17:31:49');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (79,'Create',19,'hotel','HotelRoomRate','Create','',NULL,0,'2018-12-25 17:32:04','2018-12-25 17:32:04');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (80,'Update',19,'hotel','HotelRoomRate','Update','',NULL,0,'2018-12-25 18:38:07','2018-12-25 18:38:07');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (81,'Delete',19,'hotel','HotelRoomRate','Delete','',NULL,0,'2018-12-25 18:38:21','2018-12-25 18:38:21');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (82,'Hotel Detail',12,'hotel','Hotel','HotelDetail','',NULL,0,'2018-12-27 03:36:23','2018-12-27 03:36:23');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (83,'Hotel Rooms',12,'hotel','Hotel','HotelRooms','',NULL,0,'2018-12-27 04:28:00','2018-12-27 04:28:00');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (84,'Index',20,'partner','PartnerPriceType','Index','',NULL,0,'2018-12-30 03:53:42','2018-12-30 03:53:42');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (85,'View',20,'partner','PartnerPriceType','View','',NULL,0,'2018-12-30 03:55:33','2018-12-30 03:55:33');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (86,'Create',20,'partner','PartnerPriceType','Create','',NULL,0,'2018-12-30 03:55:42','2018-12-30 03:55:42');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (87,'Update',20,'partner','PartnerPriceType','Update','',NULL,0,'2018-12-30 03:55:56','2018-12-30 03:55:56');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (88,'Delete',20,'partner','PartnerPriceType','Delete','',NULL,0,'2018-12-30 03:56:06','2018-12-30 03:56:06');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (89,'Index',21,'partner','PartnerLimit','Index','',NULL,0,'2018-12-30 07:38:07','2018-12-30 07:38:07');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (90,'View',21,'partner','PartnerLimit','View','',NULL,0,'2018-12-30 07:38:20','2018-12-30 07:38:20');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (91,'Create',21,'partner','PartnerLimit','Create','',NULL,0,'2018-12-30 07:38:29','2018-12-30 07:38:29');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (92,'Update',21,'partner','PartnerLimit','Update','',NULL,0,'2018-12-30 07:38:38','2018-12-30 07:38:38');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (93,'Delete',21,'partner','PartnerLimit','Delete','',NULL,0,'2018-12-30 07:38:47','2018-12-30 07:38:47');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (94,'Index',22,'booking','Booking','Index','',NULL,0,'2018-12-31 02:01:45','2018-12-31 02:01:45');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (95,'Reservation',22,'booking','Booking','Reservation','',NULL,0,'2018-12-31 02:01:57','2018-12-31 02:01:57');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (96,'Create',22,'booking','Booking','Create','',NULL,0,'2018-12-31 02:02:09','2018-12-31 02:02:09');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (97,'View',22,'booking','Booking','View','',NULL,0,'2018-12-31 02:02:18','2018-12-31 02:02:18');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (98,'Update',22,'booking','Booking','Update','',NULL,0,'2018-12-31 02:02:27','2018-12-31 02:02:27');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (99,'Delete',22,'booking','Booking','Delete','',NULL,0,'2018-12-31 02:02:55','2018-12-31 02:02:55');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (100,'Index',23,'partner','PartnerPayment','Index','',NULL,0,'2018-12-31 02:03:24','2018-12-31 02:03:24');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (101,'Create',23,'partner','PartnerPayment','Create','',NULL,0,'2018-12-31 02:03:37','2018-12-31 02:03:37');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (102,'View',23,'partner','PartnerPayment','View','',NULL,0,'2018-12-31 02:03:50','2018-12-31 02:03:50');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (103,'Update',23,'partner','PartnerPayment','Update','',NULL,0,'2018-12-31 02:04:02','2018-12-31 02:04:02');
insert  into `permission_actions`(`id`,`title`,`permission_entity_id`,`module`,`controller`,`action`,`meta_code`,`parent`,`deleted`,`created`,`modified`) values (104,'Delete',23,'partner','PartnerPayment','Delete','',NULL,0,'2018-12-31 02:04:42','2018-12-31 02:04:42');

/*Table structure for table `permission_entities` */

DROP TABLE IF EXISTS `permission_entities`;

CREATE TABLE `permission_entities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_group_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_permission_group_id` (`permission_group_id`),
  CONSTRAINT `FK_permission_group_id` FOREIGN KEY (`permission_group_id`) REFERENCES `permission_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_entities` */

insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (1,'Permission Groups',1,0,'2016-04-10 21:34:37','2016-04-10 21:34:37');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (2,'Permission Entities',1,0,'2016-04-10 21:34:45','2016-04-10 21:34:45');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (3,'Permission Actions',1,0,'2016-04-10 21:34:51','2016-04-10 21:34:51');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (4,'Role Permission',1,0,'2016-04-10 21:34:59','2016-04-10 21:34:59');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (5,'Settings',1,0,'2016-04-10 21:35:04','2016-04-10 21:35:04');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (6,'Manage Admin',2,0,'2016-04-10 21:44:41','2016-04-10 21:44:41');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (7,'Manage CSR',2,0,'2016-04-10 21:44:49','2016-04-10 21:44:49');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (8,'Manage Clients',2,0,'2016-04-10 21:44:57','2016-04-10 21:44:57');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (9,'Manage All Users',2,0,'2016-04-10 21:45:12','2016-04-10 21:45:12');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (10,'Manage Roles',2,0,'2016-04-10 21:45:19','2016-04-10 21:45:19');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (11,'Facilities',4,0,'2018-12-23 06:33:57','2018-12-23 06:33:57');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (12,'Hotel',5,0,'2018-12-23 06:37:44','2018-12-23 06:37:44');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (13,'Market Type',6,0,'2018-12-23 06:41:02','2018-12-23 06:41:02');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (14,'Partner',7,0,'2018-12-23 06:43:03','2018-12-23 06:43:03');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (15,'Meal Type',8,0,'2018-12-24 16:25:59','2018-12-24 16:25:59');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (16,'Rooms type',9,0,'2018-12-24 16:27:26','2018-12-24 16:27:26');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (17,'View Type',10,0,'2018-12-24 16:28:36','2018-12-24 16:28:36');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (18,'Hotel Rooms',5,0,'2018-12-25 14:43:00','2018-12-25 14:43:00');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (19,'Hotel Room Rates',5,0,'2018-12-25 17:31:21','2018-12-25 17:31:21');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (20,'Partner Price Type',7,0,'2018-12-30 03:53:11','2018-12-30 03:53:11');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (21,'Partner Limit',7,0,'2018-12-30 07:37:51','2018-12-30 07:37:51');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (22,'Booking',3,0,'2018-12-31 02:01:29','2018-12-31 02:01:29');
insert  into `permission_entities`(`id`,`title`,`permission_group_id`,`deleted`,`created`,`modified`) values (23,'Payment',7,0,'2018-12-31 02:03:11','2018-12-31 02:03:11');

/*Table structure for table `permission_groups` */

DROP TABLE IF EXISTS `permission_groups`;

CREATE TABLE `permission_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `permission_groups` */

insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (1,'Permission & Settings',0,'2016-04-10 21:34:04','2016-04-10 21:34:04');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (2,'User Management',0,'2016-04-10 21:34:14','2016-04-10 21:34:14');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (3,'Booking',0,'2018-12-23 06:30:06','2018-12-23 06:30:06');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (4,'Facilities',0,'2018-12-23 06:30:16','2018-12-23 06:30:16');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (5,'Hotel',0,'2018-12-23 06:30:24','2018-12-23 06:30:24');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (6,'Market Type',0,'2018-12-23 06:33:25','2018-12-23 06:33:25');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (7,'Partner',0,'2018-12-23 06:33:36','2018-12-23 06:33:36');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (8,'Meal Type',0,'2018-12-24 16:25:23','2018-12-24 16:25:23');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (9,'Room Type',0,'2018-12-24 16:25:38','2018-12-24 16:25:38');
insert  into `permission_groups`(`id`,`title`,`deleted`,`created`,`modified`) values (10,'View Type',0,'2018-12-24 16:25:45','2018-12-24 16:25:45');

/*Table structure for table `role_permissions` */

DROP TABLE IF EXISTS `role_permissions`;

CREATE TABLE `role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_action_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rp_role_id` (`role_id`),
  KEY `FK_rp_action_id` (`permission_action_id`),
  CONSTRAINT `FK_rp_action_id` FOREIGN KEY (`permission_action_id`) REFERENCES `permission_actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_rp_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_permissions` */

insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (1,1,1,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (2,1,2,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (3,1,3,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (4,1,4,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (5,1,5,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (6,1,6,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (7,1,7,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (8,1,8,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (9,1,9,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (10,1,10,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (11,1,11,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (12,1,12,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (13,1,13,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (14,1,14,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (15,1,15,0,'2016-04-10 21:58:55','2019-01-03 00:17:16');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (16,1,16,0,'2016-04-10 21:58:55','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (17,1,17,0,'2016-04-10 21:58:55','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (18,1,18,0,'2016-04-10 21:58:55','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (19,1,19,0,'2016-04-10 21:58:55','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (20,1,20,0,'2016-04-10 21:58:55','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (21,1,21,0,'2016-04-10 21:58:55','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (22,1,22,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (23,1,23,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (24,1,24,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (25,1,25,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (26,1,26,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (27,1,27,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (28,1,28,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (29,1,29,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (30,1,30,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (31,1,31,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (32,1,32,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (33,1,33,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (34,1,34,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (35,1,35,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (36,1,36,0,'2016-04-10 21:58:56','2019-01-03 00:17:17');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (39,2,19,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (40,2,20,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (41,2,21,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (42,2,22,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (43,2,23,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (44,2,24,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (45,2,25,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (46,2,26,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (47,2,27,0,'2016-04-10 21:58:56','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (48,2,28,0,'2016-04-10 21:58:57','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (184,2,133,1,'2018-06-24 10:27:37','2018-06-24 10:27:37');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (185,2,37,0,'2018-12-23 06:51:54','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (186,2,38,0,'2018-12-23 06:51:54','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (187,2,39,0,'2018-12-23 06:51:54','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (188,2,40,0,'2018-12-23 06:51:54','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (189,2,41,0,'2018-12-23 06:51:54','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (190,2,42,0,'2018-12-23 06:51:54','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (191,2,43,0,'2018-12-23 06:51:54','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (192,2,44,0,'2018-12-23 06:51:55','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (193,2,45,0,'2018-12-23 06:51:55','2019-01-03 00:17:20');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (194,2,46,0,'2018-12-23 06:51:55','2019-01-03 00:17:20');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (195,2,47,0,'2018-12-23 06:51:55','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (196,2,48,0,'2018-12-23 06:51:55','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (197,2,49,0,'2018-12-23 06:51:55','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (198,2,50,0,'2018-12-23 06:51:55','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (199,2,51,0,'2018-12-23 06:51:55','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (200,2,52,0,'2018-12-23 06:51:56','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (201,2,53,0,'2018-12-23 06:51:56','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (202,2,54,0,'2018-12-23 06:51:56','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (203,2,55,0,'2018-12-23 06:51:56','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (204,2,56,0,'2018-12-23 06:51:56','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (205,2,57,0,'2018-12-24 16:30:13','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (206,2,58,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (207,2,59,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (208,2,60,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (209,2,61,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (210,2,62,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (211,2,63,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (212,2,64,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (213,2,65,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (214,2,66,0,'2018-12-24 16:30:14','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (215,2,67,0,'2018-12-24 16:30:14','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (216,2,68,0,'2018-12-24 16:30:15','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (217,2,69,0,'2018-12-24 16:30:15','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (218,2,70,0,'2018-12-24 16:30:15','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (219,2,71,0,'2018-12-24 16:30:15','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (220,2,72,0,'2018-12-25 14:44:18','2019-01-03 00:17:20');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (221,2,73,0,'2018-12-25 14:44:18','2019-01-03 00:17:20');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (222,2,74,0,'2018-12-25 14:44:18','2019-01-03 00:17:20');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (223,2,75,0,'2018-12-25 14:44:18','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (224,2,76,0,'2018-12-25 14:44:18','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (225,2,77,0,'2018-12-25 18:38:51','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (226,2,78,0,'2018-12-25 18:38:51','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (227,2,79,0,'2018-12-25 18:38:51','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (228,2,80,0,'2018-12-25 18:38:51','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (229,2,81,0,'2018-12-25 18:38:51','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (230,2,82,0,'2018-12-27 03:37:27','2019-01-03 00:17:20');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (231,2,83,0,'2018-12-27 04:28:17','2019-01-03 00:17:20');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (232,4,42,0,'2018-12-29 07:53:57','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (233,4,43,1,'2018-12-29 07:53:57','2019-01-03 00:16:02');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (234,4,44,1,'2018-12-29 07:53:58','2018-12-31 02:05:32');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (235,4,45,0,'2018-12-29 07:53:58','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (236,4,46,1,'2018-12-29 07:53:58','2019-01-02 23:51:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (237,4,82,0,'2018-12-29 07:53:58','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (238,4,83,0,'2018-12-29 07:53:58','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (239,3,77,1,'2018-12-29 07:53:58','2018-12-31 02:05:30');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (240,3,78,1,'2018-12-29 07:53:58','2018-12-31 02:05:30');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (241,3,79,1,'2018-12-29 07:53:58','2018-12-31 02:05:30');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (242,3,80,1,'2018-12-29 07:53:58','2018-12-31 02:05:30');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (243,3,81,1,'2018-12-29 07:53:58','2018-12-31 02:05:30');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (244,3,52,0,'2018-12-29 07:54:19','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (245,3,53,0,'2018-12-29 07:54:19','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (246,3,54,0,'2018-12-29 07:54:19','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (247,3,55,0,'2018-12-29 07:54:19','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (248,3,56,0,'2018-12-29 07:54:20','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (249,2,84,0,'2018-12-30 03:56:33','2019-01-03 00:17:21');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (250,2,85,0,'2018-12-30 03:56:33','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (251,2,86,0,'2018-12-30 03:56:33','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (252,2,87,0,'2018-12-30 03:56:33','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (253,2,88,0,'2018-12-30 03:56:33','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (254,3,84,0,'2018-12-30 03:56:35','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (255,3,85,0,'2018-12-30 03:56:35','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (256,3,86,0,'2018-12-30 03:56:35','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (257,3,87,0,'2018-12-30 03:56:35','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (258,3,88,0,'2018-12-30 03:56:35','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (259,2,89,0,'2018-12-30 07:39:21','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (260,2,90,0,'2018-12-30 07:39:21','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (261,2,91,0,'2018-12-30 07:39:21','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (262,2,92,0,'2018-12-30 07:39:21','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (263,2,93,0,'2018-12-30 07:39:21','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (264,3,89,0,'2018-12-30 07:39:24','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (265,3,90,0,'2018-12-30 07:39:24','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (266,3,91,0,'2018-12-30 07:39:24','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (267,3,92,0,'2018-12-30 07:39:24','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (268,3,93,0,'2018-12-30 07:39:24','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (269,2,94,0,'2018-12-31 02:05:24','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (270,2,95,0,'2018-12-31 02:05:24','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (271,2,96,0,'2018-12-31 02:05:24','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (272,2,97,0,'2018-12-31 02:05:25','2019-01-03 00:17:18');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (273,2,98,0,'2018-12-31 02:05:25','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (274,2,99,0,'2018-12-31 02:05:25','2019-01-03 00:17:19');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (275,2,100,0,'2018-12-31 02:05:28','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (276,2,101,0,'2018-12-31 02:05:28','2019-01-03 00:17:22');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (277,2,102,0,'2018-12-31 02:05:28','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (278,2,103,0,'2018-12-31 02:05:28','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (279,2,104,0,'2018-12-31 02:05:28','2019-01-03 00:17:23');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (280,3,94,0,'2018-12-31 02:05:30','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (281,3,95,0,'2018-12-31 02:05:30','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (282,3,96,0,'2018-12-31 02:05:30','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (283,3,97,0,'2018-12-31 02:05:30','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (284,3,98,0,'2018-12-31 02:05:30','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (285,3,99,0,'2018-12-31 02:05:30','2019-01-03 00:17:24');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (286,3,100,0,'2018-12-31 02:05:31','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (287,3,101,0,'2018-12-31 02:05:31','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (288,3,102,0,'2018-12-31 02:05:31','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (289,3,103,0,'2018-12-31 02:05:31','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (290,3,104,0,'2018-12-31 02:05:32','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (291,4,72,0,'2019-01-02 23:51:24','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (292,4,73,0,'2019-01-02 23:51:25','2019-01-03 00:17:25');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (293,4,74,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (294,4,75,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (295,4,76,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (296,4,77,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (297,4,78,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (298,4,79,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (299,4,80,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (300,4,81,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (301,6,77,0,'2019-01-02 23:51:25','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (302,6,78,0,'2019-01-02 23:51:26','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (303,6,79,0,'2019-01-02 23:51:26','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (304,6,80,0,'2019-01-02 23:51:26','2019-01-03 00:17:26');
insert  into `role_permissions`(`id`,`role_id`,`permission_action_id`,`deleted`,`created`,`modified`) values (305,6,81,0,'2019-01-02 23:51:26','2019-01-03 00:17:26');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`title`,`deleted`,`created`,`modified`) values (1,'Super Admin',0,'2018-12-19 15:05:00','2018-12-19 15:05:00');
insert  into `roles`(`id`,`title`,`deleted`,`created`,`modified`) values (2,'Admin',0,'2018-12-19 15:05:00','2018-12-19 15:05:00');
insert  into `roles`(`id`,`title`,`deleted`,`created`,`modified`) values (3,'Partners',0,'2018-12-19 15:05:00','2018-12-23 06:46:02');
insert  into `roles`(`id`,`title`,`deleted`,`created`,`modified`) values (4,'Hotel',0,'2016-04-09 15:05:00','2016-04-09 15:05:00');
insert  into `roles`(`id`,`title`,`deleted`,`created`,`modified`) values (5,'Operations',0,'2018-12-19 15:05:00','2018-12-19 15:05:00');
insert  into `roles`(`id`,`title`,`deleted`,`created`,`modified`) values (6,'Reservation  Company',0,'2018-12-23 06:47:29','2018-12-23 06:47:29');

/*Table structure for table `room_facilities` */

DROP TABLE IF EXISTS `room_facilities`;

CREATE TABLE `room_facilities` (
  `room_id` int(11) NOT NULL,
  `facilitie_id` int(11) NOT NULL,
  KEY `room_room` (`room_id`),
  KEY `room_facilities` (`facilitie_id`),
  CONSTRAINT `room_facilities` FOREIGN KEY (`facilitie_id`) REFERENCES `facilities` (`id`),
  CONSTRAINT `room_room` FOREIGN KEY (`room_id`) REFERENCES `hotel_rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `room_facilities` */

/*Table structure for table `room_pictures` */

DROP TABLE IF EXISTS `room_pictures`;

CREATE TABLE `room_pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_picture` (`room_id`),
  CONSTRAINT `room_picture` FOREIGN KEY (`room_id`) REFERENCES `hotel_rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `room_pictures` */

/*Table structure for table `room_type` */

DROP TABLE IF EXISTS `room_type`;

CREATE TABLE `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `room_type` */

insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (1,'Single',0,'2018-12-24 17:32:09','2018-12-24 17:32:09');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (2,'Double',0,'2018-12-24 17:32:17','2018-12-24 17:32:17');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (3,'Triple',0,'2018-12-24 17:32:22','2018-12-24 17:32:22');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (4,'Quad',0,'2018-12-24 17:34:15','2018-12-24 17:34:15');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (5,'Quint',0,'2018-12-24 17:34:22','2018-12-24 17:34:22');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (6,'Hexa',0,'2018-12-24 17:34:29','2018-12-24 17:34:29');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (7,'One-Room Suite',0,'2018-12-24 17:34:35','2018-12-24 17:34:35');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (8,'Two-Rooms Suite',0,'2018-12-24 17:34:41','2018-12-24 17:34:41');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (9,'Three Rooms Suite',0,'2018-12-24 17:34:47','2018-12-24 17:34:47');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (10,'Presidential Suite',0,'2018-12-24 17:34:52','2018-12-24 17:34:52');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (11,'Royal Suite',0,'2018-12-24 17:34:57','2018-12-24 17:34:57');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (12,'Junior Suite',0,'2018-12-24 17:35:01','2018-12-24 17:35:01');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (13,'Senior Suite',0,'2018-12-24 17:35:09','2018-12-24 17:35:09');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (14,'Executive Suite',0,'2018-12-24 17:35:14','2018-12-24 17:35:14');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (15,'Small Studio Double',0,'2018-12-24 17:35:24','2018-12-24 17:35:24');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (16,'Small Studio Triple',0,'2018-12-24 17:35:30','2018-12-24 17:35:30');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (17,'Large Studio Triple',0,'2018-12-24 17:50:13','2018-12-24 17:50:13');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (18,'Large Studio Quad',0,'2018-12-24 17:50:25','2018-12-24 17:50:25');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (19,'2-Rooms Apartment',0,'2018-12-24 17:50:31','2018-12-24 17:50:31');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (20,'3-Rooms Apartment',0,'2018-12-24 17:50:36','2018-12-24 17:50:36');
insert  into `room_type`(`id`,`title`,`deleted`,`created`,`modified`) values (21,'4-Rooms Apartment',0,'2018-12-24 17:50:43','2018-12-24 17:50:43');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_limit` float(10,2) NOT NULL,
  `checking_limit` float(10,2) NOT NULL,
  `vat` float(10,2) DEFAULT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`booking_limit`,`checking_limit`,`vat`,`meta_key`,`meta_value`,`created`,`modified`) values (1,0.00,2.00,0.00,'admin_role','2','2016-04-09 17:43:51','2016-04-09 17:43:51');
insert  into `settings`(`id`,`booking_limit`,`checking_limit`,`vat`,`meta_key`,`meta_value`,`created`,`modified`) values (2,0.00,3.00,0.00,'client_role','3','2016-04-09 17:43:51','2016-04-09 17:43:51');
insert  into `settings`(`id`,`booking_limit`,`checking_limit`,`vat`,`meta_key`,`meta_value`,`created`,`modified`) values (3,0.00,4.00,0.00,'csr_role','4','2016-04-09 17:43:51','2016-04-09 17:43:51');
insert  into `settings`(`id`,`booking_limit`,`checking_limit`,`vat`,`meta_key`,`meta_value`,`created`,`modified`) values (4,0.00,0.00,0.00,'holiday_emails','asd','2016-06-03 01:36:33','2017-12-05 13:38:34');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_role` (`role_id`),
  CONSTRAINT `FK_user_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`first_name`,`last_name`,`email`,`username`,`password`,`role_id`,`child_id`,`photo`,`status`,`deleted`,`created`,`modified`) values (1,'Super','Admin','azizrattani@hotmail.com','superadmin','0e7517141fb53f21ee439b355b5a1d0a',1,NULL,NULL,1,0,'2016-04-09 15:05:01','2016-06-03 07:57:55');
insert  into `users`(`id`,`first_name`,`last_name`,`email`,`username`,`password`,`role_id`,`child_id`,`photo`,`status`,`deleted`,`created`,`modified`) values (2,'Aziz','Rattani','aziz@ideabox.com.pk','admin','0192023a7bbd73250516f069df18b500',2,1,NULL,1,0,'2016-06-03 01:36:44','2016-10-19 03:39:32');
insert  into `users`(`id`,`first_name`,`last_name`,`email`,`username`,`password`,`role_id`,`child_id`,`photo`,`status`,`deleted`,`created`,`modified`) values (3,'Dar Al-Eiman Royal','','dar-aleiman-royal@yahoo.com','dar_al-eiman','e10adc3949ba59abbe56e057f20f883e',4,1,NULL,1,0,'2019-01-02 19:44:21','2019-01-02 19:44:21');
insert  into `users`(`id`,`first_name`,`last_name`,`email`,`username`,`password`,`role_id`,`child_id`,`photo`,`status`,`deleted`,`created`,`modified`) values (4,'Hotel 2','','azizrattani@hotmail.com','hotel 2','e10adc3949ba59abbe56e057f20f883e',4,2,NULL,1,0,'2019-01-03 00:15:18','2019-01-03 00:15:18');
insert  into `users`(`id`,`first_name`,`last_name`,`email`,`username`,`password`,`role_id`,`child_id`,`photo`,`status`,`deleted`,`created`,`modified`) values (5,'Aziz Rattani','','azizrattani@hotmail.com','admin','0192023a7bbd73250516f069df18b500',4,3,NULL,1,0,'2019-01-03 01:50:11','2019-01-03 01:50:11');

/*Table structure for table `view_type` */

DROP TABLE IF EXISTS `view_type`;

CREATE TABLE `view_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `create_by` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `view_type` */

insert  into `view_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (1,'No View',NULL,'2018-12-24 17:53:21','2018-12-24 17:53:21','0');
insert  into `view_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (2,'City View',NULL,'2018-12-24 17:53:28','2018-12-24 17:53:28','0');
insert  into `view_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (3,'Haram View',NULL,'2018-12-24 17:53:34','2018-12-24 17:53:34','0');
insert  into `view_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (4,'Kaabah View',NULL,'2018-12-24 17:53:39','2018-12-24 17:53:39','0');
insert  into `view_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (5,'Partial Haram View',NULL,'2018-12-24 17:53:44','2018-12-24 17:53:44','0');
insert  into `view_type`(`id`,`title`,`create_by`,`created`,`modified`,`deleted`) values (6,'Partial Kaabah View',NULL,'2018-12-24 17:53:55','2018-12-24 17:53:55','0');

/* Trigger structure for table `admin_users` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `admin_users_insert` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `admin_users_insert` AFTER INSERT ON `admin_users` FOR EACH ROW INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
SELECT `first_name` AS `first_name`, `last_name` AS `last_name`,
`user_name` AS `username`, `password` AS `password`, `email_add` AS `email`,
`is_active` AS `status`, `id` AS `child_id`, '2' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
FROM `admin_users` WHERE `id` = NEW.`id` */$$


DELIMITER ;

/* Trigger structure for table `admin_users` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `admin_users_update` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `admin_users_update` AFTER UPDATE ON `admin_users` FOR EACH ROW BEGIN
        IF NEW.`id` NOT IN (
			SELECT u.`child_id`
			FROM `users` u
			WHERE (NEW.`id` = u.`child_id` AND u.`role_id` = 2)
		)
		THEN
			INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
			SELECT `first_name` AS `first_name`, `last_name` AS `last_name`,
			`user_name` AS `username`, `password` AS `password`, `email_add` AS `email`,
			`is_active` AS `status`, `id` AS `child_id`, '2' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
			FROM `admin_users` WHERE `id` = NEW.`id`;
		ELSE
			UPDATE `users` SET
			`first_name` = New.`first_name`,
			`last_name` = New.`last_name`,
			`username` = New.`user_name`,
			`password` = New.`password`,
			`email` = New.`email_add`,
			`status` = New.`is_active` 
			WHERE `child_id` = New.`id` AND `role_id` = '2';
        END IF;
    END */$$


DELIMITER ;

/* Trigger structure for table `admin_users` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `admin_users_delete` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `admin_users_delete` BEFORE DELETE ON `admin_users` FOR EACH ROW UPDATE `users` SET `deleted` = 1, `status` = 0
WHERE `child_id` = Old.`id` AND `role_id` = 2 */$$


DELIMITER ;

/* Trigger structure for table `hotel` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `hotel_insert` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `hotel_insert` AFTER INSERT ON `hotel` FOR EACH ROW BEGIN
	
		INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
SELECT `name` AS `first_name`, '' AS `last_name`,
`username` AS `username`, MD5(`password`) AS `password`, `email` AS `email`,
`status` AS `status`, `id` AS `child_id`, '4' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
FROM `hotel` WHERE `id` = NEW.`id`;
                
	
    END */$$


DELIMITER ;

/* Trigger structure for table `hotel` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `hotel_update` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `hotel_update` AFTER UPDATE ON `hotel` FOR EACH ROW BEGIN
        IF NEW.`id` NOT IN (
			SELECT u.`child_id`
			FROM `users` u
			WHERE (NEW.`id` = u.`child_id` AND u.`role_id` = 4)
		)
		THEN
			INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
			SELECT  `name` AS `first_name`, '' AS `last_name`,
			`username` AS `username`, MD5(`password`) AS `password`, `email` AS `email`,
			`status` AS `status`, `id` AS `child_id`, '4' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
			FROM `hotel` WHERE `id` = NEW.`id`;
		ELSE
			UPDATE `users` SET
			`first_name` = New.`name`,
			`last_name` = '',
			`email` = New.`email`,
			`username` = New.`username`,
			`password` = MD5(New.`password`),
			`status` = New.`status` 
			WHERE `child_id` = New.`id` AND `role_id` = '4';
        END IF;
    END */$$


DELIMITER ;

/* Trigger structure for table `hotel` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `hotel_delete` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `hotel_delete` BEFORE DELETE ON `hotel` FOR EACH ROW UPDATE `users` SET `deleted` = 1, `status` = 0
WHERE `child_id` = Old.`id` AND `role_id` = 4 */$$


DELIMITER ;

/* Trigger structure for table `partners` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `staff_insert` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `staff_insert` AFTER INSERT ON `partners` FOR EACH ROW BEGIN
	
		INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
SELECT `name` AS `first_name`, '' AS `last_name`,
`username` AS `username`, MD5(`password`) AS `password`, `email` AS `email`,
`status` AS `status`, `id` AS `child_id`, '3' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
FROM `partners` WHERE `id` = NEW.`id`;
                
    END */$$


DELIMITER ;

/* Trigger structure for table `partners` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `staff_update` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `staff_update` AFTER UPDATE ON `partners` FOR EACH ROW BEGIN
        IF NEW.`id` NOT IN (
			SELECT u.`child_id`
			FROM `users` u
			WHERE (NEW.`id` = u.`child_id` AND u.`role_id` = 3)
		)
		THEN
			INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
			SELECT  `name` AS `first_name`, '' AS `last_name`,
			`username` AS `username`, MD5(`password`) AS `password`, `email` AS `email`,
			`status` AS `status`, `id` AS `child_id`, '3' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
			FROM `staff` WHERE `id` = NEW.`id`;
		ELSE
			UPDATE `users` SET
			`first_name` = New.`name`,
			`last_name` = '',
			`email` = New.`email`,
			`username` = New.`username`,
			`password` = MD5(New.`password`),
			`status` = New.`status` 
			WHERE `child_id` = New.`id` AND `role_id` = '3';
        END IF;
    END */$$


DELIMITER ;

/* Trigger structure for table `partners` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `staff_delete` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `staff_delete` BEFORE DELETE ON `partners` FOR EACH ROW UPDATE `users` SET `deleted` = 1, `status` = 0
WHERE `child_id` = Old.`id` AND `role_id` = 3 */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
