-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 03, 2019 at 12:45 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 5.6.30

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ab7247_clickurhotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `email_add` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `added_on` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `user_name`, `email_add`, `password`, `first_name`, `last_name`, `is_admin`, `is_active`, `added_on`) VALUES
(1, 'admin', 'aziz@ideabox.com.pk', '0192023a7bbd73250516f069df18b500', 'Aziz', 'Rattani', 1, 1, '2014-10-08 13:14:26'),
(17, 'aziz', 'azizrattani@hotmail.com2', 'b85dc795ba17cb243ab156f8c52124e1', 'Aziz', 'Rattani', 1, 1, '2018-06-06 16:09:12'),
(18, 'admin2', 'admin2@aa.com', 'e00cf25ad42683b3df678c61f42c6bda', 'admin2', 'admin2', 1, 1, '2018-07-21 07:12:40');

--
-- Triggers `admin_users`
--
DROP TRIGGER IF EXISTS `admin_users_delete`;
DELIMITER $$
CREATE TRIGGER `admin_users_delete` BEFORE DELETE ON `admin_users` FOR EACH ROW UPDATE `users` SET `deleted` = 1, `status` = 0
WHERE `child_id` = Old.`id` AND `role_id` = 2
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `admin_users_insert`;
DELIMITER $$
CREATE TRIGGER `admin_users_insert` AFTER INSERT ON `admin_users` FOR EACH ROW INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
SELECT `first_name` AS `first_name`, `last_name` AS `last_name`,
`user_name` AS `username`, `password` AS `password`, `email_add` AS `email`,
`is_active` AS `status`, `id` AS `child_id`, '2' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
FROM `admin_users` WHERE `id` = NEW.`id`
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `admin_users_update`;
DELIMITER $$
CREATE TRIGGER `admin_users_update` AFTER UPDATE ON `admin_users` FOR EACH ROW BEGIN
        IF NEW.`id` NOT IN (
			SELECT u.`child_id`
			FROM `users` u
			WHERE (NEW.`id` = u.`child_id` AND u.`role_id` = 2)
		)
		THEN
			INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
			SELECT `first_name` AS `first_name`, `last_name` AS `last_name`,
			`user_name` AS `username`, `password` AS `password`, `email_add` AS `email`,
			`is_active` AS `status`, `id` AS `child_id`, '2' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
			FROM `admin_users` WHERE `id` = NEW.`id`;
		ELSE
			UPDATE `users` SET
			`first_name` = New.`first_name`,
			`last_name` = New.`last_name`,
			`username` = New.`user_name`,
			`password` = New.`password`,
			`email` = New.`email_add`,
			`status` = New.`is_active` 
			WHERE `child_id` = New.`id` AND `role_id` = '2';
        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `checkin` date DEFAULT NULL,
  `checkout` date DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `noofnight` int(11) DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `btype` enum('book','request') DEFAULT 'book',
  `full_guest_name` varchar(255) NOT NULL,
  `smooking` enum('yes','no') DEFAULT 'no',
  `special_request` text,
  `nationality` varchar(255) NOT NULL,
  `meal_plan` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `adult` varchar(255) DEFAULT NULL,
  `child` varchar(255) DEFAULT NULL,
  `exp_arrive` varchar(255) DEFAULT NULL,
  `extra_charge` float(10,2) DEFAULT '0.00',
  `discount` float(10,2) DEFAULT '0.00',
  `book_request` varchar(5) NOT NULL,
  `request_email` enum('yes','no') DEFAULT 'no',
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `deleted` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `staff_id`, `checkin`, `checkout`, `amount`, `noofnight`, `hotel_id`, `btype`, `full_guest_name`, `smooking`, `special_request`, `nationality`, `meal_plan`, `email`, `phone`, `adult`, `child`, `exp_arrive`, `extra_charge`, `discount`, `book_request`, `request_email`, `created`, `modified`, `deleted`) VALUES
(1, 3, '2019-01-04', '2019-01-10', 230.40, 6, 1, 'book', 'Muhammad Rehan', 'no', NULL, 'PAKISTAN', NULL, 'engineer.rehan@gmail.com', '03322441889', NULL, NULL, NULL, 0.00, 0.00, 'yes', 'no', '2019-01-02', '2019-01-02', '0'),
(2, 1, '2019-01-10', '2019-01-15', 780.00, 5, 2, 'request', 'Test Irfan', 'no', NULL, 'PAKISTAN', NULL, 'erphanqadiri@gmail.com', '0532804970', NULL, NULL, NULL, 0.00, 0.00, 'yes', 'no', '2019-01-02', '2019-01-02', '0');

-- --------------------------------------------------------

--
-- Table structure for table `booking_details`
--

DROP TABLE IF EXISTS `booking_details`;
CREATE TABLE `booking_details` (
  `id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `passport_no` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `booking_rooms`
--

DROP TABLE IF EXISTS `booking_rooms`;
CREATE TABLE `booking_rooms` (
  `id` int(11) NOT NULL,
  `room_rate_id` int(11) DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `booking_date` date DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `no_of_room` int(11) DEFAULT NULL,
  `room_rate` float(10,2) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  `deleted` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_rooms`
--

INSERT INTO `booking_rooms` (`id`, `room_rate_id`, `booking_id`, `booking_date`, `room_id`, `no_of_room`, `room_rate`, `created`, `modified`, `deleted`) VALUES
(1, 1, 1, '2019-01-04', 1, 4, 4.50, '2019-01-02', '2019-01-02', '0'),
(2, 2, 1, '2019-01-05', 1, 4, 4.50, '2019-01-02', '2019-01-02', '0'),
(3, 3, 1, '2019-01-06', 1, 4, 4.50, '2019-01-02', '2019-01-02', '0'),
(4, 4, 1, '2019-01-07', 1, 4, 4.50, '2019-01-02', '2019-01-02', '0'),
(5, 5, 1, '2019-01-08', 1, 4, 4.50, '2019-01-02', '2019-01-02', '0'),
(6, 6, 1, '2019-01-09', 1, 4, 4.50, '2019-01-02', '2019-01-02', '0'),
(7, 7, 1, '2019-01-10', 1, 4, 4.50, '2019-01-02', '2019-01-02', '0'),
(8, 88, 1, '2019-01-04', 2, 3, 4.80, '2019-01-02', '2019-01-02', '0'),
(9, 89, 1, '2019-01-05', 2, 3, 4.80, '2019-01-02', '2019-01-02', '0'),
(10, 90, 1, '2019-01-06', 2, 3, 4.80, '2019-01-02', '2019-01-02', '0'),
(11, 91, 1, '2019-01-07', 2, 3, 4.80, '2019-01-02', '2019-01-02', '0'),
(12, 92, 1, '2019-01-08', 2, 3, 4.80, '2019-01-02', '2019-01-02', '0'),
(13, 93, 1, '2019-01-09', 2, 3, 4.80, '2019-01-02', '2019-01-02', '0'),
(14, 94, 1, '2019-01-10', 2, 3, 4.80, '2019-01-02', '2019-01-02', '0'),
(15, 179, 2, '2019-01-10', 3, 1, 130.00, '2019-01-02', '2019-01-02', '0'),
(16, 180, 2, '2019-01-11', 3, 1, 130.00, '2019-01-02', '2019-01-02', '0'),
(17, 181, 2, '2019-01-12', 3, 1, 130.00, '2019-01-02', '2019-01-02', '0'),
(18, 182, 2, '2019-01-13', 3, 1, 130.00, '2019-01-02', '2019-01-02', '0'),
(19, 183, 2, '2019-01-14', 3, 1, 130.00, '2019-01-02', '2019-01-02', '0'),
(20, 184, 2, '2019-01-15', 3, 1, 130.00, '2019-01-02', '2019-01-02', '0');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `drive_type` tinytext,
  `sort_order` int(11) DEFAULT NULL,
  `inspection` float(10,2) DEFAULT NULL,
  `inspection2` float(10,2) DEFAULT '0.00',
  `inspection3` float(10,2) DEFAULT '0.00',
  `inspection4` float(10,2) DEFAULT '0.00',
  `inspection5` float(10,2) DEFAULT '0.00',
  `vanning` float(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country`, `drive_type`, `sort_order`, `inspection`, `inspection2`, `inspection3`, `inspection4`, `inspection5`, `vanning`) VALUES
(1, 'AFGHANISTAN', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(2, 'ANGOLA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(3, 'ANGUILLA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(4, 'ANTIGUA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(5, 'ARUBA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(6, 'AUSTRALIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(7, 'BAHAMAS', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(8, 'BANGKOK', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(9, 'BANGLADESH', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(10, 'BARBADOS', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(11, 'BELGIUM', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(12, 'BELIZE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(13, 'BERMUDA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(14, 'BOLIVIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(15, 'BOTSWANA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(16, 'BRAZIL', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(17, 'BURUNDI', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(18, 'CAMEROON', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(19, 'CANADA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(20, 'CAYMAN  ISLANDS', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(21, 'CHILE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(22, 'CHINA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(23, 'CHRISTMAS ISLAND', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(24, 'DOMINICA, COMMONWEALTH OF', 'LHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(25, 'CONGO', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(26, 'COTE DIVOIRE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(27, 'CUBA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(28, 'CYPRUS', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(29, 'DEMO. REP OF CONGO', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(30, 'DOMINICAN REPUBLIC', 'LHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(31, 'DOUALA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(32, 'EAST TIMOR', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(33, 'ECUADOR', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(34, 'EGYPT', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(35, 'ETHIOPIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(36, 'FIJI', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(37, 'FREE PORT', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(38, 'FREETOWN', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(39, 'GABON', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(40, 'GAMBIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(41, 'GEORGIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(42, 'GERMANY', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(43, 'GHANA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(44, 'GIBRALTAR', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(45, 'GREECE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(46, 'GRENADA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(47, 'GUAM', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(48, 'GUINEA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(49, 'GUYANA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(50, 'HAITI', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(51, 'HONDURAS', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(52, 'HONG KONG', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(53, 'ICELAND', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(55, 'INDONESIA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(56, 'IRELAND', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(59, 'JAMAICA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(60, 'JAPAN', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(61, 'KENYA', 'RHD', NULL, 250.00, 400.00, 500.00, 0.00, 0.00, 0.00),
(62, 'KIRIBATI', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(63, 'KUWAIT', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(64, 'LESOTHO', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(65, 'LIBERIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(66, 'LIBYAN ARAB JAMAHIRIYA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(67, 'MADAGASCAR', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(68, 'MALAWI', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(69, 'MALAYSIA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(70, 'MALDIVES', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(71, 'MALI', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(72, 'MALTA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(73, 'MANGOLIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(74, 'MARITIUS', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(75, 'MARSHALL ISLANDS', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(76, 'MAURITANIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(77, 'MICRONESIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(78, 'MONTSERRAT', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(79, 'MOZAMBIQUE', 'RHD', NULL, 100.00, 100.00, 100.00, 100.00, 0.00, 0.00),
(80, 'NAMIBIA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(81, 'NAURU', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(82, 'NETHERLANDS', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(83, 'NEW CALEDONIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(84, 'NEW ZEALAND', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(85, 'NIGER', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(86, 'NIGERIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(87, 'NORFOLK ISLAND', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(88, 'NORTHLAND ANTILLES', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(89, 'NORWAY', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(90, 'NOUE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(91, 'NUKUALOFA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(92, 'OMAN', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(93, 'PAKISTAN', 'RHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 350.00),
(94, 'PALAU', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(95, 'PANAMA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(96, 'PAPUA NEW GUINEA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(97, 'PERU', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(98, 'PHILIPPINES', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(99, 'PHILIPSBURG', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(100, 'POLAND', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(101, 'PORTUGAL', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(102, 'QATAR', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(103, 'RUSSIAN FEDERATION', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(104, 'RWANDA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(105, 'SAMOA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(106, 'SAO TOME AND PRINCIPE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(107, 'SAUDI ARABIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(108, 'SENEGAL', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(109, 'SIERRA LEONE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(110, 'SINGAPORE', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(111, 'SOLOMON ISLANDS', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(112, 'SOUTH AFRICA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(113, 'SPAIN', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(114, 'SRI LANKA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(115, 'ST KITTS', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(116, 'ST LUCIA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(117, 'ST VINCENT', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(118, 'SUDAN', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(119, 'SURINAME', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(120, 'SWAZILAND', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(121, 'TANZANIA', 'RHD', NULL, 250.00, 400.00, 500.00, 0.00, 0.00, 0.00),
(122, 'THAILAND', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(123, 'TOGO', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(124, 'TONGA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(125, 'TRINIDAD TOBAGO', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(126, 'TUNISIA', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(127, 'TURKEY', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(128, 'TUVALU', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(129, 'UAE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(130, 'UGANDA', 'RHD', NULL, 250.00, 400.00, 500.00, 0.00, 0.00, 0.00),
(131, 'UNITED KINGDOM', 'RHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(132, 'VANUATU', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(133, 'VIETNAM', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(134, 'VIRGIN ISLANDS US', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(135, 'ZAIRE', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(136, 'ZAMBIA', 'RHD', NULL, 250.00, 400.00, 500.00, 0.00, 0.00, 0.00),
(137, 'ZIMBABWE', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(138, 'BENIN', 'LHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(139, 'INDIA', 'RHD', NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00),
(140, 'CHAD', 'LHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(141, 'UNITED STATES OF AMERICA', 'LHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(142, 'S.KOREA', 'LHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(143, 'KOREA', 'LHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(144, 'TAIWAN', 'LHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(145, 'ST MAARTEN', 'LHD', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00),
(146, '', '', NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

DROP TABLE IF EXISTS `facilities`;
CREATE TABLE `facilities` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `title`, `description`, `deleted`, `created`, `modified`) VALUES
(1, 'Breakfast and dinner', '', 0, '2018-12-24 19:38:09', '2018-12-24 19:38:09'),
(2, 'Guest Houses ', '', 0, '2018-12-24 19:38:23', '2018-12-24 19:38:23'),
(3, 'Serviced Apartments ', '', 0, '2018-12-24 19:38:41', '2018-12-24 19:38:41'),
(4, 'Boutique', '', 0, '2018-12-24 19:38:59', '2018-12-24 19:38:59');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `short_desc` text NOT NULL,
  `description` text,
  `hotel_star` int(11) NOT NULL,
  `langitude` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` int(5) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `phone1` varchar(100) NOT NULL,
  `phone2` varchar(100) DEFAULT NULL,
  `phone3` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `hotel_policies` text,
  `pet` char(1) NOT NULL DEFAULT '0',
  `liquor` char(1) NOT NULL DEFAULT '0',
  `smoking` char(1) NOT NULL DEFAULT '0',
  `child_age_from` int(3) DEFAULT NULL,
  `child_age_to` int(3) DEFAULT NULL,
  `checkin_time` time NOT NULL,
  `checkout_time` time NOT NULL,
  `total_room` int(11) NOT NULL,
  `total_floor` int(11) NOT NULL,
  `build_year` int(4) NOT NULL,
  `mealOption` int(11) DEFAULT NULL,
  `rate_type` int(11) DEFAULT NULL,
  `breakfast_rate` float(10,2) DEFAULT '0.00',
  `lunch_rate` float(10,2) DEFAULT '0.00',
  `dinner_rate` float(10,2) DEFAULT '0.00',
  `one_night_start` int(11) DEFAULT NULL,
  `one_night_end` int(11) DEFAULT NULL,
  `cancel_50_start` int(11) DEFAULT NULL,
  `cancel_50_end` int(11) DEFAULT NULL,
  `cancel_100_start` int(11) DEFAULT NULL,
  `cancel_100_end` int(11) DEFAULT NULL,
  `free_cancel_start` int(11) DEFAULT NULL,
  `free_cancel_end` int(11) DEFAULT NULL,
  `market_type` varchar(255) NOT NULL,
  `meal_option` varchar(255) DEFAULT NULL,
  `meal_type` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `username`, `password`, `short_desc`, `description`, `hotel_star`, `langitude`, `latitude`, `address`, `city`, `state`, `country`, `zipcode`, `phone1`, `phone2`, `phone3`, `email`, `fax`, `website`, `hotel_policies`, `pet`, `liquor`, `smoking`, `child_age_from`, `child_age_to`, `checkin_time`, `checkout_time`, `total_room`, `total_floor`, `build_year`, `mealOption`, `rate_type`, `breakfast_rate`, `lunch_rate`, `dinner_rate`, `one_night_start`, `one_night_end`, `cancel_50_start`, `cancel_50_end`, `cancel_100_start`, `cancel_100_end`, `free_cancel_start`, `free_cancel_end`, `market_type`, `meal_option`, `meal_type`, `status`, `active`, `deleted`, `created`, `modified`) VALUES
(1, 'Dallah Taibah', 'dallahtaibah', '123456', 'Dallah Taibah', NULL, 4, '23.52', '54.15', 'Makkah Mukarramah', 'Makka', NULL, NULL, '', '+96612345678', '', '', 'info@dallahtaibah', NULL, 'dallahtaibah', NULL, '0', '0', '0', 0, 10, '15:00:00', '12:00:00', 100, 19, 2012, NULL, 2, 0.00, 0.00, 0.00, 1, 1, 1, 1, 1, 1, 1, 1, '2', '2', '2', '1', 1, 0, '2019-01-02 17:38:41', '2019-01-02 17:38:41'),
(2, 'Dallah Ajyad', 'dajyad', 'm9292012', '2-Star Deluxe property at Ajyad Street. 450 meters from Haram Sharif.', NULL, 2, '23.446725', '39.453632', 'Ajyad Street', 'Makka', NULL, NULL, '', '0125001024', '', '', 'irfanedhi@gmail.com', NULL, '', NULL, '0', '0', '0', NULL, NULL, '16:00:00', '13:00:00', 100, 10, 2000, NULL, 1, 20.00, 30.00, 30.00, 3, 7, 1, 3, 0, 1, 8, 365, '2,3,4,5', '2', '1,2,3,4', '1', 1, 0, '2019-01-02 22:46:25', '2019-01-02 22:50:23');

--
-- Triggers `hotel`
--
DROP TRIGGER IF EXISTS `hotel_delete`;
DELIMITER $$
CREATE TRIGGER `hotel_delete` BEFORE DELETE ON `hotel` FOR EACH ROW UPDATE `users` SET `deleted` = 1, `status` = 0
WHERE `child_id` = Old.`id` AND `role_id` = 4
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `hotel_insert`;
DELIMITER $$
CREATE TRIGGER `hotel_insert` AFTER INSERT ON `hotel` FOR EACH ROW BEGIN
	
		INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
SELECT `name` AS `first_name`, '' AS `last_name`,
`username` AS `username`, MD5(`password`) AS `password`, `email` AS `email`,
`status` AS `status`, `id` AS `child_id`, '4' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
FROM `hotel` WHERE `id` = NEW.`id`;
                
	
    END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `hotel_update`;
DELIMITER $$
CREATE TRIGGER `hotel_update` AFTER UPDATE ON `hotel` FOR EACH ROW BEGIN
        IF NEW.`id` NOT IN (
			SELECT u.`child_id`
			FROM `users` u
			WHERE (NEW.`id` = u.`child_id` AND u.`role_id` = 4)
		)
		THEN
			INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
			SELECT  `name` AS `first_name`, '' AS `last_name`,
			`username` AS `username`, MD5(`password`) AS `password`, `email` AS `email`,
			`status` AS `status`, `id` AS `child_id`, '4' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
			FROM `hotel` WHERE `id` = NEW.`id`;
		ELSE
			UPDATE `users` SET
			`first_name` = New.`name`,
			`last_name` = '',
			`email` = New.`email`,
			`username` = New.`username`,
			`password` = MD5(New.`password`),
			`status` = New.`status` 
			WHERE `child_id` = New.`id` AND `role_id` = '4';
        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_cancellation_policy`
--

DROP TABLE IF EXISTS `hotel_cancellation_policy`;
CREATE TABLE `hotel_cancellation_policy` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `one_night_start` int(11) NOT NULL,
  `one_night_end` int(11) NOT NULL,
  `cancel_50_start` int(11) NOT NULL,
  `cancel_50_end` int(11) NOT NULL,
  `cancel_100_start` int(11) NOT NULL,
  `cancel_100_end` int(11) NOT NULL,
  `free_cancel_start` int(11) NOT NULL,
  `free_cancel_end` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `deleted` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_contact`
--

DROP TABLE IF EXISTS `hotel_contact`;
CREATE TABLE `hotel_contact` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `contact_type` enum('contact','reservation','account') NOT NULL DEFAULT 'contact',
  `person_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `deleted` char(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_facilities`
--

DROP TABLE IF EXISTS `hotel_facilities`;
CREATE TABLE `hotel_facilities` (
  `hotel_id` int(11) NOT NULL,
  `facilities_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_market`
--

DROP TABLE IF EXISTS `hotel_market`;
CREATE TABLE `hotel_market` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `market_id` int(11) NOT NULL,
  `price` float(10,2) DEFAULT NULL COMMENT 'price is per person',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_market_type`
--

DROP TABLE IF EXISTS `hotel_market_type`;
CREATE TABLE `hotel_market_type` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `market_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_meal_rate`
--

DROP TABLE IF EXISTS `hotel_meal_rate`;
CREATE TABLE `hotel_meal_rate` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `bookDate` date NOT NULL,
  `breakfast` float(10,2) DEFAULT NULL,
  `lunch` float(10,2) DEFAULT NULL,
  `dinner` float(10,2) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_meal_type`
--

DROP TABLE IF EXISTS `hotel_meal_type`;
CREATE TABLE `hotel_meal_type` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `meal_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_picture`
--

DROP TABLE IF EXISTS `hotel_picture`;
CREATE TABLE `hotel_picture` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_rooms`
--

DROP TABLE IF EXISTS `hotel_rooms`;
CREATE TABLE `hotel_rooms` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `title` tinytext NOT NULL,
  `description` text,
  `max_user` int(11) NOT NULL,
  `no_of_room` int(5) NOT NULL,
  `room_type` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel_rooms`
--

INSERT INTO `hotel_rooms` (`id`, `hotel_id`, `title`, `description`, `max_user`, `no_of_room`, `room_type`, `views`, `deleted`, `created`, `modified`) VALUES
(1, 1, 'Double - No View', 'Double', 2, 20, 2, 1, 0, '2019-01-02 17:43:12', '2019-01-02 17:45:12'),
(2, 1, 'Double - City View', 'Double', 2, 10, 2, 2, 0, '2019-01-02 17:43:52', '2019-01-02 17:45:27'),
(3, 2, 'Double Room Only', 'Double', 2, 5, 2, 2, 0, '2019-01-02 22:55:38', '2019-01-02 22:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_room_rate`
--

DROP TABLE IF EXISTS `hotel_room_rate`;
CREATE TABLE `hotel_room_rate` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `bookdate` date DEFAULT NULL,
  `price` float(10,2) NOT NULL,
  `price_weekend` float(10,2) DEFAULT NULL,
  `four_days` float(10,2) DEFAULT NULL,
  `no_of_room` int(11) DEFAULT NULL,
  `room_book` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel_room_rate`
--

INSERT INTO `hotel_room_rate` (`id`, `room_id`, `bookdate`, `price`, `price_weekend`, `four_days`, `no_of_room`, `room_book`, `created_by`, `deleted`, `created`, `modified`) VALUES
(1, 1, '2019-01-04', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(2, 1, '2019-01-05', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(3, 1, '2019-01-06', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(4, 1, '2019-01-07', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(5, 1, '2019-01-08', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(6, 1, '2019-01-09', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(7, 1, '2019-01-10', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(8, 1, '2019-01-11', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(9, 1, '2019-01-12', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(10, 1, '2019-01-13', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(11, 1, '2019-01-14', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(12, 1, '2019-01-15', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(13, 1, '2019-01-16', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(14, 1, '2019-01-17', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(15, 1, '2019-01-18', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(16, 1, '2019-01-19', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(17, 1, '2019-01-20', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(18, 1, '2019-01-21', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(19, 1, '2019-01-22', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(20, 1, '2019-01-23', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(21, 1, '2019-01-24', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(22, 1, '2019-01-25', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(23, 1, '2019-01-26', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(24, 1, '2019-01-27', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(25, 1, '2019-01-28', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(26, 1, '2019-01-29', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(27, 1, '2019-01-30', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(28, 1, '2019-01-31', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(29, 1, '2019-02-01', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(30, 1, '2019-02-02', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(31, 1, '2019-02-03', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(32, 1, '2019-02-04', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(33, 1, '2019-02-05', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(34, 1, '2019-02-06', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(35, 1, '2019-02-07', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(36, 1, '2019-02-08', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(37, 1, '2019-02-09', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(38, 1, '2019-02-10', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(39, 1, '2019-02-11', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(40, 1, '2019-02-12', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(41, 1, '2019-02-13', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(42, 1, '2019-02-14', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(43, 1, '2019-02-15', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(44, 1, '2019-02-16', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(45, 1, '2019-02-17', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(46, 1, '2019-02-18', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(47, 1, '2019-02-19', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(48, 1, '2019-02-20', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(49, 1, '2019-02-21', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(50, 1, '2019-02-22', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(51, 1, '2019-02-23', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(52, 1, '2019-02-24', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(53, 1, '2019-02-25', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(54, 1, '2019-02-26', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(55, 1, '2019-02-27', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(56, 1, '2019-02-28', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(57, 1, '2019-03-01', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(58, 1, '2019-03-02', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(59, 1, '2019-03-03', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(60, 1, '2019-03-04', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(61, 1, '2019-03-05', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(62, 1, '2019-03-06', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(63, 1, '2019-03-07', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(64, 1, '2019-03-08', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(65, 1, '2019-03-09', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(66, 1, '2019-03-10', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(67, 1, '2019-03-11', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(68, 1, '2019-03-12', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(69, 1, '2019-03-13', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(70, 1, '2019-03-14', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(71, 1, '2019-03-15', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(72, 1, '2019-03-16', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(73, 1, '2019-03-17', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(74, 1, '2019-03-18', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(75, 1, '2019-03-19', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(76, 1, '2019-03-20', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(77, 1, '2019-03-21', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(78, 1, '2019-03-22', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(79, 1, '2019-03-23', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(80, 1, '2019-03-24', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(81, 1, '2019-03-25', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(82, 1, '2019-03-26', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(83, 1, '2019-03-27', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(84, 1, '2019-03-28', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(85, 1, '2019-03-29', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(86, 1, '2019-03-30', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(87, 1, '2019-03-31', 450.00, 480.00, NULL, 10, 0, 2, '0', '2019-01-02 17:49:20', '2019-01-02 17:49:20'),
(88, 2, '2019-01-04', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(89, 2, '2019-01-05', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(90, 2, '2019-01-06', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(91, 2, '2019-01-07', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(92, 2, '2019-01-08', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(93, 2, '2019-01-09', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(94, 2, '2019-01-10', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(95, 2, '2019-01-11', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(96, 2, '2019-01-12', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(97, 2, '2019-01-13', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(98, 2, '2019-01-14', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(99, 2, '2019-01-15', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(100, 2, '2019-01-16', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(101, 2, '2019-01-17', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(102, 2, '2019-01-18', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(103, 2, '2019-01-19', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(104, 2, '2019-01-20', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(105, 2, '2019-01-21', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(106, 2, '2019-01-22', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(107, 2, '2019-01-23', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(108, 2, '2019-01-24', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(109, 2, '2019-01-25', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(110, 2, '2019-01-26', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(111, 2, '2019-01-27', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(112, 2, '2019-01-28', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(113, 2, '2019-01-29', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(114, 2, '2019-01-30', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(115, 2, '2019-01-31', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(116, 2, '2019-02-01', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(117, 2, '2019-02-02', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(118, 2, '2019-02-03', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(119, 2, '2019-02-04', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(120, 2, '2019-02-05', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(121, 2, '2019-02-06', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(122, 2, '2019-02-07', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(123, 2, '2019-02-08', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(124, 2, '2019-02-09', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(125, 2, '2019-02-10', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(126, 2, '2019-02-11', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(127, 2, '2019-02-12', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(128, 2, '2019-02-13', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(129, 2, '2019-02-14', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(130, 2, '2019-02-15', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(131, 2, '2019-02-16', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(132, 2, '2019-02-17', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(133, 2, '2019-02-18', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(134, 2, '2019-02-19', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(135, 2, '2019-02-20', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(136, 2, '2019-02-21', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(137, 2, '2019-02-22', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(138, 2, '2019-02-23', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(139, 2, '2019-02-24', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(140, 2, '2019-02-25', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(141, 2, '2019-02-26', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(142, 2, '2019-02-27', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(143, 2, '2019-02-28', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(144, 2, '2019-03-01', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(145, 2, '2019-03-02', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(146, 2, '2019-03-03', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(147, 2, '2019-03-04', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(148, 2, '2019-03-05', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(149, 2, '2019-03-06', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(150, 2, '2019-03-07', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(151, 2, '2019-03-08', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(152, 2, '2019-03-09', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(153, 2, '2019-03-10', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(154, 2, '2019-03-11', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(155, 2, '2019-03-12', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(156, 2, '2019-03-13', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(157, 2, '2019-03-14', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(158, 2, '2019-03-15', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(159, 2, '2019-03-16', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(160, 2, '2019-03-17', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(161, 2, '2019-03-18', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(162, 2, '2019-03-19', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(163, 2, '2019-03-20', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(164, 2, '2019-03-21', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(165, 2, '2019-03-22', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(166, 2, '2019-03-23', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(167, 2, '2019-03-24', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(168, 2, '2019-03-25', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(169, 2, '2019-03-26', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(170, 2, '2019-03-27', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(171, 2, '2019-03-28', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(172, 2, '2019-03-29', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(173, 2, '2019-03-30', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(174, 2, '2019-03-31', 480.00, 500.00, NULL, 10, 0, 2, '0', '2019-01-02 18:20:29', '2019-01-02 18:20:29'),
(175, 3, '2019-01-06', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(176, 3, '2019-01-07', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(177, 3, '2019-01-08', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(178, 3, '2019-01-09', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(179, 3, '2019-01-10', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(180, 3, '2019-01-11', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(181, 3, '2019-01-12', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(182, 3, '2019-01-13', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(183, 3, '2019-01-14', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(184, 3, '2019-01-15', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(185, 3, '2019-01-16', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(186, 3, '2019-01-17', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(187, 3, '2019-01-18', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(188, 3, '2019-01-19', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(189, 3, '2019-01-20', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(190, 3, '2019-01-21', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(191, 3, '2019-01-22', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(192, 3, '2019-01-23', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(193, 3, '2019-01-24', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(194, 3, '2019-01-25', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(195, 3, '2019-01-26', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(196, 3, '2019-01-27', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(197, 3, '2019-01-28', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(198, 3, '2019-01-29', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(199, 3, '2019-01-30', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(200, 3, '2019-01-31', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(201, 3, '2019-02-01', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(202, 3, '2019-02-02', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(203, 3, '2019-02-03', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(204, 3, '2019-02-04', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(205, 3, '2019-02-05', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(206, 3, '2019-02-06', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(207, 3, '2019-02-07', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(208, 3, '2019-02-08', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(209, 3, '2019-02-09', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(210, 3, '2019-02-10', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(211, 3, '2019-02-11', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(212, 3, '2019-02-12', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(213, 3, '2019-02-13', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(214, 3, '2019-02-14', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(215, 3, '2019-02-15', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(216, 3, '2019-02-16', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(217, 3, '2019-02-17', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(218, 3, '2019-02-18', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(219, 3, '2019-02-19', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(220, 3, '2019-02-20', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(221, 3, '2019-02-21', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(222, 3, '2019-02-22', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(223, 3, '2019-02-23', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(224, 3, '2019-02-24', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(225, 3, '2019-02-25', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(226, 3, '2019-02-26', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(227, 3, '2019-02-27', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(228, 3, '2019-02-28', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(229, 3, '2019-03-01', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(230, 3, '2019-03-02', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(231, 3, '2019-03-03', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(232, 3, '2019-03-04', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(233, 3, '2019-03-05', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(234, 3, '2019-03-06', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(235, 3, '2019-03-07', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(236, 3, '2019-03-08', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(237, 3, '2019-03-09', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(238, 3, '2019-03-10', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(239, 3, '2019-03-11', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(240, 3, '2019-03-12', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(241, 3, '2019-03-13', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(242, 3, '2019-03-14', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(243, 3, '2019-03-15', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(244, 3, '2019-03-16', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(245, 3, '2019-03-17', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(246, 3, '2019-03-18', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(247, 3, '2019-03-19', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(248, 3, '2019-03-20', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(249, 3, '2019-03-21', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(250, 3, '2019-03-22', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(251, 3, '2019-03-23', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(252, 3, '2019-03-24', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(253, 3, '2019-03-25', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(254, 3, '2019-03-26', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(255, 3, '2019-03-27', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(256, 3, '2019-03-28', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(257, 3, '2019-03-29', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(258, 3, '2019-03-30', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(259, 3, '2019-03-31', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(260, 3, '2019-04-01', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(261, 3, '2019-04-02', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(262, 3, '2019-04-03', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(263, 3, '2019-04-04', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(264, 3, '2019-04-05', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(265, 3, '2019-04-06', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(266, 3, '2019-04-07', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(267, 3, '2019-04-08', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(268, 3, '2019-04-09', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(269, 3, '2019-04-10', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(270, 3, '2019-04-11', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(271, 3, '2019-04-12', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(272, 3, '2019-04-13', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(273, 3, '2019-04-14', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(274, 3, '2019-04-15', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(275, 3, '2019-04-16', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(276, 3, '2019-04-17', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(277, 3, '2019-04-18', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(278, 3, '2019-04-19', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(279, 3, '2019-04-20', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(280, 3, '2019-04-21', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(281, 3, '2019-04-22', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(282, 3, '2019-04-23', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(283, 3, '2019-04-24', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(284, 3, '2019-04-25', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(285, 3, '2019-04-26', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(286, 3, '2019-04-27', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(287, 3, '2019-04-28', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(288, 3, '2019-04-29', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(289, 3, '2019-04-30', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(290, 3, '2019-05-01', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(291, 3, '2019-05-02', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(292, 3, '2019-05-03', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35'),
(293, 3, '2019-05-04', 130.00, NULL, NULL, 5, 0, 2, '0', '2019-01-02 22:56:35', '2019-01-02 22:56:35');

-- --------------------------------------------------------

--
-- Table structure for table `market_type`
--

DROP TABLE IF EXISTS `market_type`;
CREATE TABLE `market_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `deleted` char(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `market_type`
--

INSERT INTO `market_type` (`id`, `title`, `description`, `deleted`, `created_by`, `created`, `modified`) VALUES
(2, 'All Market', '', '0', NULL, '2018-12-24 18:00:34', '2018-12-24 18:00:34'),
(3, 'Far-East', '', '0', NULL, '2018-12-24 18:01:26', '2018-12-24 18:01:26'),
(4, 'Turkey', '', '0', NULL, '2018-12-24 18:01:32', '2018-12-24 18:01:32'),
(5, 'UK', '', '0', NULL, '2018-12-24 18:01:37', '2018-12-24 18:01:37');

-- --------------------------------------------------------

--
-- Table structure for table `meals_type`
--

DROP TABLE IF EXISTS `meals_type`;
CREATE TABLE `meals_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `create_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meals_type`
--

INSERT INTO `meals_type` (`id`, `title`, `create_by`, `created`, `modified`, `deleted`) VALUES
(1, 'Room Only', 0, '2018-12-24 17:56:35', '2018-12-24 17:56:35', '0'),
(2, 'Breakfast', 0, '2018-12-24 17:56:40', '2018-12-24 17:56:40', '0'),
(3, 'Lunch', 0, '2018-12-24 17:56:47', '2018-12-26 20:06:18', '0'),
(4, 'Dinner', 0, '2018-12-24 17:56:52', '2018-12-26 20:06:26', '0');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `url` text,
  `user_id` int(11) NOT NULL,
  `read` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '0 mean Sales office',
  `name` varchar(255) NOT NULL,
  `cell` varchar(255) NOT NULL,
  `cell2` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `joining_date` date NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `parent_id`, `name`, `cell`, `cell2`, `email`, `address`, `image`, `joining_date`, `username`, `password`, `status`, `created`, `modified`, `deleted`) VALUES
(1, 0, 'Sale Office', '03135268452', '', 'saleoffice@sales.com', 'house 123', NULL, '2019-01-01', 'saleoffice', '123456', '1', '2018-12-31 23:17:31', '2018-12-31 23:17:31', 0),
(2, 1, 'Branch office', '12345567', '', 'aa@aa.com', '', NULL, '2019-01-01', 'branchoffice', '123456', '1', '2018-12-31 23:28:28', '2018-12-31 23:28:28', 0),
(3, 1, 'Taibah.pk', '+923322441889', '', 'bh.memon786@gmail.com', 'Jilani Center Karachi', NULL, '2019-01-03', 'misbah', '123456', '1', '2019-01-02 17:22:07', '2019-01-02 17:22:07', 0),
(4, 3, 'Irfan Travels', '+923322441889', '', 'irfanedhi@gmail.com', '', NULL, '2019-01-03', 'irfanedhi@gmail.com', '123456', '1', '2019-01-02 18:07:58', '2019-01-02 18:07:58', 0);

--
-- Triggers `partners`
--
DROP TRIGGER IF EXISTS `staff_delete`;
DELIMITER $$
CREATE TRIGGER `staff_delete` BEFORE DELETE ON `partners` FOR EACH ROW UPDATE `users` SET `deleted` = 1, `status` = 0
WHERE `child_id` = Old.`id` AND `role_id` = 3
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `staff_insert`;
DELIMITER $$
CREATE TRIGGER `staff_insert` AFTER INSERT ON `partners` FOR EACH ROW BEGIN
	
		INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
SELECT `name` AS `first_name`, '' AS `last_name`,
`username` AS `username`, MD5(`password`) AS `password`, `email` AS `email`,
`status` AS `status`, `id` AS `child_id`, '3' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
FROM `partners` WHERE `id` = NEW.`id`;
                
    END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `staff_update`;
DELIMITER $$
CREATE TRIGGER `staff_update` AFTER UPDATE ON `partners` FOR EACH ROW BEGIN
        IF NEW.`id` NOT IN (
			SELECT u.`child_id`
			FROM `users` u
			WHERE (NEW.`id` = u.`child_id` AND u.`role_id` = 3)
		)
		THEN
			INSERT INTO `users` (`first_name`, `last_name`, `username`, `password`, `email`, `status`, `child_id`, `role_id`, `created`, `modified`)
			SELECT  `name` AS `first_name`, '' AS `last_name`,
			`username` AS `username`, MD5(`password`) AS `password`, `email` AS `email`,
			`status` AS `status`, `id` AS `child_id`, '3' AS `role_id`, NOW() AS `created`, NOW() AS `modified` 
			FROM `staff` WHERE `id` = NEW.`id`;
		ELSE
			UPDATE `users` SET
			`first_name` = New.`name`,
			`last_name` = '',
			`email` = New.`email`,
			`username` = New.`username`,
			`password` = MD5(New.`password`),
			`status` = New.`status` 
			WHERE `child_id` = New.`id` AND `role_id` = '3';
        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `partner_limit`
--

DROP TABLE IF EXISTS `partner_limit`;
CREATE TABLE `partner_limit` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL DEFAULT '0',
  `booking_limit` float(10,2) NOT NULL,
  `checkin_limit` float(10,2) NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL COMMENT 'if null mean continue',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner_limit`
--

INSERT INTO `partner_limit` (`id`, `staff_id`, `booking_limit`, `checkin_limit`, `date_start`, `date_end`, `deleted`, `created`, `modified`, `created_by`) VALUES
(1, 2, 25000.00, 25.00, '2019-01-01 00:00:00', NULL, 0, '2018-12-31 23:29:55', '2018-12-31 23:29:55', 1),
(2, 3, 20000.00, 50.00, '2019-01-03 00:00:00', NULL, 0, '2019-01-02 17:24:11', '2019-01-02 17:24:11', 1),
(3, 4, 5000.00, 25.00, '2019-01-03 00:00:00', NULL, 0, '2019-01-02 18:09:34', '2019-01-02 18:09:34', 3);

-- --------------------------------------------------------

--
-- Table structure for table `partner_login_logs`
--

DROP TABLE IF EXISTS `partner_login_logs`;
CREATE TABLE `partner_login_logs` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner_login_logs`
--

INSERT INTO `partner_login_logs` (`id`, `staff_id`, `login_date`, `logout_date`, `created`, `modified`, `deleted`) VALUES
(1, 1, '2019-01-01 10:17:44', '2019-01-01 10:20:28', '2019-01-01 10:17:44', '2019-01-01 10:17:44', 0),
(2, 1, '2019-01-01 10:28:01', '2019-01-01 10:30:00', '2019-01-01 10:28:01', '2019-01-01 10:28:01', 0),
(3, 2, '2019-01-01 10:30:07', '2019-01-01 10:30:23', '2019-01-01 10:30:07', '2019-01-01 10:30:07', 0),
(4, 1, '2019-01-01 19:02:43', '2019-01-03 04:20:29', '2019-01-01 19:02:43', '2019-01-01 19:02:43', 0),
(5, 1, '2019-01-03 04:20:29', '2019-01-03 04:27:09', '2019-01-03 04:20:29', '2019-01-03 04:20:29', 0),
(6, 3, '2019-01-03 05:06:56', '2019-01-03 05:12:44', '2019-01-03 05:06:56', '2019-01-03 05:06:56', 0),
(7, 3, '2019-01-03 05:14:22', '2019-01-03 05:19:41', '2019-01-03 05:14:22', '2019-01-03 05:14:22', 0),
(8, 3, '2019-01-03 05:21:05', NULL, '2019-01-03 05:21:05', '2019-01-03 05:21:05', 0),
(9, 1, '2019-01-03 09:58:20', '2019-01-03 10:00:52', '2019-01-03 09:58:20', '2019-01-03 09:58:20', 0),
(10, 1, '2019-01-03 10:01:20', '2019-01-03 10:18:07', '2019-01-03 10:01:20', '2019-01-03 10:01:20', 0);

-- --------------------------------------------------------

--
-- Table structure for table `partner_payment`
--

DROP TABLE IF EXISTS `partner_payment`;
CREATE TABLE `partner_payment` (
  `id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `pay_date` date DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `reference_number` varchar(255) DEFAULT NULL,
  `narration` text,
  `bid` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `partner_price_type`
--

DROP TABLE IF EXISTS `partner_price_type`;
CREATE TABLE `partner_price_type` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `price_type` char(1) NOT NULL DEFAULT '1' COMMENT '1=flat,2=percentage,3=hybrid',
  `fix_price` float(10,2) DEFAULT NULL,
  `floating_price` float(10,2) DEFAULT NULL,
  `hybrid_condition` char(1) DEFAULT NULL COMMENT '1= which ever is low, 2= which ever is high',
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `deleted` char(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner_price_type`
--

INSERT INTO `partner_price_type` (`id`, `staff_id`, `price_type`, `fix_price`, `floating_price`, `hybrid_condition`, `date_from`, `date_to`, `created_by`, `deleted`, `created`, `modified`) VALUES
(1, 2, '3', 5.00, 1.00, '2', '2019-01-01', NULL, 1, '0', '2018-12-31 23:29:29', '2018-12-31 23:29:29'),
(2, 3, '3', 5.00, 1.00, '2', '2019-01-03', NULL, 1, '0', '2019-01-02 17:23:20', '2019-01-02 17:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `permission_actions`
--

DROP TABLE IF EXISTS `permission_actions`;
CREATE TABLE `permission_actions` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_entity_id` int(11) NOT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_actions`
--

INSERT INTO `permission_actions` (`id`, `title`, `permission_entity_id`, `module`, `controller`, `action`, `meta_code`, `parent`, `deleted`, `created`, `modified`) VALUES
(1, 'List', 1, 'permissions', 'Groups', 'Index', '', NULL, 0, '2016-04-10 21:35:29', '2016-04-10 21:35:29'),
(2, 'Add', 1, 'permissions', 'Groups', 'Create', '', 1, 0, '2016-04-10 21:35:45', '2016-04-10 21:36:37'),
(3, 'Edit', 1, 'permissions', 'Groups', 'Update', '', 1, 0, '2016-04-10 21:36:03', '2016-04-10 21:36:32'),
(4, 'Delete', 1, 'permissions', 'Groups', 'Delete', '', 1, 0, '2016-04-10 21:36:19', '2016-04-10 21:36:19'),
(5, 'List', 2, 'permissions', 'Entities', 'Index', '', NULL, 0, '2016-04-10 21:37:42', '2016-04-10 21:37:42'),
(6, 'Add', 2, 'permissions', 'Entities', 'Create', '', 5, 0, '2016-04-10 21:37:54', '2016-04-10 21:37:54'),
(7, 'Edit', 2, 'permissions', 'Entities', 'Update', '', 5, 0, '2016-04-10 21:38:23', '2016-04-10 21:38:23'),
(8, 'Delete', 2, 'permissions', 'Entities', 'Delete', '', 5, 0, '2016-04-10 21:38:40', '2016-04-10 21:38:40'),
(9, 'List', 3, 'permissions', 'Actions', 'Index', '', NULL, 0, '2016-04-10 21:39:35', '2016-04-10 21:39:35'),
(10, 'Add', 3, 'permissions', 'Actions', 'Create', '', 9, 0, '2016-04-10 21:40:11', '2016-04-10 21:40:11'),
(11, 'Edit', 3, 'permissions', 'Actions', 'Update', '', 9, 0, '2016-04-10 21:40:23', '2016-04-10 21:40:23'),
(12, 'Delete', 3, 'permissions', 'Actions', 'Delete', '', 9, 0, '2016-04-10 21:40:40', '2016-04-10 21:40:40'),
(13, 'Ajax - Get Parents', 3, 'permissions', 'Actions', 'GetParents', '', 9, 0, '2016-04-10 21:41:34', '2016-04-10 21:41:34'),
(14, 'List', 4, 'permissions', 'Manage', 'Index', '', NULL, 0, '2016-04-10 21:42:16', '2016-04-10 21:42:16'),
(15, 'Edit', 4, 'permissions', 'Manage', 'Index', 'update', NULL, 0, '2016-04-10 21:42:42', '2016-04-10 21:42:42'),
(16, 'List', 5, 'permissions', 'Settings', 'Index', '', NULL, 0, '2016-04-10 21:43:17', '2016-04-10 21:43:17'),
(17, 'Add', 5, 'permissions', 'Settings', 'Create', '', 16, 0, '2016-04-10 21:43:32', '2016-04-10 21:43:32'),
(18, 'Edit', 5, 'permissions', 'Settings', 'Update', '', 16, 0, '2016-04-10 21:43:53', '2016-04-10 21:43:53'),
(19, 'List', 6, 'users', 'Admins', 'Index', '', NULL, 0, '2016-04-10 21:45:39', '2016-04-10 21:45:39'),
(20, 'Add', 6, 'users', 'Admins', 'Create', '', 19, 0, '2016-04-10 21:45:50', '2016-04-10 21:45:50'),
(21, 'Edit', 6, 'users', 'Admins', 'Update', '', 19, 0, '2016-04-10 21:46:01', '2016-04-10 21:46:01'),
(22, 'Delete', 6, 'users', 'Admins', 'Delete', '', 19, 0, '2016-04-10 21:46:13', '2016-04-10 21:46:13'),
(23, 'List', 7, 'users', 'Csr', 'Index', '', NULL, 0, '2016-04-10 21:46:53', '2016-04-10 21:46:53'),
(24, 'Add', 7, 'users', 'Csr', 'Create', '', 23, 0, '2016-04-10 21:47:15', '2016-04-10 21:47:15'),
(25, 'Edit', 7, 'users', 'Csr', 'Update', '', 23, 0, '2016-04-10 21:47:37', '2016-04-10 21:47:37'),
(26, 'Delete', 7, 'users', 'Csr', 'Delete', '', 23, 0, '2016-04-10 21:47:50', '2016-04-10 21:47:50'),
(27, 'List', 8, 'users', 'Clients', 'Index', '', NULL, 0, '2016-04-10 21:48:16', '2016-04-10 21:48:16'),
(28, 'Edit', 8, 'users', 'Clients', 'Update', '', 27, 0, '2016-04-10 21:48:33', '2016-04-10 21:48:33'),
(29, 'List', 9, 'users', 'Manage', 'Index', '', NULL, 0, '2016-04-10 21:49:18', '2016-04-10 21:49:18'),
(30, 'Add', 9, 'users', 'Manage', 'Create', '', 29, 0, '2016-04-10 21:49:40', '2016-04-10 21:49:40'),
(31, 'Edit', 9, 'users', 'Manage', 'Update', '', 29, 0, '2016-04-10 21:49:53', '2016-04-10 21:49:53'),
(32, 'Delete', 9, 'users', 'Manage', 'Delete', '', 29, 0, '2016-04-10 21:50:07', '2016-04-10 21:50:07'),
(33, 'List', 10, 'users', 'Roles', 'Index', '', NULL, 0, '2016-04-10 21:51:01', '2016-04-10 21:51:01'),
(34, 'Add', 10, 'users', 'Roles', 'Create', '', 33, 0, '2016-04-10 21:51:15', '2016-04-10 21:51:15'),
(35, 'Edit', 10, 'users', 'Roles', 'Update', '', 33, 0, '2016-04-10 21:51:29', '2016-04-10 21:51:29'),
(36, 'Delete', 10, 'users', 'Roles', 'Delete', '', 33, 0, '2016-04-10 21:51:44', '2016-04-10 21:51:44'),
(37, 'Index', 11, 'facilities', 'Facilities', 'Index', '', NULL, 0, '2018-12-23 06:36:15', '2018-12-23 06:36:15'),
(38, 'View', 11, 'facilities', 'Facilities', 'View', '', NULL, 0, '2018-12-23 06:36:28', '2018-12-23 06:36:28'),
(39, 'Create', 11, 'facilities', 'Facilities', 'Create', '', NULL, 0, '2018-12-23 06:36:43', '2018-12-23 06:36:43'),
(40, 'Update', 11, 'facilities', 'Facilities', 'Update', '', NULL, 0, '2018-12-23 06:36:53', '2018-12-23 06:36:53'),
(41, 'Delete', 11, 'facilities', 'Facilities', 'Delete', '', NULL, 0, '2018-12-23 06:37:24', '2018-12-23 06:37:24'),
(42, 'Index', 12, 'hotel', 'Hotel', 'Index', '', NULL, 0, '2018-12-23 06:40:04', '2018-12-23 06:40:04'),
(43, 'View', 12, 'hotel', 'Hotel', 'View', '', NULL, 0, '2018-12-23 06:40:14', '2018-12-23 06:40:14'),
(44, 'Create', 12, 'hotel', 'Hotel', 'Create', '', NULL, 0, '2018-12-23 06:40:25', '2018-12-23 06:40:25'),
(45, 'Update', 12, 'hotel', 'Hotel', 'Update', '', NULL, 0, '2018-12-23 06:40:36', '2018-12-23 06:40:36'),
(46, 'Delete', 12, 'hotel', 'Hotel', 'Delete', '', NULL, 0, '2018-12-23 06:40:44', '2018-12-23 06:40:44'),
(47, 'Index', 13, 'markettype', 'Markettype', 'Index', '', NULL, 0, '2018-12-23 06:41:19', '2018-12-23 06:41:19'),
(48, 'View', 13, 'markettype', 'Markettype', 'View', '', NULL, 0, '2018-12-23 06:41:28', '2018-12-23 06:41:28'),
(49, 'Create', 13, 'markettype', 'Markettype', 'Create', '', NULL, 0, '2018-12-23 06:41:38', '2018-12-23 06:41:38'),
(50, 'Update', 13, 'markettype', 'Markettype', 'Update', '', NULL, 0, '2018-12-23 06:41:46', '2018-12-23 06:41:46'),
(51, 'Delete', 13, 'markettype', 'Markettype', 'Delete', '', NULL, 0, '2018-12-23 06:41:53', '2018-12-23 06:41:53'),
(52, 'Index', 14, 'partner', 'Partners', 'Index', '', NULL, 0, '2018-12-23 06:43:19', '2018-12-23 06:43:19'),
(53, 'View', 14, 'partner', 'Partners', 'View', '', NULL, 0, '2018-12-23 06:43:27', '2018-12-23 06:43:27'),
(54, 'Create', 14, 'partner', 'Partners', 'Create', '', NULL, 0, '2018-12-23 06:43:35', '2018-12-23 06:43:35'),
(55, 'Update', 14, 'partner', 'Partners', 'Update', '', NULL, 0, '2018-12-23 06:43:43', '2018-12-23 06:43:43'),
(56, 'Delete', 14, 'partner', 'Partners', 'Delete', '', NULL, 0, '2018-12-23 06:43:52', '2018-12-23 06:43:52'),
(57, 'Index', 15, 'mealtype', 'Mealstype', 'Index', '', NULL, 0, '2018-12-24 16:26:15', '2018-12-24 16:26:15'),
(58, 'View', 15, 'mealtype', 'Mealstype', 'View', '', NULL, 0, '2018-12-24 16:26:27', '2018-12-24 16:26:27'),
(59, 'Create', 15, 'mealtype', 'Mealstype', 'Create', '', NULL, 0, '2018-12-24 16:26:38', '2018-12-24 16:26:38'),
(60, 'Update', 15, 'mealtype', 'Mealstype', 'Update', '', NULL, 0, '2018-12-24 16:26:48', '2018-12-24 16:26:48'),
(61, 'Delete', 15, 'mealtype', 'Mealstype', 'Delete', '', NULL, 0, '2018-12-24 16:26:58', '2018-12-24 16:26:58'),
(62, 'Index', 16, 'roomtype', 'RoomType', 'Index', '', NULL, 0, '2018-12-24 16:27:39', '2018-12-24 16:27:39'),
(63, 'View', 16, 'roomtype', 'RoomType', 'View', '', NULL, 0, '2018-12-24 16:27:50', '2018-12-24 16:27:50'),
(64, 'Create', 16, 'roomtype', 'RoomType', 'Create', '', NULL, 0, '2018-12-24 16:28:02', '2018-12-24 16:28:02'),
(65, 'Update', 16, 'roomtype', 'RoomType', 'Update', '', NULL, 0, '2018-12-24 16:28:11', '2018-12-24 16:28:11'),
(66, 'Delete', 16, 'roomtype', 'RoomType', 'Delete', '', NULL, 0, '2018-12-24 16:28:21', '2018-12-24 16:28:21'),
(67, 'Index', 17, 'viewtype', 'ViewType', 'Index', '', NULL, 0, '2018-12-24 16:28:48', '2018-12-24 16:28:48'),
(68, 'View', 17, 'viewtype', 'ViewType', 'View', '', NULL, 0, '2018-12-24 16:28:59', '2018-12-24 16:28:59'),
(69, 'Create', 17, 'viewtype', 'ViewType', 'Create', '', NULL, 0, '2018-12-24 16:29:13', '2018-12-24 16:29:13'),
(70, 'Update', 17, 'viewtype', 'ViewType', 'Update', '', NULL, 0, '2018-12-24 16:29:24', '2018-12-24 16:29:24'),
(71, 'Delete', 17, 'viewtype', 'ViewType', 'Delete', '', NULL, 0, '2018-12-24 16:29:31', '2018-12-24 16:29:31'),
(72, 'Index', 18, 'hotel', 'HotelRooms', 'Index', '', NULL, 0, '2018-12-25 14:43:13', '2018-12-25 14:43:13'),
(73, 'View', 18, 'hotel', 'HotelRooms', 'View', '', NULL, 0, '2018-12-25 14:43:25', '2018-12-25 14:43:25'),
(74, 'Create', 18, 'hotel', 'HotelRooms', 'Create', '', NULL, 0, '2018-12-25 14:43:37', '2018-12-25 14:43:37'),
(75, 'Update', 18, 'hotel', 'HotelRooms', 'Update', '', NULL, 0, '2018-12-25 14:43:46', '2018-12-25 14:43:46'),
(76, 'Delete', 18, 'hotel', 'HotelRooms', 'Delete', '', NULL, 0, '2018-12-25 14:43:59', '2018-12-25 14:43:59'),
(77, 'Index', 19, 'hotel', 'HotelRoomRate', 'Index', '', NULL, 0, '2018-12-25 17:31:36', '2018-12-25 17:31:36'),
(78, 'View', 19, 'hotel', 'HotelRoomRate', 'View', '', NULL, 0, '2018-12-25 17:31:49', '2018-12-25 17:31:49'),
(79, 'Create', 19, 'hotel', 'HotelRoomRate', 'Create', '', NULL, 0, '2018-12-25 17:32:04', '2018-12-25 17:32:04'),
(80, 'Update', 19, 'hotel', 'HotelRoomRate', 'Update', '', NULL, 0, '2018-12-25 18:38:07', '2018-12-25 18:38:07'),
(81, 'Delete', 19, 'hotel', 'HotelRoomRate', 'Delete', '', NULL, 0, '2018-12-25 18:38:21', '2018-12-25 18:38:21'),
(82, 'Hotel Detail', 12, 'hotel', 'Hotel', 'HotelDetail', '', NULL, 0, '2018-12-27 03:36:23', '2018-12-27 03:36:23'),
(83, 'Hotel Rooms', 12, 'hotel', 'Hotel', 'HotelRooms', '', NULL, 0, '2018-12-27 04:28:00', '2018-12-27 04:28:00'),
(84, 'Index', 20, 'partner', 'PartnerPriceType', 'Index', '', NULL, 0, '2018-12-30 03:53:42', '2018-12-30 03:53:42'),
(85, 'View', 20, 'partner', 'PartnerPriceType', 'View', '', NULL, 0, '2018-12-30 03:55:33', '2018-12-30 03:55:33'),
(86, 'Create', 20, 'partner', 'PartnerPriceType', 'Create', '', NULL, 0, '2018-12-30 03:55:42', '2018-12-30 03:55:42'),
(87, 'Update', 20, 'partner', 'PartnerPriceType', 'Update', '', NULL, 0, '2018-12-30 03:55:56', '2018-12-30 03:55:56'),
(88, 'Delete', 20, 'partner', 'PartnerPriceType', 'Delete', '', NULL, 0, '2018-12-30 03:56:06', '2018-12-30 03:56:06'),
(89, 'Index', 21, 'partner', 'PartnerLimit', 'Index', '', NULL, 0, '2018-12-30 07:38:07', '2018-12-30 07:38:07'),
(90, 'View', 21, 'partner', 'PartnerLimit', 'View', '', NULL, 0, '2018-12-30 07:38:20', '2018-12-30 07:38:20'),
(91, 'Create', 21, 'partner', 'PartnerLimit', 'Create', '', NULL, 0, '2018-12-30 07:38:29', '2018-12-30 07:38:29'),
(92, 'Update', 21, 'partner', 'PartnerLimit', 'Update', '', NULL, 0, '2018-12-30 07:38:38', '2018-12-30 07:38:38'),
(93, 'Delete', 21, 'partner', 'PartnerLimit', 'Delete', '', NULL, 0, '2018-12-30 07:38:47', '2018-12-30 07:38:47'),
(94, 'Index', 22, 'booking', 'Booking', 'Index', '', NULL, 0, '2018-12-31 02:01:45', '2018-12-31 02:01:45'),
(95, 'Reservation', 22, 'booking', 'Booking', 'Reservation', '', NULL, 0, '2018-12-31 02:01:57', '2018-12-31 02:01:57'),
(96, 'Create', 22, 'booking', 'Booking', 'Create', '', NULL, 0, '2018-12-31 02:02:09', '2018-12-31 02:02:09'),
(97, 'View', 22, 'booking', 'Booking', 'View', '', NULL, 0, '2018-12-31 02:02:18', '2018-12-31 02:02:18'),
(98, 'Update', 22, 'booking', 'Booking', 'Update', '', NULL, 0, '2018-12-31 02:02:27', '2018-12-31 02:02:27'),
(99, 'Delete', 22, 'booking', 'Booking', 'Delete', '', NULL, 0, '2018-12-31 02:02:55', '2018-12-31 02:02:55'),
(100, 'Index', 23, 'partner', 'PartnerPayment', 'Index', '', NULL, 0, '2018-12-31 02:03:24', '2018-12-31 02:03:24'),
(101, 'Create', 23, 'partner', 'PartnerPayment', 'Create', '', NULL, 0, '2018-12-31 02:03:37', '2018-12-31 02:03:37'),
(102, 'View', 23, 'partner', 'PartnerPayment', 'View', '', NULL, 0, '2018-12-31 02:03:50', '2018-12-31 02:03:50'),
(103, 'Update', 23, 'partner', 'PartnerPayment', 'Update', '', NULL, 0, '2018-12-31 02:04:02', '2018-12-31 02:04:02'),
(104, 'Delete', 23, 'partner', 'PartnerPayment', 'Delete', '', NULL, 0, '2018-12-31 02:04:42', '2018-12-31 02:04:42');

-- --------------------------------------------------------

--
-- Table structure for table `permission_entities`
--

DROP TABLE IF EXISTS `permission_entities`;
CREATE TABLE `permission_entities` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_group_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_entities`
--

INSERT INTO `permission_entities` (`id`, `title`, `permission_group_id`, `deleted`, `created`, `modified`) VALUES
(1, 'Permission Groups', 1, 0, '2016-04-10 21:34:37', '2016-04-10 21:34:37'),
(2, 'Permission Entities', 1, 0, '2016-04-10 21:34:45', '2016-04-10 21:34:45'),
(3, 'Permission Actions', 1, 0, '2016-04-10 21:34:51', '2016-04-10 21:34:51'),
(4, 'Role Permission', 1, 0, '2016-04-10 21:34:59', '2016-04-10 21:34:59'),
(5, 'Settings', 1, 0, '2016-04-10 21:35:04', '2016-04-10 21:35:04'),
(6, 'Manage Admin', 2, 0, '2016-04-10 21:44:41', '2016-04-10 21:44:41'),
(7, 'Manage CSR', 2, 0, '2016-04-10 21:44:49', '2016-04-10 21:44:49'),
(8, 'Manage Clients', 2, 0, '2016-04-10 21:44:57', '2016-04-10 21:44:57'),
(9, 'Manage All Users', 2, 0, '2016-04-10 21:45:12', '2016-04-10 21:45:12'),
(10, 'Manage Roles', 2, 0, '2016-04-10 21:45:19', '2016-04-10 21:45:19'),
(11, 'Facilities', 4, 0, '2018-12-23 06:33:57', '2018-12-23 06:33:57'),
(12, 'Hotel', 5, 0, '2018-12-23 06:37:44', '2018-12-23 06:37:44'),
(13, 'Market Type', 6, 0, '2018-12-23 06:41:02', '2018-12-23 06:41:02'),
(14, 'Partner', 7, 0, '2018-12-23 06:43:03', '2018-12-23 06:43:03'),
(15, 'Meal Type', 8, 0, '2018-12-24 16:25:59', '2018-12-24 16:25:59'),
(16, 'Rooms type', 9, 0, '2018-12-24 16:27:26', '2018-12-24 16:27:26'),
(17, 'View Type', 10, 0, '2018-12-24 16:28:36', '2018-12-24 16:28:36'),
(18, 'Hotel Rooms', 5, 0, '2018-12-25 14:43:00', '2018-12-25 14:43:00'),
(19, 'Hotel Room Rates', 5, 0, '2018-12-25 17:31:21', '2018-12-25 17:31:21'),
(20, 'Partner Price Type', 7, 0, '2018-12-30 03:53:11', '2018-12-30 03:53:11'),
(21, 'Partner Limit', 7, 0, '2018-12-30 07:37:51', '2018-12-30 07:37:51'),
(22, 'Booking', 3, 0, '2018-12-31 02:01:29', '2018-12-31 02:01:29'),
(23, 'Payment', 7, 0, '2018-12-31 02:03:11', '2018-12-31 02:03:11');

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
CREATE TABLE `permission_groups` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_groups`
--

INSERT INTO `permission_groups` (`id`, `title`, `deleted`, `created`, `modified`) VALUES
(1, 'Permission & Settings', 0, '2016-04-10 21:34:04', '2016-04-10 21:34:04'),
(2, 'User Management', 0, '2016-04-10 21:34:14', '2016-04-10 21:34:14'),
(3, 'Booking', 0, '2018-12-23 06:30:06', '2018-12-23 06:30:06'),
(4, 'Facilities', 0, '2018-12-23 06:30:16', '2018-12-23 06:30:16'),
(5, 'Hotel', 0, '2018-12-23 06:30:24', '2018-12-23 06:30:24'),
(6, 'Market Type', 0, '2018-12-23 06:33:25', '2018-12-23 06:33:25'),
(7, 'Partner', 0, '2018-12-23 06:33:36', '2018-12-23 06:33:36'),
(8, 'Meal Type', 0, '2018-12-24 16:25:23', '2018-12-24 16:25:23'),
(9, 'Room Type', 0, '2018-12-24 16:25:38', '2018-12-24 16:25:38'),
(10, 'View Type', 0, '2018-12-24 16:25:45', '2018-12-24 16:25:45');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `deleted`, `created`, `modified`) VALUES
(1, 'Super Admin', 0, '2018-12-19 15:05:00', '2018-12-19 15:05:00'),
(2, 'Admin', 0, '2018-12-19 15:05:00', '2018-12-19 15:05:00'),
(3, 'Partners', 0, '2018-12-19 15:05:00', '2018-12-23 06:46:02'),
(4, 'Hotel', 0, '2016-04-09 15:05:00', '2016-04-09 15:05:00'),
(5, 'Operations', 0, '2018-12-19 15:05:00', '2018-12-19 15:05:00'),
(6, 'Reservation  Company', 0, '2018-12-23 06:47:29', '2018-12-23 06:47:29');

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_action_id` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role_id`, `permission_action_id`, `deleted`, `created`, `modified`) VALUES
(1, 1, 1, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(2, 1, 2, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(3, 1, 3, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(4, 1, 4, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(5, 1, 5, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(6, 1, 6, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(7, 1, 7, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(8, 1, 8, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(9, 1, 9, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(10, 1, 10, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(11, 1, 11, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(12, 1, 12, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(13, 1, 13, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(14, 1, 14, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(15, 1, 15, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(16, 1, 16, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(17, 1, 17, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(18, 1, 18, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(19, 1, 19, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(20, 1, 20, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(21, 1, 21, 0, '2016-04-10 21:58:55', '2019-01-02 14:58:52'),
(22, 1, 22, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(23, 1, 23, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(24, 1, 24, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(25, 1, 25, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(26, 1, 26, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(27, 1, 27, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(28, 1, 28, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(29, 1, 29, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(30, 1, 30, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(31, 1, 31, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(32, 1, 32, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(33, 1, 33, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(34, 1, 34, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(35, 1, 35, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(36, 1, 36, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(39, 2, 19, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(40, 2, 20, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(41, 2, 21, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(42, 2, 22, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(43, 2, 23, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(44, 2, 24, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(45, 2, 25, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(46, 2, 26, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(47, 2, 27, 0, '2016-04-10 21:58:56', '2019-01-02 14:58:52'),
(48, 2, 28, 0, '2016-04-10 21:58:57', '2019-01-02 14:58:52'),
(184, 2, 133, 1, '2018-06-24 10:27:37', '2018-06-24 10:27:37'),
(185, 2, 37, 0, '2018-12-23 06:51:54', '2019-01-02 14:58:52'),
(186, 2, 38, 0, '2018-12-23 06:51:54', '2019-01-02 14:58:52'),
(187, 2, 39, 0, '2018-12-23 06:51:54', '2019-01-02 14:58:52'),
(188, 2, 40, 0, '2018-12-23 06:51:54', '2019-01-02 14:58:52'),
(189, 2, 41, 0, '2018-12-23 06:51:54', '2019-01-02 14:58:52'),
(190, 2, 42, 0, '2018-12-23 06:51:54', '2019-01-02 14:58:52'),
(191, 2, 43, 0, '2018-12-23 06:51:54', '2019-01-02 14:58:52'),
(192, 2, 44, 0, '2018-12-23 06:51:55', '2019-01-02 14:58:52'),
(193, 2, 45, 0, '2018-12-23 06:51:55', '2019-01-02 14:58:52'),
(194, 2, 46, 0, '2018-12-23 06:51:55', '2019-01-02 14:58:52'),
(195, 2, 47, 0, '2018-12-23 06:51:55', '2019-01-02 14:58:53'),
(196, 2, 48, 0, '2018-12-23 06:51:55', '2019-01-02 14:58:53'),
(197, 2, 49, 0, '2018-12-23 06:51:55', '2019-01-02 14:58:53'),
(198, 2, 50, 0, '2018-12-23 06:51:55', '2019-01-02 14:58:53'),
(199, 2, 51, 0, '2018-12-23 06:51:55', '2019-01-02 14:58:53'),
(200, 2, 52, 0, '2018-12-23 06:51:56', '2019-01-02 14:58:53'),
(201, 2, 53, 0, '2018-12-23 06:51:56', '2019-01-02 14:58:53'),
(202, 2, 54, 0, '2018-12-23 06:51:56', '2019-01-02 14:58:53'),
(203, 2, 55, 0, '2018-12-23 06:51:56', '2019-01-02 14:58:53'),
(204, 2, 56, 0, '2018-12-23 06:51:56', '2019-01-02 14:58:53'),
(205, 2, 57, 0, '2018-12-24 16:30:13', '2019-01-02 14:58:53'),
(206, 2, 58, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(207, 2, 59, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(208, 2, 60, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(209, 2, 61, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(210, 2, 62, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(211, 2, 63, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(212, 2, 64, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(213, 2, 65, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(214, 2, 66, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(215, 2, 67, 0, '2018-12-24 16:30:14', '2019-01-02 14:58:53'),
(216, 2, 68, 0, '2018-12-24 16:30:15', '2019-01-02 14:58:53'),
(217, 2, 69, 0, '2018-12-24 16:30:15', '2019-01-02 14:58:53'),
(218, 2, 70, 0, '2018-12-24 16:30:15', '2019-01-02 14:58:53'),
(219, 2, 71, 0, '2018-12-24 16:30:15', '2019-01-02 14:58:53'),
(220, 2, 72, 0, '2018-12-25 14:44:18', '2019-01-02 14:58:52'),
(221, 2, 73, 0, '2018-12-25 14:44:18', '2019-01-02 14:58:52'),
(222, 2, 74, 0, '2018-12-25 14:44:18', '2019-01-02 14:58:52'),
(223, 2, 75, 0, '2018-12-25 14:44:18', '2019-01-02 14:58:52'),
(224, 2, 76, 0, '2018-12-25 14:44:18', '2019-01-02 14:58:52'),
(225, 2, 77, 0, '2018-12-25 18:38:51', '2019-01-02 14:58:52'),
(226, 2, 78, 0, '2018-12-25 18:38:51', '2019-01-02 14:58:52'),
(227, 2, 79, 0, '2018-12-25 18:38:51', '2019-01-02 14:58:52'),
(228, 2, 80, 0, '2018-12-25 18:38:51', '2019-01-02 14:58:52'),
(229, 2, 81, 0, '2018-12-25 18:38:51', '2019-01-02 14:58:52'),
(230, 2, 82, 0, '2018-12-27 03:37:27', '2019-01-02 14:58:52'),
(231, 2, 83, 0, '2018-12-27 04:28:17', '2019-01-02 14:58:52'),
(232, 4, 42, 1, '2018-12-29 07:53:57', '2018-12-31 02:05:32'),
(233, 4, 43, 1, '2018-12-29 07:53:57', '2018-12-31 02:05:32'),
(234, 4, 44, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(235, 4, 45, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(236, 4, 46, 1, '2018-12-29 07:53:58', '2018-12-31 02:05:32'),
(237, 4, 82, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(238, 4, 83, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(239, 3, 77, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(240, 3, 78, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(241, 3, 79, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(242, 3, 80, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(243, 3, 81, 0, '2018-12-29 07:53:58', '2019-01-02 14:58:53'),
(244, 3, 52, 0, '2018-12-29 07:54:19', '2019-01-02 14:58:53'),
(245, 3, 53, 0, '2018-12-29 07:54:19', '2019-01-02 14:58:53'),
(246, 3, 54, 0, '2018-12-29 07:54:19', '2019-01-02 14:58:53'),
(247, 3, 55, 0, '2018-12-29 07:54:19', '2019-01-02 14:58:53'),
(248, 3, 56, 0, '2018-12-29 07:54:20', '2019-01-02 14:58:53'),
(249, 2, 84, 0, '2018-12-30 03:56:33', '2019-01-02 14:58:53'),
(250, 2, 85, 0, '2018-12-30 03:56:33', '2019-01-02 14:58:53'),
(251, 2, 86, 0, '2018-12-30 03:56:33', '2019-01-02 14:58:53'),
(252, 2, 87, 0, '2018-12-30 03:56:33', '2019-01-02 14:58:53'),
(253, 2, 88, 0, '2018-12-30 03:56:33', '2019-01-02 14:58:53'),
(254, 3, 84, 0, '2018-12-30 03:56:35', '2019-01-02 14:58:53'),
(255, 3, 85, 0, '2018-12-30 03:56:35', '2019-01-02 14:58:53'),
(256, 3, 86, 0, '2018-12-30 03:56:35', '2019-01-02 14:58:53'),
(257, 3, 87, 0, '2018-12-30 03:56:35', '2019-01-02 14:58:53'),
(258, 3, 88, 0, '2018-12-30 03:56:35', '2019-01-02 14:58:53'),
(259, 2, 89, 0, '2018-12-30 07:39:21', '2019-01-02 14:58:53'),
(260, 2, 90, 0, '2018-12-30 07:39:21', '2019-01-02 14:58:53'),
(261, 2, 91, 0, '2018-12-30 07:39:21', '2019-01-02 14:58:53'),
(262, 2, 92, 0, '2018-12-30 07:39:21', '2019-01-02 14:58:53'),
(263, 2, 93, 0, '2018-12-30 07:39:21', '2019-01-02 14:58:53'),
(264, 3, 89, 0, '2018-12-30 07:39:24', '2019-01-02 14:58:53'),
(265, 3, 90, 0, '2018-12-30 07:39:24', '2019-01-02 14:58:53'),
(266, 3, 91, 0, '2018-12-30 07:39:24', '2019-01-02 14:58:53'),
(267, 3, 92, 0, '2018-12-30 07:39:24', '2019-01-02 14:58:53'),
(268, 3, 93, 0, '2018-12-30 07:39:24', '2019-01-02 14:58:53'),
(269, 2, 94, 0, '2018-12-31 02:05:24', '2019-01-02 14:58:52'),
(270, 2, 95, 0, '2018-12-31 02:05:24', '2019-01-02 14:58:52'),
(271, 2, 96, 0, '2018-12-31 02:05:24', '2019-01-02 14:58:52'),
(272, 2, 97, 0, '2018-12-31 02:05:25', '2019-01-02 14:58:52'),
(273, 2, 98, 0, '2018-12-31 02:05:25', '2019-01-02 14:58:52'),
(274, 2, 99, 0, '2018-12-31 02:05:25', '2019-01-02 14:58:52'),
(275, 2, 100, 0, '2018-12-31 02:05:28', '2019-01-02 14:58:53'),
(276, 2, 101, 0, '2018-12-31 02:05:28', '2019-01-02 14:58:53'),
(277, 2, 102, 0, '2018-12-31 02:05:28', '2019-01-02 14:58:53'),
(278, 2, 103, 0, '2018-12-31 02:05:28', '2019-01-02 14:58:53'),
(279, 2, 104, 0, '2018-12-31 02:05:28', '2019-01-02 14:58:53'),
(280, 3, 94, 0, '2018-12-31 02:05:30', '2019-01-02 14:58:53'),
(281, 3, 95, 0, '2018-12-31 02:05:30', '2019-01-02 14:58:53'),
(282, 3, 96, 0, '2018-12-31 02:05:30', '2019-01-02 14:58:53'),
(283, 3, 97, 0, '2018-12-31 02:05:30', '2019-01-02 14:58:53'),
(284, 3, 98, 0, '2018-12-31 02:05:30', '2019-01-02 14:58:53'),
(285, 3, 99, 0, '2018-12-31 02:05:30', '2019-01-02 14:58:53'),
(286, 3, 100, 0, '2018-12-31 02:05:31', '2019-01-02 14:58:53'),
(287, 3, 101, 0, '2018-12-31 02:05:31', '2019-01-02 14:58:53'),
(288, 3, 102, 0, '2018-12-31 02:05:31', '2019-01-02 14:58:53'),
(289, 3, 103, 0, '2018-12-31 02:05:31', '2019-01-02 14:58:53'),
(290, 3, 104, 0, '2018-12-31 02:05:32', '2019-01-02 14:58:53'),
(291, 4, 72, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(292, 4, 73, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(293, 4, 74, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(294, 4, 75, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(295, 4, 76, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(296, 4, 77, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(297, 4, 78, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(298, 4, 79, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(299, 4, 80, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(300, 4, 81, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(301, 6, 72, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(302, 6, 73, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(303, 6, 74, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(304, 6, 75, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(305, 6, 76, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(306, 6, 77, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(307, 6, 78, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(308, 6, 79, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(309, 6, 80, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53'),
(310, 6, 81, 0, '2019-01-02 14:58:53', '2019-01-02 14:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `room_facilities`
--

DROP TABLE IF EXISTS `room_facilities`;
CREATE TABLE `room_facilities` (
  `room_id` int(11) NOT NULL,
  `facilitie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room_pictures`
--

DROP TABLE IF EXISTS `room_pictures`;
CREATE TABLE `room_pictures` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

DROP TABLE IF EXISTS `room_type`;
CREATE TABLE `room_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `title`, `deleted`, `created`, `modified`) VALUES
(1, 'Single', 0, '2018-12-24 17:32:09', '2018-12-24 17:32:09'),
(2, 'Double', 0, '2018-12-24 17:32:17', '2018-12-24 17:32:17'),
(3, 'Triple', 0, '2018-12-24 17:32:22', '2018-12-24 17:32:22'),
(4, 'Quad', 0, '2018-12-24 17:34:15', '2018-12-24 17:34:15'),
(5, 'Quint', 0, '2018-12-24 17:34:22', '2018-12-24 17:34:22'),
(6, 'Hexa', 0, '2018-12-24 17:34:29', '2018-12-24 17:34:29'),
(7, 'One-Room Suite', 0, '2018-12-24 17:34:35', '2018-12-24 17:34:35'),
(8, 'Two-Rooms Suite', 0, '2018-12-24 17:34:41', '2018-12-24 17:34:41'),
(9, 'Three Rooms Suite', 0, '2018-12-24 17:34:47', '2018-12-24 17:34:47'),
(10, 'Presidential Suite', 0, '2018-12-24 17:34:52', '2018-12-24 17:34:52'),
(11, 'Royal Suite', 0, '2018-12-24 17:34:57', '2018-12-24 17:34:57'),
(12, 'Junior Suite', 0, '2018-12-24 17:35:01', '2018-12-24 17:35:01'),
(13, 'Senior Suite', 0, '2018-12-24 17:35:09', '2018-12-24 17:35:09'),
(14, 'Executive Suite', 0, '2018-12-24 17:35:14', '2018-12-24 17:35:14'),
(15, 'Small Studio Double', 0, '2018-12-24 17:35:24', '2018-12-24 17:35:24'),
(16, 'Small Studio Triple', 0, '2018-12-24 17:35:30', '2018-12-24 17:35:30'),
(17, 'Large Studio Triple', 0, '2018-12-24 17:50:13', '2018-12-24 17:50:13'),
(18, 'Large Studio Quad', 0, '2018-12-24 17:50:25', '2018-12-24 17:50:25'),
(19, '2-Rooms Apartment', 0, '2018-12-24 17:50:31', '2018-12-24 17:50:31'),
(20, '3-Rooms Apartment', 0, '2018-12-24 17:50:36', '2018-12-24 17:50:36'),
(21, '4-Rooms Apartment', 0, '2018-12-24 17:50:43', '2018-12-24 17:50:43');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `booking_limit` float(10,2) NOT NULL,
  `checking_limit` float(10,2) NOT NULL,
  `vat` float(10,2) DEFAULT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `booking_limit`, `checking_limit`, `vat`, `meta_key`, `meta_value`, `created`, `modified`) VALUES
(1, 0.00, 2.00, 0.00, 'admin_role', '2', '2016-04-09 17:43:51', '2016-04-09 17:43:51'),
(2, 0.00, 3.00, 0.00, 'client_role', '3', '2016-04-09 17:43:51', '2016-04-09 17:43:51'),
(3, 0.00, 4.00, 0.00, 'csr_role', '4', '2016-04-09 17:43:51', '2016-04-09 17:43:51'),
(4, 0.00, 0.00, 0.00, 'holiday_emails', 'asd', '2016-06-03 01:36:33', '2017-12-05 13:38:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `username`, `password`, `role_id`, `child_id`, `photo`, `status`, `deleted`, `created`, `modified`) VALUES
(1, 'Super', 'Admin', 'azizrattani@hotmail.com', 'superadmin', '0e7517141fb53f21ee439b355b5a1d0a', 1, NULL, NULL, 1, 0, '2016-04-09 15:05:01', '2016-06-03 07:57:55'),
(2, 'Aziz', 'Rattani', 'aziz@ideabox.com.pk', 'admin', '0192023a7bbd73250516f069df18b500', 2, 1, NULL, 1, 0, '2016-06-03 01:36:44', '2016-10-19 03:39:32'),
(34, 'Sale Office', '', 'saleoffice@sales.com', 'saleoffice', 'e10adc3949ba59abbe56e057f20f883e', 3, 1, NULL, 1, 0, '2018-12-31 23:17:31', '2018-12-31 23:17:31'),
(35, 'Branch office', '', 'aa@aa.com', 'branchoffice', 'e10adc3949ba59abbe56e057f20f883e', 3, 2, NULL, 1, 0, '2018-12-31 23:28:28', '2018-12-31 23:28:28'),
(36, 'Taibah.pk', '', 'bh.memon786@gmail.com', 'misbah', 'e10adc3949ba59abbe56e057f20f883e', 3, 3, NULL, 1, 0, '2019-01-02 17:22:07', '2019-01-02 17:22:07'),
(37, 'Dallah Taibah', '', 'info@dallahtaibah', 'dallahtaibah', 'e10adc3949ba59abbe56e057f20f883e', 4, 1, NULL, 1, 0, '2019-01-02 17:38:41', '2019-01-02 17:38:41'),
(38, 'Irfan Travels', '', 'irfanedhi@gmail.com', 'irfanedhi@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 4, NULL, 1, 0, '2019-01-02 18:07:58', '2019-01-02 18:07:58'),
(39, 'Dallah Ajyad', '', 'irfanedhi@gmail.com', 'dajyad', '5c14fd17fe9d46f45f3ff652bbf2e3c5', 4, 2, NULL, 1, 0, '2019-01-02 22:46:25', '2019-01-02 22:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `view_type`
--

DROP TABLE IF EXISTS `view_type`;
CREATE TABLE `view_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `create_by` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` char(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `view_type`
--

INSERT INTO `view_type` (`id`, `title`, `create_by`, `created`, `modified`, `deleted`) VALUES
(1, 'No View', NULL, '2018-12-24 17:53:21', '2018-12-24 17:53:21', '0'),
(2, 'City View', NULL, '2018-12-24 17:53:28', '2018-12-24 17:53:28', '0'),
(3, 'Haram View', NULL, '2018-12-24 17:53:34', '2018-12-24 17:53:34', '0'),
(4, 'Kaabah View', NULL, '2018-12-24 17:53:39', '2018-12-24 17:53:39', '0'),
(5, 'Partial Haram View', NULL, '2018-12-24 17:53:44', '2018-12-24 17:53:44', '0'),
(6, 'Partial Kaabah View', NULL, '2018-12-24 17:53:55', '2018-12-24 17:53:55', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_emailAdd` (`email_add`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staff_booking` (`staff_id`),
  ADD KEY `hotel_id` (`hotel_id`);

--
-- Indexes for table `booking_details`
--
ALTER TABLE `booking_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `details_booking` (`booking_id`);

--
-- Indexes for table `booking_rooms`
--
ALTER TABLE `booking_rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookingRooms_booking` (`booking_id`),
  ADD KEY `bookingRooms_rooms` (`room_rate_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_cancellation_policy`
--
ALTER TABLE `hotel_cancellation_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_contact`
--
ALTER TABLE `hotel_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_contact` (`hotel_id`);

--
-- Indexes for table `hotel_facilities`
--
ALTER TABLE `hotel_facilities`
  ADD KEY `hotelFacilities_hotel` (`hotel_id`),
  ADD KEY `hotelFacilities_facilities` (`facilities_id`);

--
-- Indexes for table `hotel_market`
--
ALTER TABLE `hotel_market`
  ADD PRIMARY KEY (`id`),
  ADD KEY `market_hotel` (`hotel_id`),
  ADD KEY `market_market` (`market_id`);

--
-- Indexes for table `hotel_market_type`
--
ALTER TABLE `hotel_market_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_meal_rate`
--
ALTER TABLE `hotel_meal_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `hotel_meal_type`
--
ALTER TABLE `hotel_meal_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `meal_type_id` (`meal_type_id`);

--
-- Indexes for table `hotel_picture`
--
ALTER TABLE `hotel_picture`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_pictures` (`hotel_id`);

--
-- Indexes for table `hotel_rooms`
--
ALTER TABLE `hotel_rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_id` (`hotel_id`),
  ADD KEY `room_type` (`room_type`),
  ADD KEY `views` (`views`);

--
-- Indexes for table `hotel_room_rate`
--
ALTER TABLE `hotel_room_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_room_rate` (`room_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `market_type`
--
ALTER TABLE `market_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meals_type`
--
ALTER TABLE `meals_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `self_relation` (`parent_id`);

--
-- Indexes for table `partner_limit`
--
ALTER TABLE `partner_limit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `limit_staff` (`staff_id`);

--
-- Indexes for table `partner_login_logs`
--
ALTER TABLE `partner_login_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staff_login` (`staff_id`);

--
-- Indexes for table `partner_payment`
--
ALTER TABLE `partner_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `partner_id` (`partner_id`);

--
-- Indexes for table `partner_price_type`
--
ALTER TABLE `partner_price_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `staff_id` (`staff_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `permission_actions`
--
ALTER TABLE `permission_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_permission_entity_id` (`permission_entity_id`);

--
-- Indexes for table `permission_entities`
--
ALTER TABLE `permission_entities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_permission_group_id` (`permission_group_id`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_rp_role_id` (`role_id`),
  ADD KEY `FK_rp_action_id` (`permission_action_id`);

--
-- Indexes for table `room_facilities`
--
ALTER TABLE `room_facilities`
  ADD KEY `room_room` (`room_id`),
  ADD KEY `room_facilities` (`facilitie_id`);

--
-- Indexes for table `room_pictures`
--
ALTER TABLE `room_pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_picture` (`room_id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_user_role` (`role_id`);

--
-- Indexes for table `view_type`
--
ALTER TABLE `view_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `booking_details`
--
ALTER TABLE `booking_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking_rooms`
--
ALTER TABLE `booking_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hotel_cancellation_policy`
--
ALTER TABLE `hotel_cancellation_policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_contact`
--
ALTER TABLE `hotel_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_market`
--
ALTER TABLE `hotel_market`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_market_type`
--
ALTER TABLE `hotel_market_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_meal_rate`
--
ALTER TABLE `hotel_meal_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_meal_type`
--
ALTER TABLE `hotel_meal_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_picture`
--
ALTER TABLE `hotel_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_rooms`
--
ALTER TABLE `hotel_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hotel_room_rate`
--
ALTER TABLE `hotel_room_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=294;

--
-- AUTO_INCREMENT for table `market_type`
--
ALTER TABLE `market_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `meals_type`
--
ALTER TABLE `meals_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `partner_limit`
--
ALTER TABLE `partner_limit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `partner_login_logs`
--
ALTER TABLE `partner_login_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `partner_payment`
--
ALTER TABLE `partner_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partner_price_type`
--
ALTER TABLE `partner_price_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permission_actions`
--
ALTER TABLE `permission_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `permission_entities`
--
ALTER TABLE `permission_entities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=311;

--
-- AUTO_INCREMENT for table `room_pictures`
--
ALTER TABLE `room_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `view_type`
--
ALTER TABLE `view_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `staff_booking` FOREIGN KEY (`staff_id`) REFERENCES `partners` (`id`);

--
-- Constraints for table `booking_details`
--
ALTER TABLE `booking_details`
  ADD CONSTRAINT `details_booking` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`);

--
-- Constraints for table `booking_rooms`
--
ALTER TABLE `booking_rooms`
  ADD CONSTRAINT `bookingRooms_booking` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`),
  ADD CONSTRAINT `bookingRooms_rooms` FOREIGN KEY (`room_rate_id`) REFERENCES `hotel_room_rate` (`id`);

--
-- Constraints for table `hotel_contact`
--
ALTER TABLE `hotel_contact`
  ADD CONSTRAINT `hotel_contact` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`);

--
-- Constraints for table `hotel_facilities`
--
ALTER TABLE `hotel_facilities`
  ADD CONSTRAINT `hotelFacilities_facilities` FOREIGN KEY (`facilities_id`) REFERENCES `facilities` (`id`),
  ADD CONSTRAINT `hotelFacilities_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`);

--
-- Constraints for table `hotel_market`
--
ALTER TABLE `hotel_market`
  ADD CONSTRAINT `market_hotel` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `market_market` FOREIGN KEY (`market_id`) REFERENCES `market_type` (`id`);

--
-- Constraints for table `hotel_meal_rate`
--
ALTER TABLE `hotel_meal_rate`
  ADD CONSTRAINT `hotel_meal_rate_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `hotel_meal_rate_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `hotel_meal_type`
--
ALTER TABLE `hotel_meal_type`
  ADD CONSTRAINT `hotel_meal_type_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `hotel_meal_type_ibfk_2` FOREIGN KEY (`meal_type_id`) REFERENCES `meals_type` (`id`);

--
-- Constraints for table `hotel_picture`
--
ALTER TABLE `hotel_picture`
  ADD CONSTRAINT `hotel_pictures` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`);

--
-- Constraints for table `hotel_rooms`
--
ALTER TABLE `hotel_rooms`
  ADD CONSTRAINT `hotel_rooms_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`),
  ADD CONSTRAINT `hotel_rooms_ibfk_2` FOREIGN KEY (`room_type`) REFERENCES `room_type` (`id`),
  ADD CONSTRAINT `hotel_rooms_ibfk_3` FOREIGN KEY (`views`) REFERENCES `view_type` (`id`);

--
-- Constraints for table `hotel_room_rate`
--
ALTER TABLE `hotel_room_rate`
  ADD CONSTRAINT `hotel_room_rate_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `room_room_rate` FOREIGN KEY (`room_id`) REFERENCES `hotel_rooms` (`id`);

--
-- Constraints for table `partner_limit`
--
ALTER TABLE `partner_limit`
  ADD CONSTRAINT `limit_staff` FOREIGN KEY (`staff_id`) REFERENCES `partners` (`id`);

--
-- Constraints for table `partner_login_logs`
--
ALTER TABLE `partner_login_logs`
  ADD CONSTRAINT `staff_login` FOREIGN KEY (`staff_id`) REFERENCES `partners` (`id`);

--
-- Constraints for table `partner_payment`
--
ALTER TABLE `partner_payment`
  ADD CONSTRAINT `partner_payment_ibfk_1` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`);

--
-- Constraints for table `partner_price_type`
--
ALTER TABLE `partner_price_type`
  ADD CONSTRAINT `partner_price_type_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `partners` (`id`),
  ADD CONSTRAINT `partner_price_type_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `permission_actions`
--
ALTER TABLE `permission_actions`
  ADD CONSTRAINT `FK_permission_entity_id` FOREIGN KEY (`permission_entity_id`) REFERENCES `permission_entities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_entities`
--
ALTER TABLE `permission_entities`
  ADD CONSTRAINT `FK_permission_group_id` FOREIGN KEY (`permission_group_id`) REFERENCES `permission_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD CONSTRAINT `FK_rp_action_id` FOREIGN KEY (`permission_action_id`) REFERENCES `permission_actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_rp_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `room_facilities`
--
ALTER TABLE `room_facilities`
  ADD CONSTRAINT `room_facilities` FOREIGN KEY (`facilitie_id`) REFERENCES `facilities` (`id`),
  ADD CONSTRAINT `room_room` FOREIGN KEY (`room_id`) REFERENCES `hotel_rooms` (`id`);

--
-- Constraints for table `room_pictures`
--
ALTER TABLE `room_pictures`
  ADD CONSTRAINT `room_picture` FOREIGN KEY (`room_id`) REFERENCES `hotel_rooms` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_user_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
